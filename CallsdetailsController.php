<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;

require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class CallsdetailsController extends Controller
{
  
    
    public function view_calls_det_report(){	
		
		if(Util::isAuthorized("view_calls_det_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_calls_det_report")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log('Queue Details Report','View');
        $userid=session('userid');
        $usertypeid=session('usertypeid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getqueues  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();

                if($usertypeid=='17'){
                    $users = DB::select("SELECT a.`allo_agent_id`,b.`id`,b.`username` 
                                         FROM `tbl_agent_allocation` as a 
                                         right join `user_master` as b ON a.`allo_agent_id`=b.`id`
                                         inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
                                         where c.`title`= 'Csp_Agent' and b.`com_id`= $get_com_id->com_id group by b.`username`  ;");
                        }else{
                            $users = DB::select("SELECT a.`allo_agent_id`,b.`id`,b.`username` 
                                               FROM `tbl_agent_allocation` as a 
                                               right join `user_master` as b ON a.`allo_agent_id`=b.`id`
                                               inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
                                               where c.`title`= 'Csp_Agent' and b.`com_id`= $get_com_id->com_id and b.id=".$userid." group by b.`username`  ;");
                        }
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Call Details Report Dashboard",$username,"View Call Details Report");

        return view('view_calls_det_report',compact('getqueues','users','get_com_data'));   

	}

    public function search_call_det_report_old(Request $request){  
   
        $userid=session('userid');    
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first(); 
        $queue_id= $request->input('queue_id');
        $agnt_id= $request->input('agnt_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');


        $data = $dataq = DB::table('tbl_calls_evnt')
        ->select(
                 'user_master.username',
                 'tbl_calls_evnt.agnt_sipid',
                 'tbl_calls_evnt.date',
                 'tbl_calls_evnt.call_type',
                 'tbl_calls_evnt.to_caller_num',
                 'tbl_calls_evnt.frm_caller_num',
                 'tbl_calls_evnt.uniqueid',
                 'tbl_calls_evnt.cre_datetime',
                 /*'tbl_calls_evnt.agnt_queueid',*/
		DB::raw('CONCAT_WS("-",tbl_calls_evnt.agnt_queueid,asterisk.queues_config.descr) AS agnt_queueid'),
                 'tbl_calls_evnt.agnt_userid',
                 'tbl_calls_evnt.status',
                 'tbl_calls_evnt.desc',
                 'tbl_calls_evnt.linkedid as cur_linkedid',
		'tbl_com_mst.com_name',
                 DB::raw("(select SEC_TO_TIME(ROUND(tbl_calls_evnt.answer_sec_count))) as tot_time"),
                 
                 DB::raw("(SELECT tbl_calls_evnt.desc as cdb_desc FROM tbl_calls_evnt 
                                WHERE tbl_calls_evnt.linkedid=cur_linkedid and status = 'ENTERQUEUE'
                                 order by id desc limit 1 ) as cdb_desc"),

                 DB::raw("(SELECT ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count)) as hold_sec FROM tbl_calls_hold_evnts 
                                WHERE tbl_calls_hold_evnts.linkedid=tbl_calls_evnt.linkedid) as hold_sec"),
                
                 DB::raw("(SELECT tbl_calls_evnt.hangup_datatime as hangup_datatime FROM tbl_calls_evnt 
                                WHERE tbl_calls_evnt.linkedid=cur_linkedid
                                 order by id desc limit 1 ) as hangup_datatime"),
                DB::raw("(SELECT asteriskcdrdb.cdr.recordingfile as recordingfile FROM asteriskcdrdb.cdr 
                                WHERE cdr.linkedid=cur_linkedid and cdr.recordingfile!=''
                                group by calldate  limit 1 ) as recordingfile"),

                
                                 
                 DB::raw("(select SEC_TO_TIME(ROUND(tbl_calls_evnt.answer_sec_count-IFNULL(hold_sec,0))) )as talk_time"),

                 DB::raw("(SELECT SEC_TO_TIME(ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count))) as hold_time FROM tbl_calls_hold_evnts 
                         WHERE tbl_calls_hold_evnts.linkedid=tbl_calls_evnt.linkedid) as hold_time")
                 ) 
        ->join('user_master','user_master.id','=','tbl_calls_evnt.agnt_userid')
        ->join('tbl_com_mst','tbl_com_mst.id','=','user_master.com_id')
	->leftjoin('asterisk.queues_config','queues_config.extension','=','tbl_calls_evnt.agnt_queueid')
        ->whereBetween('tbl_calls_evnt.cre_datetime', array($frm_date, $to_date));
        //->Where('user_master.com_id',$get_com_id->com_id);
 	if($com_id!="All")
        {

        	$data = $dataq->Where('user_master.com_id',$com_id);
	}

        
        if($queue_id!="All")
        {
            $data = $dataq->Where('tbl_calls_evnt.agnt_queueid',$queue_id); 
        }

        if($agnt_id!="All")
        {
            $data = $dataq->Where('tbl_calls_evnt.agnt_userid',$agnt_id); 
        }
        $data = $dataq->groupby('tbl_calls_evnt.uniqueid'); 
        $data = $dataq->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Call Details Report",$username,"Search Call Details Report");

        return compact('data',$data);
        
    }
	public function search_call_det_report(Request $request){  

        $userid=session('userid');
	$usertypeid=session('usertypeid');
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first(); 
        $queue_id= $request->input('queue_id');
        $agnt_id= $request->input('agnt_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');
	if($usertypeid=='17'){
            $users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor') and a.`com_id`= $get_com_id->com_id  ;");
		}else if ($usertypeid=='19'){
			$users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList  
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
			where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor' OR c.`title`= 'Super_User' OR c.`title`= 'Team_Leader') and a.`com_id`= $get_com_id->com_id  ;");
		
		}else if ($usertypeid=='20'){
			$users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
								FROM `tbl_tmldr_agnt_allo` as a 
								right join `user_master` as b ON a.`agnt_userid`=b.`id`
								inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
								where a.`tmldr_userid` = $userid ;");		
		}else{
            		$users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where c.`title`= 'Csp_Agent' and a.`com_id`= $get_com_id->com_id and a.id=".$userid."  ;");
       		 } 
		
		$queue_list=DB::select("SELECT GROUP_CONCAT(extension) AS extension_list FROM asterisk.queues_config WHERE `com_id`= $get_com_id->com_id"); 

		$user_array = $users[0]->userList;
		$extension_array = $queue_list[0]->extension_list;
		$where="";
		$where1="";
		$where2="";
		$where3="";
		$where="`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' AND '$to_date'";
		 if($com_id!="All")
        	{
			$where1=" AND `user_master`.`com_id` = $com_id ";
		}else{
			$where1=" AND `user_master`.`com_id` = $get_com_id->com_id ";
		}
		if($queue_id!="All")
        	{
			$where2=" AND tbl_calls_evnt.agnt_queueid = $queue_id ";
        	}
        if($agnt_id!="All")			
		{ 
			$where3=" AND tbl_calls_evnt.agnt_userid =$agnt_id";
		}
		else
		{	
			$where3=" AND tbl_calls_evnt.agnt_userid IN ($user_array) ";
		}


		$data = DB::select("SELECT
                    `tbl_calls_evnt`.`agnt_sipid`,
                    `tbl_calls_evnt`.`date`,
                    `tbl_calls_evnt`.`call_type`,
                    `tbl_calls_evnt`.`to_caller_num`,
                    `tbl_calls_evnt`.`frm_caller_num`,
                    `tbl_calls_evnt`.`uniqueid`,
                    max(`tbl_calls_evnt`.`cre_datetime`) AS cre_datetime,
                    `tbl_calls_evnt`.`uniqueid` AS `cur_uniqueid`,
                    `tbl_calls_evnt`.`linkedid` AS `cur_linkedid`,
		    `tbl_com_mst`.`com_name`,
                     (SELECT SEC_TO_TIME(`tbl_calls_evnt`.`ring_sec_count`) as waiting FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1) AS waiting,
                    (SELECT `tbl_calls_evnt`.`status` FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1) AS status,
                    (SELECT CONCAT_WS('-',`tbl_calls_evnt`.`agnt_queueid`,`asterisk`.`queues_config`.`descr`) FROM tbl_calls_evnt INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid`  WHERE `tbl_calls_evnt`.`cre_datetime` = cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1 ) AS agnt_queueid,
                    (SELECT `tbl_calls_evnt`.`agnt_userid` FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1) AS agnt_userid,
                    (SELECT `tbl_calls_evnt`.`desc` FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1) AS `desc`,
                    (SELECT `user_master`.`username` FROM user_master WHERE `user_master`.`id` = `tbl_calls_evnt`.`agnt_userid`  LIMIT 1) AS username,
                    (
                    SELECT
                        SEC_TO_TIME(
                        ROUND( tbl_calls_evnt.answer_sec_count )) FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1) AS tot_time,
                    ( SELECT tbl_calls_evnt.DESC AS cdb_desc FROM tbl_calls_evnt WHERE tbl_calls_evnt.uniqueid = cur_uniqueid AND STATUS = 'ENTERQUEUE' ORDER BY id DESC LIMIT 1 ) AS cdb_desc,
                    ( SELECT ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec FROM tbl_calls_hold_evnts WHERE tbl_calls_hold_evnts.linkedid = tbl_calls_evnt.linkedid ) AS hold_sec,
                    ( SELECT tbl_calls_evnt.hangup_datatime AS hangup_datatime FROM tbl_calls_evnt WHERE tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1 ) AS hangup_datatime,
                    (
                    SELECT
                        asteriskcdrdb.cdr.recordingfile AS recordingfile 
                    FROM
                        asteriskcdrdb.cdr 
                    WHERE
                        cdr.linkedid = cur_linkedid 
                        AND cdr.recordingfile != '' 
                    GROUP BY
                        calldate 
                        LIMIT 1 
                    ) AS recordingfile,
                    ( SELECT SEC_TO_TIME( ROUND( tbl_calls_evnt.answer_sec_count - IFNULL( hold_sec, 0 ))) FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.uniqueid = cur_uniqueid ORDER BY id DESC LIMIT 1) AS talk_time,
                    ( SELECT SEC_TO_TIME( ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time FROM tbl_calls_hold_evnts WHERE tbl_calls_hold_evnts.linkedid = tbl_calls_evnt.linkedid ) AS hold_time 
                FROM
                    `tbl_calls_evnt`
                    INNER JOIN `user_master` ON `user_master`.`id` = `tbl_calls_evnt`.`agnt_userid`
		     INNER JOIN `tbl_com_mst` ON `tbl_com_mst`.`id` =`user_master`.`com_id` 
                WHERE
                    $where $where1 $where2 $where3 
                GROUP BY
                    `tbl_calls_evnt`.`uniqueid`
                ORDER BY 
                    `tbl_calls_evnt`.`cre_datetime` DESC,
                    `tbl_calls_evnt`.`uniqueid` DESC,
                    `tbl_calls_evnt`.`linkedid` DESC");
                        


                        return compact('data',$data);
                        
    }


}