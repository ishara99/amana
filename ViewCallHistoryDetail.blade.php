@include('header_new')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>Call Log Detail
            - {{"$contact->title . $contact->firstname $contact->lastname"}}
        </h1>
    </section>

    <!-- Main content -->
    <br><br>
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box-primary">
                    <!-- /.box-header -->
                    <div class="box box-primary">

                        <div style="padding-left:10px">
                            <div>
                            </div>
                            <div>
                                <!--  <iframe id="myIframe" style="height:44px"></iframe>  -->
                            </div>
                            <!-- palyer file-->
                        </div>
                    <!-- /.box-header -->
                        <div class="box-body paddingmin" style="padding-left:1px; margin-top:3px">
                            <div>
                                <table id="historytable" class="table table-bordered table-striped tablerowsize">
                                    <thead class="table_head">
                                    <tr>
                                        <th>Call Date & Time</th>
										<th>Create Date & Time</th>
                                        <th>Number</th>
                                        <th>Call Log</th>
                                        <th>Call Log Type</th>
										<th>User</th>
										<th>Edit</th>
										</tr>
                                    </thead>
                                    <tbody>
                                    @foreach($callhistory as $callhistory)
                                        <tr id="">
                                            <td style="width: 12%"> {{$callhistory->call_datetime }}</td>
                                            <td style="width: 12%"> {{$callhistory->created_datetime }}</td>
                                            <td style="width: 10%">{{$callhistory->pho_number }}</td>
                                            <td style="width: 32%"><?php echo str_replace('<br>', "\n", $callhistory->call_log) ?></td>
                                            <td style="width: 32%">{{$callhistory->log_type  }}</td>
											<td style="width: 32%">{{$callhistory->username  }}</td>
											 <td style=" width:5%;" align="center">
											 <?php if(session()->get('usertypeid')==17){ ?>
                                            <a style="color: #000000" href="editCallLogDetail?number={{$callhistory->pho_number }}&id={{$contact->id}}&callid={{$callhistory->id}}"><span class="glyphicon glyphicon-edit"></span></a>
											 <?php  } ?>
                                        </td>
                                        </tr>@endforeach
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                </div>

                <!-- ./col -->
            </div>
            <!-- /.row -->
        </div>

    </section>

    <!-- /.content -->

</div>
<!-- /.content-wrapper -->

@include('footer')

<script>
    $('#historytable').DataTable({
        "processing": true,
        "pageLength": 25,
        "order": [[1,'desc']]
    });
</script>

</body>
</html>




