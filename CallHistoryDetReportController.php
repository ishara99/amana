<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;
use DateTime;
use DatePeriod;
use DateInterval;


require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class CallHistoryDetReportController extends Controller{
  
    
    public function view_callhistory_detail_report(){	
		
		if(Util::isAuthorized("view_callhistory_detail_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_callhistory_detail_report")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log('Callhistory','View');
        
        

       $userid=session('userid');
        $usertypeid=session()->get('usertypeid');


        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();
                            if($usertypeid=='17'){
            $users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor') and a.`com_id`= $get_com_id->com_id group by a.`username`  ;");
        }else if ($usertypeid=='19'){
            $users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
            where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor' OR c.`title`= 'Super_User' OR c.`title`= 'Team_Leader') and a.`com_id`= $get_com_id->com_id group by a.`username`  ;");
        
        }else if ($usertypeid=='20'){
            $users = DB::select("SELECT a.`agnt_userid`, b.`id` , b.`username` 
                                FROM `tbl_tmldr_agnt_allo` as a 
                                right join `user_master` as b ON a.`agnt_userid`=b.`id`
                                inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
                                where a.`tmldr_userid` = $userid ;");       
        }else{
            $users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where c.`title`= 'Csp_Agent' and a.`com_id`= $get_com_id->com_id and a.id=".$userid." group by a.`username`  ;");
        }
      
        $presets = (new PresetRemarkController())->retrieveAll();
    
         $ipaddress = (new UsersController())->get_client_ip();
         $username=session()->get('username');
         Util::user_auth_log($ipaddress,"User open the Call History Detail dashboard-$username",$username,"view Call History Detail");


        return view('view_callhistory_detail_report',compact('users','presets'));

	}

    public function search_callhistory_detail_report(Request $request){  
   
        $condition2 = array();
    $condition3 = array();

    $agent_id = $request->input('agent_id');
    $user_type_id=session()->get('usertypeid');
    $user = session('userid');

    $get_com_id  = DB::table('user_master')
                            ->where('id',$user)
                            ->first();

    $startdate = $request->input('startdate_val');
    $enddate = $request->input('enddate_val');
    $contactnumber = $request->input('contactnumber');
    $pre_rem1 = $request->input('pre_rem1');
    $pre_rem2 = $request->input('pre_rem2');
    $call_type = $request->input('call_type');

    if ($contactnumber != "") {
        $condition2['csp_callhistory_detail.pho_number'] = $contactnumber;
    }

    $dataq = DB::table('csp_callhistory_detail')
        ->select(
            'csp_callhistory_detail.*', 'user_master.username', DB::raw("CONCAT_WS( ' ',CONCAT_WS( '.',csp_contact_master.title,csp_contact_master.firstname),csp_contact_master.lastname) AS contact_name"), DB::raw("CONCAT_WS( ', ',csp_contact_master.address_line1,csp_contact_master.address_line2,csp_contact_master.city_state_province,csp_contact_master.zip) AS cantactaddress"), DB::raw('(select csp_preset_remark.remark
                    AS preset1 from csp_preset_remark where csp_callhistory_detail.cat_one_prerem_id  =   csp_preset_remark.id ) as preset1'), DB::raw('(select csp_preset_remark.remark
                    AS preset2 from csp_preset_remark where csp_callhistory_detail.cat_two_prerem_id  =   csp_preset_remark.id ) as preset2')
        )
        ->join('user_master', 'user_master.id', '=', 'csp_callhistory_detail.created_userid')
        ->leftjoin('csp_contact_master', 'csp_callhistory_detail.cus_id', '=', 'csp_contact_master.id');



    if (!empty($condition2)) {
        $dataq = $dataq->where($condition2);
    }
    if ($request->input('agent_id') != "All") {
        
        $dataq = $dataq->where('csp_callhistory_detail.created_userid', $agent_id);
    }
    if($request->input('agent_id') == "All"){
        if($user_type_id=="18"){
            $dataq = $dataq->where('csp_callhistory_detail.created_userid', $user);
        }
        
    }
    if ($request->input('pre_rem1') != "All") {
        $dataq = $dataq->where('csp_callhistory_detail.cat_one_prerem_id', $pre_rem1);
    }
    if ($request->input('pre_rem2') != "All") {
        $dataq = $dataq->where('csp_callhistory_detail.cat_two_prerem_id', $pre_rem2);
    }
    if ($request->input('call_type') != "All") {
        $dataq = $dataq->where('csp_callhistory_detail.log_type', $call_type);
    } else {
        // $dataq = $dataq->whereIn('csp_callhistory_detail.log_type', array("Inbound", "Outbound"));
    }
     $dataq = $dataq->where('user_master.com_id', $get_com_id->com_id);

// $data = $dataq->whereBetween('csp_callhistory.call_datetime', array($startdate, $enddate))

    $data=$dataq->where(function ($dataq) use ($startdate,$enddate) {
        $dataq->whereBetween('csp_callhistory_detail.created_datetime', array($startdate, $enddate))
              ->orwhereBetween('csp_callhistory_detail.call_datetime', array($startdate, $enddate));
    })->get();


    $ipaddress = (new UsersController())->get_client_ip();
    $username = session()->get('username');
    Util::user_auth_log($ipaddress, "User Search Call History Detail", $username, "Search Call History Detail");
    return compact('data', $data);
        
    }




}