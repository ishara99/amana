<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Carbon\Carbon;
use Session;

require app_path() . '/Http/Helpers/helpers.php';
require app_path() . '/../vendor/autoload.php';

class AgentdashboardController extends Controller {

    public function index() {

	if (Util::isAuthorized("Agentdashboard") == 'LOGGEDOUT') {
	    return redirect('/');
	}
	if (Util::isAuthorized("Agentdashboard") == 'DENIED') {
	    return view('permissiondenide');
	}
	$endpoint = session()->get('endpoint');
	$linkedid ="";
	$getdndstatus = DB::table('asterisk.ps_contacts')
		->select('status', 'status_des')
		->where('endpoint', $endpoint)
		->first();
		
		$getlogout_reasons  = DB::table('phonikip_db.status_list')
			->select('status') 
			->where('type_flag', 'LOGOUT')
			->get();
		
		$getbreak_reasons  = DB::table('phonikip_db.status_list')
			->select('status') 
			->where('type_flag', 'BREAK')
			->get();

		$getoffline_reasons  = DB::table('phonikip_db.status_list')
			->select('status') 
			->where('type_flag', 'OFFLINE')
			->get();
			$outbound_st = "Online";

			$get_startpart = DB::table('tbl_agnt_evnt')
				->select('tbl_agnt_evnt.*')
				->where('tbl_agnt_evnt.agnt_sipid', '=', $endpoint)
				->where('tbl_agnt_evnt.agnt_event', '=', "Outbound On")
				->orderBy('tbl_agnt_evnt.id', 'desc')
				->first();
			if (!empty($get_startpart)) 
			{
				$check_softphone_rec = DB::table('tbl_agnt_evnt')
					->select('tbl_agnt_evnt.*')
					->where('tbl_agnt_evnt.id_of_prtone', '=', $get_startpart->id)
					->where('tbl_agnt_evnt.agnt_event', '=', "Outbound Off")
					->orderBy('tbl_agnt_evnt.id', 'desc')
					->first();

					if (!empty($check_softphone_rec)) 
					{
						
						$outbound_st = "Outbound Off";
					}
					else
					{
						$outbound_st = "Outbound On";
					}
			}
			else
			{
				$outbound_st = "Online";
			}

			$categories = (new CallHistoryController())->getCategoryList();
			$inquiries = (new CallHistoryController())->getInquaryList();
			
			// dd($categories);

		return view('Agentdashboard',compact('getdndstatus','getlogout_reasons','getbreak_reasons','outbound_st','linkedid','getoffline_reasons','categories','inquiries'));


    }

    public function indexOld() {

		if (Util::isAuthorized("Agentdashboard") == 'LOGGEDOUT') {
		    return redirect('/');
		}
		if (Util::isAuthorized("Agentdashboard") == 'DENIED') {
		    return view('permissiondenide');
		}
		$endpoint = session()->get('endpoint');
		$linkedid ="";
		$getdndstatus = DB::table('asterisk.ps_contacts')
			->select('status', 'status_des')
			->where('endpoint', $endpoint)
			->first();
			
			$getlogout_reasons  = DB::table('phonikip_db.status_list')
				->select('status') 
				->where('type_flag', 'LOGOUT')
				->get();
			
			$getbreak_reasons  = DB::table('phonikip_db.status_list')
				->select('status') 
				->where('type_flag', 'BREAK')
				->get();
				$outbound_st = "Online";

				$get_startpart = DB::table('tbl_agnt_evnt')
					->select('tbl_agnt_evnt.*')
					->where('tbl_agnt_evnt.agnt_sipid', '=', $endpoint)
					->where('tbl_agnt_evnt.agnt_event', '=', "Outbound On")
					->orderBy('tbl_agnt_evnt.id', 'desc')
					->first();
				if (!empty($get_startpart)) 
				{
					$check_softphone_rec = DB::table('tbl_agnt_evnt')
						->select('tbl_agnt_evnt.*')
						->where('tbl_agnt_evnt.id_of_prtone', '=', $get_startpart->id)
						->where('tbl_agnt_evnt.agnt_event', '=', "Outbound Off")
						->orderBy('tbl_agnt_evnt.id', 'desc')
						->first();

						if (!empty($check_softphone_rec)) 
						{
							
							$outbound_st = "Outbound Off";
						}
						else
						{
							$outbound_st = "Outbound On";
						}
				}
				else
				{
					$outbound_st = "Online";
				}
			return view('Agentdashboard_old',compact('getdndstatus','getlogout_reasons','getbreak_reasons','outbound_st','linkedid'));

		$getlogout_reasons = DB::table('phonikip_db.status_list')
			->select('status')
			->where('type_flag', 'LOGOUT')
			->get();

		$getbreak_reasons = DB::table('phonikip_db.status_list')
			->select('status')
			->where('type_flag', 'BREAK')
			->get();

		$outbound_st = "Online";

		$get_startpart = DB::table('tbl_agnt_evnt')
			->select('tbl_agnt_evnt.*')
			->where('tbl_agnt_evnt.agnt_sipid', '=', $endpoint)
			->where('tbl_agnt_evnt.agnt_event', '=', "Outbound On")
			->orderBy('tbl_agnt_evnt.id', 'desc')
			->first();
		if (!empty($get_startpart)) {
		    $check_softphone_rec = DB::table('tbl_agnt_evnt')
			    ->select('tbl_agnt_evnt.*')
			    ->where('tbl_agnt_evnt.id_of_prtone', '=', $get_startpart->id)
			    ->where('tbl_agnt_evnt.agnt_event', '=', "Outbound Off")
			    ->orderBy('tbl_agnt_evnt.id', 'desc')
			    ->first();

		    if (!empty($check_softphone_rec)) {

			$outbound_st = "Outbound Off";
		    } else {
			$outbound_st = "Outbound On";
		    }
		} else {
		    $outbound_st = "Online";
		}

		return view('Agentdashboard_old', compact('getdndstatus', 'getlogout_reasons', 'getbreak_reasons', 'outbound_st'));
    }


	public function get_abn_det(){
		$data = DB::table('tbl_cl_abndon_event')
					->select('tbl_cl_abndon_event.*')
					->get();
		// print_r($get_clbck_det); exit();
	return compact('data', $data);
    }

    public function by_softphone_Agentdashboard() {
	if (Util::isAuthorized("Agentdashboard") == 'LOGGEDOUT') {
	    return redirect('/');
	}
	if (Util::isAuthorized("Agentdashboard") == 'DENIED') {
	    return view('permissiondenide');
	}
	$endpoint = session()->get('endpoint');

	$frm_caller_num = $_GET['frm_caller_num'];
	$linkedid = $_GET['linkedid'];
	session()->put('linkedid', $linkedid);
	session()->put('CallerIDNum', $frm_caller_num);

	$getdndstatus = DB::table('asterisk.ps_contacts')
		->select('status', 'status_des')
		->where('endpoint', $endpoint)
		->first();

	$getlogout_reasons = DB::table('phonikip_db.status_list')
		->select('status')
		->where('type_flag', 'LOGOUT')
		->get();

	$getbreak_reasons = DB::table('phonikip_db.status_list')
		->select('status')
		->where('type_flag', 'BREAK')
		->get();

$getoffline_reasons  = DB::table('phonikip_db.status_list')
		->select('status') 
		->where('type_flag', 'OFFLINE')
		->get();
	$outbound_st = "Online";

	$get_startpart = DB::table('tbl_agnt_evnt')
		->select('tbl_agnt_evnt.*')
		->where('tbl_agnt_evnt.agnt_sipid', '=', $endpoint)
		->where('tbl_agnt_evnt.agnt_event', '=', "Outbound On")
		->orderBy('tbl_agnt_evnt.id', 'desc')
		->first();
	if (!empty($get_startpart)) {
	    $check_softphone_rec = DB::table('tbl_agnt_evnt')
		    ->select('tbl_agnt_evnt.*')
		    ->where('tbl_agnt_evnt.id_of_prtone', '=', $get_startpart->id)
		    ->where('tbl_agnt_evnt.agnt_event', '=', "Outbound Off")
		    ->orderBy('tbl_agnt_evnt.id', 'desc')
		    ->first();

	    if (!empty($check_softphone_rec)) {

		$outbound_st = "Outbound Off";
	    } else {
		$outbound_st = "Outbound On";
	    }
	} else {
	    $outbound_st = "Online";
	}
	$this->saveCallLog($frm_caller_num, $linkedid);

			$categories = (new CallHistoryController())->getCategoryList();
			$inquiries = (new CallHistoryController())->getInquaryList();


	return view('Agentdashboard', compact('getdndstatus', 'getlogout_reasons', 'getbreak_reasons', 'outbound_st','linkedid','getoffline_reasons','categories','inquiries'));
    }

    // public function inboundcall(){	
    // 	$number = $_GET['number'];
    // 	return view('inbound') 
    // 	->with('number',$number);
    // }
    // public function act_hold(){
    // 	/*
    // 		Not used Yet  */
    // 	$app='hello'; // Stasis app name
    // 	$context='from-dnd';
    // 	$endpoint = $_GET['endpoint'];
    // 	$ariConnector = new \phpari('hello');
    // 	$channels = new \channels($ariConnector);
    // 	$channels_arr = $channels->show();
    // 	//$socket = $_GET['socket'];
    // 	$channelid = "";
    // 	foreach($channels_arr as $value)
    // 	{	
    // 			$ch_exp=explode("/",$value['name']);
    // 			$ch_endpoint=explode("-",$ch_exp[1]);
    // 			if($ch_endpoint[0]==$endpoint)
    // 			{
    // 				$channelid = $value['id'];
    // 			}
    // 	}
    // 	$response = $channels->channel_hold($channelid); // Hold channel
    // 	$channels->mute($channelid,'in');
    // 	echo $channelid;
    // }
    public function obmode_status() {
	/* This function make user online and log the agent event */
	$endpoint = $_GET['endpoint'];
	$cre_datetime = date("Y-m-d H:i:s");
	$checkstatus = DB::table('asterisk.ps_contacts')
		->where('endpoint', $endpoint)
		->first();
	//this checks whether softphone registered or not
	if (!empty($checkstatus)) {
	    if ($checkstatus->status == "Online") {
		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Outbound',
			    'status_des' => '',
			    'update_datetime' => $cre_datetime]);

		Util::startpart_agent_event_log("Outbound On", session('endpoint'), "");
		// this function record start part of agent event log.

		return "Outbound On";
		//changed means record saved succesfull
	    } else if ($checkstatus->status == "Outbound") {
		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Online',
			    'status_des' => '',
			    'update_datetime' => $cre_datetime]);

		Util::endpart_agent_event_log("Outbound On", "Outbound Off", session('endpoint'), "");

		return "Outbound Off";
		//false means agent already online
	    }
	} else {
	    return "Notfound";
	    //notfound means sogtphone not registered
	}
    }

    public function dnd_off() {
	/* This function make user online and log the agent event */
	$endpoint = $_GET['endpoint'];
	$cre_datetime = date("Y-m-d H:i:s");
	$checkstatus = DB::table('asterisk.ps_contacts')
		->where('endpoint', $endpoint)
		->first();
	//this checks whether softphone registered or not
	if (!empty($checkstatus)) {
	    if ($checkstatus->status == "Offline") {
		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Online',
			    'status_des' => '',
			    'update_datetime' => $cre_datetime]);

		Util::startpart_agent_event_log("Online", session('endpoint'), "");
		// this function record start part of agent event log.

		return "changed";
		//changed means record saved succesfull
	    } else {
		return "false";
		//false means agent already online
	    }
	} else {
	    return "Notfound";
	    //notfound means sogtphone not registered
	}
    }

    public function Conf_call() {
		$endpoint = $_GET['endpoint'];
		$conf_num = $_GET['conf_num'];
		$company_name = config('app.company_name');
		$cookie = '/var/www/html/' . $company_name . '/ck.txt';
	
		$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
		echo "<br>";
	
		$ariConnector = new \phpari('hello');
		$channels = new \channels($ariConnector);
		$channels_arr = $channels->show();
		//dd($channels_arr);
		$endchannelid = "";
		$endlinkedid = "";
		$callchannelid = "";
		$calllinkedid = "";
	
		foreach ($channels_arr as $value) {
			print_r($value);
			$ch_exp = explode("/", $value['name']);
			$ch_endpoint = explode("-", $ch_exp[1]);
			if ($ch_endpoint[0] == $endpoint) 
			{
				//print_r($value);
				$endchannelid = $value['name'];
				$endlinkedid = $value['id'];
			}
	
			foreach ($value['caller'] as $val_caller) 
			{
				if ($val_caller == $conf_num) 
				{
					$callchannelid = $value['name'];
					$calllinkedid = $value['id'];
					//print_r($callchannelid);
				}
			}
		}
		//exit;
		$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
		//$cookie_get=app_path().'/../ck.txt';
	
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=ConfbridgeKick&ActionID=ConfbridgeKick&Conference=$endpoint&Channel=all");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;
		echo "<br>";
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Redirect&ActionID=154&Channel=$endchannelid&ExtraChannel=$callchannelid&Exten=$endpoint&ExtraExten=$endpoint&context=from-conf&ExtraContext=from-conf&Priority=1&ExtraPriority=1");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;
		echo "<br>";
		DB::table('phonikip_db.tbl_calls_evnt')
			->wherein('dest_uniqueid', [$calllinkedid,$endlinkedid])
			 ->where('status', 'ANSWER')
			->update(['desc' => 'Conference Call']);
		sleep(2);
		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Busy',
			    'status_des' => "Conf"]);
	
		}

    // public function play_dtmf(){
    // 	$endpoint = $_GET['endpoint'];
    // 	$conf_num = $_GET['conf_num'];
    // 	$dtmf_num = $_GET['dtmf_num'];
    // 	$ariConnector = new \phpari('hello');
    // 	$channels = new \channels($ariConnector);
    // 	$channels_arr = $channels->show();
    // 	//dd($channels_arr);
    // 	$endchannelid = "";
    // 	foreach($channels_arr as $value)
    // 	{	
    // 			$ch_exp=explode("/",$value['name']);
    // 			$ch_endpoint=explode("-",$ch_exp[1]);
    // 			if($ch_endpoint[0]==$endpoint)
    // 			{
    // 				$endchannelid = $value['name'];
    // 			}
    // 	}
    // 	echo $endchannelid;
    // 	if($endchannelid!="")
    // 	{
    // 		$cookie='ck.txt';
    // 		$ch = curl_init('http://'.$_SERVER['SERVER_ADDR'].':8088/rawman?action=login&username=ami_user&secret=passw0rd');
    // 		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    // 		curl_setopt($ch, CURLOPT_POST, 1);
    // 		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
    // 		curl_setopt($ch, CURLOPT_POSTFIELDS,"");
    // 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // 		$result = curl_exec($ch);
    // 		curl_close ($ch);
    // 		echo $result;
    // 		echo "<br>";
    // 		//echo $endchannelid."-".$callchannelid;
    // 		$company_name=config('app.company_name');
    // 		$cookie_get='/var/www/html/'.$company_name.'/ck.txt';
    // 		// $ch = curl_init();
    // 		// curl_setopt($ch, CURLOPT_URL,"http://".$_SERVER['SERVER_ADDR'].":8088/rawman?action=Originate&ActionID=$endpoint&Channel=PJSIP/$endpoint&Data=lakshan&Exten=$endpoint&context=play-dtmf&Priority=1&CallerID=$dtmf_num");
    // 		// curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
    // 		// curl_setopt($ch, CURLOPT_POST, 1);
    // 		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
    // 		// curl_setopt($ch, CURLOPT_POSTFIELDS,"");
    // 		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // 		// $response = curl_exec($ch);
    // 		// curl_close ($ch);
    // 		// echo $response;
    // 		// echo "<br>";
    // 		$ch = curl_init();
    // 		curl_setopt($ch, CURLOPT_URL,"http://".$_SERVER['SERVER_ADDR'].":8088/rawman?action=PJSIPShowSubscriptionsOutbound&ActionID=168");
    // 		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
    // 		curl_setopt($ch, CURLOPT_POST, 1);
    // 		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
    // 		curl_setopt($ch, CURLOPT_POSTFIELDS,"");
    // 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // 		$response = curl_exec($ch);
    // 		curl_close ($ch);
    // 		echo $response;
    // 		echo "<br>";
    // 		// $cookie_get=app_path().'/../ck.txt';
    // 		// $ch = curl_init();
    // 		// curl_setopt($ch, CURLOPT_URL,"http://".$_SERVER['SERVER_ADDR'].":8088/rawman?action=PlayDTMF&ActionID=$endpoint&Channel=Local/$endpoint@from-internal&Digit=$dtmf_num&Duration=260");
    // 		// curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
    // 		// curl_setopt($ch, CURLOPT_POST, 1);
    // 		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
    // 		// curl_setopt($ch, CURLOPT_POSTFIELDS,"");
    // 		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // 		// $response = curl_exec($ch);
    // 		// curl_close ($ch);
    // 		// echo $response;
    // 		// echo "<br>";
    // 	}
    // }
    public function play_dtmf() {
	$endpoint = $_GET['endpoint'];
	$dial_dtmf_num = $_GET['dial_dtmf_num'];
	$dtmf_num = $_GET['dtmf_num'];
	echo $dial_dtmf_num;

	$ariConnector = new \phpari('hello');
	$channels = new \channels($ariConnector);
	$channels_arr = $channels->show();
	print_r($channels_arr);

	$endchannelid = "";

	foreach ($channels_arr as $value) {


	    foreach ($value['caller'] as $val_caller) {

		if ($val_caller == $dial_dtmf_num) {
		    $endchannelid = $value['name'];
		}
	    }
	}
	echo $endchannelid;

	if ($endchannelid != "") {
		$company_name = config('app.company_name');
	    $cookie = '/var/www/html/' . $company_name . '/ck.txt';

	    $ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
	    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $result = curl_exec($ch);
	    curl_close($ch);
	    echo $result;
	    echo "<br>";

	    
	    $cookie_get = '/var/www/html/' . $company_name . '/ck.txt';



	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=PlayDTMF&ActionID=$endpoint&Channel=$endchannelid&Digit=$dtmf_num&Duration=260");
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    echo $response;
	    echo "<br>";
	}
    }

  	public function Attended_transfer(){
		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		
		$ariConnector = new \phpari('hello');
		$channels = new \channels($ariConnector);
		$channels_arr = $channels->show();
		$endchannelid = "";
		$calltype = "outbound";
 //dd($channels_arr);
// 		echo $calltype;

		if(strlen($dial_num) > 7 )
		{
			$incoming_num = $_GET['incoming_num'];
			$linkedid = $_GET['incoming_linkedid'];
			//echo $linkedid;
			//exit;
			foreach($channels_arr as $value)
			{	
				if($value['id']==$linkedid)
				{
					$calltype = "inbound";
				}
			}
			
			$dial_channelid = "";
			$incoming_channelid = "";
			$incoming_channelid = "";
			$incoming_linkedid = "";
			$dial_unqid = "";
			foreach($channels_arr as $value)
			{	

				if($value['id']==$linkedid and $calltype =="inbound")
				{
					$incoming_channelid = $value['name'];
					$incoming_linkedid  = $value['id'];							
					//echo "incoming - ".$incoming_channelid;	
				}


				if($value['dialplan']['context'] == "from-pstn")
				{
					$get_dial_endpoint  = DB::table('phonikip_db.tbl_calls_evnt')
											->select('agnt_sipid') 
											->where('dest_uniqueid', $value['id'])
											->first();
					if($get_dial_endpoint->agnt_sipid == $endpoint)
				 	{
						if($dial_unqid== "")
						{
							$dial_channelid = $value['name'];
							//print_r($callchannelid);
							//echo "dial - ".$dial_channelid;
							$dial_unqid = $value['id'];
							//echo "dial - ".$dial_unqid;
						}else
						{
									$incoming_channelid = $value['name'];
									$incoming_linkedid  = $value['id'];	
									//echo "dial 2 - ".$incoming_linkedid;	
						}
					}
				}
					

					if($incoming_channelid != "" && $dial_channelid != "")
					{
						$company_name=config('app.company_name');
						$cookie='/var/www/html/'.$company_name.'/ck.txt';
						$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
						curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
						curl_setopt($ch, CURLOPT_POSTFIELDS,"");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close ($ch);
						
						//$company_name=config('app.company_name');
						$cookie_get='/var/www/html/'.$company_name.'/ck.txt';
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,"http://localhost:8088/rawman?action=Bridge&ActionID=Bridge_atten&Channel1=$dial_channelid&Channel2=$incoming_channelid&Tone=Both");
						curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
						curl_setopt($ch, CURLOPT_POSTFIELDS,"");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$response = curl_exec($ch);
						curl_close ($ch);
						// echo $response;
						// echo "<br>";
						
						$get_dial_linkedid  = DB::table('phonikip_db.tbl_calls_evnt')
											->select('linkedid','to_caller_num') 
											->where('dest_uniqueid', $dial_unqid)
											->first();
						//echo $dial_unqid;
						if( $calltype =="inbound")
						{
							DB::table('phonikip_db.tbl_calls_evnt')
											->where('linkedid', $incoming_linkedid)
											->where('status', 'ANSWER')
											->update(['desc' => 'AttendedTransfer',
													'to_trans_no' => "$get_dial_linkedid->to_caller_num"]);
						}else
						{
							DB::table('phonikip_db.tbl_calls_evnt')
							->where('dest_uniqueid', $incoming_linkedid)
							->where('status', 'ANSWER')
							->update(['desc' => 'AttendedTransfer',
									'to_trans_no' => "$get_dial_linkedid->to_caller_num"]);
						}
											
						$dial_channelid = "";
						$incoming_channelid = "";
						$dial_unqid = "";
						$incoming_linkedid = "";
						echo "true";
					}
					

			}
//echo "false";

		}else
		{
			foreach($channels_arr as $value)
			{	
				
					$ch_exp=explode("/",$value['name']);
					$ch_endpoint=explode("-",$ch_exp[1]);
					if($ch_endpoint[0]==$endpoint)
					{
						$endchannelid = $value['name'];
					}

			}
			//echo $endchannelid;
			$company_name=config('app.company_name');
			$cookie='/var/www/html/'.$company_name.'/ck.txt';
			$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
			curl_setopt($ch, CURLOPT_POSTFIELDS,"");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close ($ch);
			//echo $result;
			//echo "<br>";

			//$company_name=config('app.company_name');
			$cookie_get='/var/www/html/'.$company_name.'/ck.txt';
			

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://localhost:8088/rawman?action=Atxfer&ActionID=BlindTransfer&Channel=$endchannelid&Context=from-internal&Exten=$dial_num");
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
			curl_setopt($ch, CURLOPT_POSTFIELDS,"");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close ($ch);
			//echo $response;
			//echo "<br>";
			
			DB::table('phonikip_db.tbl_calls_evnt')
											->where('linkedid', $incoming_linkedid)
											->where('status', 'ANSWER')
											->update(['desc' => 'AttendedTransfer',
													'to_trans_no' => "$dial_num"]);
		}

	
	}

 	public function Blind_transfer(){
		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		$ariConnector = new \phpari('hello');
		$channels = new \channels($ariConnector);
		$channels_arr = $channels->show();
		$endchannelid = "";
		$cre_datetime = date("Y-m-d H:i:s");
		$userid=session('userid');
		
		$com_id = util::get_com_id_by_user($userid);
		$get_out_context  = DB::table('tbl_com_mst')
							->Where('id',$com_id)
							->first('out_context');

		$out_context = $get_out_context->out_context;
		
		foreach($channels_arr as $value)
		{	
			
				$ch_exp=explode("/",$value['name']);
				$ch_endpoint=explode("-",$ch_exp[1]);
				if($ch_endpoint[0]==$endpoint)
				{
					$endchannelid = $value['name'];
				}

		}
		$company_name=config('app.company_name');
		$cookie='/var/www/html/'.$company_name.'/ck.txt';
		$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
		curl_setopt($ch, CURLOPT_POSTFIELDS,"");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close ($ch);
		echo $result;
		echo "<br>";

		
		$cookie_get='/var/www/html/'.$company_name.'/ck.txt';
		//$cookie_get=app_path().'/../ck.txt';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://localhost:8088/rawman?action=BlindTransfer&ActionID=BlindTransfer&Channel=$endchannelid&Context=from-internal&Exten=$dial_num");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
		curl_setopt($ch, CURLOPT_POSTFIELDS,"");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close ($ch);
		echo $response;
		echo "<br>";
		
		
		DB::table('asterisk.ps_contacts')
					->where('endpoint', $endpoint)
					->update(['status' => 'Online',
							  'status_des' => '',
							  'linkedid' => '',
							  'update_datetime' => $cre_datetime]);

	
	}
    public function hangup_channel() {
	$endpoint = $_GET['endpoint'];

	$ariConnector = new \phpari('hello');
	$channels = new \channels($ariConnector);
	$channels_arr = $channels->show();
	$endchannelid = "";

	foreach ($channels_arr as $value) {

	    $ch_exp = explode("/", $value['name']);
	    $ch_endpoint = explode("-", $ch_exp[1]);
	    if ($ch_endpoint[0] == $endpoint) {
		$endchannelid = $value['name'];
	    }
	}
	$cookie = 'ck.txt';
	$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	echo $result;
	echo "<br>";

	$company_name = config('app.company_name');
	$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
	//$cookie_get=app_path().'/../ck.txt';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Hangup&ActionID=hangup&Channel=$endchannelid&Cause=16");
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
	//echo $response;
	//echo "<br>";
	$getdndstatus = DB::table('asterisk.ps_contacts')
		->select('status', 'status_des')
		->where('endpoint', $endpoint)
		->first();
	if (!empty($getdndstatus->status)) {
	    echo $getdndstatus->status;
	}
    }

    public function chanspy_channel() {
	$endpoint = $_GET['endpoint'];
	$dial_num = $_GET['dial_num'];
	$context = $_GET['context'];

	$cookie = 'ck.txt';
	$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	echo $result;
	echo "<br>";


	$company_name = config('app.company_name');
	$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
	//$cookie_get=app_path().'/../ck.txt';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=123&Channel=PJSIP/$endpoint&Exten=$dial_num&context=$context&Priority=1&CallerID=$dial_num");
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
	echo $response;
	echo "<br>";
    }

	public function Add_conf() {
		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		$userid=session('userid');
			$com_id = util::get_com_id_by_user($userid);
			$get_out_context  = DB::table('tbl_com_mst')
							->Where('id',$com_id)
							->first('out_context');

			$out_context = $get_out_context->out_context;

		if (strlen($dial_num) > 7) {
			$company_name = config('app.company_name');
			$cookie = '/var/www/html/' . $company_name . '/ck.txt';
	
			$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			echo $result;
			echo "<br>";
	
			$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
	
			$ariConnector = new \phpari('hello');
			$channels = new \channels($ariConnector);
			$channels_arr = $channels->show();
			$dial_channelid = "";
			$dial_unqid= "";
			foreach($channels_arr as $value)
			{
				if($value['dialplan']['context'] == "from-pstn")
				{
					$get_dial_endpoint  = DB::table('phonikip_db.tbl_calls_evnt')
											->select('agnt_sipid') 
											->where('dest_uniqueid', $value['id'])
											->first();
					if($get_dial_endpoint->agnt_sipid == $endpoint)
				 	{
						if($dial_unqid== "")
						{
							$dial_channelid = $value['name'];
							//print_r($callchannelid);
							//echo "dial - ".$dial_channelid;
							$dial_unqid = $value['id'];
							//echo "dial - ".$dial_unqid;
						}
					}
				}
				if($dial_channelid != "")
					{
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Redirect&ActionID=redirect_chnl&Channel=$dial_channelid&Exten=$endpoint&context=from-conf&Priority=1");
						curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
						curl_setopt($ch, CURLOPT_POSTFIELDS, "");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$response = curl_exec($ch);
						curl_close($ch);
						echo $response;
						echo "<br>";
					}
			}
	
	
		// $socket = fsockopen($_SERVER['SERVER_ADDR'], "5038");
		// fputs($socket, "Action: Login\r\n");
		// fputs($socket, "Username: ami_user\r\n");
		// fputs($socket, "Secret: passw0rd\r\n\r\n");
		// $val_arr = array();
		// $event = "";
		// while ($ret = fgets($socket)) {
		// 	//print_r($ret);
		// 	if (substr($ret, 0, 6) == "Event:") {
		// 	$e = explode(':', $ret);
		// 	$event = trim($e[1]);
		// 	}
	
		// 	if ($event == "DialBegin") {
		// 	$data = explode(':', $ret);
		// 	if (trim($data[0]) != "" && ( $data[0] == "Context" || $data[0] == "ConnectedLineNum" || $data[0] == "DestChannel" || $data[0] == "DestUniqueid" || $data[0] == "Channel" )) {
		// 		$val_arr[$data[0]] = trim($data[1]);
		// 	}
	
	
		// 	if ($data[0] == "DestUniqueid") {
		// 		if (array_key_exists('ConnectedLineNum', $val_arr)) {
		// 			if (trim($val_arr['ConnectedLineNum']) == trim($dial_num) or trim($val_arr['DestCallerIDNum']) == trim($dial_num)) {
		// 				$callchannelid = $val_arr["DestChannel"];
		// 				$endchannelid_hang = $val_arr["Channel"];
		// 				$DestUniqueid = $val_arr["DestUniqueid"];
		// 				echo $callchannelid;
		// 				echo $endchannelid_hang;
						

		// 				$ch = curl_init();
		// 				curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Hangup&ActionID=hangup&Channel=$endchannelid_hang&Cause=16");
		// 				curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		// 				curl_setopt($ch, CURLOPT_POST, 1);
		// 				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		// 				curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		// 				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 				$response = curl_exec($ch);
		// 				curl_close($ch);
		// 				echo $response;
		// 				echo "<br>";
		// 				DB::table('phonikip_db.tbl_calls_evnt')
		// 				->where('dest_uniqueid', $DestUniqueid)
		// 				->delete();
		// 				break;
		// 			}
		// 		}
		// 	}
		// 	}
		// }

			//print_r($channels_arr_hang);
		} else {
			$company_name = config('app.company_name');
			$cookie = '/var/www/html/' . $company_name . '/ck.txt';
			$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			echo $result;
			echo "<br>";
	
	
			$company_name = config('app.company_name');
			$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
			//$cookie_get=app_path().'/../ck.txt';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=123&Channel=PJSIP/$dial_num&Exten=$endpoint&context=from-conf&Priority=1&CallerID=$dial_num");
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);
			echo $response;
			echo "<br>";
		}

		sleep(5);
		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Busy',
			    'status_des' => "Conf"]);
		}
    public function Add_conf_old() {
		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		$userid=session('userid');
			$com_id = util::get_com_id_by_user($userid);
			$get_out_context  = DB::table('tbl_com_mst')
							->Where('id',$com_id)
							->first('out_context');

			$out_context = $get_out_context->out_context;
		if (strlen($dial_num) > 7) {
			$company_name = config('app.company_name');
			$cookie = '/var/www/html/' . $company_name . '/ck.txt';
	
			$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			echo $result;
			echo "<br>";
	
			$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
	
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=$endpoint&Channel=PJSIP/$endpoint&Data=lakshan&Exten=$dial_num&context=from-internal&Priority=1&CallerID=$endpoint");
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);
			echo $response;
			echo "<br>";
			//sleep(2);
	
	
		$socket = fsockopen($_SERVER['SERVER_ADDR'], "5038");
		fputs($socket, "Action: Login\r\n");
		fputs($socket, "Username: ami_user\r\n");
		fputs($socket, "Secret: passw0rd\r\n\r\n");
		$val_arr = array();
		$event = "";
		while ($ret = fgets($socket)) {
			//print_r($ret);
			if (substr($ret, 0, 6) == "Event:") {
			$e = explode(':', $ret);
			$event = trim($e[1]);
			}
	
			if ($event == "DialBegin") {
			$data = explode(':', $ret);
			if (trim($data[0]) != "" && ( $data[0] == "Context" || $data[0] == "ConnectedLineNum" || $data[0] == "DestChannel" || $data[0] == "DestUniqueid" || $data[0] == "Channel" )) {
				$val_arr[$data[0]] = trim($data[1]);
			}
	
	
			if ($data[0] == "DestUniqueid") {
				if (array_key_exists('ConnectedLineNum', $val_arr)) {
					if (trim($val_arr['ConnectedLineNum']) == trim($dial_num) or trim($val_arr['DestCallerIDNum']) == trim($dial_num)) {
						$callchannelid = $val_arr["DestChannel"];
						$endchannelid_hang = $val_arr["Channel"];
						$DestUniqueid = $val_arr["DestUniqueid"];
						echo $callchannelid;
						echo $endchannelid_hang;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Redirect&ActionID=redirect_chnl&Channel=$callchannelid&Exten=$endpoint&context=from-conf&Priority=1");
						curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
						curl_setopt($ch, CURLOPT_POSTFIELDS, "");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$response = curl_exec($ch);
						curl_close($ch);
						echo $response;
						echo "<br>";
	
	
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Hangup&ActionID=hangup&Channel=$endchannelid_hang&Cause=16");
						curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
						curl_setopt($ch, CURLOPT_POSTFIELDS, "");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$response = curl_exec($ch);
						curl_close($ch);
						echo $response;
						echo "<br>";
						DB::table('phonikip_db.tbl_calls_evnt')
						->where('dest_uniqueid', $DestUniqueid)
						->delete();
						break;
					}
				}
			}
			}
		}

			//print_r($channels_arr_hang);
		} else {
			$company_name = config('app.company_name');
			$cookie = '/var/www/html/' . $company_name . '/ck.txt';
			$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			echo $result;
			echo "<br>";
	
	
			$company_name = config('app.company_name');
			$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
			//$cookie_get=app_path().'/../ck.txt';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=123&Channel=PJSIP/$dial_num&Exten=$endpoint&context=from-conf&Priority=1&CallerID=$dial_num");
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);
			echo $response;
			echo "<br>";
		}

		sleep(2);
		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Busy',
			    'status_des' => "Conf"]);
		}


    public function Dial_call() {
		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		$company_name = config('app.company_name');
		$cookie = '/var/www/html/' . $company_name . '/ck.txt';
		
			$userid=session('userid');
			$com_id = util::get_com_id_by_user($userid);
			$get_out_context  = DB::table('tbl_com_mst')
							->Where('id',$com_id)
							->first('out_context');

			$out_context = $get_out_context->out_context;


			$check_callb_req = DB::SELECT("SELECT `id` FROM `tbl_callback_mst` WHERE SUBSTRING(`number`, -9, 9) = SUBSTRING($dial_num, -9, 9) ORDER BY `datetime` DESC LIMIT 1");

			if($check_callb_req){

			    $id=$check_callb_req[0]->id;
			    DB::UPDATE("UPDATE `tbl_callback_mst` SET `status` = 1 WHERE id='$id' ");
			}

			$check_aban_callb = DB::SELECT("SELECT `id` FROM `tbl_calls_evnt` WHERE SUBSTRING(`frm_caller_num`, -9, 9) = SUBSTRING($dial_num, -9, 9) AND `tbl_calls_evnt`.`desc` = 'ABANDON' ORDER BY `cre_datetime` DESC LIMIT 1");

			if($check_aban_callb){
			    
			    $id=$check_aban_callb[0]->id;
			    DB::UPDATE("UPDATE `tbl_calls_evnt` SET `cbstatus` = 1 WHERE id='$id' ");
			}
     

		//$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
		$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		 echo $result;
		// echo "<br>";

		$company_name = config('app.company_name');
		$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
		//$cookie_get=app_path().'/../ck.txt';
		//echo $cookie_get;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=$endpoint&Channel=PJSIP/$endpoint&Data=lakshan&Exten=$dial_num&context=$out_context&Priority=1&CallerID=$endpoint");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		 echo $response;
		 //echo "<br>";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=CoreShowChannels&ActionID=$endpoint");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);

		$ariConnector = new \phpari('hello');
		$channels = new \channels($ariConnector);
		$channels_arr = $channels->show();
		//dd($channels_arr);
		$endchannelid = "";
		$callchannelid = "";

		foreach ($channels_arr as $value) {
		    //print_r($value);
		    foreach ($value['caller'] as $val_caller) {
			if ($val_caller == $endpoint) {
			    $callchannelid = $value['id'];
			}
		    }
		}
		session()->put('linkedid', $callchannelid);
		return $callchannelid;
    }
    public function callback_request_Dial_call(){

		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		$linkunid = $_GET['linkunid'];
		$userid=session('userid');

		DB::table('tbl_callback_mst')
		            ->where('uniqueid', $linkunid)
		            ->update(['status' => '1']);

		$com_id = util::get_com_id_by_user($userid);
		$get_out_context  = DB::table('tbl_com_mst')
							->Where('id',$com_id)
							->first('out_context');

		$out_context = $get_out_context->out_context;
    	
		$company_name = config('app.company_name');
		$cookie = '/var/www/html/' . $company_name . '/ck.txt';

		$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, ""); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		 //echo $result;
		 //echo "<br>";

		$company_name = config('app.company_name');
		$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
		//$cookie_get=app_path().'/../ck.txt';
		//echo $cookie_get;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=$endpoint&Channel=PJSIP/$endpoint&Data=lakshan&Exten=$dial_num&context=$out_context&Priority=1&CallerID=$endpoint");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		// echo $response;
		// echo "<br>";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=CoreShowChannels&ActionID=$endpoint");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);


		$ariConnector = new \phpari('hello');
		$channels = new \channels($ariConnector);
		$channels_arr = $channels->show();
		//dd($channels_arr);
		$endchannelid = "";
		$callchannelid = "";
		if(is_array($channels_arr) || is_object($channels_arr)){

			foreach ($channels_arr as $value) {
		    //print_r($value);
			    foreach ($value['caller'] as $val_caller) {
			 
					if ($val_caller == $endpoint) {

					    $callchannelid = $value['id'];
					    session()->put('linkedid', $callchannelid);

					}
			    }
			}

			$getQueueInfo = DB::select("SELECT agnt_queueid FROM `tbl_calls_evnt` WHERE linkedid = '$linkunid'" );
			if($getQueueInfo){
				$queueid = $getQueueInfo[0]->agnt_queueid;
			}else{
				$queueid='';
			}

			$getCallbackreqInfo = DB::select("SELECT * FROM `tbl_callback_mst` WHERE uniqueid = '$linkunid' " );
			$getCallbackinfo = DB::select("SELECT * FROM `csp_callhistory` WHERE unq_id = '$linkunid'" );


				if(!$getCallbackinfo ){

					$data = array(
						'unq_id' => $getCallbackreqInfo[0]->uniqueid,
						'call_datetime' => $getCallbackreqInfo[0]->datetime,
						'sipid' => $endpoint,
						'cus_id'=>"",
						'queue_id' => $queueid,
						'pho_number' => $dial_num,
						'log_type'=> "Callback Request",
						'callback_Status' => 'Dialed Without Comment',
						'callback_unq_id' => $callchannelid,
						'created_userid' => $userid,
						'callback_datetime' => NOW(),
						
					);
			  
					
					$insertSuccess=DB::table('csp_callhistory')
					->insert($data);

					DB::UPDATE("UPDATE `csp_callhistory` SET `log_type` = 'Callback Request', `callback_Status` = 'Dialed Without Comment', `callback_unq_id` = '$callchannelid',  pho_number = '$dial_num',  `callback_datetime` = NOW() where `unq_id` = '$linkunid' ");
				}else{

					// $date = NOW();
					DB::UPDATE("UPDATE `csp_callhistory` SET `log_type` = 'Callback Request', `callback_Status` = 'Dialed Without Comment', `callback_unq_id` = '$callchannelid', pho_number = '$dial_num', `callback_datetime` = NOW(), `created_userid` = '$userid' where `unq_id` = '$linkunid' ");
				}

				$cusInfo = DB::SELECT("SELECT `id` FROM `csp_contact_master` WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING($dial_num, -9, 9) LIMIT 1");

				if($cusInfo){
					$cus_id=$cusInfo[0]->id;
				}else{
					$cus_id='';
				}

				$dataout = array(
					'unq_id' => $callchannelid,
					'call_datetime' => NOW(),
					'sipid' => $endpoint,
					'cus_id'=>$cus_id,
					'pho_number' => $dial_num,
					'call_log'=> "Callback Request Dial",
					'log_type' => "Outbound",				
					'created_userid' => $userid,
					
				);
		  
				
				$insertSuccess=DB::table('csp_callhistory')
				->insert($dataout);

				$data = array(
					'unq_id' => $linkunid,
					'call_datetime' => NOW(),
					'pho_number' => $dial_num,
					'sipid' => $endpoint,
					'queue_id'=>"",
					'cus_id'=>$cus_id,
					'log_type'=>"CallbackRequest",
					'call_log'=> "",
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'callback_unq_id' => $callchannelid,
					'callback_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			
				$insertSuccess=DB::table('csp_callhistory_detail')
							->insert($data);

				

		}


		return $callchannelid;
    }
  //   public function getPresetDetails(){

  //   	$preset = (new PresetRemarkController())->retrieveAll();
  //   	$typeOne = (new PresetRemarkController())->retrieveTypeOneData();
  //   	$typeTwo = (new PresetRemarkController())->retrieveTypeTwoData();

  //   	$data = array();

  //   	$data[] = [

  //           'preset' => $preset,
  //   		'typeOne' => $typeOne,
		// 	'typeTwo' => $typeTwo
		// ];

  //   	return compact('data', $data);
  //   }

    public function callback_Dial_call() {

		$endpoint = $_GET['endpoint'];
		$dial_num = $_GET['dial_num'];
		$linkunid = $_GET['linkunid'];

		DB::table('tbl_calls_evnt')
            ->where('linkedid', $linkunid)
            ->update(['cbstatus' => '1']);

		$company_name = config('app.company_name');
		$cookie = '/var/www/html/' . $company_name . '/ck.txt';

		$userid=session('userid');

		$com_id = util::get_com_id_by_user($userid);
		$get_out_context  = DB::table('tbl_com_mst')
							->Where('id',$com_id)
							->first('out_context');

		$out_context = $get_out_context->out_context;

		$ch = curl_init('http://localhost:8088/rawman?action=login&username=ami_user&secret=passw0rd');
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		// echo $result;
		// echo "<br>";

		$company_name = config('app.company_name');
		$cookie_get = '/var/www/html/' . $company_name . '/ck.txt';
	

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=Originate&ActionID=$endpoint&Channel=PJSIP/$endpoint&Data=lakshan&Exten=$dial_num&context=$out_context&Priority=1&CallerID=$endpoint");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		 //echo $response;
		// echo "<br>";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost:8088/rawman?action=CoreShowChannels&ActionID=$endpoint");
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		//echo "test";

		$ariConnector = new \phpari('hello');
		$channels = new \channels($ariConnector);
		$channels_arr = $channels->show();
		//dd($channels_arr);
		$endchannelid = "";
		$callchannelid = "";

		if(is_array($channels_arr) || is_object($channels_arr)){

			foreach ($channels_arr as $value) {
		    //print_r($value);
			    foreach ($value['caller'] as $val_caller) {
			 
					if ($val_caller == $endpoint) {
						
					    $callchannelid = $value['id'];
					    session()->put('linkedid', $callchannelid);

					}
			    }
			}

			$getQueueInfo = DB::select("SELECT * FROM `tbl_calls_evnt` WHERE linkedid = '$linkunid' " );

			if($getQueueInfo){
				$queueid = $getQueueInfo[0]->agnt_queueid;
				$frm_caller_num=$getQueueInfo[0]->frm_caller_num;
				$agnt_sipid=$getQueueInfo[0]->agnt_sipid;
				$agnt_userid=$getQueueInfo[0]->agnt_userid;
				$cre_datetime=$getQueueInfo[0]->cre_datetime;
			}else{
				$queueid='';
				$frm_caller_num='';
				$agnt_sipid='';
				$agnt_userid='';
				$cre_datetime='';
			}

			$getCallInfo = DB::select("SELECT * FROM `csp_callhistory` WHERE `unq_id` = '$linkunid' " );

			// if($getCallInfo){

			// 	$result = DB::update("UPDATE `csp_callhistory` 
			// 	SET `log_type` = 'Abandon Callback', `callback_Status` = 'Dialed Without Comment', `callback_unq_id` = '$callchannelid', `queue_id` = '$queueid', `callback_datetime` = NOW() 
			// 	WHERE `unq_id` = $linkunid");

			//}else{

				$data = array(
					'unq_id' => $linkunid,
					'call_datetime' => NOW(),
					'pho_number' => $frm_caller_num,
					'sipid' => $agnt_sipid,
					'queue_id' => $queueid,
					'log_type'=> "Abandon Callback",
					'callback_unq_id' => $callchannelid,
					'callback_Status' => 'Dialed Without Comment',
					'created_userid' => $agnt_userid,
					'created_datetime' => $cre_datetime,
					'callback_datetime' => NOW()
					
				);

				$insertSuccess=DB::table('csp_callhistory')
				->insert($data);
			//}

			$cusInfo = DB::SELECT("SELECT `id` FROM `csp_contact_master` WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING($dial_num, -9, 9) LIMIT 1");

			if($cusInfo){
				$cus_id=$cusInfo[0]->id;
			}else{
				$cus_id='';
			}

			$dataout = array(
				'unq_id' => $callchannelid,
				'call_datetime' => NOW(),
				'sipid' => $endpoint,
				'cus_id'=> $cus_id,
				'pho_number' => $dial_num,
				'call_log'=> "Abandon Callback Dial",
				'log_type' => "Outbound",				
				'created_userid' => $userid,
				
			);
	  		
			$insertSuccess=DB::table('csp_callhistory')
			->insert($dataout);

				$data = array(
					'unq_id' => $linkunid,
					'call_datetime' => NOW(),
					'pho_number' => $dial_num,
					'sipid' => $endpoint,
					'queue_id'=>$queueid,
					'cus_id'=>$cus_id,
					'log_type'=>"AbandonCallback",
					'call_log'=> "",
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'callback_unq_id' => $callchannelid,
					'callback_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			
				$insertSuccess=DB::table('csp_callhistory_detail')
							->insert($data);

			

		}
		
		return $callchannelid;
    }

public function check_callback_before_add() {
	$linkunid = $_GET['linkunid'];

	$getCallInfo = DB::select("SELECT * FROM `csp_callhistory` WHERE `unq_id` = '$linkunid' LIMIT 1" );

	if($getCallInfo){
		echo "OK";
	}else{
		echo "ERROR";
	}

}

    public function opensocket() {

	$endpoint = $_GET['endpoint'];
	$socket = fsockopen($_SERVER['SERVER_ADDR'], "5038");
	fputs($socket, "Action: Login\r\n");
	fputs($socket, "Username: ami_user\r\n");
	fputs($socket, "Secret: passw0rd\r\n\r\n");
	$val_arr = array();
	$event = "";
	while ($ret = fgets($socket)) {
	    //print_r($ret);
	    if (substr($ret, 0, 6) == "Event:") {
		$e = explode(':', $ret);
		$event = trim($e[1]);
	    }

	    if ($event == "DialBegin") {
		$data = explode(':', $ret);
		if (trim($data[0]) != "" && ( $data[0] == "CallerIDNum" || $data[0] == "DestExten" || $data[0] == "Linkedid" || $data[0] == "DestCallerIDNum" )) {
		    $val_arr[$data[0]] = trim($data[1]);
		}


		if ($data[0] == "DestCallerIDNum") {
		    if (array_key_exists('CallerIDNum', $val_arr)) {
			if (trim($val_arr['CallerIDNum']) == trim($endpoint)) {

			    session()->put('linkedid', $val_arr["Linkedid"]);
			    session()->put('CallerIDNum', $val_arr["CallerIDNum"]);
			    //echo trim($val_arr['CallerIDNum']);
			    $get_facility_no = DB::table('phonikip_db.autodial_batch')
				    ->select('facility_no')
				    ->where('autodial_batch.cus_number', '=', $val_arr["DestCallerIDNum"])
				    ->where('autodial_batch.user_id', '=', session('userid'))
				    ->where('autodial_batch.dial_status', '=', 'Dial Start')
				    ->first();
			    if (!empty($get_facility_no)) {
				$val_arr["facility_no"] = $get_facility_no->facility_no;
				$val_arr["call_type"] = "camp_outbound";
				//dd($val_arr);
				return $val_arr;
				break;
			    }
			}
		    }
		}

		if ($data[0] == "DestExten") {
		    if (trim($val_arr['DestExten']) == trim($endpoint)) {


			session()->put('linkedid', $val_arr["Linkedid"]);
			session()->put('CallerIDNum', $val_arr["CallerIDNum"]);
			//echo trim($val_arr['CallerIDNum']);
			$val_arr["call_type"] = "inbound";
			return $val_arr;
			break;
		    }
		}
	    }
	}
    }
    public function save_break_exc_reason() {
    //   
		$endpoint = session('endpoint');

		$br_ex_reason = $_POST['br_ex_reason'];
		// print_r($br_ex_reason);exit();
		// exit();

		DB::table('tbl_agnt_evnt')
				->where('tbl_agnt_evnt.agnt_event', '=', "Break End")
				->where('tbl_agnt_evnt.agnt_sipid',$endpoint)
				->orderBy('tbl_agnt_evnt.id','desc')
				->take(1)
				->update(['time_exceeded_reason' => $br_ex_reason]);

		return redirect()->back();
	}
	public function check_break_exceeded() {
		
		$endpoint = session('endpoint');

		$get_start_time = DB::table('tbl_agnt_evnt')
							->select('cre_datetime','agnt_desc','status_list.time_limit')
							->join('status_list', 'tbl_agnt_evnt.agnt_desc', '=', 'status_list.status') 	
							->where('tbl_agnt_evnt.agnt_event', '=', "Break Start")
							->orderby('tbl_agnt_evnt.id', 'desc')
							->first();
							
									
		$start_brk_time = $get_start_time->cre_datetime;
		$time_limit = $get_start_time->time_limit;
		
		$start_datetime = strtotime($start_brk_time);
        $endtime = strtotime(date("Y-m-d H:i:s"));
        $evnt_sec_count =  round(abs($endtime - $start_datetime),2);


		 if($evnt_sec_count > $time_limit )
		 {
			 return "true";
			
		 } else{
			return "false";
		 }
		 	
	}
	public function changebreakstatus() 
{
	$endpoint = $_GET['endpoint'];
	$brkreason = $_GET['brkreason'];
	//brkreason only has value when start a break
	$cre_datetime = date("Y-m-d H:i:s");
	// print_r($cre_datetime);
	$userid=session()->get('userid');
	
	// print_r($time);exit();
	$today = date("Y-m-d"); 
	$day = Carbon::createFromFormat('Y-m-d', $today)->format('l');
	$time = date("H:i:s"); 

				// Session::forget('flash_message','Your break Request has been sent for the approval !!!');
	$checkstatus = DB::table('asterisk.ps_contacts')
		->where('endpoint', $endpoint)
		->first();
	if (!empty($checkstatus)) 
		{
			if ($checkstatus->status == "Break") 
			{
				DB::table('asterisk.ps_contacts')
					->where('endpoint', $endpoint)
					->update(['status' => 'Online',
						'status_des' => "",
						'update_datetime' => $cre_datetime]);
				Util::endpart_agent_event_log("Break Start", "Break End", session('endpoint'), "");


				$endpoint = session('endpoint');

				$get_start_time = DB::table('tbl_agnt_evnt')
									->select('cre_datetime','agnt_desc','status_list.time_limit')
									->join('status_list', 'tbl_agnt_evnt.agnt_desc', '=', 'status_list.status') 	
									->where('tbl_agnt_evnt.agnt_event', '=', "Break Start")
									->orderby('tbl_agnt_evnt.id', 'desc')
									->first();
									
											
				$start_brk_time = $get_start_time->cre_datetime;
				$time_limit = $get_start_time->time_limit;
				
				$start_datetime = strtotime($start_brk_time);
				$endtime = strtotime(date("Y-m-d H:i:s"));
				$evnt_sec_count =  round(abs($endtime - $start_datetime),2);


				if($evnt_sec_count > $time_limit )
				{
					echo "exceed";
					
				} 
				else{
					echo "off";
				}
				
				//echo "off";
			//this means break off successfull
			} 
			else if ($checkstatus->status == "Online") 
				{
					$get_penal_time =	DB::table('tbl_break_request')
											->select('sup_penalty_time','sup_act_datetime')
											->where('user_id',$userid)
											->where('sup_action','=',"Rejected With Penalty Time")
											->where('day',$day)
											->where('sup_act_datetime','<',$cre_datetime)
											->first();
					// print_r($get_penal_time);exit();
					if(!empty($get_penal_time->sup_penalty_time)) 
					{
						$sup_action = $get_penal_time->sup_act_datetime;
						$cur_time = strtotime(date("Y-m-d H:i:s"));
						$sup_action_time = strtotime($sup_action);
						$diff = round(abs($cur_time - $sup_action_time )/60,2);

						if($get_penal_time->sup_penalty_time > $diff)
						{
							Session::put('flash_message','You Can not request the break Untill finishing the penalty time !!!');
								return redirect()->back();
						}else
						{
							DB::table('tbl_break_request')
									->where('sip_id',$endpoint)
									->where('user_id',$userid)
									->where('day',$day)
									->limit(1)
									->update([
										'sup_action' => "Rejected with penalty time - complete" ]);

							$check_br_det =	DB::table('tbl_break_schedule_master')
									->select('record_day','start_time','end_time')
									->where('start_time', '<', $time )
									->where('end_time','>', $time)
									->where('record_day','=', $day)
									->first();
							//dd($check_br_det);
							if(!empty($check_br_det->record_day)) 
							{
								$insert_br_req = DB::table('tbl_break_request')
														->insert(
															['user_id' => session('userid'),
															'sip_id' => $endpoint,
															'day' => $day,
															'break_type' => $brkreason,
															'sup_action' => 'Fresh',
															'cre_datetime' => Carbon::now()]);
									Session::put('flash_message','Your break Request has been sent for the approval !!!');	
									// echo "BreakRequested";
										return redirect()->back();
							}
							return redirect()->back();
						}
					
						// $minutes_to_add = $get_penal_time->sup_penalty_time;
						// $penal_time_second = $minutes_to_add * 60 ;
						// $rec_time = $get_penal_time->sup_act_datetime;
						
						// $endtime = Carbon::parse($rec_time)
						// 				->addSeconds($penal_time_second)
						// 				->format('Y-m-d H:i:s');

						// $cre_datetime_ = date('Y-m-d H:i:s');
						// $cre_datetime_ = date('Y-m-d H:i:s', strtotime($cre_datetime_));   
						// $rec_time_ = date('Y-m-d H:i:s', strtotime($rec_time));
						// $endtime_ = date('Y-m-d H:i:s', strtotime($endtime));   
						
						// if ($cre_datetime_ <= $endtime_){   
							
						// }
					}
					else
					{
						// Session::forget('flash_message','You Can not request the break Untill finishing the penalty time !!!');
						$check_br_det =	DB::table('tbl_break_schedule_master')
													->select('record_day','start_time','end_time')
													->where('start_time', '<', $time )
													->where('end_time','>', $time)
													->where('record_day','=', $day)
													->first();
													//dd($check_br_det);
						if(!empty($check_br_det->record_day)) 
						{
							$insert_br_req = DB::table('tbl_break_request')
													->insert(
														['user_id' => session('userid'),
														'sip_id' => $endpoint,
														'day' => $day,
														'break_type' => $brkreason,
														'sup_action' => 'Fresh',
														'cre_datetime' => Carbon::now()]);
								Session::put('flash_message','Your break Request has been sent for the approval !!!');	
								// echo "BreakRequested";
									return redirect()->back();
						}
						else 
						{
							DB::table('asterisk.ps_contacts')
								->where('endpoint', $endpoint)
								->update(['status' => 'Break',
									'status_des' => $brkreason,
									'update_datetime' => $cre_datetime]);
							Util::startpart_agent_event_log("Break Start", $endpoint, $brkreason);
							echo "on";
								// this means break on successfull
						}		
					}
				}			
			else if ($checkstatus->status == "Offline" || $checkstatus->status == "Busy") 
			{
				echo "Offline";		//offline means user not online or on a call 
			}
			else 
			{
				echo "Notfound";
					// this means device not registered
			}	//this condition for only start break			
		}
		if ($brkreason != "") 
			{
				return redirect()->back();
			}
}						 
 
     public function start_acw() {
	$endpoint = $_GET['endpoint'];
	$acw_num = $_GET['acw_num'];
		$checkstatus = DB::table('asterisk.ps_contacts')
		->where('endpoint', $endpoint)
		->first();
	//this checks whether softphone registered or not
	if (!empty($checkstatus)) {
	    if ($checkstatus->status == "Online") {
				return "Online";
			}else
			{
				//$linkid = $_GET['linkid'];
				$linkid = session('linkedid');

				if ($linkid != "") {
				    $acw_linkedid = $linkid;
				} else {
				    $ariConnector = new \phpari('hello');
				    $channels = new \channels($ariConnector);
				    $channels_arr = $channels->show();
				    //dd($channels_arr);
				    $endchannelid = "";
				    $callchannelid = "";

				    foreach ($channels_arr as $value) {
					foreach ($value['caller'] as $val_caller) {
					    //dd($value);
					    if ($val_caller == $endpoint) {
						$callchannelid = $value['id'];
					    }
					
				    }
				    $acw_linkedid = $callchannelid;}
				    //echo $callchannelid;
				}

				if($acw_linkedid==""){
					$acw_linkedid=$checkstatus->linkedid;
				}

				$insert_acw = DB::table("tbl_acw_tmp")
					->insert(
					['user_id' => session('userid'),
					    'sip_id' => $endpoint,
					    'linkid' => $acw_linkedid,
					    'from_caller_num' => $acw_num
					]
				);

				DB::table('asterisk.ps_contacts')
					->where('endpoint', $endpoint)
					->update(['status' => 'ACW']);
				if ($insert_acw) {
				    return $acw_linkedid;
				}
			}
		}
    }


    public function end_acw() {
	$endpoint = $_GET['endpoint'];
	$acw_num = $_GET['acw_num'];
	$substr_acwnum = substr($acw_num, -9);

	$linkid = session('linkedid');
	//echo $linkid;
	$cre_datetime = date("Y-m-d H:i:s");
	//echo $linkid;

		$getlinkedid = DB::table('phonikip_db.tbl_acw_tmp')
		->where('sip_id', $endpoint)
		->orderBy('id',"desc")
		->first();
	$linkid = $getlinkedid->linkid;
	$check_record = DB::table('phonikip_db.tbl_calls_evnt')
		->select('hangup_datatime', 'frm_caller_num', 'to_caller_num')
		->where('tbl_calls_evnt.linkedid', '=', trim($linkid))
		->whereNotNull('tbl_calls_evnt.hangup_datatime')
		->first();
	//echo "hellow";
	//print_r($check_record);
	if (!empty($check_record->hangup_datatime)) {
	    //echo "hellow";
	    $start_datetime = strtotime($check_record->hangup_datatime);
	    $endtime = strtotime(date("Y-m-d H:i:s"));
	    $acw_sec_count = round(abs($endtime - $start_datetime) / 60 * 60, 2);
	    //echo $acw_sec_count;
	    //$acw_sec_count  = DB::table("SELECT TIMESTAMPDIFF(SECOND, $check_record->hangup_datatime,$cre_datetime)");
	    //echo $check_record->frm_caller_num;
	    if (!empty($check_record->frm_caller_num)) {
		$update_call_event = DB::table('phonikip_db.tbl_calls_evnt')
			->where('linkedid', trim($linkid))
			->where('frm_caller_num', $check_record->frm_caller_num)
			->where('hangup_datatime', $check_record->hangup_datatime)
			->update([
		    'acw_sec_count' => $acw_sec_count,
		    'acwend_datatime' => $cre_datetime]);
	    } else {
		$update_call_event = DB::table('phonikip_db.tbl_calls_evnt')
			->where('linkedid', trim($linkid))
			->where('to_caller_num', $check_record->to_caller_num)
			->where('hangup_datatime', $check_record->hangup_datatime)
			->update([
		    'acw_sec_count' => $acw_sec_count,
		    'acwend_datatime' => $cre_datetime]);
	    }
	    //print_r($update_call_event);
	    if ($update_call_event) {
		DB::table('phonikip_db.tbl_acw_tmp')
			->where('sip_id', $endpoint)
			->delete();

		DB::table('asterisk.ps_contacts')
			->where('endpoint', $endpoint)
			->update(['status' => 'Online',
			    'status_des' => '',
			    'update_datetime' => $cre_datetime]);

		return "acw off";
	    }
	}
    }

    public function dnd_on() {
	$endpoint = $_GET['endpoint'];
	$offlinereason = $_GET['offlinereason'];
	$cre_datetime = date("Y-m-d H:i:s");
	$checkstatus = DB::table('asterisk.ps_contacts')
		->where('endpoint', $endpoint)
		->first();

	if (!empty($checkstatus)) 
	{
	  
			DB::table('asterisk.ps_contacts')
				->where('endpoint', $endpoint)
				->update(['status' => 'Offline',
			    	'status_des' => '',
					'linkedid' => '',
			    	'update_datetime' => $cre_datetime]);

		Util::endpart_agent_event_log("Online", "Offline", session('endpoint'), $offlinereason);

		return "changed";
	
	} else {
	    return "Notfound";
	    //this will return when device not registered
	}
    }

    public function getdetailsofcaller() {
	$endpoint = $_GET['endpoint'];
	$cl_number = $_GET['cl_number'];
	$linkedid = $_GET['linkedid'];
	//echo $cl_number;
	//$cl_number = "769167754";
	$primary_number = "764005696";
	$contact_id = "9326";
	$contact = (new ContactController())->retrieveAllContact($cl_number);
	//$this->saveCallLog($cl_number, $linkedid);
	//	dd($contact);

	if (count($contact) >= 1 || count($contact) == 0) {

	    return view('CallList_agntdb')
			    ->with('contact', $contact)
			    ->with('sipid', $endpoint)
			    ->with('number', $cl_number)->with('linkedid', $linkedid);
	    echo "success";
	} else if (count($contact) == 1) {

	    $card = '<div class="col-md-12">';
	    $card .= '<div class="box box-danger">';
	    $card .= '<div class="box-header with-border">';
	    $card .= '<input type="hidden" name="incoming_num" id="incoming_num" value="' . $cl_number . '" />';
	    $card .= '<h3 class="box-title">Customer Info - ' . $cl_number . '</h3>';
	    $card .= '<div class="box-tools pull-right">';
	    $card .= '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
	    $card .= '</button>';
	    $card .= '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>';
	    $card .= '</div>';
	    $card .= '</div>';
	    $card .= '<div class="box-body" id="pieChartContent">';
	    $card .= '<table style="margin-top:10px;width:100%">';
	    $card .= '<tr>';
	    $card .= '<td><strong>Contact Name: ' . $contact[0]->title . ' ' . $contact[0]->firstname . ' ' . $contact[0]->lastname . '</strong></td>';
	    $card .= '<td><strong>Primary Contact: ' . $contact[0]->primary_contact . '</strong></td>';
	    $card .= '<td><strong>Secondary Contact 1: ' . $contact[0]->secondary_contact . '</strong></td>';
	    $card .= '<td><strong>Secondary Contact 2: ' . $contact[0]->secondary_contact2 . '</strong></td>';
	    $card .= '<td><strong>Email: ' . $contact[0]->email . '</strong></td>';
	    $card .= '<td><a style="color: #000000" target="_blank" href="inboundCallUpdateRecordInfo/' . $contact[0]->primary_contact . '/' . $contact[0]->id . '/' . $cl_number . '/' . $endpoint . '">';
	    $card .= '<span class="glyphicon glyphicon-eye-open"></span></a></td>';
	    $card .= '</tr>';
	    $card .= '</table>';
	    $card .= '</div>';
	    $card .= '</div>';
	    $card .= '</div>';
	    return $card;
	}
	//return Redirect::to(url('frmdb_inboundCallUpdateRecordInfo/'.$cl_number .'/'.$contact_id.'/'.$primary_number.'/'.$endpoint));
    }

 public function getdetailsofagnt() 
{
	 $data =DB::SELECT("SELECT
								tbl_eval_agnt_mst.id,
								tbl_eval_agnt_mst.eval_name,
								tbl_eval_agnt_mst.eval_contxt_id,
								tbl_eval_agnt_mst.frm_date,
								tbl_eval_agnt_mst.to_date,
								tbl_eval_agnt_mst.rec_option,
								tbl_eval_agnt_mst.rec_num,
								tbl_eval_agnt_mst.com_id,
								tbl_eval_agnt_mst.cre_datetime,
								tbl_eval_agnt_mst.cre_userid,
								tbl_eval_contxt_mst.eval_contxt_name,
								user_master.username,
								(SELECT Count(tbl_eval_agnt_rec.rec_name) FROM tbl_eval_agnt_rec where tbl_eval_agnt_rec.eval_id = tbl_eval_agnt_mst.id) AS total_rec
								FROM
								tbl_eval_agnt_mst
								INNER JOIN tbl_eval_contxt_mst ON tbl_eval_agnt_mst.eval_contxt_id = tbl_eval_contxt_mst.id AND tbl_eval_contxt_mst.id = tbl_eval_agnt_mst.eval_contxt_id
								INNER JOIN user_master ON tbl_eval_agnt_mst.cre_userid = user_master.id
								");

                                    
		$ipaddress = (new UsersController())->get_client_ip();
		$username=session()->get('username');
		Util::user_auth_log($ipaddress,"Search agent evaluation master data",$username,"Search agent evaluation master data");
		
		return compact('data',$data);
}



    public function getQueInformation() {
	$linkeid = $_GET['linkeid'];
	$checkstatus = DB::table('tbl_calls_evnt')
		->where('linkedid', $linkeid)
		->where('status', "ENTERQUEUE")
		->first();
	if (!empty($checkstatus)) {
	    return $checkstatus->agnt_queueid;
	} else {
	    return "Notfound";
	    //this will return when device not registered
	}
    }

    public function getIncommingNo() {
	$linkeid = $_GET['linkeid'];
	$checkstatus = DB::table('tbl_calls_evnt')
		->where('linkedid', $linkeid)
		->where('status', "ENTERQUEUE")
		->first();
	if (!empty($checkstatus)) {
	    return $checkstatus->frm_caller_num;
	} else {
	    return "Notfound";
	    //this will return when device not registered
	}
    }

    public function getQueDestination() {
	$endpoint = $_GET['endpoint'];
	$checkstatus = DB::table('asterisk.queues_config')
		->where('extension', $endpoint)
		->first();
	if (!empty($checkstatus)) {
	    return $checkstatus->descr;
	} else {
	    return "Notfound";
	    //this will return when device not registered
	}
    }

    public function searchdata() {
	//exit();
	$userid=session('userid');
    $com_id = util::get_com_id_by_user($userid);

	$data = DB::table('tbl_phone_bk')
		->join('tbl_com_mst', 'com_id', '=', 'tbl_com_mst.id')
		->select('tbl_phone_bk.*', DB::raw('CONCAT(title," ",first_name," ", last_name) as contact_name'), 'tbl_com_mst.com_name as company')
		->where('tbl_phone_bk.com_id', $com_id)
		->get();

	return compact('data', $data);
    }
	public function saveCallLog($pho_number,$linkedid){

		$endpoint=session()->get('endpoint');	
		$userid=session()->get('userid');
		$com_id = util::get_com_id_by_user($userid);	
		$getLogInfo = DB::select("SELECT	
		csp_callhistory.id,	
		csp_callhistory.unq_id,	
		csp_callhistory.pho_number	
			FROM	
		csp_callhistory	
			WHERE	
		csp_callhistory.unq_id ='".$linkedid."' AND	
		csp_callhistory.pho_number = '".$pho_number."'" );	
		if(empty($getLogInfo)){	
			$getQueueInfo = DB::select("SELECT agnt_queueid FROM `tbl_calls_evnt` WHERE linkedid = $linkedid" );
			if(!empty($getLogInfo))
			{
				$queueid = $getQueueInfo[0]->agnt_queueid;
			}else
			{
				$queueid = "";
			}
			
			
			$data = array(	
				'unq_id' => $linkedid,	
				'call_datetime' => NOW(),	
				'pho_number' => $pho_number,	
				'sipid' => $endpoint,	
				'queue_id'=> $queueid,
				'cus_id'=>"",	
				'log_type'=> "Inbound",	
				'call_log'=> "Automate Call Log",	
				'cat_one_prerem_id'=> "",	
				'cat_two_prerem_id'=> "",	
				'created_userid' => $userid,	
				'created_datetime' => NOW(),
			);	
			$insertSuccess=DB::table('csp_callhistory')	
				->insert($data);


					
		}	
				
		
	
			
		
		
			// if($insertSuccess){	
			// 	echo 'Call Log Added Successfully!';	
			// }else{	
					
			// 	echo 'Data Saving Error! ';	
			// }		
	}
	public function search_abn_details(){  

		$endpoint = session()->get('endpoint');

		$data = array();

		$getdata=DB::SELECT("SELECT DISTINCT 
			`tbl_calls_evnt`.`linkedid` AS `rec_linkedid`,
			`tbl_calls_evnt`.`cre_datetime` AS incoming_date,
			`tbl_calls_evnt`.`frm_caller_num` AS incoming_num, 
			 `queues_config`.`extension`, `queues_config`.`descr`, 
			(SELECT SEC_TO_TIME( ROUND( tbl_calls_evnt.ring_sec_count ) ) ) AS waiting_time,
			`tbl_calls_evnt`.`date`,
			(SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName` 
			FROM `csp_contact_master` 
			WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`incoming_num`, -9, 9) LIMIT 1) AS `fullName`
			FROM `tbl_calls_evnt`   
			JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
			WHERE 
			`tbl_calls_evnt`.`date` BETWEEN CURDATE() AND (CURDATE() + 1) 
			AND `tbl_calls_evnt`.`desc` = 'ABANDON' 
			AND `tbl_calls_evnt`.`cbstatus` = 0
			AND `tbl_calls_evnt`.`agnt_queueid`  IN (SELECT `id` FROM `asterisk`.`queues_details` WHERE `keyword` = 'member' AND `data` LIKE '%$endpoint%')
			
			ORDER BY `tbl_calls_evnt`.`cre_datetime` DESC");

		if($getdata){

			foreach ($getdata as $value) {

				$incoming_num = $value->incoming_num;
				$incoming_date = $value->incoming_date;

				if(in_array($incoming_num, array_column($data, 'incoming_num'))){

					continue;

				}else{

					$check_num_leave=DB::SELECT("SELECT id
                            from `tbl_callback_mst` cb 
                            where 
                            DATE(cb.datetime)=DATE(NOW()) AND 
                            cb.in_datetime = '0000-00-00 00:00:00'
                            AND (SUBSTRING(cb.`number`, -9, 9)=SUBSTRING($incoming_num, -9, 9) 

                            OR SUBSTRING(
                            (SELECT frm_caller_num 
                            FROM `tbl_calls_evnt` ev 
                            WHERE ev.uniqueid = cb.`uniqueid` LIMIT 1), -9, 9)=SUBSTRING($incoming_num, -9, 9)
                        	)
                        	
                        	");

					if($check_num_leave){

					}else{

	
							$data[] = [

								'incoming_num' => $incoming_num,
								'rec_linkedid' => $value->rec_linkedid,
								'incoming_date' => $value->incoming_date,
								'extension' => $value->extension,
								'descr' => $value->descr,
								'waiting_time' => $value->waiting_time,
								'fullName' => $value->fullName
							];

					}

				}
				
			}			
			// dd (in_array('0117121600', array_column($data, 'incoming_num')));
		}


		return compact('data');
        
    }
    public function get_clbck_det(){

		$endpoint = session()->get('endpoint');
		$userid = session()->get('userid');
		$com_id = util::get_com_id_by_user($userid);
		$data = array();

		$getdata = DB::SELECT("SELECT DISTINCT 
			`tbl_callback_mst`.`uniqueid` AS uniqueid,
			 `tbl_callback_mst`.`datetime` AS credate,
			  `tbl_callback_mst`.`number` AS callnumber,
			   `tbl_callback_mst`.`did` AS did,
			    `tbl_callback_mst`.`queue` AS queue, 
			    queues_config.descr,
					(SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName`
					 FROM `csp_contact_master` 
					 WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`callnumber`, -9, 9) LIMIT 1) AS `fullName`,
					(SELECT frm_caller_num FROM `tbl_calls_evnt` ev WHERE ev.uniqueid = `tbl_callback_mst`.`uniqueid`  LIMIT 1) AS `frm_caller_num`
					  FROM `tbl_callback_mst`
					LEFT JOIN `csp_callhistory` ON `tbl_callback_mst`.`uniqueid` = `csp_callhistory`.unq_id
					LEFT JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_callback_mst`.`queue`
					WHERE tbl_callback_mst.queue IN (Select `id` FROM `asterisk`.`queues_details` WHERE `keyword` = 'member' AND `data` Like '%$endpoint%') 
		AND (LENGTH(`tbl_callback_mst`.`in_uniqueid`)=0) 
		AND tbl_callback_mst.status=0 
		
		UNION ALL
		
		SELECT DISTINCT 
		`tbl_callback_mst`.`uniqueid` AS uniqueid,
		 `tbl_callback_mst`.`datetime` AS credate,
		  `tbl_callback_mst`.`number` AS callnumber,
		   `tbl_callback_mst`.`did` AS did,
		    `tbl_callback_mst`.`queue` AS queue,
		     queues_config.descr,
					(SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName` 
					FROM `csp_contact_master` 
					WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`callnumber`, -9, 9) LIMIT 1) AS `fullName`,
					(SELECT src FROM `asteriskcdrdb`.`cdr` cdr WHERE cdr.uniqueid = `tbl_callback_mst`.`uniqueid`  LIMIT 1) AS `frm_caller_num`
					  FROM `tbl_callback_mst`
					LEFT JOIN `csp_callhistory` ON `tbl_callback_mst`.`uniqueid` = `csp_callhistory`.unq_id
					LEFT JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_callback_mst`.`queue`
					WHERE tbl_callback_mst.queue = 'IVR'
					 and did IN (SELECT box_name from tbl_srvbox_mst Where cat_id=2 and com_id=$com_id) 
		AND (LENGTH(`tbl_callback_mst`.`in_uniqueid`)=0) AND tbl_callback_mst.status=0 ");	

		if($getdata){

			foreach ($getdata as $value) {
				# code...

				$callnumber = $value->callnumber;


				$check_call_back=DB::SELECT("SELECT id
                            from `tbl_calls_evnt` ev 
                            where ev.cre_datetime > '$value->credate'
                            AND ev.call_type='outbound'
                            AND SUBSTRING(ev.`to_caller_num`, -9, 9)=SUBSTRING($callnumber, -9, 9) 
                            ");

				if($check_call_back){

				}else{
					

					$data[] = [

						'callnumber' => $callnumber,
						'uniqueid' => $value->uniqueid,
						'credate' => $value->credate,
						'did' => $value->did,
						'queue' => $value->queue,
						'descr' => $value->descr,
						'fullName' => $value->fullName,
						'frm_caller_num' => $value->frm_caller_num
					];

				}

			}

		}

		// print_r($data); exit();
		return compact('data');
    }
   

}
