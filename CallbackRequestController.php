<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;
use DateTime;
use DatePeriod;
use DateInterval;


require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class CallbackRequestController extends Controller{
  
    
    public function view_callback_req_report(){	
		
		if(Util::isAuthorized("callback_request_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("callback_request_report")=='DENIED'){
            return view('permissiondenide');
        }
		
        Util::log('Callback Request Details Report','View');
        
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getdndstatus  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();
      
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Callback Request Report Dashboard",$username,"View Callback Request Report");


        return view('view_callback_request_report',compact('getdndstatus','get_com_data'));

	}

    public function search_callback_req_report(Request $request){  
   
        $userid=session('userid');
        $getcomid = DB::SELECT("SELECT com_id FROM `user_master` WHERE `id` = $userid");
        $comid = $getcomid[0]->com_id;
        
        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();  
        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');


        $data=DB::SELECT("SELECT DISTINCT 
                        `tbl_callback_mst`.`uniqueid` AS uniqueid,
                        `tbl_callback_mst`.`in_datetime`,
                        `tbl_callback_mst`.`id`,
                        `tbl_callback_mst`.`status`,
                         `tbl_callback_mst`.`datetime` AS credate,
                          `tbl_callback_mst`.`number` AS callnumber,
                           `tbl_callback_mst`.`did` AS did,
                            `tbl_callback_mst`.`queue` AS queue,
                             `csp_callhistory_detail`.`callback_Status` AS `callbackStatus`,
                             CONCAT(`user_master`.`fname`,' ',`user_master`.`lname`) AS `agnt_name`,
                              `csp_callhistory_detail`.`log_type` AS `log_type`,
                               `queues_config`.`descr`,
                                `tbl_callback_mst`.`did`,
                                 `csp_callhistory_detail`.`sipid`,
                                  `csp_callhistory_detail`.`callback_datetime`,
                                  (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_one_prerem_id`) AS `cat_one_prerem_id`,
                                   (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_two_prerem_id`) AS `cat_two_prerem_id`,
                                    `csp_callhistory_detail`.`call_log`,
                                     (SELECT status FROM `tbl_calls_evnt` WHERE `uniqueid` = `csp_callhistory_detail`.`callback_unq_id` ) AS `callbackStatus`,
            (SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName` FROM `csp_contact_master` WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`callnumber`, -9, 9) LIMIT 1) AS `fullName`,

            (SELECT IF(queue='IVR',(SELECT src FROM `asteriskcdrdb`.`cdr` cdr WHERE cdr.uniqueid = `tbl_callback_mst`.`uniqueid`  LIMIT 1),(SELECT frm_caller_num FROM `tbl_calls_evnt` ev WHERE ev.uniqueid = `tbl_callback_mst`.`uniqueid`  LIMIT 1)) ) AS `frm_caller_num`

            FROM `tbl_callback_mst`
            LEFT JOIN `csp_callhistory_detail` ON `tbl_callback_mst`.`uniqueid` = `csp_callhistory_detail`.unq_id and `csp_callhistory_detail`.log_type = 'CallbackRequest'
            LEFT JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_callback_mst`.`queue`
            LEFT JOIN `user_master` ON `user_master`.`id` = `csp_callhistory_detail`.created_userid
            WHERE IF ('$queue_id' != 'All' , `tbl_callback_mst`.`queue` = '$queue_id', (`tbl_callback_mst`.`queue` IS NOT NULL OR did IN (SELECT box_name from tbl_srvbox_mst Where cat_id=2 and com_id=$comid))) AND  IF ('$com_id' != 'All' , `queues_config`.`com_id` = '$com_id', (`queues_config`.`com_id` = $comid OR did IN (SELECT box_name from tbl_srvbox_mst Where cat_id=2 and com_id=$comid)) ) AND (`tbl_callback_mst`.`datetime` BETWEEN '$frm_date' AND '$to_date') GROUP BY `tbl_callback_mst`.`datetime` ");



        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Abandon Calls Report",$username,"Search Abandon Calls Report");
        
        return compact('data',$data);
        
    }

public function updatecallback(){ 
        $id = $_GET['id'];
        // $orgstatus = $_GET['status'];
    // Route::get('updatecallback', 'CallbReportController@updatecallback');
    
    $tablest  = DB::table('tbl_callback_mst') 
        ->where('id', $id)
        ->first();
        $status=0;

        
        if($tablest->status == 0){
            $status=1;
        }else{
            $status=0;
        }

        DB::table('tbl_callback_mst')
            ->where('id', $id)
            ->update(['status' => $status]);

         echo "OK";
    }


    public function search_callback_req_report_for_mail(Request $request){  
   
        $userid=224;
        $getcomid = DB::SELECT("SELECT com_id FROM `user_master` WHERE `id` = $userid");
        $comid = $getcomid[0]->com_id;
        
        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();  
        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');


        $data=DB::SELECT("SELECT DISTINCT 
                        `tbl_callback_mst`.`uniqueid` AS uniqueid,
                        `tbl_callback_mst`.`in_datetime`,
                        `tbl_callback_mst`.`id`,
                        `tbl_callback_mst`.`status`,
                         `tbl_callback_mst`.`datetime` AS credate,
                          `tbl_callback_mst`.`number` AS callnumber,
                           `tbl_callback_mst`.`did` AS did,
                            `tbl_callback_mst`.`queue` AS queue,
                             `csp_callhistory_detail`.`callback_Status` AS `callbackStatus`,
                             CONCAT(`user_master`.`fname`,' ',`user_master`.`lname`) AS `agnt_name`,
                              `csp_callhistory_detail`.`log_type` AS `log_type`,
                               `queues_config`.`descr`,
                                `tbl_callback_mst`.`did`,
                                 `csp_callhistory_detail`.`sipid`,
                                  `csp_callhistory_detail`.`callback_datetime`,
                                  (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_one_prerem_id`) AS `cat_one_prerem_id`,
                                   (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_two_prerem_id`) AS `cat_two_prerem_id`,
                                    `csp_callhistory_detail`.`call_log`,
                                     (SELECT status FROM `tbl_calls_evnt` WHERE `uniqueid` = `csp_callhistory_detail`.`callback_unq_id` ) AS `callbackStatus`,
            (SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName` FROM `csp_contact_master` WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`callnumber`, -9, 9) LIMIT 1) AS `fullName`,
            
            (SELECT IF(queue='IVR',(SELECT src FROM `asteriskcdrdb`.`cdr` cdr WHERE cdr.uniqueid = `tbl_callback_mst`.`uniqueid`  LIMIT 1),(SELECT frm_caller_num FROM `tbl_calls_evnt` ev WHERE ev.uniqueid = `tbl_callback_mst`.`uniqueid`  LIMIT 1)) ) AS `frm_caller_num`

            FROM `tbl_callback_mst`
            LEFT JOIN `csp_callhistory_detail` ON `tbl_callback_mst`.`uniqueid` = `csp_callhistory_detail`.unq_id and `csp_callhistory_detail`.log_type = 'CallbackRequest'
            LEFT JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_callback_mst`.`queue`
            LEFT JOIN `user_master` ON `user_master`.`id` = `csp_callhistory_detail`.created_userid
            WHERE IF ('$queue_id' != 'All' , `tbl_callback_mst`.`queue` = '$queue_id', (`tbl_callback_mst`.`queue` IS NOT NULL OR did IN (SELECT box_name from tbl_srvbox_mst Where cat_id=2 and com_id=$comid))) AND  IF ('$com_id' != 'All' , `queues_config`.`com_id` = '$com_id', (`queues_config`.`com_id` = $comid OR did IN (SELECT box_name from tbl_srvbox_mst Where cat_id=2 and com_id=$comid)) ) AND (`tbl_callback_mst`.`datetime` BETWEEN '$frm_date' AND '$to_date') GROUP BY `tbl_callback_mst`.`datetime` ");

        
        return compact('data',$data);
        
    }


}