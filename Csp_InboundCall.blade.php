<!DOCTYPE html>


<!-- Content Wrapper. Contains page content -->
<button type="button" style="float: right;" onclick="close_csp_div();"aria-label="Close" >
				<span aria-hidden="true" style="color:red">&times;</span>
	</button>
<div class="content-wrapper  caller_info caller_info_ex"  style="padding:20px;" >
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1 class="form_caption">Inbound Call Register</h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                    <form class="form-horizontal" action="{{url('insertticket')}}" method="post" id="form_new_ticket" autocomplete="off">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{csrf_field()}}
						</div>
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        
                       

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important; font-weight: 100;">Incoming Call</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input type="hidden" value="{{$number}}" class="form-control textfeilds" id="contact_number" name="contact_number">
                                        <input type="" value="{{$number}}" disabled class="form-control textfeilds" id="incomingcall" name="incomingcall">
                                        <input type="hidden" value="{{$sipid}}" disabled class="form-control textfeilds" id="sipid" name="sipid">
                                        <input type="hidden" value="{{$contact['id']}}" disabled class="form-control textfeilds" id="cusid" name="cusid">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style=" display: none;">Registration No.</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style=" display: none;">
                                        <<input type="text" style="width: 250%" required id="registration_no" name="registration_no" readonly class="form-control textfeilds" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-select-list" style=" display: none;">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style="font-weight: 100;">Contact type</label>
                                    </div>
                                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                        <select class="form-control select2 custom-select-value" id="type_id" name="type_id" required style="width: 97%">
                                                            @foreach($contacttypes as $contacttype){
                                                                <option 
                                                                    value="1">
                                                                    {{$contacttype->type_name}}
                                                                </option>
                                                            @endforeach
                                        </select>
                                    </div> 
                                    <input type="hidden" id="syncNo" name="syncNo" value="" /> 
                                </div>                    
                            </div>   
                            <div class="form-select-list">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important; font-weight: 100; font-weight: 100;">Title</label>
                                    </div>
                                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                        <select id="title" name="title" class="form-control select2 custom-select-value" style="width: 55%" value="" >
											<option value="NA"  <?php if($contact['title']=="NA"){ ?>selected="selected" <?php } ?>></option>
                                            <option value="Mr"  <?php if($contact['title']=="Mr"){ ?>selected="selected" <?php } ?>>Mr.</option>
                                            <option value="Mrs"  <?php if($contact['title']=="Mrs"){ ?>selected="selected" <?php } ?>>Mrs.</option>
                                            <option value="Miss " <?php if($contact['title']=="Miss"){ ?>selected="selected" <?php } ?>>Miss.</option>
                                            <option value="Rev"  <?php if($contact['title']=="Rev"){ ?>selected="selected" <?php } ?>>Rev.</option>
                                            <option value="Dr"  <?php if($contact['title']=="Dr"){ ?>selected="selected" <?php } ?>>Dr.</option>
                                        </select>
                                    </div>  
                                </div>                    
                            </div>                 
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important; font-weight: 100; font-weight: 100;">First Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <input tabindex="2" type="text" class="form-control textfeilds" name="firstname" id="firstname" required value="<?php echo $contact['firstname'];?>">
                                    </div>
                                 </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important; font-weight: 100; font-weight: 100;">Last Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="3" type="text" class="form-control textfeilds" id="lastname" name="lastname" required value="<?php echo $contact['lastname'];?>" >
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required" style="margin-top: 0px !important; font-weight: 100; font-weight: 100;">Primary Number</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="5" readOnly type="text" class="form-control textfeilds" name="primary_contact" id="primary_contact" required value="{{$contact['primary_contact']}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" >Secondary Contact 1</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                        <input tabindex="6" type="text" class="form-control textfeilds" name="secondary_contact" id="secondary_contact"value="<?php echo $contact['secondary_contact'];?>">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Secondary Contact 2</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="6" type="text" class="form-control textfeilds" name="secondary_contact2" id="secondary_contact2"value="<?php echo $contact['secondary_contact2'];?>">
                                     
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Email</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="7" type="text" class="form-control textfeilds" id="email" name="email"  value="<?php echo $contact['email'];?>" >
                                    </div>
                               </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="margin-top: 35px; ">
                            <div class="form-group-inner">
                                <div class="row">
                                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Address Line 1</label>
                                   </div>
                                   <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
                                   <input tabindex="12" type="text" class="form-control textfeilds" id="address_line1" name="address_line1" value="<?php echo $contact['address_line1'];?>">
                                   </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Address Line 2</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="13" type="text" class="form-control textfeilds" id="address_line2" name="address_line2" value="<?php echo $contact['address_line2'];?>">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">City</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="14" type="text" class="form-control textfeilds" id="city_state_province" name="city_state_province" value="<?php echo $contact['city_state_province'];?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Zip Code</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <input tabindex="15" type="text" class="form-control textfeilds" id="zip" name="zip" value="<?php echo $contact['zip'];?>">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Medium</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="margin-top: 5px;">
                                    <input tabindex="15" type="radio" id="langS" <?php if($contact['mediumS']=="1"){ ?>checked="checked" <?php } ?> name="lang" value="sinhala"> Sinhala
														<input tabindex="15" type="radio" id="langT" <?php if($contact['mediumT']=="1"){ ?>checked="checked" <?php } ?> name="lang" value="tamil"> Tamil
														<input tabindex="15" type="radio" id="langE" <?php if($contact['mediumE']=="1"){ ?>checked="checked" <?php } ?> name="lang" value="english"> English
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Remark</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <textarea class="form-control textfeilds" id="cus_remark" name="cus_remark" style=""><?php echo $contact['remark'];?></textarea>
                                    </div>
                               </div>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                        </div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top:30px; margin-left:0px;" >
                                <div class="button-style-four btn-mg-b-10">
                                    <button type="button" class="btn btn-custon-four btn-success attr_btn" style="width:100px !important; color: #fff;background-color: #625cb8; border-color: #564cae;" name="submit" onclick="addCallLog()" >Add Call Log</button>
                                    <button type="button" class="btn btn-custon-four btn-success attr_btn" onclick="addTicket()" style="width:100px !important">Add Ticket</button>
                                    <button class="btn btn-custon-four btn-success attr_btn" type="button" onclick="updatecontact();" style="width:118px; color: #fff;background-color: #f1983d; border-color: #ebb233;"> Update Contact
                                                </button>									
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            </div>
                        </div>
                    <div id="myDIVT" style="display:none;">
					    <form >
                        <input type="hidden" name="contact_id" @if($contact) value="@if(!empty($contact['CRMNo'])){{ trim(json_encode($contact['CRMNo'],true),'"') }}@endif"
                                                               @else value="{{$number}}" @endif>
                        <input type="hidden" name="user_id" value="{{session()->get('userid')}}">
                        <input type="hidden" name="selected_title" value="" />
                        <input type="hidden" value="{{$contact['id']}}" class="form-control textfeilds" id="cusid" name="cusid">
                        <input type="hidden" value="{{$sipid}}" class="form-control textfeilds"  id="sipid" name="sipid">
                        <input type="hidden" value="{{$number}}"  class="form-control textfeilds" id="incomingcall" name="incomingcall">
                        <input type="hidden" value="{{$linkedid}}" class="form-control textfeilds" id="linkedid" name="linkedid">
                    {{csrf_field()}}
                            
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                                        <label for="tickettype" class="login2 pull-right pull-right-pro" style="margin-top: 3px !important;">Ticket Type</label>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-9 col-xs-12" style="padding-top: 5px;">
                                        <input id="" value="Complaint" name="htickettype" style=" width: 30px;" type="radio">Complaint
                                        <input id="" value="Inquiry" name="htickettype" style="width: 30px;" type="radio"> Inquiry
                                        <input id="" value="Request" name="htickettype" style="width: 30px;" type="radio"> Request
                                        <input id="" onclick="" value="Other" name="htickettype" style="width: 30px;" type="radio">Other
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                               </div>
                            </div>                
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important; font-weight: 100;">Service Unit</label>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-9 col-xs-12">
                                    <input class="awesomplete form-control textfields input-required" data-autofirst="true" list="preset1data" id="level1" name="level1"
                                                           data-minchars="1" onchange="getValue('level1','preset1data','hlevel1');getleveltwodata(this.value);"style="width: 100%;" required="required"/>
                                                    <datalist id="preset1data">
                                                    @foreach($level1s as $level1)
                                                            <option data-value='{{$level1->id}}' value='{{$level1->name}}'/></option>
                                                        @endforeach
                                                    </datalist>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <input type="hidden" id="hlevel1" name="hlevel1" value=""/>
                                 </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important; font-weight: 100;">Location</label>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-9 col-xs-12">
                                        <input class="form-control textfields" data-autofirst="true" list="preset1data2" id="level2" style="width: 100%; height: 5%;" name="level2" data-minchars="1"
                                                           onchange="getValue('level2','preset1data2','hlevel2');getlevelthrdata(this.value);" required="required"/>
                                                    <datalist id="preset1data2">
                                                    </datalist>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <input type="hidden" id="hlevel2" name="hlevel2" value=""/>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important; font-weight: 100;">Category</label>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-9 col-xs-12">
                                        <input class="form-control textfields" data-autofirst="true" list="preset1data3" id="level3"  style="width: 100%; height: 5%;"name="level3" data-minchars="1"
                                                           onchange="getValue('level3','preset1data3','hlevel3');getkpilvloflvlthr(this.value);" required="required"/>
                                        <datalist id="preset1data3">
                                        </datalist>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <input type="hidden" id="hlevel3" name="hlevel3" value=""/>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important; font-weight: 100;">KPI Level / Subject</label>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-9 col-xs-12">
                                        <input class="form-control textfields" data-autofirst="true" list="preset1data1" style="width: 100%; height: 5%;" id="kpi_level" name="kpi_level" data-minchars="1"
                                                           onchange="getValue('kpi_level','preset1data1','hkpi_level');" required="required"/>
                                        <datalist id="preset1data1">
                                        </datalist>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <input type="hidden" id="hkpi_level" name="hkpi_level" value=""/>
                               </div>
                            </div>
                            <div class="form-select-list">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important; font-weight: 100;">Ticket Capture Mode</label>
                                    </div>
                                <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12" >
                                    <select tabindex="1" class="form-control select2 custom-select-value"  name="hticketcapturemode" id="hticketcapturemode" style="width: 100%; padding-top: 3px; padding-bottom: 3px;" required>
                                        <option value="Call Centre">Call Centre</option>
                                        <option value="E Mail">E Mail</option>
                                        <option value="Web">Web</option>
                                        <option value="Social Media">Social Media(FB / Whatsapp / Viber)</option>
                                        <option value="Other">Other</option>
                                    </select> 
                                </div>
                                <div class="col-lg-4">
                                </div> 
                            </div>     
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important;">Reference No’s</label>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control textfields" id="order_ref" name="order_ref" value="" style=" width: 100%; height: 5%;"/>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Reference Person</label>
                                    </div>
                                    <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control textfields" id="order_ref_person" name="order_ref_person" value="" style=" width: 100%; height: 5%;"/>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" style="margin-top: 3px !important;">Ticket Data / Comments</label>
                                    </div>
                                    <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
                                        <textarea class="form-control textfields input-required" rows="4" id="remarkTicket" name="remarkTicket" required></textarea>
                             
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                               </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin-top:30px; margin-left:35px;" >
                                <div class="button-style-four btn-mg-b-10">
                                    <button type="button" class="btn btn-custon-four btn-success attr_btn"  onclick="submitTicket();" >Save</button>
                                    <button type="button" class="btn btn-custon-four btn-danger attr_btn" onclick="window.location.reload()">Cancel</button>   
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                            </div>
                        </div>   
                        </div>
                        </form>
                    </div>      
                </div>
                <!-- Add Call Log-->
                <div id="myDIV" style="display:none;">
					<form class="form-horizontal" method="POST" id="form_new" name="form_new" action=" ../../../../savedata" autocomplete="off">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$contact['id']}}" class="form-control textfeilds" id="cusid" name="cusid">
                        <input type="hidden" value="{{$sipid}}" class="form-control textfeilds"  id="sipid" name="sipid">
                        <input type="hidden" value="{{$number}}"  class="form-control textfeilds" id="incomingcall" name="incomingcall">
                        <input type="hidden" name="agent" id="agent" value="<?php echo session()->get('userid'); ?>" />
                        <input type="hidden" name="linkid" id="linkid" value="{{$linkedid}}" />
                        <div class="col-lg-8 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group-inner">
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Preset Remark 1</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <select class="form-control custom-select-value" style="width:120%" id="category" name="category" onchange="addPreset('category')">
                                            <option value="All">Select Preset Remark 1</option>
                                            @foreach($type_one as $category)
                                            <option value="{{$category->id}}">{{$category->remark}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Preset Remark 2</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <select class="form-control custom-select-value" style="width:120%" id="inquiry" name="inquiry" onchange="addPreset('inquiry')">
                                            <option value="All">Select Preset Remark 2</option>
                                            @foreach($type_two as $inquiry)
                                            <option value="{{$inquiry->id}}">{{$inquiry->remark}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required">Remark</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                        <textarea class="form-control" style="padding-bottom: 10px ; width:120%" rows="4" name="remark" id="remark" required></textarea>
                                    </div>
                               </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                        </div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin-top:30px; margin-left:225px;" >
                                <div class="button-style-four btn-mg-b-10">
                                    <button type="button" class="btn btn-custon-four btn-success attr_btn"  onclick="saveRecord();" >Save</button>
                                    <button type="button" class="btn btn-custon-four btn-danger attr_btn" onclick="window.history.back()">Cancel</button>   
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-2 col-sm-12 col-xs-12">
                            </div>
                        </div>                                         
                        </form>
                    </div>      
                </div>                                               
            <div class="sparkline13-list">
                <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <div class="row">
                        <div class="col-md-12" >
                        <!-- Custom Tabs -->
                            <div class="nav-tabs-custom"style="background-color:#c2d4f2;" >
                                <ul class="nav nav-tabs">
                                     <li class="active" id="tab_div1" style="padding-right: 0px;">
                                        <a id="tab1_li" href="#tab_1" onclick="colorizetab1()" style="background-color:#d7eceb" data-toggle="tab">Call History</a>
                                    </li>
                                    <li>
                                        <a id="tab2_li" href="#tab_2" onclick="colorizetab2()" data-toggle="tab">Fresh Tickets</a>
                                    </li>
                                    <li>
                                        <a id="tab4_li" href="#tab_4" onclick="colorizetab4()" data-toggle="tab">Work In Progress</a>
                                    </li>
                                    <li>
                                        <a id="tab3_li" href="#tab_3" onclick="colorizetab3()" data-toggle="tab">Closed Tickets</a>
                                    </li>            
                                </ul>
<br><br>
                                <div class="col-md-12 tab-content" style="background-color:#c2d4f2;">
                                    <div id="tab_1" class="tab-pane active">
                                        <table id="historytable" class="table table-bordered table-striped tablerowsize">
                                            <thead class="table_head">
                                                <tr>
                                                    <th>Date / Time</th>
                                                    <th>Number</th>
                                                    <th>Log Type</th>
                                                    <th>Comment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               @if($callhistory)
                                                @foreach($callhistory as $row)
                                                    <tr>
                                                        <td style="width:10%;">{{$row->call_datetime}}</td>
                                                        <td style="width:1%;">{{$row->pho_number}}</td>
                                                        <td style="width:5%;">{{$row->log_type}}</td>
                                                        <td style="width:39%;">{{$row->call_log}}</td>
                                                                           
                                                    </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                    
                                    <!-- /.tab-pane -->
                                    <div id="tab_2" class="tab-pane">
                                        <table id="opentable" class="table table-bordered table-striped tablerowsize">
                                            <thead class="table_head">
                                                <tr>
                                                    <th>Date / Time</th>
                                                    <th>Ticket #</th>
                                                    <!--<th>Add Comment</th>-->
                                                    <th>Ticket Type </th>
                                                    <th>Service Unit</th>
                                                    <th>Location</th>
                                                    <th>Category</th>
								                    <th>Ticket Capture Mode </th>
                                                    <th>Order Reference</th>
                                                    <th>Reference Person</th>
                                                    <th>Remark</th>               
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if($freshtickets)
                                            @foreach($freshtickets as $openticket)
                                                <tr>
                                                    <td style="width:12%;">{{$openticket->date_time}}</td>
                                                    <td style="width:8%;"><a href="{{url ('ticket').'/'.$openticket->id}}">{{$openticket->ticket_num}}</a></td>
                                                    <!--<td style="width:8%;"><a onclick="showaddcomment({{$openticket->id}});">Comment</a></td>-->
									                <td style="width:8%;">{{$openticket->tickettype}}</td>
                                                    <td style="width:15%;">{{$openticket->lvlone_name}}</td>
                                                    <td style="width:1%;" align="">{{$openticket->lvltwo_name}}</td>
                                                    <td style="width:14%;">{{$openticket->lvlthr_name}}</td>
									                <td style="width:14%;">{{$openticket->ticketcapturemode}}</td>
									                <td style="width:14%;">{{$openticket->orderreference}}</td>
									                <td style="width:14%;">{{$openticket->referenceperson}}</td>
                                                    <td style="width:46%;">{{$openticket->remark}}</td>
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>

                                    <!-- /.tab-pane -->
                                    <div id="tab_4" class="tab-pane">
                                        <table id="workingtable" class="table table-bordered table-striped tablerowsize">
                                            <thead class="table_head">
                                                <tr>
                                                    <th>Date / Time</th>
                                                    <th>Ticket #</th>
                                                    <!--<th>Add Comment</th>-->
                                                    <th>Ticket Type </th>
                                                    <th>Service Unit</th>
                                                    <th>Location</th>
                                                    <th>Category</th>
								                    <th>Ticket Capture Mode </th>
                                                    <th>Order Reference</th>
                                                    <th>Reference Person</th>
                                                    <th>Remark</th>               
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if($workingProgress)
                                            @foreach($workingProgress as $working)
                                                <tr>
                                                    <td style="width:12%;">{{$working->date_time}}</td>
                                                    <td style="width:8%;"><a href="{{url ('ticket').'/'.$working->id}}">{{$working->ticket_num}}</a></td>
                                                    <!--<td style="width:8%;"><a onclick="showaddcomment({{$working->id}});">Comment</a></td>-->
                                                    <td style="width:8%;">{{$working->tickettype}}</td>
                                                    <td style="width:15%;">{{$working->lvlone_name}}</td>
                                                    <td style="width:1%;" align="">{{$working->lvltwo_name}}</td>
                                                    <td style="width:14%;">{{$working->lvlthr_name}}</td>
                                                    <td style="width:14%;">{{$working->ticketcapturemode}}</td>
                                                    <td style="width:14%;">{{$working->orderreference}}</td>
                                                    <td style="width:14%;">{{$working->referenceperson}}</td>
                                                    <td style="width:46%;">{{$working->remark}}</td>
                                                 </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>

                                     <!-- /.tab-pane -->
                                    <div id="tab_3" class="tab-pane">
                                        <table id="closedtable" class="table table-bordered table-striped tablerowsize">
                                            <thead class="table_head">
                                                <tr>
                                                    <th>Date / Time</th>
                                                    <th>Ticket #</th>
                                                    <!--<th>Add Comment</th>-->
                                                    <th>Ticket Type </th>
                                                    <th>Service Unit</th>
                                                    <th>Location</th>
                                                    <th>Category</th>
								                    <th>Ticket Capture Mode </th>
                                                    <th>Order Reference</th>
                                                    <th>Reference Person</th>
                                                    <th>Remark</th>               
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if($closedtickets)
                                            @foreach($closedtickets as $closedticket)
                                                <tr>
                                                    <td style="width:12%;">{{$closedticket->date_time}}</td>
                                                    <td style="width:8%;" align="center">
                                                    {{--{{$closedticket->id}}--}}
                                                    <a href="{{url ('ticket').'/'.$closedticket->id}}">{{$closedticket->ticket_num}}</a></td>
                                                    <td style="width:8%;">{{$closedticket->tickettype}}</td>
                                                    <td style="width:15%;">{{$closedticket->lvlone_name}}</td>
                                                    <td style="width:1%;" align="">{{$closedticket->lvltwo_name}}</td>
                                                    <td style="width:14%;">{{$closedticket->lvlthr_name}}</td>
                                                    <td style="width:14%;">{{$closedticket->ticketcapturemode}}</td>
                                                    <td style="width:14%;">{{$closedticket->orderreference}}</td>
                                                    <td style="width:14%;">{{$closedticket->referenceperson}}</td>
                                                    <td style="width:46%;">{{$closedticket->remark}}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                            </div>
                            <!-- nav-tabs-custom -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div class="modal" id="myModal">
    <div class="modal-dialog">
    <form class="form-horizontal" action="{{ url('/addcustomer_inq') }}" method="post" id="add_clips" name="add_clips"  enctype="multipart/form-data" >
    {{csrf_field()}}
        <div class="modal-content">
            <div class="modal-header">
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--<span aria-hidden="true">&times;</span></button>--}}
                <h4 class="modal-title">Add Comment</h4>
            </div>

            <div class="modal-body">
                <div class="form-group marginelement">
                        <div class="col-md-12 paddingmin"style="padding-left: 13px;" id="tkt_details_header">
                           
                        </div>
                    </div>
                <br>
                <div class="form-group marginelement">
                    <div class="col-md-12 paddingmin" style="padding-left: 35px;">
                        <textarea class="form-control" rows="4" name="inq_comment" id="inq_comment" placeholder="Comment..."></textarea>
                        <input type="hidden" value="{{$number}}"
                                                               class="form-control textfeilds"
                                                               id="contact_number" name="contact_number">
                    </div>
                </div>
            </div>

            <br>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updatestatus()">Save</button>
                <button type="button" class="btn btfn-default pull-right" data-dismiss="modal" onclick=closemodal();>Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
        </form>
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

</div>

<div class="modal fade" id="exampleModal_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ticket Details</h5>
        <button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="ticket_model_new">
      
      </div>
      <div class="modal-footer" id="ticket_model_footer_new">

      </div>
    </div>
  </div>
</div>
<br><br>

</body>
</html>

