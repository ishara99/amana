<!DOCTYPE html>
<html>

@include('header_new')
<script>
            $(document).ready(function () {
                $('input[type="search"]').css(
                    {'width':'350px','display':'inline-block'}
                );
            });
        </script>

<!-- Content Wrapper. Contains page content -->
<!-- Start body -->

<div class="content-wrapper">
<div class="col-lg-12 ">
	<h1 class="form_caption">Call History Detail</h1>
</div>
<div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
                    <form class="form-horizontal" action="{{url('searchContact')}}"  method="post" id="form">
                    <div class="form-group-inner">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="form-group-inner" style="width:100%;">
                                            <select class="form-control   textfeilds" id="agent_id" name="agent_id" required>
                                                        <option value="All">Agent</option>
                                                        <?php 
                                                            foreach($users as $value){ ?>
                                                                <option value="<?php echo $value->id ?>">
                                                                <?php echo $value->username ?></option>

                                                        <?php } ?>

                                                    </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="user" name="user" value="{{ Session::get('username')}}" >
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-left: 10px;">
                                        <div class="form-group-inner" style="width:100%; ">
                                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                                <label class="login2 pull-right pull-right-pro">From</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                                <input type="text" class="some_class" value="<?php echo date("Y-m-d 00:00:00"); ?>" name="startdate" id="startdate" style="width: 214px;margin-left: 10px;margin-left: 9px!;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-left: 10px;">
                                        <div class="form-group-inner" style="width:100%; ">
                                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                                <label class="login2 pull-right pull-right-pro">To</label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                                <input type="text" class="some_class" value="<?php echo date("Y-m-d 23:00:00"); ?>" name="enddate" id="enddate" style="width: 214px;margin-left: 10px;margin-left: 9px!;">
                                            </div>
                                        </div>
                                    </div>
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-left: 10px;">
                                        <div class="form-group-inner" style="width:100%; ">
                                            <div class="col-lg-5 col-md-3 col-sm-3 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                                <label class="login2 pull-right pull-right-pro">Contact number</label>
                                            </div>
                                            <div class="col-lg-5 col-md-9 col-sm-8 col-xs-12" style="padding-left:10px">
                                                <input class="form-control textfeilds" id="contactnumber" name="contactnumber" style="width: 100%" required="" onkeyup="validateNumber(this.value);" type="text" autocomplete="off">
                                            </div>
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
                                            <div class="button-style-four btn-mg-b-10">
                                                <button type="button" class="btn btn-custon-four btn-success attr_btn" style="width:78px; " onclick="searchdata()">Search &nbsp</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="form-group-inner" style="width:50%;">
                                           <select class="form-control   textfeilds" id="pre_rem1" name="pre_rem1" required>
                                                        <option value="All">Select preset Remark 1</option>
                                                        <?php 
                                                            foreach($presets as $value){ ?>
                                                            @if($value->type==1)
                                                                <option value="<?php echo $value->id ?>">
                                                                <?php echo $value->remark ?></option>
                                                            @endif
                                                        <?php } ?>

                                                    </select>
                                    </div>
                                </div> 
								<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;margin-left: -129px;">
                                        <div class="form-group-inner" style="width:67%;">
                                           <select class="form-control   textfeilds" id="pre_rem2" name="pre_rem2" required>
                                                        <option value="All">Select preset Remark 2</option>
                                                        <?php 
                                                            foreach($presets as $value){ ?>
                                                             @if($value->type==2)
                                                                <option value="<?php echo $value->id ?>">
                                                                <?php echo $value->remark ?></option>
                                                                @endif

                                                        <?php } ?>

                                                    </select>
                                    </div>
                                </div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="form-group-inner" style="width:50%;margin-left: 10px;">
                                           <select class="form-control   textfeilds" id="call_type" name="call_type" required>
                                                            <option value="All">Call Type</option>
                                                            <option value="AbandonCallback">AbandonCallback</option>
                                                            <option value="CallbackRequest">CallbackRequest</option>
                                                            <option value="Inbound">Inbound Call</option>
                                                            <option value="Outbound">Outbound Call</option>
                                                        </select>
                                    </div>
                                </div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                        <br>
                                </div>
                            </div>
                        </div>   
                    </form>
			    </div>
		    </div>	
    <div class="sparkline13-list" >
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
                <table id="call_table" class="table table-bordered table-striped tablerowsize" cellspacing="0" width="100%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Date time</p></th>
                            <th><p class="text-center">Created Date time</p></th>
                                        <th><p class="text-center">Phone Number</p></th>
                                        <th><p class="text-center">Contact Name</p></th>
                                        <th><p class="text-center">Contact Address</p></th>
                                        <th><p class="text-center">Agent</p></th>
                                        <th><p class="text-center">Call Type</p></th>
                                        <th><p class="text-center">Pre.Remark 1</p></th>
                                        <th><p class="text-center">Pre.Remark 2</p></th>
                                        <th><p class="text-center">Comment</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">

</div>


<!-- ./col -->
</div>
<!-- /.row -->
</div>
<br><br>
<script>
    //$.datetimepicker.setLocale('en');
    $('.some_class').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
    });

    $('#datetimepicker_dark').datetimepicker({theme: 'dark'})

</script>
<script>
    //var table = $('#tickettable').DataTable();
   /* $('#contactnumber').on( 'keyup', function () {
    $('#tickettable').DataTable().search( this.value ).draw();*/
//} );

    function validateNumber(obj){
	var regex = /^[0-9]+$/;
	if(!obj.match(regex))
      {
       document.getElementById('contactnumber').value='';
      }else{
	  //alert(obj);
	  //$('.dataTables_filter input').val(obj);
      }
    }

    function settodate(obj){
        //alert(obj);
        var fromdate = new Date(obj);
        //console.log(date);
        fromdate.setDate(fromdate.getDate() + 7);
        var CurrentDateTime = fromdate.getFullYear().toString()+"-"+((fromdate.getMonth()+1).toString().length==2?(fromdate.getMonth()+1).toString():"0"+(fromdate.getMonth()+1).toString())+"-"+(fromdate.getDate().toString().length==2?fromdate.getDate().toString():"0"+fromdate.getDate().toString())+" "+(fromdate.getHours().toString().length==2?fromdate.getHours().toString():"0"+fromdate.getHours().toString())+":"+((parseInt(fromdate.getMinutes()/5)*5).toString().length==2?(parseInt(fromdate.getMinutes()/5)*5).toString():"0"+(parseInt(fromdate.getMinutes()/5)*5).toString())+":00";

        $('#enddate').val(CurrentDateTime);


    }
    
    function cancelform(){
        history.back();
    }
    function searchdata() {
        var contactnumber = document.getElementById("contactnumber").value;
        var agent_id = document.getElementById("agent_id").value;
        var startdate = document.getElementById("startdate").value;
        var enddate = document.getElementById("enddate").value;
        var pre_rem1 = document.getElementById("pre_rem1").value;
        var pre_rem1_ = $('#pre_rem1 option:selected').text();
        pre_rem1_1 = pre_rem1_.trim(); 
        pre_rem1_1 = pre_rem1_1.replace("&", " and ");
        var pre_rem2 = document.getElementById("pre_rem2").value;
        var pre_rem2_ = $('#pre_rem2 option:selected').text();
        pre_rem2_2 = pre_rem2_.trim(); 
        pre_rem2_2 = pre_rem2_2.replace("&", " and ");  
        var call_type = document.getElementById("call_type").value;
        var selected_agent = $('#agent_id option:selected').text();
        selected_agent = selected_agent.trim();
        var user = document.getElementById("user").value;
        var report_name = "Call History";
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds();
        

        $("#call_table").dataTable().fnDestroy();

        $('#call_table').DataTable({
            "processing": true,
            "ajax": {
                "url": "search_callhistory_detail_report",
                "type": "GET",
                data: {
                        contactnumber: contactnumber,
                        agent_id: agent_id,
                        startdate_val: startdate,
                        enddate_val: enddate,
                        pre_rem1: pre_rem1,
                        pre_rem2: pre_rem2,
                        call_type: call_type,
                        
                },
            },
            "columns": [
                {"data": "call_datetime"},
                {"data": "created_datetime"},
                {"data": "pho_number"},
                {"data": "contact_name"},
                {"data": "cantactaddress"},
                {"data": "username"},
                {"data": "log_type"},
                {"data": "preset1"},
                {"data": "preset2"},
                {"data": "call_log"}
            ],
            "columnDefs": [
                { className: "text-center", "targets": [ 1,2 ]}
            ],
            "order": [[ 1, "desc" ]],
            "pageLength": 25,
           
            dom: 'Bfrtip',

        buttons: [

             {

                extend     : 'pdfHtml5',

                orientation: 'landscape',

                pageSize   : 'A4'

            },

            'excelHtml5' 

             

        ]
     });
    }
    function init() {
        var element = getsubdeps(document.getElementById('deps').value);
    }

</script>
<script>
//$.datetimepicker.setLocale('en');
$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

$('#datetimepicker_dark').datetimepicker({
    theme: 'dark'
})
</script>
<script>
    $('#tickettable').DataTable({
        "processing": true,
        "pageLength": 25,
        "scrollX": true,
    });
</script>
@include('footer')

</body>
</html>
      
