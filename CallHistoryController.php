<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use DateTime;
use App\Http\Util;

class callHistoryController extends Controller
{
    public function index(Request $request){
if (Util::isAuthorized("popupcallhistory") == 'LOGGEDOUT') {
	    return redirect('/');
	}
	if (Util::isAuthorized("popupcallhistory") == 'DENIED') {
	    return view('permissiondenide');
	}
	Util::log('Callhistory', 'View');

	$callhistory = ($this->retrieveCallhistory($request->input('id')));
	$contact = (new ContactController())->retrieveContactById($request->input('contactid'));

	return view('ViewCallHistory')
			->with('contact', $contact)
			->with('callhistory', $callhistory);		
	}

public function popupcallhistorydetail(Request $request){
if (Util::isAuthorized("popupcallhistory") == 'LOGGEDOUT') {
	    return redirect('/');
	}
	if (Util::isAuthorized("popupcallhistory") == 'DENIED') {
	    return view('permissiondenide');
	}
	Util::log('Callhistory', 'View');

	$callhistory = ($this->retrieveCallhistoryDetail($request->input('id')));
	$contact = (new ContactController())->retrieveContactById($request->input('contactid'));

	return view('ViewCallHistoryDetail')
			->with('contact', $contact)
			->with('callhistory', $callhistory);		
	}

	public function batchInfo(){
	 $client=$_GET['client'];
        if($client!="All")
        {
        
        $data= DB::select('SELECT DISTINCT
customer_master.batch_id AS id
FROM
customer_master
WHERE
customer_master.campign_id ='.$client);
        return compact('data');
        }else
        {
            
            return ;  

        }
	}
	public function getAllContact(){
		$campign=$_GET['campign'];
		$batch=$_GET['batch'];
        if($batch!="All")
        {
        
        $data= DB::select('SELECT DISTINCT
autodial_batch.cus_number
FROM
autodial_batch
WHERE
autodial_batch.campaign_id ='.$campign.' AND
autodial_batch.batch_id ='. $batch
);
        return compact('data');
        }else
        {
            
            return ;  

        }
	}
	public function getSearchData(){
		$campign=$_GET['campign'];
        $batch=$_GET['batch'];
        $contact=$_GET['contact'];
		$data = DB::select("SELECT
autodial_remark.remark_id,
autodial_remark.cus_number,
autodial_remark.remark_date,
autodial_remark.cus_remark,
autodial_remark.agent_cs
FROM
autodial_remark
WHERE
autodial_remark.cus_number =".$contact." AND
autodial_remark.campaign_id = ".$campign."
AND autodial_remark.batch_id=".$batch."
ORDER BY
autodial_remark.remark_date DESC
");
         return compact('data',$data);
	}
	public function viewProfile($id){
		$selectCusInfo = DB::select("SELECT customer_master.id,customer_master.policy_no,customer_master.bus_type,
customer_master.title_cf,customer_master.title,customer_master.`name`,customer_master.contact_no,customer_master.name_cf,customer_master.nic_no,customer_master.add_01,
customer_master.nic_no_cf,customer_master.add_02,customer_master.add_01_cf,customer_master.add_03,customer_master.add_02_cf,customer_master.add_04_cf,
customer_master.add_04,customer_master.add_03_cf,customer_master.tranc_type_cf,customer_master.tranc_type,customer_master.bus_cls_cd,
customer_master.bus_cls_cd_cf,customer_master.brnch_cd,customer_master.brnch_cd_cf,customer_master.agnt_brk_cd,customer_master.agnt_brk_cd_cf,
customer_master.vhcl_no,customer_master.vhcl_no_cf,customer_master.com_date,customer_master.com_date_cf,customer_master.exp_date,customer_master.exp_date_cf,
customer_master.sum_assu_cf,customer_master.sum_assu,customer_master.gross_prem_cf,customer_master.gross_prem,customer_master.chnl_cd_cf,
customer_master.chnl_cd,customer_master.batch_id,customer_master.strd_rmk_id,customer_master.cust_rmk,customer_master.campign_id,customer_master.email,
customer_master.dob,autodial_csv_master.upload_date ,
tbl_stremarks.st_remarks
FROM
customer_master
INNER JOIN autodial_csv_master ON customer_master.batch_id = autodial_csv_master.batch_id
LEFT JOIN tbl_stremarks ON customer_master.strd_rmk_id = tbl_stremarks.id
WHERE customer_master.id =$id");
		$doc_list=DB::SELECT("SELECT * FROM cli_doc_list WHERE cli_id=".$id);
		 return view('clientProfileView',compact('selectCusInfo','doc_list'));
         
	}
	public function retrieveCallhistory($contactid){
        return DB::table('csp_callhistory')
            ->select('csp_callhistory.*', 'user_master.username')
            ->join('user_master', 'user_master.id', '=', 'csp_callhistory.created_userid')
            ->where('cus_id', $contactid)
            ->orderBy('created_datetime', 'desc')
            ->get();
    }

public function retrieveCallhistoryDetail($contactid){
        return DB::table('csp_callhistory_detail')
            ->select('csp_callhistory_detail.*', 'user_master.username')
            ->join('user_master', 'user_master.id', '=', 'csp_callhistory_detail.created_userid')
            ->where('cus_id', $contactid)
            ->orderBy('created_datetime', 'desc')
            ->get();
    }
 public function view_findcallhistory(){
        if(Util::isAuthorized("view_findcallhistory")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_findcallhistory")=='DENIED'){
            return view('permissiondenide');
        }
		Util::log('Callhistory','View');
		
        

       $userid=session('userid');
        $usertypeid=session()->get('usertypeid');

        // $get_user =DB::SELECT("SELECT username 
        //                        FROM `user_master`
        //                        where `com_id` = '3' ");

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();
                            if($usertypeid=='17'){
            $users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor') and a.`com_id`= $get_com_id->com_id group by a.`username`  ;");
		}else if ($usertypeid=='19'){
			$users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
			where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor' OR c.`title`= 'Super_User' OR c.`title`= 'Team_Leader') and a.`com_id`= $get_com_id->com_id group by a.`username`  ;");
		
		}else if ($usertypeid=='20'){
			$users = DB::select("SELECT a.`agnt_userid`, b.`id` , b.`username` 
								FROM `tbl_tmldr_agnt_allo` as a 
								right join `user_master` as b ON a.`agnt_userid`=b.`id`
								inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
								where a.`tmldr_userid` = $userid ;");		
		}else{
            $users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where c.`title`= 'Csp_Agent' and a.`com_id`= $get_com_id->com_id and a.id=".$userid." group by a.`username`  ;");
        }
      
        $presets = (new PresetRemarkController())->retrieveAll();
    
		 $ipaddress = (new UsersController())->get_client_ip();
         $username=session()->get('username');
         Util::user_auth_log($ipaddress,"User open the Call History dashboard-$username",$username,"view Call History");    
        return view('view_findcallhistory',compact('users','presets'));
    }

    function SaveCallLog_original(){
		$pho_number = $_GET['pho_number'];
        $cus_name = $_GET['cus_name'];
		$order_no = $_GET['order_no'];
		$category = $_GET['category'];
		$inquiry = $_GET['inquiry'];
		$remark = $_GET['remark'];
		$userid=session('userid');
		$location = $_GET['location'];
			
			$getCallStatusInfo = DB::select("SELECT
	tbl_calls_evnt.linkedid,
	tbl_calls_evnt.`status`,
	tbl_calls_evnt.`agnt_sipid`
	FROM
	tbl_calls_evnt
	WHERE
	tbl_calls_evnt.frm_caller_num = '".$pho_number."'
	ORDER BY
	tbl_calls_evnt.cre_datetime DESC
	LIMIT 1" );
	$linkedid=$getCallStatusInfo[0]->linkedid;
	$agent_sipid=$getCallStatusInfo[0]->agnt_sipid;

        // if()
		 
				$data = array(
					'unq_id' => $linkedid,
					'call_datetime' => NOW(),
					'pho_number' => $pho_number,
					'sipid' => $agent_sipid,
					'cus_id'=>"",
					'cus_name'=>$cus_name,
					'order_no'=>$order_no,
					'log_type'=> "Agent Call Log",
					'call_log'=> $remark,
					'category'=> "$category",
					'inquiry'=> "$inquiry",
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'location' => $location,
					
				);
	  
			
			$insertSuccess=DB::table('csp_agent_callhistory')
				->insert($data);

			if($insertSuccess){
				echo 'Call Log Added Successfully!';
			}else{
				echo 'Data Saving Error! ';
			}
			
		}
function SaveCallLog(){
	$pho_number = $_GET['pho_number'];
	$category = $_GET['category'];
	$inquiry = $_GET['inquiry'];
	$remark = $_GET['remark'];
	$cusid = $_GET['cusid'];
	$userid=session('userid');
	$com_id = util::get_com_id_by_user($userid);
	$endpoint=session('endpoint');
	$today=date("Y-m-d");
	$linkedid=$_GET['linkid'];
	// dd($linkedid);
	$agent_sipid="";
		$getCallStatusInfo = DB::select("SELECT
											tbl_calls_evnt.linkedid,
											tbl_calls_evnt.`status`,
											tbl_calls_evnt.`agnt_sipid`
											FROM
											tbl_calls_evnt
											WHERE
											tbl_calls_evnt.frm_caller_num = '".$pho_number."'  AND tbl_calls_evnt.date='".$today."'
											ORDER BY
											tbl_calls_evnt.cre_datetime DESC
											LIMIT 1" );
											$linkedid=$linkedid;
											//$agent_sipid=$getCallStatusInfo[0]->agnt_sipid;

		$getLogInfo = DB::select("SELECT
											csp_callhistory.id,
											csp_callhistory.unq_id,
											csp_callhistory.pho_number,
											csp_callhistory.call_log
												FROM
											csp_callhistory
												WHERE
											csp_callhistory.unq_id ='".$linkedid."' AND
												csp_callhistory.pho_number like '%".$pho_number."%'" );

		

		if(!empty($getLogInfo)){

			$getQueueInfo = DB::select("SELECT agnt_queueid FROM `tbl_calls_evnt` WHERE linkedid = '".$linkedid."'" );
		 // dd($getQueueInfo);
			if(!empty($getLogInfo))
			{
				$queueid = $getQueueInfo[0]->agnt_queueid;
			}else
			{
				$queueid = "";
			}
			
			$date_time=date('Y-m-d H:i:s');
		
			$updateSuccess= DB::update('update csp_callhistory set
			cat_one_prerem_id=?,cat_two_prerem_id=?,call_log=?,queue_id=?,updated_datetime=? where unq_id = ? AND pho_number like ? ORDER BY call_datetime DESC',
		   [$category,$inquiry,$getLogInfo[0]->call_log." - ".$remark,$queueid,$date_time,$linkedid,"%".$pho_number."%"]);
			 // dd($updateSuccess);


			$data = array(
					'unq_id' => $linkedid,
					'call_datetime' => NOW(),
					'pho_number' => $pho_number,
					'sipid' => $endpoint,
					'queue_id'=>$queueid,
					'cus_id'=>$cusid,
					'log_type'=>"Inbound",
					'call_log'=> $remark,
					'cat_one_prerem_id'=>$category,
					'cat_two_prerem_id'=>$inquiry,
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			
			$insertSuccess=DB::table('csp_callhistory_detail')
				->insert($data);
	
			if($updateSuccess){
				return  'Call Log Added Successfully!';
			}else{
				return 'Data Saving Error! ';
			}	
		}
			
		}

	function SaveCallbackLog(){

		$pho_number = $_GET['pho_number'];
		$category = $_GET['category'];
		$inquiry = $_GET['inquiry'];
		$remark = $_GET['remark'];
		// $logtypetext = $_GET['logtypetext'];

		$userid=session('userid');
		$com_id = util::get_com_id_by_user($userid);
		$endpoint=session('endpoint');
		$today=date("Y-m-d");
		$linkedid=$_GET['linkid'];
		$agent_sipid="";

		$getCallStatusInfo = DB::select("SELECT tbl_calls_evnt.linkedid, tbl_calls_evnt.`status`, tbl_calls_evnt.`agnt_sipid` FROM tbl_calls_evnt WHERE tbl_calls_evnt.frm_caller_num = '$pho_number'  AND tbl_calls_evnt.date='$today' ORDER BY tbl_calls_evnt.cre_datetime DESC LIMIT 1" );

		$linkedid=$linkedid;
		//$agent_sipid=$getCallStatusInfo[0]->agnt_sipid;

		$getLogInfo = DB::select("SELECT csp_callhistory.id, csp_callhistory.unq_id, csp_callhistory.pho_number FROM csp_callhistory WHERE csp_callhistory.unq_id ='$linkedid' AND csp_callhistory.pho_number = '$pho_number'" ); 

		$selectStatus = DB::select("SELECT * FROM `csp_callhistory` WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND (`csp_callhistory`.`call_datetime` BETWEEN CONCAT(CURDATE()+ '00:00:00') AND NOW())"); 


		// $data = array(
		// 			'unq_id' => $linkedid,
		// 			'call_datetime' => NOW(),
		// 			'pho_number' => $pho_number,
		// 			'sipid' => $endpoint,
		// 			'queue_id'=>"",
		// 			'cus_id'=>"",
		// 			'log_type'=>"",
		// 			'call_log'=> $remark,
		// 			'cat_one_prerem_id'=>$category,
		// 			'cat_two_prerem_id'=>$inquiry,
		// 			'created_userid' => $userid,
		// 			'created_datetime' => NOW(),
		// 			'com_id' => $com_id,
					
		// 		);
	  
			
		// 	$insertSuccess=DB::table('csp_callhistory_detail')
		// 		->insert($data);

		if($selectStatus){

			if(!$selectStatus[0]->cat_one_prerem_id){

				$returnValue = DB::UPDATE("UPDATE `csp_callhistory`
					SET `csp_callhistory`.`cat_one_prerem_id` = '$category', `csp_callhistory`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory`.`callback_Status` = 'Dialed With Comment', `csp_callhistory`.`call_log` = '$remark' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND (`csp_callhistory`.`call_datetime` BETWEEN CONCAT(CURDATE()+ '00:00:00') AND NOW())");

				$r = DB::UPDATE("UPDATE `csp_callhistory_detail`
					SET `csp_callhistory_detail`.`cat_one_prerem_id` = '$category', `csp_callhistory_detail`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory_detail`.`callback_Status` = 'Dialed With Comment', `csp_callhistory_detail`.`call_log` = '$remark' 
					WHERE `csp_callhistory_detail`.`unq_id` = '$linkedid' and (`csp_callhistory_detail`.`log_type` ='CallbackRequest' OR `csp_callhistory_detail`.`log_type` ='AbandonCallback')");


				if($returnValue != 0){

					echo 'Call Log Added Successfully!';

				}else{

					echo 'Data Saving Error!1 ';
				}

			}else{

				$returnValue = DB::UPDATE("UPDATE `csp_callhistory`
					INNER JOIN `tbl_calls_evnt`
					ON `csp_callhistory`.unq_id =  `tbl_calls_evnt`.`linkedid`
					SET `csp_callhistory`.`cat_one_prerem_id` = '$category', `csp_callhistory`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory`.`callback_Status` = 'Dialed With Comment', `csp_callhistory`.`call_log` = '$remark' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND `csp_callhistory`.`unq_id` = '$linkedid' ");

				$r = DB::UPDATE("UPDATE `csp_callhistory_detail`
					SET `csp_callhistory_detail`.`cat_one_prerem_id` = '$category', `csp_callhistory_detail`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory_detail`.`callback_Status` = 'Dialed With Comment', `csp_callhistory_detail`.`call_log` = '$remark' 
					WHERE `csp_callhistory_detail`.`unq_id` = '$linkedid' and (`csp_callhistory_detail`.`log_type` ='CallbackRequest' OR `csp_callhistory_detail`.`log_type` ='AbandonCallback')");

				$returns = DB::UPDATE("UPDATE `csp_callhistory`
					SET `csp_callhistory`.`callback_Status` = 'Dialed With Comment' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND (`csp_callhistory`.`call_datetime` BETWEEN CONCAT(CURDATE()+ '00:00:00') AND NOW())");


				if($returnValue != 0){

					echo 'Call Log Added Successfully!';

				}else{

					echo 'Data Saving Error!2 ';
				}
			}

		}else{

			$returnValue = DB::UPDATE("UPDATE `csp_callhistory`
					INNER JOIN `tbl_calls_evnt`
					ON `csp_callhistory`.unq_id =  `tbl_calls_evnt`.`linkedid`
					SET `csp_callhistory`.`cat_one_prerem_id` = '$category', `csp_callhistory`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory`.`callback_Status` = 'Dialed With Comment', `csp_callhistory`.`call_log` = '$remark' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND `csp_callhistory`.`unq_id` = '$linkedid' ");

			$r = DB::UPDATE("UPDATE `csp_callhistory_detail`
					SET `csp_callhistory_detail`.`cat_one_prerem_id` = '$category', `csp_callhistory_detail`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory_detail`.`callback_Status` = 'Dialed With Comment', `csp_callhistory_detail`.`call_log` = '$remark' 
					WHERE `csp_callhistory_detail`.`unq_id` = '$linkedid' and (`csp_callhistory_detail`.`log_type` ='CallbackRequest' OR `csp_callhistory_detail`.`log_type` ='AbandonCallback') ");


			if($returnValue != 0){

				echo 'Call Log Added Successfully!';

			}else{

				echo 'Data Saving Error!3 ';
			}

		}

		

	}


		
		public function agentcalllogdashboard(){
			 if(Util::isAuthorized("view_findcallhistory")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_findcallhistory")=='DENIED'){
            return view('permissiondenide');
        }
		Util::log('Callhistory','View');
		$userid=session('userid');
		$usertypeid=session('usertypeid');
		$get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getqueues  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();
		if($usertypeid=='17'){
			$AgentList = DB::select("SELECT a.`allo_agent_id`,b.`id`,b.`username` 
								 FROM `tbl_agent_allocation` as a 
								 right join `user_master` as b ON a.`allo_agent_id`=b.`id`
								 inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
								 where c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor' and b.`com_id`= $get_com_id->com_id group by b.`username`  ;");
				}else{
					$AgentList = DB::select("SELECT a.`allo_agent_id`,b.`id`,b.`username` 
									   FROM `tbl_agent_allocation` as a 
									   right join `user_master` as b ON a.`allo_agent_id`=b.`id`
									   inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
									   where c.`title`= 'Csp_Agent'  OR c.`title`= 'Csp_Supervisor'  and b.`com_id`= $get_com_id->com_id and b.id=".$userid." group by b.`username`  ;");
				}
			//$AgentList=DB::SELECT('SELECT username,id FROM user_master WHERE user_type_id=18');	
			$ipaddress = (new UsersController())->get_client_ip();
			$username=session()->get('username');
			Util::user_auth_log($ipaddress,"User open the Agent Call Logs Dashboard",$username,"View Agent Call Logs");	
			return view('agentCallLogHistory',compact('AgentList'));
		}
		public function getAgentWiseNoList(){
	 $agent=$_GET['agent'];
        if($agent!="All")
        {
        
        $data= DB::select('SELECT DISTINCT
csp_agent_callhistory.pho_number
FROM
csp_agent_callhistory
WHERE
csp_agent_callhistory.created_userid ='.$agent.' AND `sipid` IS NULL' );
        return compact('data');
        }else
        {
            
            return ;  

        }
		}
		public function searchCallLogData(){
			$agent=$_GET['agent'];
        		$phone=$_GET['phone'];
        		$category=$_GET['category'];
			$inquiry=$_GET['inquiry'];
			$from=$_GET['from'];
			$to=$_GET['to'];

		$where="";
		$where1="";
		$where2="";
                $where3="";	
		$where4="";	
		$getAbandonInfo = DB::select("SELECT GROUP_CONCAT('''',linkedid, '''') AS ablist	
										FROM tbl_calls_evnt	
        								where tbl_calls_evnt.cre_datetime BETWEEN '$from' AND '$to'	
										AND tbl_calls_evnt.desc = 'ABANDON'");	
		$abandonList=$getAbandonInfo[0]->ablist;
                if($agent!="All"){
			$where3=" AND csp_callhistory.created_userid =$agent";
		}

		if($phone!=""){
			$where=" AND pho_number=$phone";
		}
		if($category!=""){
			$where1=" AND category='$category'";
		}
		if($inquiry!=""){
			$where2=" AND inquiry='$inquiry'";
		}
		if($abandonList!=""){	
			$where4=" AND unq_id not in ($abandonList)";	
		}
		$data = DB::select("SELECT * ,
user_master.username
FROM
csp_callhistory
INNER JOIN user_master ON csp_callhistory.created_userid = user_master.id
WHERE
/*csp_callhistory.log_type='Agent Call Log' AND*/ `sipid` IS NOT NULL AND (csp_callhistory.created_datetime BETWEEN '$from' AND '$to') $where4 $where3 $where $where1 $where2	
 GROUP BY unq_id ORDER BY
csp_callhistory.created_userid DESC
");
$ipaddress = (new UsersController())->get_client_ip();
$username=session()->get('username');
Util::user_auth_log($ipaddress,"User Search Agent Call Logs",$username,"Search Agent Call Logs");
         return compact('data',$data);
			
		}
		public function getCategoryList(){
			$userid=session('userid');
    		$com_id = util::get_com_id_by_user($userid);
			
			$data= DB::select("SELECT *
				FROM
				csp_preset_remark
				WHERE
				csp_preset_remark.type =1
				AND csp_preset_remark.com_id='$com_id' 
				");
			return $data;
			}

		public function getInquaryList(){
			$userid=session('userid');
    		$com_id = util::get_com_id_by_user($userid);

			$data= DB::select("SELECT *
			FROM
			csp_preset_remark
			WHERE
			csp_preset_remark.type =2
			AND csp_preset_remark.com_id='$com_id' 
			");
			
			return $data;
			
		}


public function getCategoryList_alt(){
			$userid=session('userid');
    		$com_id = util::get_com_id_by_user($userid);
	

			return DB::table('tbl_type_one')->where('status','1')->where('com_id',$com_id)->get();
			}

public function getInquaryList_alt(){
			$userid=session('userid');
    		$com_id = util::get_com_id_by_user($userid);

    		return DB::table('tbl_type_two')->where('status','1')->where('com_id',$com_id)->get();
			
		}


		public function findcallhistory(Request $request) {

    $condition2 = array();
    $condition3 = array();

    $agent_id = $request->input('agent_id');
	$user_type_id=session()->get('usertypeid');
    $user = session('userid');

    $get_com_id  = DB::table('user_master')
                            ->where('id',$user)
                            ->first();

    $startdate = $request->input('startdate_val');
    $enddate = $request->input('enddate_val');
    $contactnumber = $request->input('contactnumber');
    $pre_rem1 = $request->input('pre_rem1');
    $pre_rem2 = $request->input('pre_rem2');
    $call_type = $request->input('call_type');

    if ($contactnumber != "") {
        $condition2['csp_callhistory.pho_number'] = $contactnumber;
    }

    $dataq = DB::table('csp_callhistory')
        ->select(
            'csp_callhistory.*', 'user_master.username', DB::raw("CONCAT_WS( ' ',CONCAT_WS( '.',csp_contact_master.title,csp_contact_master.firstname),csp_contact_master.lastname) AS contact_name"), DB::raw("CONCAT_WS( ', ',csp_contact_master.address_line1,csp_contact_master.address_line2,csp_contact_master.city_state_province,csp_contact_master.zip) AS cantactaddress"), DB::raw('(select csp_preset_remark.remark
                    AS preset1 from csp_preset_remark where csp_callhistory.cat_one_prerem_id  =   csp_preset_remark.id ) as preset1'), DB::raw('(select csp_preset_remark.remark
                    AS preset2 from csp_preset_remark where csp_callhistory.cat_two_prerem_id  =   csp_preset_remark.id ) as preset2')
        )
        ->join('user_master', 'user_master.id', '=', 'csp_callhistory.created_userid')
        ->leftjoin('csp_contact_master', 'csp_callhistory.cus_id', '=', 'csp_contact_master.id');



    if (!empty($condition2)) {
        $dataq = $dataq->where($condition2);
    }
    if ($request->input('agent_id') != "All") {
		
        $dataq = $dataq->where('csp_callhistory.created_userid', $agent_id);
    }
	if($request->input('agent_id') == "All"){
		if($user_type_id=="18"){
			$dataq = $dataq->where('csp_callhistory.created_userid', $user);
		}
		
	}
    if ($request->input('pre_rem1') != "All") {
        $dataq = $dataq->where('csp_callhistory.cat_one_prerem_id', $pre_rem1);
    }
    if ($request->input('pre_rem2') != "All") {
        $dataq = $dataq->where('csp_callhistory.cat_two_prerem_id', $pre_rem2);
    }
    if ($request->input('call_type') != "All") {
        $dataq = $dataq->where('csp_callhistory.log_type', $call_type);
    } else {
        $dataq = $dataq->whereIn('csp_callhistory.log_type', array("Inbound", "Outbound"));
    }
     $dataq = $dataq->where('user_master.com_id', $get_com_id->com_id);

// $data = $dataq->whereBetween('csp_callhistory.call_datetime', array($startdate, $enddate))

    $data=$dataq->where(function ($dataq) use ($startdate,$enddate) {
	    $dataq->whereBetween('csp_callhistory.created_datetime', array($startdate, $enddate))
	          ->orwhereBetween('csp_callhistory.call_datetime', array($startdate, $enddate));
	})->get();

    // $data = $dataq->orderBy('csp_callhistory.created_datetime','DESC')
    //     ->get();


    /* $dataq = DB::table('asteriskcdrdb.cdr')
      ->select(
      'cdr.uniqueid',
      DB::raw('(select call_log from csp_callhistory where csp_callhistory.unq_id  =   cdr.uniqueid) as call_log'),
      'cdr.calldate',
      'user_master.username',
      'cdr.dst')
      ->join('user_master','user_master.sip_id','=','cdr.dst')
      ->Where('cdr.disposition','ANSWERED')
      ->Where('cdr.dcontext','from-internal')
      ->Where('user_master.cc','1')
      ->Where('cdr.src',$contactnumber);
      $data = $dataq ->whereBetween('cdr.calldate', array($startdate, $enddate))
      ->get(); */

    $ipaddress = (new UsersController())->get_client_ip();
    $username = session()->get('username');
    Util::user_auth_log($ipaddress, "User Search Call History", $username, "Search Call History");
    return compact('data', $data);
    }
	public function editCallLog(Request $request) {
    if (Util::isAuthorized("popupcallhistory") == 'LOGGEDOUT') {
        return redirect('/');
    }
    if (Util::isAuthorized("popupcallhistory") == 'DENIED') {
        return view('permissiondenide');
    }
    Util::log('Callhistory', 'View');

    $callhistory = ($this->retrieveCallhistoryRecord($request->input('callid')));
    $contact = (new ContactController())->retrieveContactById($request->input('id'));
    $presets = (new PresetRemarkController())->retrieveAll();

    return view('EditCallLog')
            ->with('contact', $contact)
            ->with('presets', $presets)
            ->with('callhistory', $callhistory);
    }

    public function editCallLogDetail(Request $request) {
    if (Util::isAuthorized("popupcallhistory") == 'LOGGEDOUT') {
        return redirect('/');
    }
    if (Util::isAuthorized("popupcallhistory") == 'DENIED') {
        return view('permissiondenide');
    }
    Util::log('Callhistory', 'View');

    $callhistory = ($this->retrieveCallhistoryRecordDetail($request->input('callid')));
    $contact = (new ContactController())->retrieveContactById($request->input('id'));
    $presets = (new PresetRemarkController())->retrieveAll();

    return view('EditCallLogDetail')
            ->with('contact', $contact)
            ->with('presets', $presets)
            ->with('callhistory', $callhistory);
    }

	public function retrieveCallhistoryRecord($callid) {
    return DB::table('csp_callhistory')
            ->select('csp_callhistory.*', 'user_master.username')
            ->join('user_master', 'user_master.id', '=', 'csp_callhistory.created_userid')
            ->where('csp_callhistory.id', $callid)
            ->orderBy('created_datetime', 'desc')
            ->get();
    }

    public function retrieveCallhistoryRecordDetail($callid) {
    return DB::table('csp_callhistory_detail')
            ->select('csp_callhistory_detail.*', 'user_master.username')
            ->join('user_master', 'user_master.id', '=', 'csp_callhistory_detail.created_userid')
            ->where('csp_callhistory_detail.id', $callid)
            ->orderBy('created_datetime', 'desc')
            ->get();
    }

    public function updatemanualcalllog(Request $request) {

    $cusid = $request->input('cusid');
    $pho_number = $request->input('pho_number');
    $remark = $request->input('remark');
    $remark_old = $request->input('remark_old');
    $callid = $request->input('callid');
    $userid = session('userid');
     $com_id = util::get_com_id_by_user($userid);
    $preset1 = $request->input('preset1');
    $cur_date = date("Y-m-d h:i:s");
    $updatedRemark = $cur_date . '-' . $remark . "<br>"."Previous" . $remark_old;

    if ($preset1 == "All") {
        $preset1 = "";
    }
    $preset2 = $request->input('preset2');
    if ($preset2 == "All") {
        $preset2 = "";
    }

    $saveRecord = DB::table('csp_callhistory')
        ->where('id', $callid)
        ->update([
        'cus_id' => $cusid,
        'cat_one_prerem_id' => $preset1,
        'cat_two_prerem_id' => $preset2,
        'call_log' => "$updatedRemark",
        'update_user' => $userid,
        'updated_datetime' => $cur_date,
    ]);


    $ipaddress = (new UsersController())->get_client_ip();
    $username = session()->get('username');
    Util::user_auth_log($ipaddress, "Supervisor Update Call Log", $username, "Update Call Log");
    return redirect()->back();
    }

    public function updatemanualcalllogDetail(Request $request) {

    $cusid = $request->input('cusid');
    $pho_number = $request->input('pho_number');
    $remark = $request->input('remark');
    $remark_old = $request->input('remark_old');
    $callid = $request->input('callid');
    $userid = session('userid');
     $com_id = util::get_com_id_by_user($userid);
    $preset1 = $request->input('preset1');
    $cur_date = date("Y-m-d h:i:s");
    $updatedRemark = $cur_date . '-' . $remark . "<br>"."Previous" . $remark_old;

    if ($preset1 == "All") {
        $preset1 = "";
    }
    $preset2 = $request->input('preset2');
    if ($preset2 == "All") {
        $preset2 = "";
    }

    $saveRecord = DB::table('csp_callhistory_detail')
        ->where('id', $callid)
        ->update([
        'cus_id' => $cusid,
        'cat_one_prerem_id' => $preset1,
        'cat_two_prerem_id' => $preset2,
        'call_log' => "$updatedRemark",
        'update_user' => $userid,
        'updated_datetime' => $cur_date,
    ]);



    $ipaddress = (new UsersController())->get_client_ip();
    $username = session()->get('username');
    Util::user_auth_log($ipaddress, "Supervisor Update Call Log", $username, "Update Call Log");
    return redirect()->back();
    }
   
	}
