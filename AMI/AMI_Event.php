#!/usr/bin/php -q
<?php
ini_set("default_socket_timeout", -1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Colombo');

try
    {
        //$conn = new PDO ("mysql:host=mysql1005.mochahost.com;dbname=krvinod_ccs","krvinod_rock","abc@123abc");
        $conn = new PDO("mysql:host=localhost;dbname=asteriskcdrdb","root",'fThWmZq4');
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    catch(PDOException $e)
    {
        echo "error found<br>".$e->getMessage();
    }


$cur_date=date('Y-m-d H:i:s');
$socket = fsockopen("127.0.0.1","5038");
//print_r($socket);
fputs($socket, "Action: Login\r\n");
fputs($socket, "Username: ami_user\r\n");
fputs($socket, "Secret: passw0rd\r\n\r\n");
$col_arr = array();
$val_arr = array();
$event = "";
$count=0;
while($ret = fgets($socket)){

    if(substr($ret,0,6) == "Event:"){
        $e = explode(':', $ret);
        $event = trim($e[1]);
    }
    
    
    if($event == "DialBegin"){
        $data = explode(':', $ret);
        $val_arr['Cre_datetime'] = date('Y-m-d H:i:s');
        //var_dump($ret);
        if(trim($data[0])!= "" && ($data[0] =="Event" ||$data[0] =="Linkedid" || $data[0] =="Exten" || $data[0] =="Channel" || $data[0] =="CallerIDNum" || $data[0] =="CallerIDName" 
        || $data[0] =="ConnectedLineNum" || $data[0] =="Context" || $data[0] =="Uniqueid" || $data[0] =="DestChannel" || $data[0] =="DestCallerIDNum" || $data[0] =="DestCallerIDName"
        || $data[0] =="DestContext"  || $data[0] =="DestUniqueid"  || $data[0] =="DialString"))
        {
            //$val_arr[$data[0]] = $data[1];
            $val_arr[$data[0]] = preg_replace('/\s+/', '', preg_replace('/\n+/', '', $data[1]));
           //echo  $count.$data[0]."-".$data[1];
        }
         if($data[0]=="DialString" )
         {
            
            $sql = "INSERT INTO tbl_ami_events (Cre_datetime,
                        Event,
                        Linkedid,
                        Exten,
                        Frm_channel,
                        Frm_caller_id,
                        Frm_callerid_name,
                        ConnectedLineNum,
                        Frm_context,
                        uniqueid,
                        Dest_channel,
                        Dest_caller_id,
                        Dest_callerid_name,
                        Dest_context,
                        Dest_uniqueid,
                        Status
                                )VALUES (:Cre_datetime,
                                        :Event,
                                        :Linkedid,
                                        :Exten,
                                        :Channel,
                                        :CallerIDNum,
                                        :CallerIDName,
                                        :ConnectedLineNum,
                                        :Context,
                                        :Uniqueid,
                                        :DestChannel,
                                        :DestCallerIDNum,
                                        :DestCallerIDName,
                                        :DestContext,
                                        :DestUniqueid,
                                        :DialString)";



              if(count($val_arr)>9)
              {
                // try {
                //   $conn->prepare($sql)->execute($val_arr);
                // }
                // catch(Exception $e) {
                //   echo 'Message: ' .$e->getMessage();
                // }
                  if($val_arr["Context"] == "ext-queues")
                  {

                  
                      $endpoint_str1= explode('@',$val_arr["DialString"]);
                      $endpoint_str2= explode('/',$endpoint_str1[0]);
                     $endpoint = $endpoint_str2[1];
                      try {
                    
                        $stmt = $conn->prepare("SELECT status FROM asterisk.ps_contacts WHERE endpoint = '$endpoint'");
                        $stmt->execute();
                      

                        if ($stmt->rowCount() > 0){
                            $check_status = $stmt->fetch(PDO::FETCH_ASSOC);
                            $status = $check_status['status'];
                            //echo $status;
                            if($status == "Online")
                            {
                              try {
                                $conn->prepare($sql)->execute($val_arr);
                              }
                              catch(Exception $e) {
                                echo 'Message: ' .$e->getMessage();
                              }
                            }
                          
                            // do something
                        }
                        
                      } catch(PDOException $e) {
                        echo "Error: " . $e->getMessage();
                      }

                  }else if($val_arr["Context"] == "from-internal")
                  {
                    $endpoint_str1= explode('/',$val_arr["DestChannel"]);
                    $endpoint_str2= explode('@',$endpoint_str1[1]);
                    $endpoint_str3= explode('-',$endpoint_str2[0]);
                    $endpoint_echo = $endpoint_str3[0];
                    $endpoint = $endpoint_str3[0];
                   // echo $endpoint_echo."<br>";
                    try {
                  
                      $stmt = $conn->prepare("SELECT status,linkedid,status_des FROM asterisk.ps_contacts WHERE endpoint = '$endpoint' ");
                      $stmt->execute();
                    

                            if ($stmt->rowCount() > 0)
                            {
                                $check_st = $stmt->fetch(PDO::FETCH_ASSOC);
                              //echo  $status_echo = $check_st['status']."<br>";
                              $status = $check_st['status'];
                                if( $status == "Online")
                                    { 
                                          try {
                                            $conn->prepare($sql)->execute($val_arr);
                                          }
                                          catch(Exception $e) {
                                            echo 'Message: ' .$e->getMessage();
                                          }

                                    }
                            }
                            
                          } catch(PDOException $e) {
                          echo "Error: " . $e->getMessage();
                        }
                      }else
                      {
                        try {
                          $conn->prepare($sql)->execute($val_arr);
                        }
                        catch(Exception $e) {
                          echo 'Message: ' .$e->getMessage();
                        }
                      }
              }
            $val_arr = array();
            
         }
        } 


        if($event == "DialEnd"){
            $data = explode(':', $ret);
            $val_arr['Cre_datetime'] = date('Y-m-d H:i:s');
            //var_dump($ret);
            if(trim($data[0])!= "" && ($data[0] =="Event" || $data[0] =="Linkedid" || $data[0] =="Channel" || $data[0] =="CallerIDNum" || $data[0] =="CallerIDName" 
            || $data[0] =="ConnectedLineNum" || $data[0] =="Context" || $data[0] =="Uniqueid" || $data[0] =="DestChannel" || $data[0] =="DestCallerIDNum" || $data[0] =="DestCallerIDName"
            || $data[0] =="DestContext"  || $data[0] =="DestUniqueid"  || $data[0] =="DialStatus"))
            {
                $val_arr[$data[0]] = preg_replace('/\s+/', '', preg_replace('/\n+/', '', $data[1]));
              //  echo  $count.$data[0]."-".$data[1];
            }
             if($data[0]=="DialStatus")
             {
                
                $sql = "INSERT INTO tbl_ami_events (Cre_datetime,
                            Event,
                            Linkedid,
                            Frm_channel,
                            Frm_caller_id,
                            Frm_callerid_name,
                            ConnectedLineNum,
                            Frm_context,
                            uniqueid,
                            Dest_channel,
                            Dest_caller_id,
                            Dest_callerid_name,
                            Dest_context,
                            Dest_uniqueid,
                            Status
                                    )VALUES (:Cre_datetime,
                                            :Event,
                                            :Linkedid,
                                            :Channel,
                                            :CallerIDNum,
                                            :CallerIDName,
                                            :ConnectedLineNum,
                                            :Context,
                                            :Uniqueid,
                                            :DestChannel,
                                            :DestCallerIDNum,
                                            :DestCallerIDName,
                                            :DestContext,
                                            :DestUniqueid,
                                            :DialStatus)";
             
                if(count($val_arr)>9 && $val_arr["DialStatus"] != "CONGESTION" )
                {
                  // try {
                  //   $conn->prepare($sql)->execute($val_arr);
                  // }
                  // catch(Exception $e) {
                  //   echo 'Message: ' .$e->getMessage();
                  // }  

                  if( $val_arr["Context"] == "ext-queues" or $val_arr["Context"] == "from-internal" )
                  {
                    $endpoint_str1= explode('/',$val_arr["Channel"]);
                    $endpoint_str2= explode('@',$endpoint_str1[1]);
                    $endpoint_str3= explode('-',$endpoint_str2[0]);
                    $endpoint = $endpoint_str3[0];
                    try {
                  
                      $stmt = $conn->prepare("SELECT linkedid,status FROM asterisk.ps_contacts WHERE endpoint = '$endpoint' ");
                      $stmt->execute();
                    

                      if ($stmt->rowCount() > 0){
                          $check_linkedid = $stmt->fetch(PDO::FETCH_ASSOC);
                          $linkedid = $check_linkedid['linkedid'];
                          $status = $check_linkedid['status'];
                          //echo $status;
                          if($linkedid == $val_arr["Linkedid"] && $status !="Online" )
                          {
                            try {
                              $conn->prepare($sql)->execute($val_arr);
                            }
                            catch(Exception $e) {
                              echo 'Message: ' .$e->getMessage();
                            }
                          }
                        
                          // do something
                      }
                      
                    } catch(PDOException $e) {
                      echo "Error: " . $e->getMessage();
                    }
                  }else if($val_arr["Context"] == "macro-dial-one")
                  {
                    $endpoint_str1= explode('/',$val_arr["DestChannel"]);
                    $endpoint_str2= explode('@',$endpoint_str1[1]);
                    $endpoint_str3= explode('-',$endpoint_str2[0]);
                    $endpoint = $endpoint_str3[0];
                    $ConnectedLineNum = $val_arr["ConnectedLineNum"];
                   
                    try {
                  
                      $stmt = $conn->prepare("SELECT linkedid,status FROM asterisk.ps_contacts WHERE (endpoint = '$endpoint' OR endpoint = '$ConnectedLineNum') ");
                      $stmt->execute();
                    

                      if ($stmt->rowCount() > 0){
                          $check_linkedid = $stmt->fetch(PDO::FETCH_ASSOC);
                          $linkedid = $check_linkedid['linkedid'];
                          $status = $check_linkedid['status'];
                          //echo $status;
                          // echo  $ConnectedLineNum ;
                          // echo $endpoint; 
                          if($linkedid == $val_arr["Linkedid"] && $status !="Online")
                          {
                            try {
                              $conn->prepare($sql)->execute($val_arr);
                            }
                            catch(Exception $e) {
                              echo 'Message: ' .$e->getMessage();
                            }
                          }
                        
                          // do something
                      }
                      
                    } catch(PDOException $e) {
                      echo "Error: " . $e->getMessage();
                    }
                  }
                  else
                  {

                    try {
                      $conn->prepare($sql)->execute($val_arr);
                    }
                    catch(Exception $e) {
                      echo 'Message: ' .$e->getMessage();
                    }  
                    
                  }      
                      
                  
                }
                unset($val_arr);
                
             }
            } 
      

            if($event == "BlindTransfer"){
                $data = explode(':', $ret);
                $val_arr['Cre_datetime'] = date('Y-m-d H:i:s');
                //var_dump($ret);
                if(trim($data[0])!= "" && ($data[0] =="Event" || $data[0] =="TransfererChannel" || $data[0] =="Extension" || $data[0] =="TransfererCallerIDNum" || $data[0] =="TransfererCallerIDName" 
                || $data[0] =="TransfererConnectedLineNum" || $data[0] =="TransfererContext" || $data[0] =="TransfererUniqueid" || $data[0] =="TransfereeChannel" || $data[0] =="TransfereeCallerIDNum" || $data[0] =="TransfereeCallerIDName"
                || $data[0] =="TransfereeContext"  || $data[0] =="TransfereeUniqueid" || $data[0] =="TransfererLinkedid" || $data[0] =="Result"))
                {
                    $val_arr[$data[0]] = preg_replace('/\s+/', '', preg_replace('/\n+/', '', $data[1]));
                  //  echo  $count.$data[0]."-".$data[1];
                }
                 if($data[0]=="Extension")
                 {
                    
                    $sql = "INSERT INTO tbl_ami_events (Cre_datetime,
                                Event,
                                Linkedid,
                                Exten,
                                Frm_channel,
                                Frm_caller_id,
                                Frm_callerid_name,
                                ConnectedLineNum,
                                Frm_context,
                                uniqueid,
                                Dest_channel,
                                Dest_caller_id,
                                Dest_callerid_name,
                                Dest_context,
                                Dest_uniqueid,
                                Status
                                        )VALUES (:Cre_datetime,
                                                :Event,
                                                :TransfererLinkedid,
                                                :Extension,
                                                :TransfererChannel,
                                                :TransfererCallerIDNum,
                                                :TransfererCallerIDName,
                                                :TransfererConnectedLineNum,
                                                :TransfererContext,
                                                :TransfererUniqueid,
                                                :TransfereeChannel,
                                                :TransfereeCallerIDNum,
                                                :TransfereeCallerIDName,
                                                :TransfereeContext,
                                                :TransfereeUniqueid,
                                                :Result)";
           
                  try {
                    $conn->prepare($sql)->execute($val_arr);
                  }
                  catch(Exception $e) {
                    echo 'Message: ' .$e->getMessage();
                  }
                    unset($val_arr);
                    
                 }
                } 


                if($event == "AttendedTransfer"){
                    $data = explode(':', $ret);
                    $val_arr['Cre_datetime'] = date('Y-m-d H:i:s');
                    //var_dump($ret);
                    if(trim($data[0])!= "" && ($data[0] =="Event" || $data[0] =="OrigTransfererChannel" || $data[0] =="OrigTransfererCallerIDNum" || $data[0] =="OrigTransfererCallerIDName" 
                    || $data[0] =="OrigTransfererConnectedLineNum" || $data[0] =="OrigTransfererContext" || $data[0] =="OrigTransfererUniqueid" || $data[0] =="TransfereeChannel" || $data[0] =="TransfereeCallerIDNum" || $data[0] =="TransfereeCallerIDName"
                    || $data[0] =="TransfereeContext"  || $data[0] =="TransfereeUniqueid"  || $data[0] =="Result"))
                    {
                        $val_arr[$data[0]] = preg_replace('/\s+/', '', preg_replace('/\n+/', '', $data[1]));
                      //  echo  $count.$data[0]."-".$data[1];
                    }
                     if($data[0]=="TransfereeUniqueid")
                     {
                        
                        $sql = "INSERT INTO tbl_ami_events (Cre_datetime,
                                    Event,
                                    Frm_channel,
                                    Frm_caller_id,
                                    Frm_callerid_name,
                                    ConnectedLineNum,
                                    Frm_context,
                                    uniqueid,
                                    Dest_channel,
                                    Dest_caller_id,
                                    Dest_callerid_name,
                                    Dest_context,
                                    Dest_uniqueid,
                                    Status
                                            )VALUES (:Cre_datetime,
                                                    :Event,
                                                    :OrigTransfererChannel,
                                                    :OrigTransfererCallerIDNum,
                                                    :OrigTransfererCallerIDName,
                                                    :OrigTransfererConnectedLineNum,
                                                    :OrigTransfererContext,
                                                    :OrigTransfererUniqueid,
                                                    :TransfereeChannel,
                                                    :TransfereeCallerIDNum,
                                                    :TransfereeCallerIDName,
                                                    :TransfereeContext,
                                                    :TransfereeUniqueid,
                                                    :Result)";
                       // echo "$sql";
                       // $val_arr['MusicClass'] = "";
                       
                      // echo "hellow 4";
                       // var_dump($val_arr);
                       try {
                        $conn->prepare($sql)->execute($val_arr);
                      }
                      catch(Exception $e) {
                        echo 'Message: ' .$e->getMessage();
                      }
                        unset($val_arr);
                        
                     }
                    } 
    
            

                    if($event == "Unhold" || $event == "Hold"){
                        $data = explode(':', $ret);
                        $val_arr['Cre_datetime'] = date('Y-m-d H:i:s');
                        //var_dump($ret);
                        if(trim($data[0])!= "" && ($data[0] =="Event" || $data[0] =="Linkedid" || $data[0] =="Channel" || $data[0] =="CallerIDNum" || $data[0] =="CallerIDName" 
                        || $data[0] =="ConnectedLineNum" || $data[0] =="Context" || $data[0] =="Uniqueid"))
                        {
                            $val_arr[$data[0]] = preg_replace('/\s+/', '', preg_replace('/\n+/', '', $data[1]));
                          //  echo  $count.$data[0]."-".$data[1];
                        }
                         if($data[0]=="Linkedid")
                         {
                            
                            $sql = "INSERT INTO tbl_ami_events (Cre_datetime,
                                        Event,
                                        Linkedid,
                                        Frm_channel,
                                        Frm_caller_id,
                                        Frm_callerid_name,
                                        ConnectedLineNum,
                                        Frm_context,
                                        uniqueid)VALUES (:Cre_datetime,
                                                        :Event,
                                                        :Linkedid,
                                                        :Channel,
                                                        :CallerIDNum,
                                                        :CallerIDName,
                                                        :ConnectedLineNum,
                                                        :Context,
                                                        :Uniqueid)";
                           // echo "$sql";
                           // $val_arr['MusicClass'] = "";
                           
                           //echo "hellow 5";
                            //var_dump($val_arr);
                            try {
                              $conn->prepare($sql)->execute($val_arr);
                            }
                            catch(Exception $e) {
                              echo 'Message: ' .$e->getMessage();
                            }
                            unset($val_arr);;
                            
                         }
                        } 

                        if($event == "Hangup"){
                            $data = explode(':', $ret);
                            $val_arr['Cre_datetime'] = date('Y-m-d H:i:s');
                            //$val_arr['Status'] = "HANGUP";
                            //var_dump($ret);
                            if(trim($data[0])!= "" && ($data[0] =="Event" || $data[0] =="Linkedid" || $data[0] =="Channel" || $data[0] =="CallerIDNum" || $data[0] =="CallerIDName" 
                            || $data[0] =="ConnectedLineNum" || $data[0] =="Context"  || $data[0] =="Uniqueid"))
                            {
                                $val_arr[$data[0]] = preg_replace('/\s+/', '', preg_replace('/\n+/', '', $data[1]));
                              //  echo  $count.$data[0]."-".$data[1];
                            }
                             if($data[0]=="Linkedid")
                             {
                                
                                $sql = "INSERT INTO tbl_ami_events (Cre_datetime,
                                            Event,
                                            Linkedid,
                                            Frm_channel,
                                            Frm_caller_id,
                                            Frm_callerid_name,
                                            ConnectedLineNum,
                                            Frm_context,
                                            uniqueid)VALUES (:Cre_datetime,
                                                            :Event,
                                                            :Linkedid,
                                                            :Channel,
                                                            :CallerIDNum,
                                                            :CallerIDName,
                                                            :ConnectedLineNum,
                                                            :Context,
                                                            :Uniqueid
                                                            )";

                          // try {
                          //   $conn->prepare($sql)->execute($val_arr);
                          // }
                          // catch(Exception $e) {
                          //   echo 'Message: ' .$e->getMessage();
                          // }
                        if($val_arr["Context"] == "from-internal" || $val_arr["Context"] == "macro-dial-one" || $val_arr["Context"] == "from-conf")
                        {
                          $endpoint_str1= explode('/',$val_arr["Channel"]);
                          $endpoint_str2= explode('@',$endpoint_str1[1]);
                          $endpoint_str3= explode('-',$endpoint_str2[0]);
                          $endpoint = $endpoint_str3[0];
                          $stmt = $conn->prepare("SELECT status,linkedid,status_des FROM asterisk.ps_contacts WHERE endpoint = '$endpoint' ");
                            $stmt->execute();
                          

                            if ($stmt->rowCount() > 0){
                                    try {
                                      $conn->prepare($sql)->execute($val_arr);
                                    }
                                    catch(Exception $e) {
                                      echo 'Message: ' .$e->getMessage();
                                    }  
                              }
                        }else
                        {
                          $endpoint_str1= explode('/',$val_arr["Channel"]);
                          $endpoint_str2= explode('@',$endpoint_str1[1]);
                          $endpoint_str3= explode('-',$endpoint_str2[0]);
                          $endpoint = $endpoint_str3[0];
                          try {
                        
                            $stmt = $conn->prepare("SELECT status,linkedid,status_des FROM asterisk.ps_contacts WHERE endpoint = '$endpoint' ");
                            $stmt->execute();
                          

                            if ($stmt->rowCount() > 0){
                                $check_st = $stmt->fetch(PDO::FETCH_ASSOC);
                                $status = $check_st['status'];
                                $linkedid = $check_st['linkedid'];
                                $status_des = $check_st['status_des'];
                                //echo $status;
                                if( $status_des != "conf")
                                { 
                                  if($val_arr["Context"] == "from-queue-custom")
                                  { 
                                    if( $linkedid == $val_arr["Linkedid"] )
                                    {
                                      try {
                                        $conn->prepare($sql)->execute($val_arr);
                                      }
                                      catch(Exception $e) {
                                        echo 'Message: ' .$e->getMessage();
                                      }
                                    }  
                                  }else if($endpoint ==$val_arr["CallerIDNum"])
                                  {
                                    try {
                                      $conn->prepare($sql)->execute($val_arr);
                                    }
                                    catch(Exception $e) {
                                      echo 'Message: ' .$e->getMessage();
                                    }
                                  }
                                }
                                
                                else if($status != "Break" && $status != "Outbound" && $status != "Online" && $val_arr["Context"] != "dnd_on" && $val_arr["Context"] != "ext-local" )
                                {
                                  try {
                                    $conn->prepare($sql)->execute($val_arr);
                                  }
                                  catch(Exception $e) {
                                    echo 'Message: ' .$e->getMessage();
                                  }
                                }
                              
                                // do something
                            }
                            
                          } catch(PDOException $e) {
                            echo "Error: " . $e->getMessage();
                          }
                        }      
                             
                                unset($val_arr);;
                                //var_dump($val_arr);
                                
                             }
                             //$val_arr = array();
                            } 

    }//while loop




   

// sleep(5);
// exit;    
?>
