BEGIN
		DECLARE ring_sec varchar(30);
		DECLARE hold_sec varchar(30);
		DECLARE check_acw_st varchar(30);
		DECLARE check_rec_exsit varchar(30);
		DECLARE check_status varchar(30);
		DECLARE check_BF varchar(30);
		DECLARE check_ob_status varchar(30);
		DECLARE get_ob_status_id INT;
		DECLARE get_userid INT;
		DECLARE dest_cal_Length INT;
		DECLARE outoacw_sec_count INT;
		DECLARE endpoint_spilt varchar(30);
 

		
		IF NEW.Event = 'DialBegin' THEN
				

				

			IF NEW.Frm_context = 'ext-queues' THEN
					Update asterisk.ps_contacts SET 
							status = "Ringing",
							status_des = "Call",
							linkedid = NEW.Linkedid,
							uniqueid = NEW.Dest_uniqueid
							where endpoint=NEW.Dest_caller_id;

				INSERT INTO phonikip_db.tbl_calls_evnt SET 
									call_type = "Inbound",
									frm_caller_num = NEW.Frm_caller_id,
									uniqueid = NEW.uniqueid,
									linkedid = NEW.Linkedid,
									agnt_queueid = NEW.Exten,
									did_num = NEW.Dest_caller_id,
									date = CURDATE(),
									cre_datetime = NOW(),
									status = "Ringing";
			ELSEIF NEW.Frm_context = 'from-internal' and NEW.Dest_context = 'from-queue-custom' THEN

				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Dest_channel, '@', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

					Update asterisk.ps_contacts SET
							status_des = "Call",
							linkedid = NEW.uniqueid,
							uniqueid = NEW.Dest_uniqueid
							where endpoint=@endpoint_spilt;

					INSERT INTO phonikip_db.tbl_calls_evnt SET 
									call_type = "Inbound",
									frm_caller_num = NEW.Frm_caller_id,
									uniqueid = NEW.uniqueid,
									linkedid = NEW.Linkedid,
									agnt_queueid = NEW.Exten,
									did_num = NEW.Dest_caller_id,
									date = CURDATE(),
									cre_datetime = NOW(),
									status = "Ringing";

				Update phonikip_db.tbl_calls_evnt SET 
										linkedid = NEW.Linkedid
										where uniqueid = NEW.uniqueid Order by id desc limit 2;	

			ELSEIF NEW.Frm_context = 'macro-dialout-trunk' or NEW.Frm_context = 'from-blind-transfer' THEN

				 SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				 SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

				Update asterisk.ps_contacts SET 
						status = "Dialing",
						status_des = "Call",
						update_datetime= NOW(),
						linkedid= NEW.Linkedid
						where endpoint=@endpoint_spilt;
						
				SET @get_userid	 := (SELECT asterisk.ps_contacts.userid AS userid
											FROM
											asterisk.ps_contacts
											where endpoint = @endpoint_spilt);

				INSERT INTO phonikip_db.tbl_calls_evnt SET 
									call_type = "Outbound",
									to_caller_num = NEW.Dest_caller_id,
									uniqueid = NEW.uniqueid,
									linkedid = NEW.Linkedid,
									date = CURDATE(),
									cre_datetime = NOW(),
									dest_uniqueid = NEW.Dest_uniqueid,
									agnt_userid = @get_userid,
									agnt_sipid = @endpoint_spilt,
									status = "Ringing";

			ELSEIF NEW.Frm_context = 'macro-dial-one' THEN
                                        SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				 	SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
                                          
					Update asterisk.ps_contacts SET 
									status = "Dialing",
									status_des = "Call",
									update_datetime= NOW(),
									linkedid = NEW.Linkedid,
									uniqueid = NEW.Dest_uniqueid
									where endpoint=NEW.frm_caller_id;

					IF @endpoint_spilt = NEW.ConnectedLineNum  THEN
                                             Update asterisk.ps_contacts SET 
									status = "Dialing",
									status_des = "Call",
									update_datetime= NOW(),
									linkedid = NEW.Linkedid,
									uniqueid = NEW.Dest_uniqueid
									where endpoint=@endpoint_spilt;

                                            Update asterisk.ps_contacts SET 
									status = "Ringing",
									status_des = "Call",
									update_datetime= NOW(),
									linkedid = NEW.Linkedid,
									uniqueid = NEW.Dest_uniqueid
									where endpoint=NEW.Dest_caller_id;
						
				

						SET @check_rec_exsit := (SELECT phonikip_db.tbl_calls_evnt.linkedid AS linkedid
										FROM
										phonikip_db.tbl_calls_evnt
										where linkedid=NEW.Linkedid limit 1);

							SET @get_userid := (SELECT asterisk.ps_contacts.userid AS userid
												FROM
												asterisk.ps_contacts
												where endpoint = @endpoint_spilt);

							
								
								INSERT INTO phonikip_db.tbl_calls_evnt SET 
													call_type = "Internalcall",
													frm_caller_num = NEW.Dest_caller_id,
                                                                                                        to_caller_num = NEW.Dest_caller_id,
													uniqueid = NEW.uniqueid,
													linkedid = NEW.Linkedid,
													date = CURDATE(),
													cre_datetime = NOW(),
													dest_uniqueid = NEW.Dest_uniqueid,
													agnt_userid = @get_userid,
													agnt_sipid = @endpoint_spilt,
													status = "Ringing";
							

						
						
					ELSE
						
						Update asterisk.ps_contacts SET 
							status = "Ringing",
							status_des = "Call",
							linkedid = NEW.Linkedid,
							uniqueid = NEW.Dest_uniqueid
							where endpoint=NEW.Dest_caller_id;

						
						

						SET @get_userid := (SELECT asterisk.ps_contacts.userid AS userid
												FROM
												asterisk.ps_contacts
												where endpoint = NEW.Dest_caller_id);

							Update phonikip_db.tbl_calls_evnt SET 
										dest_uniqueid = NEW.Dest_uniqueid,
										agnt_userid = @get_userid,
										agnt_sipid = NEW.Dest_caller_id
										where linkedid=NEW.Linkedid Order by id desc limit 2;	
						
					
						
				      END IF;

			END IF;

		ELSEIF NEW.Event = 'DialEnd' THEN
			 IF NEW.Status = 'ANSWER' THEN

					SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Dest_channel, '-', 1));
  				 	SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
					Update asterisk.ps_contacts SET 
								status = "In Call",
								status_des = "Call",
								linkedid = NEW.Linkedid,
								update_datetime= NOW()
								where endpoint=@endpoint_spilt;
			
			
				
				END IF;	

			IF NEW.Frm_context = 'ext-queues' or NEW.Frm_context = 'from-blind-transfer' THEN

				SET @outoacw_sec_count := (SELECT phonikip_db.tbl_acw_time.acw_sec AS acw_sec
											FROM
											phonikip_db.tbl_acw_time
											INNER JOIN phonikip_db.tbl_agnt_queue_types ON phonikip_db.tbl_agnt_queue_types.type_name = phonikip_db.tbl_acw_time.agnt_queue_typeid
											and phonikip_db.tbl_agnt_queue_types.queueid = phonikip_db.tbl_acw_time.queueid

											INNER JOIN phonikip_db.tbl_calls_evnt ON phonikip_db.tbl_calls_evnt.agnt_userid = phonikip_db.tbl_agnt_queue_types.userid
											and phonikip_db.tbl_calls_evnt.agnt_queueid = phonikip_db.tbl_agnt_queue_types.queueid
											where phonikip_db.tbl_calls_evnt.linkedid=NEW.Linkedid Order by phonikip_db.tbl_calls_evnt.id desc limit 1);

				SET @get_call_date := (SELECT phonikip_db.tbl_calls_evnt.cre_datetime AS cre_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
				IF NEW.Status = 'ANSWER' THEN
					Update phonikip_db.tbl_calls_evnt SET 
										ring_sec_count = @ring_sec,
										answer_datetime = NOW(),
										outoacw_sec_count = @outoacw_sec_count,
										status = NEW.Status
										where linkedid=NEW.Linkedid   Order by id desc limit 1;
				ELSE
					Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
				END IF;

			ELSEIF (NEW.Frm_context = 'macro-dialout-trunk') THEN

				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

						Update asterisk.ps_contacts SET 
								status = "Out Call",
								status_des = "Call",
								linkedid = NEW.Linkedid,
								update_datetime= NOW()
								where endpoint=@endpoint_spilt;

				SET @outoacw_sec_count := (SELECT phonikip_db.tbl_acw_time.acw_sec AS acw_sec
											FROM
											phonikip_db.tbl_acw_time
											INNER JOIN phonikip_db.tbl_agnt_queue_types ON phonikip_db.tbl_agnt_queue_types.type_name = phonikip_db.tbl_acw_time.agnt_queue_typeid
											and phonikip_db.tbl_agnt_queue_types.queueid = phonikip_db.tbl_acw_time.queueid

											INNER JOIN phonikip_db.tbl_calls_evnt ON phonikip_db.tbl_calls_evnt.agnt_userid = phonikip_db.tbl_agnt_queue_types.userid
											and phonikip_db.tbl_calls_evnt.agnt_queueid = phonikip_db.tbl_agnt_queue_types.queueid
											where phonikip_db.tbl_calls_evnt.linkedid=NEW.Linkedid Order by phonikip_db.tbl_calls_evnt.id desc limit 1);
				SET @get_call_date := (SELECT phonikip_db.tbl_calls_evnt.cre_datetime AS cre_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
					IF NEW.Status = 'ANSWER' THEN
						Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											answer_datetime = NOW(),
											outoacw_sec_count = @outoacw_sec_count,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
					ELSE
						Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
					END IF;
                        ELSEIF NEW.Frm_context = 'macro-dial-one' THEN
                             SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Dest_channel, '-', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
                                

                                               Update asterisk.ps_contacts SET 
								status = "In Call",
								status_des = "Call",
								linkedid = NEW.Linkedid,
								update_datetime= NOW()
								where endpoint=NEW.ConnectedLineNum;


                                             IF @endpoint_spilt = NEW.ConnectedLineNum  THEN
                                                SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				               SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
                                                       Update asterisk.ps_contacts SET 
								status = "Out Call",
								status_des = "Call",
								linkedid = NEW.Linkedid,
								update_datetime= NOW()
								where endpoint=@endpoint_spilt ;

                                             END IF;

				SET @outoacw_sec_count := (SELECT phonikip_db.tbl_acw_time.acw_sec AS acw_sec
											FROM
											phonikip_db.tbl_acw_time
											INNER JOIN phonikip_db.tbl_agnt_queue_types ON phonikip_db.tbl_agnt_queue_types.type_name = phonikip_db.tbl_acw_time.agnt_queue_typeid
											and phonikip_db.tbl_agnt_queue_types.queueid = phonikip_db.tbl_acw_time.queueid

											INNER JOIN phonikip_db.tbl_calls_evnt ON phonikip_db.tbl_calls_evnt.agnt_userid = phonikip_db.tbl_agnt_queue_types.userid
											and phonikip_db.tbl_calls_evnt.agnt_queueid = phonikip_db.tbl_agnt_queue_types.queueid
											where phonikip_db.tbl_calls_evnt.linkedid=NEW.Linkedid Order by phonikip_db.tbl_calls_evnt.id desc limit 1);
				SET @get_call_date := (SELECT phonikip_db.tbl_calls_evnt.cre_datetime AS cre_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
					IF NEW.Status = 'ANSWER' THEN
						Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											answer_datetime = NOW(),
											outoacw_sec_count = @outoacw_sec_count,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
					ELSE
						Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
					END IF;
                        
			END IF;

		ELSEIF NEW.Event = 'Hangup' THEN

		

			IF ( NEW.Frm_context = 'ext-queues' or NEW.Frm_context = 'from-queue-custom') THEN

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where linkedid=NEW.Linkedid Order by id desc limit 1;

			ELSEIF (NEW.Frm_context = 'from-internal' OR NEW.Frm_context = 'ext-local' OR NEW.Frm_context = 'macro-dialout-trunk' ) THEN
			

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid and answer_datetime !="" Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where linkedid=NEW.Linkedid Order by id desc limit 1;
            ELSEIF (NEW.Frm_context = 'from-conf') THEN
			

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where (dest_uniqueid = NEW.uniqueid or uniqueid = NEW.uniqueid)  Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where (dest_uniqueid = NEW.uniqueid or uniqueid = NEW.uniqueid) Order by id desc limit 1;

			ELSEIF (NEW.Frm_context = 'macro-dial-one') THEN
			

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid and agnt_sipid=NEW.Frm_caller_id Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where linkedid=NEW.Linkedid Order by id desc limit 1;
			END IF;



			SET @check_acw_st := (SELECT phonikip_db.tbl_acw_tmp.linkid AS linkid
									FROM
									phonikip_db.tbl_acw_tmp
									where linkid=NEW.Linkedid);
			IF @check_acw_st != '' THEN

				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
		                IF @endpoint_spilt != '' THEN
                                    SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				    SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
                                END IF;

					Update asterisk.ps_contacts SET 
									status = "ACW",
									status_des = "",
									linkedid = "",
									update_datetime= NOW()
									where endpoint=@endpoint_spilt;

					Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @outoacw_sec_count
									where linkedid=NEW.Linkedid;
			
			ELSE

				SET @check_status := (SELECT phonikip_db.tbl_calls_evnt.status AS status
												FROM
												phonikip_db.tbl_calls_evnt
												where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @check_BF := (SELECT phonikip_db.tbl_calls_evnt.desc
												FROM
												phonikip_db.tbl_calls_evnt
												where linkedid=NEW.Linkedid Order by id desc limit 1);
				
				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

				SET @get_ob_status_id := (SELECT tbl_agnt_evnt.id as partone_id
														FROM
														phonikip_db.tbl_agnt_evnt
														where agnt_event='Outbound On' AND agnt_sipid =@endpoint_spilt Order by id desc limit 1);

				SET @check_ob_status := (SELECT tbl_agnt_evnt.id
														FROM
														phonikip_db.tbl_agnt_evnt
														where id_of_prtone=@get_ob_status_id AND agnt_sipid =@endpoint_spilt );  
				
				IF (@check_ob_status IS NULL and @get_ob_status_id != '') THEN

						
						Update asterisk.ps_contacts SET 
										status = "Outbound",
										status_des = "",
										linkedid = "",
										update_datetime= NOW()
										where endpoint=@endpoint_spilt;
				ELSE
					SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  					SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
					IF (@check_status != 'ANSWER' OR @check_BF = 'BlindTransfer') THEN
						SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  						SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
						
						Update asterisk.ps_contacts SET 
										status = "Online",
										status_des = "",
										linkedid = "",
										update_datetime= NOW()
										where endpoint=@endpoint_spilt;
					ELSE
						IF  (NEW.Frm_context = 'from-internal' OR  NEW.Frm_context = 'followme-sub' or NEW.Frm_context = 'from-conf') THEN
							SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  							SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
                                                        IF @endpoint_spilt != '' THEN
                                                             SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  				                             SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
                                                         END IF;

							Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid ;
							
							IF @endpoint_spilt != NEW.Frm_caller_id THEN
								SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  								SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
							END IF;
							Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid ;
						ELSE
							Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid ;

						END IF;
					END IF;
				END IF;
			END IF;
			

		ELSEIF NEW.Event = 'Hold' THEN
			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
			Update asterisk.ps_contacts SET 
							status = "Busy",
							status_des = "Hold"
							where endpoint=@endpoint_spilt;

			INSERT INTO phonikip_db.tbl_calls_hold_evnts SET 
							uniqueid = NEW.uniqueid,
							linkedid = NEW.Linkedid,
							date = CURDATE(),
							hold_datetime = NOW();
							
		ELSEIF NEW.Event = 'BlindTransfer' THEN


			SET @get_userid	 := (SELECT asterisk.ps_contacts.userid AS userid
											FROM
											asterisk.ps_contacts
											where endpoint = NEW.Frm_caller_id);
			
			Update phonikip_db.tbl_calls_evnt SET 
									`desc` = 'BlindTransfer',
									`agnt_sipid` = NEW.Frm_caller_id,
									`agnt_userid` = @get_userid,
									`to_trans_no` = NEW.Exten,
									`to_trans_linkedid` = NEW.Linkedid
									where linkedid=NEW.Linkedid and status='ANSWER' Order by id desc limit 1;

		ELSEIF NEW.Event = 'Unhold' THEN
			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
			Update asterisk.ps_contacts SET 
					status = "Busy",
					status_des = "Call"
					where endpoint=@endpoint_spilt;

			SET @get_call_date := (SELECT phonikip_db.tbl_calls_hold_evnts.hold_datetime AS hold_datetime
									FROM
									phonikip_db.tbl_calls_hold_evnts
									where linkedid=NEW.Linkedid Order by id desc limit 1);

			SET @hold_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
			Update phonikip_db.tbl_calls_hold_evnts SET 
							`unhold_datatime` = NOW(),
							`hold_sec_count` = @hold_sec
							where linkedid=NEW.Linkedid Order by id desc limit 1;
		END IF;
	END