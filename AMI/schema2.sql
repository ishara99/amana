-- MariaDB dump 10.19  Distrib 10.5.11-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: phonikip_db
-- ------------------------------------------------------
-- Server version	10.5.11-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `2`
--

DROP TABLE IF EXISTS `2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `contact_no` varchar(10) DEFAULT NULL,
  `contact_no2` varchar(10) DEFAULT NULL,
  `contact_no3` varchar(10) DEFAULT NULL,
  `lang` enum('ENGLISH','SINHALA','TAMIL') DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `camp_name` varchar(45) DEFAULT NULL,
  `disp` varchar(45) DEFAULT NULL,
  `agent` varchar(45) DEFAULT NULL,
  `attempt` int(10) unsigned DEFAULT 0,
  `call_status` varchar(45) NOT NULL,
  `call_date` datetime DEFAULT NULL,
  `status` int(10) unsigned NOT NULL,
  `schedule_status` int(10) unsigned NOT NULL,
  `schedule_date` datetime NOT NULL,
  `dnc` tinyint(4) DEFAULT 0,
  `remark1` varchar(100) DEFAULT NULL,
  `remark2` varchar(100) DEFAULT NULL,
  `remark3` varchar(100) DEFAULT NULL,
  `remark4` varchar(100) DEFAULT NULL,
  `remark5` varchar(100) DEFAULT NULL,
  `decription` varchar(200) DEFAULT NULL,
  `langchange` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `3`
--

DROP TABLE IF EXISTS `3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `contact_no` varchar(10) DEFAULT NULL,
  `contact_no2` varchar(10) DEFAULT NULL,
  `contact_no3` varchar(10) DEFAULT NULL,
  `lang` enum('ENGLISH','SINHALA','TAMIL') DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `camp_name` varchar(45) DEFAULT NULL,
  `disp` varchar(45) DEFAULT NULL,
  `agent` varchar(45) DEFAULT NULL,
  `attempt` int(10) unsigned DEFAULT 0,
  `call_status` varchar(45) NOT NULL,
  `call_date` datetime DEFAULT NULL,
  `status` int(10) unsigned NOT NULL,
  `schedule_status` int(10) unsigned NOT NULL,
  `schedule_date` datetime NOT NULL,
  `dnc` tinyint(4) DEFAULT 0,
  `remark1` varchar(100) DEFAULT NULL,
  `remark2` varchar(100) DEFAULT NULL,
  `remark3` varchar(100) DEFAULT NULL,
  `remark4` varchar(100) DEFAULT NULL,
  `remark5` varchar(100) DEFAULT NULL,
  `decription` varchar(200) DEFAULT NULL,
  `langchange` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AgentChat`
--

DROP TABLE IF EXISTS `AgentChat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AgentChat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(30) NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `message` varchar(200) NOT NULL,
  `ChatType` varchar(200) NOT NULL,
  `datetime` datetime NOT NULL,
  `com_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AgentChatNotification`
--

DROP TABLE IF EXISTS `AgentChatNotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AgentChatNotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` text NOT NULL,
  `datetime` datetime NOT NULL,
  `publisher` varchar(100) NOT NULL,
  `receiver` varchar(100) NOT NULL,
  `com_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `abc`
--

DROP TABLE IF EXISTS `abc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abc` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `api_req`
--

DROP TABLE IF EXISTS `api_req`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_req` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cli` varchar(20) DEFAULT NULL,
  `uniqueid` varchar(30) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `key1` int(11) DEFAULT NULL,
  `key2` int(11) DEFAULT NULL,
  `response` text DEFAULT NULL,
  `add_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_batch`
--

DROP TABLE IF EXISTS `autodial_batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `cus_number` bigint(20) unsigned NOT NULL,
  `batch_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `attempt` int(11) DEFAULT 0,
  `call_date` datetime DEFAULT NULL,
  `call_status` enum('ANSWERED','NO ANSWER','BUSY','FAILED') DEFAULT NULL,
  `call_schedule` datetime DEFAULT NULL,
  `cus_language` enum('Any','Sinhala','Tamil','English') DEFAULT NULL,
  `uniqueid` varchar(255) DEFAULT NULL,
  `donotcall` tinyint(1) NOT NULL DEFAULT 0,
  `dnt_cl_dup_batch` tinyint(1) DEFAULT NULL,
  `facility_no` varchar(100) DEFAULT NULL,
  `dial_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`campaign_id`,`cus_number`,`batch_id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `fk_cus_number_idx` (`cus_number`) USING BTREE,
  KEY `fk_autodial_batch_userid` (`user_id`) USING BTREE,
  CONSTRAINT `autodial_batch_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31061 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_campaign_detail`
--

DROP TABLE IF EXISTS `autodial_campaign_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_campaign_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `contact_no2` varchar(20) NOT NULL,
  `contact_no3` varchar(20) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `decription` text NOT NULL,
  `lang` varchar(100) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `batch_id` (`batch_id`) USING BTREE,
  CONSTRAINT `autodial_campaign_detail_ibfk_1` FOREIGN KEY (`batch_id`) REFERENCES `autodial_csv_master` (`batch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_campaign_master`
--

DROP TABLE IF EXISTS `autodial_campaign_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_campaign_master` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `tam_ivr` int(11) DEFAULT NULL,
  `eng_ivr` int(11) DEFAULT NULL,
  `sin_ivr` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `wait_time` int(11) DEFAULT NULL,
  `db` int(11) DEFAULT NULL,
  `cc` int(11) DEFAULT NULL,
  `srvyqngrp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`campaign_id`) USING BTREE,
  KEY `fk_sin_ivr` (`sin_ivr`) USING BTREE,
  KEY `fk_eng_ivr` (`eng_ivr`) USING BTREE,
  KEY `fk_tam_ivr` (`tam_ivr`) USING BTREE,
  CONSTRAINT `fk_eng_ivr` FOREIGN KEY (`eng_ivr`) REFERENCES `autodial_ivr_action` (`ivr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sin_ivr` FOREIGN KEY (`sin_ivr`) REFERENCES `autodial_ivr_action` (`ivr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tam_ivr` FOREIGN KEY (`tam_ivr`) REFERENCES `autodial_ivr_action` (`ivr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_campaign_user`
--

DROP TABLE IF EXISTS `autodial_campaign_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_campaign_user` (
  `campaign_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_user` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `campaign_prim_id` varchar(2) DEFAULT NULL,
  KEY `fk_campaignuser_userid` (`user_id`) USING BTREE,
  KEY `fk2_campaign_id_idx` (`campaign_id`) USING BTREE,
  CONSTRAINT `fk_campaignuser_userid` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_csv_data`
--

DROP TABLE IF EXISTS `autodial_csv_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_csv_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `contact_no` varchar(45) DEFAULT NULL,
  `contact_no2` varchar(45) DEFAULT NULL,
  `contact_no3` varchar(45) DEFAULT NULL,
  `branch` varchar(45) DEFAULT NULL,
  `fac_no` varchar(45) DEFAULT NULL,
  `clicode` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `c_name` varchar(100) DEFAULT NULL,
  `fac_type` varchar(80) DEFAULT NULL,
  `fac_amount` varchar(45) DEFAULT NULL,
  `rent_amount` varchar(45) DEFAULT NULL,
  `lease_age` varchar(45) DEFAULT NULL,
  `rent_due_date` varchar(45) DEFAULT NULL,
  `fac_status` varchar(45) DEFAULT NULL,
  `tenor` varchar(45) DEFAULT NULL,
  `v_no` varchar(45) DEFAULT NULL,
  `ast_type` varchar(80) DEFAULT NULL,
  `m_officer` varchar(45) DEFAULT NULL,
  `rent_due` varchar(45) DEFAULT NULL,
  `other_charge` varchar(45) DEFAULT NULL,
  `in_charges` varchar(45) DEFAULT NULL,
  `tot_ar` varchar(45) DEFAULT NULL,
  `last_pay_date` varchar(45) DEFAULT NULL,
  `last_pay_amt` varchar(45) DEFAULT NULL,
  `mat_rent` varchar(45) DEFAULT NULL,
  `g1name` varchar(80) DEFAULT NULL,
  `g1code` varchar(45) DEFAULT NULL,
  `g1phone1` varchar(45) DEFAULT NULL,
  `g1phone2` varchar(45) DEFAULT NULL,
  `g2name` varchar(80) DEFAULT NULL,
  `g2code` varchar(45) DEFAULT NULL,
  `g2phone1` varchar(45) DEFAULT NULL,
  `g2phone2` varchar(45) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `decription` varchar(150) DEFAULT NULL,
  `lang` varchar(150) DEFAULT NULL,
  `campign_id` int(11) DEFAULT NULL,
  `dnt_cl_dup_batch` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=181953 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_csv_master`
--

DROP TABLE IF EXISTS `autodial_csv_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_csv_master` (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `upload_date` datetime DEFAULT NULL,
  `file` varchar(45) NOT NULL,
  `used` tinyint(4) DEFAULT 0,
  `useddate` timestamp NULL DEFAULT NULL,
  `cc` int(11) DEFAULT NULL,
  `question_gid` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`batch_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autodial_remark`
--

DROP TABLE IF EXISTS `autodial_remark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autodial_remark` (
  `remark_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `cus_number` bigint(20) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `remark_date` datetime DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `cus_remark` text DEFAULT NULL,
  `unique_id` varchar(45) DEFAULT NULL,
  `call_status` varchar(20) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `facility_no` varchar(100) DEFAULT NULL,
  `agreed_date` date DEFAULT NULL,
  `agreed_amt` double DEFAULT NULL,
  `paid_amt` double DEFAULT NULL,
  `cus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`remark_id`) USING BTREE,
  KEY `fk_campaign_id_idx` (`campaign_id`) USING BTREE,
  KEY `fk_cus_number_idx` (`cus_number`) USING BTREE,
  KEY `fk_autodial_remark_userid` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `break_log`
--

DROP TABLE IF EXISTS `break_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `break_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sip_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `campaign_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_break_info_user_master1_idx` (`user_id`) USING BTREE,
  KEY `fk_break_info_status_list1_idx` (`status_id`) USING BTREE,
  KEY `timest` (`start_time`,`end_time`) USING BTREE,
  KEY `time` (`start_time`,`end_time`) USING BTREE,
  KEY `status` (`status_id`) USING BTREE,
  KEY `ag` (`user_id`) USING BTREE,
  CONSTRAINT `break_log_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status_list` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `break_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `call_cmpgn_mtrl`
--

DROP TABLE IF EXISTS `call_cmpgn_mtrl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_cmpgn_mtrl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmpgn_id` int(11) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `call_cmpgn_scrpt`
--

DROP TABLE IF EXISTS `call_cmpgn_scrpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_cmpgn_scrpt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmpgn_id` int(11) DEFAULT NULL,
  `des` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `callbacklist`
--

DROP TABLE IF EXISTS `callbacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callbacklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `callerid` int(10) unsigned NOT NULL,
  `incomedidno` int(11) NOT NULL,
  `responce` int(11) NOT NULL,
  `unique_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `callrelated_log`
--

DROP TABLE IF EXISTS `callrelated_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callrelated_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sip_id` int(11) NOT NULL,
  `callid` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `queueid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_acw_info_user_master1_idx` (`user_id`) USING BTREE,
  KEY `fk_acw_info_status_list1_idx` (`status_id`) USING BTREE,
  KEY `sip` (`sip_id`) USING BTREE,
  KEY `time` (`start_time`,`end_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_agent`
--

DROP TABLE IF EXISTS `campaign_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_agent` (
  `campaign` int(11) NOT NULL,
  `agent` int(11) NOT NULL,
  PRIMARY KEY (`campaign`,`agent`) USING BTREE,
  KEY `fk_agent_has_campaign_campaign1_idx` (`campaign`) USING BTREE,
  KEY `fk_agent_has_campaign_agent_idx` (`agent`) USING BTREE,
  CONSTRAINT `campaign_agent_ibfk_1` FOREIGN KEY (`agent`) REFERENCES `agent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `campaign_agent_ibfk_2` FOREIGN KEY (`campaign`) REFERENCES `campaign` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_phone`
--

DROP TABLE IF EXISTS `campaign_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_phone` (
  `campaign` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  PRIMARY KEY (`campaign`,`phone`) USING BTREE,
  KEY `fk_phone_has_campaign_campaign1_idx` (`campaign`) USING BTREE,
  KEY `fk_phone_has_campaign_phone1_idx` (`phone`) USING BTREE,
  CONSTRAINT `campaign_phone_ibfk_1` FOREIGN KEY (`campaign`) REFERENCES `campaign` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `campaign_phone_ibfk_2` FOREIGN KEY (`phone`) REFERENCES `phone` (`msisdn`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_subscriber`
--

DROP TABLE IF EXISTS `campaign_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_subscriber` (
  `campaign_id` int(11) NOT NULL,
  `subscriber_msisdn` int(11) NOT NULL,
  `attempt` int(11) NOT NULL DEFAULT 0,
  `retry_date` datetime DEFAULT NULL,
  `last_agent_attempted` int(11) DEFAULT NULL,
  `uniqueid` varchar(50) NOT NULL,
  `status` enum('ANSWER','NOANSWER','BUSY') DEFAULT NULL,
  PRIMARY KEY (`campaign_id`,`subscriber_msisdn`) USING BTREE,
  KEY `fk_subscriber_has_campaign_campaign1_idx` (`campaign_id`) USING BTREE,
  KEY `fk_subscriber_has_campaign_subscriber1_idx` (`subscriber_msisdn`) USING BTREE,
  KEY `fk_subscriber_has_user_master_idx` (`last_agent_attempted`) USING BTREE,
  CONSTRAINT `campaign_subscriber_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `campaign_subscriber_ibfk_2` FOREIGN KEY (`subscriber_msisdn`) REFERENCES `subscriber` (`msisdn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `campaign_subscriber_ibfk_3` FOREIGN KEY (`last_agent_attempted`) REFERENCES `user_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `calldate` datetime NOT NULL,
  `clid` varchar(80) NOT NULL DEFAULT '',
  `src` varchar(80) NOT NULL DEFAULT '',
  `dst` varchar(80) NOT NULL DEFAULT '',
  `dcontext` varchar(80) NOT NULL DEFAULT '',
  `channel` varchar(80) NOT NULL DEFAULT '',
  `dstchannel` varchar(80) NOT NULL DEFAULT '',
  `lastapp` varchar(80) NOT NULL DEFAULT '',
  `lastdata` varchar(80) NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT 0,
  `billsec` int(11) NOT NULL DEFAULT 0,
  `disposition` varchar(45) NOT NULL DEFAULT '',
  `amaflags` int(11) NOT NULL DEFAULT 0,
  `accountcode` varchar(20) NOT NULL DEFAULT '',
  `uniqueid` varchar(32) NOT NULL DEFAULT '',
  `userfield` varchar(255) NOT NULL DEFAULT '',
  `did` varchar(50) NOT NULL DEFAULT '',
  `recordingfile` varchar(255) NOT NULL DEFAULT '',
  `cnum` varchar(40) NOT NULL DEFAULT '',
  `cnam` varchar(40) NOT NULL DEFAULT '',
  `outbound_cnum` varchar(40) NOT NULL DEFAULT '',
  `outbound_cnam` varchar(40) NOT NULL DEFAULT '',
  `dst_cnam` varchar(40) NOT NULL DEFAULT '',
  KEY `calldate` (`calldate`) USING BTREE,
  KEY `dst` (`dst`) USING BTREE,
  KEY `accountcode` (`accountcode`) USING BTREE,
  KEY `uniqueid` (`uniqueid`) USING BTREE,
  KEY `did` (`did`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT 0,
  `show` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `com_holidays`
--

DROP TABLE IF EXISTS `com_holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_agent_callhistory`
--

DROP TABLE IF EXISTS `csp_agent_callhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_agent_callhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unq_id` varchar(150) DEFAULT NULL,
  `call_datetime` datetime DEFAULT NULL,
  `pho_number` varchar(20) DEFAULT NULL,
  `sipid` int(11) DEFAULT NULL,
  `cus_id` int(11) DEFAULT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `call_log` varchar(255) DEFAULT NULL,
  `cus_name` varchar(255) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `created_userid` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `inquiry` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=56276 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_callhistory`
--

DROP TABLE IF EXISTS `csp_callhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_callhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unq_id` varchar(150) DEFAULT NULL,
  `call_datetime` datetime DEFAULT NULL,
  `pho_number` varchar(35) DEFAULT NULL,
  `sipid` int(11) DEFAULT NULL,
  `queue_id` varchar(30) DEFAULT NULL,
  `cus_id` int(11) DEFAULT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `call_log` text DEFAULT NULL,
  `cat_one_prerem_id` int(11) DEFAULT NULL,
  `cat_two_prerem_id` int(11) DEFAULT NULL,
  `created_userid` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `callback_Status` varchar(30) DEFAULT NULL,
  `callback_unq_id` varchar(150) DEFAULT NULL,
  `callback_datetime` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=222896 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_callhistory_detail`
--

DROP TABLE IF EXISTS `csp_callhistory_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_callhistory_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unq_id` varchar(150) DEFAULT NULL,
  `call_datetime` datetime DEFAULT NULL,
  `pho_number` varchar(35) DEFAULT NULL,
  `sipid` int(11) DEFAULT NULL,
  `queue_id` varchar(30) DEFAULT NULL,
  `cus_id` int(11) DEFAULT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `call_log` text DEFAULT NULL,
  `cat_one_prerem_id` int(11) DEFAULT NULL,
  `cat_two_prerem_id` int(11) DEFAULT NULL,
  `created_userid` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `callback_Status` varchar(30) DEFAULT NULL,
  `callback_unq_id` varchar(150) DEFAULT NULL,
  `callback_datetime` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=64084 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_cl_logs`
--

DROP TABLE IF EXISTS `csp_cl_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_cl_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `callid` varchar(40) NOT NULL,
  `queuename` varchar(20) DEFAULT NULL,
  `agent` varchar(20) DEFAULT NULL,
  `event` varchar(20) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=691011 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_contact_master`
--

DROP TABLE IF EXISTS `csp_contact_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_contact_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index_old` varchar(20) DEFAULT NULL,
  `registration_no` varchar(10) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `primary_contact` varchar(20) NOT NULL,
  `secondary_contact` varchar(20) DEFAULT NULL,
  `secondary_contact2` varchar(20) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `address_line1` varchar(200) DEFAULT NULL,
  `address_line2` varchar(200) DEFAULT NULL,
  `city_state_province` varchar(100) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `contact_type_id` int(11) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `mobile_number` int(11) DEFAULT NULL,
  `uhid` varchar(255) DEFAULT NULL,
  `nic` varchar(15) DEFAULT NULL,
  `dateofbirth` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  `reclocation` char(10) DEFAULT NULL,
  `mediumS` int(11) NOT NULL,
  `mediumT` int(11) NOT NULL,
  `mediumE` int(11) NOT NULL,
  `insertUser` int(11) DEFAULT NULL,
  `thirdpartySyncData` int(11) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `modified_user` int(11) NOT NULL,
  `com_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `registration_no` (`registration_no`) USING BTREE,
  KEY `fk_contact_type_id` (`contact_type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=123331 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_contact_preff`
--

DROP TABLE IF EXISTS `csp_contact_preff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_contact_preff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefference` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_contact_type`
--

DROP TABLE IF EXISTS `csp_contact_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_contact_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_custom_wise_pref`
--

DROP TABLE IF EXISTS `csp_custom_wise_pref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_custom_wise_pref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `pref_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_event_log`
--

DROP TABLE IF EXISTS `csp_event_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_event_log` (
  `user_id` int(11) NOT NULL,
  `user_type` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  `form` varchar(100) NOT NULL,
  `event` varchar(250) NOT NULL,
  KEY `fk_csp_activity_log_user_master1_idx` (`user_id`) USING BTREE,
  CONSTRAINT `csp_event_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_phonebook_contact`
--

DROP TABLE IF EXISTS `csp_phonebook_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_phonebook_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` enum('Mr','Mrs','Miss','Dr','Rev') DEFAULT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `company` varchar(150) DEFAULT NULL,
  `designation` varchar(150) DEFAULT NULL,
  `work_address_line1` varchar(200) DEFAULT NULL,
  `work_address_line2` varchar(200) DEFAULT NULL,
  `work_city_state_province` varchar(100) DEFAULT NULL,
  `work_zip` int(11) DEFAULT NULL,
  `work_country` varchar(100) DEFAULT NULL,
  `work_phone` int(11) DEFAULT NULL,
  `work_email` varchar(200) DEFAULT NULL,
  `work_fax` int(11) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `home_address_line1` varchar(200) DEFAULT NULL,
  `home_address_line2` varchar(200) DEFAULT NULL,
  `home_city_state_province` varchar(100) DEFAULT NULL,
  `home_zip` int(11) DEFAULT NULL,
  `home_country` varchar(100) DEFAULT NULL,
  `home_phone` int(11) DEFAULT NULL,
  `home_mobile` int(11) DEFAULT NULL,
  `home_email` varchar(200) DEFAULT NULL,
  `im` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `csp_preset_remark`
--

DROP TABLE IF EXISTS `csp_preset_remark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csp_preset_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('1','2') NOT NULL,
  `remark` text NOT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `extension`
--

DROP TABLE IF EXISTS `extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extension` (
  `extension` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`extension`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `func_data`
--

DROP TABLE IF EXISTS `func_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `func_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `get_abandon`
--

DROP TABLE IF EXISTS `get_abandon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `get_abandon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `lang` varchar(45) NOT NULL,
  `number` varchar(45) NOT NULL,
  `did` varchar(45) NOT NULL,
  `queue` varchar(45) NOT NULL,
  `uniqueid` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `out_uniqueid` varchar(45) NOT NULL,
  `in_uniqueid` varchar(45) NOT NULL,
  `in_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1355 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `incoming_log`
--

DROP TABLE IF EXISTS `incoming_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incoming_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sip_id` int(11) NOT NULL,
  `callid` varchar(45) DEFAULT NULL,
  `answered` tinyint(1) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_receive_status_user_master1_idx` (`user_id`) USING BTREE,
  CONSTRAINT `incoming_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `incomingcdr`
--

DROP TABLE IF EXISTS `incomingcdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incomingcdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kpi_notify_user`
--

DROP TABLE IF EXISTS `kpi_notify_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kpi_notify_user` (
  `lvlone_id` int(11) DEFAULT NULL,
  `lvltwo_id` int(11) DEFAULT NULL,
  `lvlthr_id` int(11) DEFAULT NULL,
  `kpilevel_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` float DEFAULT NULL,
  `kpi_level` char(10) DEFAULT NULL,
  KEY `lvlone_id` (`lvlone_id`) USING BTREE,
  KEY `lvltwo_id` (`lvltwo_id`) USING BTREE,
  KEY `lvlthr_id` (`lvlthr_id`) USING BTREE,
  KEY `kpilevel_id` (`kpilevel_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_status`
--

DROP TABLE IF EXISTS `live_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sip_id` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `timestamp` datetime NOT NULL,
  `cli` int(10) unsigned DEFAULT NULL,
  `queuename` int(11) DEFAULT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `data` varchar(200) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_log`
--

DROP TABLE IF EXISTS `login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sip_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `campaign_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_user_login_user_master1_idx` (`user_id`) USING BTREE,
  KEY `fk_user_login_status_list1_idx` (`status_id`) USING BTREE,
  KEY `time` (`start_time`,`end_time`) USING BTREE,
  CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status_list` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `login_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_reset`
--

DROP TABLE IF EXISTS `login_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_log_id` varchar(45) DEFAULT NULL,
  `break_log_id` varchar(45) DEFAULT NULL,
  `callrelated_id` varchar(45) DEFAULT NULL,
  `given_time` datetime DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `admin_id` varchar(45) DEFAULT NULL,
  `cdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=352 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `optional_info`
--

DROP TABLE IF EXISTS `optional_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optional_info` (
  `subscriber_msisdn` int(11) NOT NULL,
  `option` enum('PREPAID','LANGUAGE','ADDRESS','NETWORK') NOT NULL,
  `val` varchar(200) NOT NULL,
  PRIMARY KEY (`subscriber_msisdn`,`option`) USING BTREE,
  KEY `fk_optional_info_subscriber1_idx` (`subscriber_msisdn`) USING BTREE,
  CONSTRAINT `optional_info_ibfk_1` FOREIGN KEY (`subscriber_msisdn`) REFERENCES `subscriber` (`msisdn`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `outgoincdr`
--

DROP TABLE IF EXISTS `outgoincdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outgoincdr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `calldate` varchar(45) NOT NULL,
  `sip_id` int(10) unsigned NOT NULL,
  `dst` varchar(20) NOT NULL,
  `channel` varchar(50) NOT NULL,
  `uniqueid` varchar(50) NOT NULL,
  `o_id` int(10) unsigned DEFAULT NULL,
  `campaign_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uniqueid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phone`
--

DROP TABLE IF EXISTS `phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phone` (
  `msisdn` int(11) NOT NULL,
  PRIMARY KEY (`msisdn`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phonebook`
--

DROP TABLE IF EXISTS `phonebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phonebook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `numb` varchar(20) NOT NULL DEFAULT '',
  `search` varchar(250) NOT NULL,
  PRIMARY KEY (`id`,`fname`,`lname`,`numb`,`search`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `privilege_type`
--

DROP TABLE IF EXISTS `privilege_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilege_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(45) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `add` tinyint(1) DEFAULT NULL,
  `edit` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qlog`
--

DROP TABLE IF EXISTS `qlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qlog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `time` varchar(26) NOT NULL DEFAULT '',
  `callid` varchar(40) DEFAULT NULL,
  `queuename` varchar(20) NOT NULL DEFAULT '',
  `agent` varchar(20) NOT NULL DEFAULT '',
  `event` varchar(20) NOT NULL,
  `data` varchar(100) NOT NULL DEFAULT '',
  `data1` varchar(40) NOT NULL DEFAULT '',
  `data2` varchar(40) NOT NULL DEFAULT '',
  `data3` varchar(40) NOT NULL DEFAULT '',
  `data4` varchar(40) NOT NULL DEFAULT '',
  `data5` varchar(40) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `callid_event` (`callid`,`event`) USING BTREE,
  KEY `uniid` (`callid`) USING BTREE,
  KEY `eventqueue` (`queuename`,`event`) USING BTREE,
  KEY `age` (`agent`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question_group_master`
--

DROP TABLE IF EXISTS `question_group_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_group_master` (
  `q_id` int(11) NOT NULL,
  `g_name` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `cc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue_group`
--

DROP TABLE IF EXISTS `queue_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_group` (
  `queue` int(11) NOT NULL,
  `group` varchar(45) DEFAULT NULL,
  `lang` varchar(45) DEFAULT NULL,
  `qname` varchar(100) DEFAULT NULL,
  `cc` int(11) DEFAULT 0,
  PRIMARY KEY (`queue`) USING BTREE,
  KEY `group` (`group`) USING BTREE,
  KEY `queue` (`queue`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue_log`
--

DROP TABLE IF EXISTS `queue_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `callid` varchar(40) NOT NULL,
  `queuename` varchar(20) DEFAULT NULL,
  `agent` varchar(20) DEFAULT NULL,
  `event` varchar(20) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue_status`
--

DROP TABLE IF EXISTS `queue_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `time` varchar(26) NOT NULL DEFAULT '',
  `callid` varchar(40) NOT NULL DEFAULT '',
  `queuename` varchar(20) NOT NULL DEFAULT '',
  `agent` varchar(20) NOT NULL DEFAULT '',
  `event` varchar(30) NOT NULL,
  `data` varchar(100) NOT NULL DEFAULT '',
  `data1` varchar(40) NOT NULL DEFAULT '',
  `data2` varchar(40) NOT NULL DEFAULT '',
  `data3` varchar(40) NOT NULL DEFAULT '',
  `data4` varchar(40) NOT NULL DEFAULT '',
  `data5` varchar(40) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `queue` (`queuename`) USING BTREE,
  KEY `event` (`event`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1981477 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `queue_status` AFTER INSERT ON `queue_status` FOR EACH ROW
BEGIN
	DECLARE entr_queue_time varchar(30);
	DECLARE waiting_sec varchar(30);
			
		IF NEW.event = 'ENTERQUEUE' THEN

				INSERT INTO phonikip_db.tbl_calls_evnt SET 
									call_type = "Inbound",
									frm_caller_num = NEW.data2,
									uniqueid = NEW.callid,
									linkedid = NEW.callid,
									agnt_queueid = NEW.queuename,
									date = CURDATE(),
									cre_datetime = NOW(),
									status = NEW.event;


UPDATE `phonikip_db`.`tbl_calls_evnt` 
SET `in_datetime` = NOW() ,`in_uniqueid` = NEW.callid,`cbstatus` = '1'
WHERE SUBSTRING(`frm_caller_num`, -9, 9)=SUBSTRING(NEW.data2, -9, 9) and 
CHAR_LENGTH(`frm_caller_num`)>0 AND `tbl_calls_evnt`.`desc` = 'ABANDON' 
AND agnt_queueid IN (SELECT `extension` from asterisk.queues_config where com_id IN (select `com_id` from asterisk.queues_config where extension=NEW.queuename COLLATE utf8_unicode_ci)) AND in_datetime='0000-00-00 00:00:00'
order by id desc limit 1;

UPDATE `phonikip_db`.`tbl_callback_mst` SET `status`= '1', `in_datetime` = NOW() ,`in_uniqueid` = NEW.callid 
WHERE SUBSTRING(`number`, -9, 9)=SUBSTRING(NEW.data2, -9, 9) and 
CHAR_LENGTH(`number`)>0 AND 
( did IN (SELECT `box_name` from tbl_srvbox_mst where com_id IN (select `com_id` from asterisk.queues_config where extension=NEW.queuename COLLATE utf8_unicode_ci)) 
OR queue IN (SELECT `extension` from asterisk.queues_config where com_id IN (select `com_id` from asterisk.queues_config where extension=NEW.queuename COLLATE utf8_unicode_ci)) ) and `status`=0 order by id desc limit 1;



		ELSEIF	NEW.event = 'RINGNOANSWER' THEN

				SET @entr_queue_time := (SELECT phonikip_db.queue_status.created as created 
										From phonikip_db.queue_status WHERE callid=NEW.callid and event='ENTERQUEUE');

				SET @waiting_sec := (SELECT TIMESTAMPDIFF(SECOND, @entr_queue_time , phonikip_db.queue_status.created) as waiting_sec 
										From phonikip_db.queue_status WHERE callid=NEW.callid and  (event='CONNECT' or event='ABANDON'));

				Update phonikip_db.tbl_calls_evnt SET 
										`desc` = NEW.event,
										ring_sec_count = @waiting_sec
										where linkedid=NEW.callid and status='ENTERQUEUE' and tbl_calls_evnt.DESC != 'RINGNOANSWER' Order by id desc limit 1;

		ELSE
			
		
				SET @entr_queue_time := (SELECT phonikip_db.queue_status.created as created 
										From phonikip_db.queue_status WHERE callid=NEW.callid and event='ENTERQUEUE');

				SET @waiting_sec := (SELECT TIMESTAMPDIFF(SECOND, @entr_queue_time , phonikip_db.queue_status.created) as waiting_sec 
										From phonikip_db.queue_status WHERE callid=NEW.callid and  (event='CONNECT' or event='ABANDON' or event='EXITWITHKEY'));

				Update phonikip_db.tbl_calls_evnt SET 
										`desc` = NEW.event,
										ring_sec_count = @waiting_sec
										where uniqueid=NEW.callid and status='ENTERQUEUE' Order by id desc limit 1;


	
		END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `receive_fax`
--

DROP TABLE IF EXISTS `receive_fax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receive_fax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calldate` datetime NOT NULL,
  `clid` varchar(20) NOT NULL,
  `src` varchar(20) NOT NULL,
  `dst` varchar(20) NOT NULL,
  `lastapp` varchar(20) NOT NULL,
  `lastdata` text NOT NULL,
  `duration` int(11) NOT NULL,
  `billsec` int(11) NOT NULL,
  `disposition` varchar(20) NOT NULL,
  `uniqueid` varchar(50) NOT NULL,
  `did` varchar(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recovary_master`
--

DROP TABLE IF EXISTS `recovary_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recovary_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `institution_name` varchar(100) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `facility_no` varchar(300) DEFAULT NULL,
  `facility_amt` double DEFAULT NULL,
  `granting_date` date DEFAULT NULL,
  `rental_amt` double DEFAULT NULL,
  `due_date` int(11) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `rentals_matured` int(11) DEFAULT NULL,
  `rentals_paid` char(20) DEFAULT NULL,
  `last_paymnt_date` date DEFAULT NULL,
  `last_paymnt_amt` double DEFAULT NULL,
  `no_rntl_arr` double DEFAULT NULL,
  `curr_arr` double DEFAULT NULL,
  `future_rec` double DEFAULT NULL,
  `tot_rec` double DEFAULT NULL,
  `asset_reg_no` varchar(75) DEFAULT NULL,
  `make` varchar(50) DEFAULT NULL,
  `asset_detail` varchar(75) DEFAULT NULL,
  `title` char(10) DEFAULT NULL,
  `first_name` varchar(300) DEFAULT NULL,
  `last_name` varchar(300) DEFAULT NULL,
  `nic_no` varchar(14) DEFAULT NULL,
  `address_one` varchar(100) DEFAULT NULL,
  `address_two` varchar(100) DEFAULT NULL,
  `address_three` varchar(100) DEFAULT NULL,
  `address_four` varchar(100) DEFAULT NULL,
  `address_five` varchar(100) DEFAULT NULL,
  `address_six` varchar(100) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `g_one_title` char(10) DEFAULT NULL,
  `g_one_firstname` varchar(300) DEFAULT NULL,
  `g_one_lastname` varchar(300) DEFAULT NULL,
  `g_one_nicno` varchar(14) DEFAULT NULL,
  `g_one_addressone` varchar(100) DEFAULT NULL,
  `g_one_addresstwo` varchar(100) DEFAULT NULL,
  `g_one_addressthree` varchar(100) DEFAULT NULL,
  `g_one_addressfour` varchar(100) DEFAULT NULL,
  `g_one_addressfive` varchar(100) DEFAULT NULL,
  `g_one_addresssix` varchar(100) DEFAULT NULL,
  `g_one_contactno` int(11) DEFAULT NULL,
  `g_two_title` char(10) DEFAULT NULL,
  `g_two_firstname` varchar(300) DEFAULT NULL,
  `g_two_lastname` varchar(300) DEFAULT NULL,
  `g_two_nicno` varchar(14) DEFAULT NULL,
  `g_two_addressone` varchar(100) DEFAULT NULL,
  `g_two_addresstwo` varchar(100) DEFAULT NULL,
  `g_two_addressthree` varchar(100) DEFAULT NULL,
  `g_two_addressfour` varchar(100) DEFAULT NULL,
  `g_two_addressfive` varchar(100) DEFAULT NULL,
  `g_two_addresssix` varchar(100) DEFAULT NULL,
  `g_two_contactno` int(11) DEFAULT NULL,
  `g_three_title` char(10) DEFAULT NULL,
  `g_three_firstname` varchar(300) DEFAULT NULL,
  `g_three_lastname` varchar(300) DEFAULT NULL,
  `g_three_nicno` varchar(14) DEFAULT NULL,
  `g_three_addressone` varchar(100) DEFAULT NULL,
  `g_three_addresstwo` varchar(100) DEFAULT NULL,
  `g_three_addressthree` varchar(100) DEFAULT NULL,
  `g_three_addressfour` varchar(100) DEFAULT NULL,
  `g_three_addressfive` varchar(100) DEFAULT NULL,
  `g_three_addresssix` varchar(100) DEFAULT NULL,
  `g_three_contactno` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1724 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sar_cus_pay_info`
--

DROP TABLE IF EXISTS `sar_cus_pay_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sar_cus_pay_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `facility_no` varchar(100) DEFAULT NULL,
  `receipt_branch` varchar(255) DEFAULT NULL,
  `receipt_tot` double DEFAULT NULL,
  `upload_datetime` datetime DEFAULT NULL,
  `upload_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sar_cuspayinfo_log`
--

DROP TABLE IF EXISTS `sar_cuspayinfo_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sar_cuspayinfo_log` (
  `batch_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `upload_date` datetime DEFAULT NULL,
  `file` varchar(45) NOT NULL,
  `used` tinyint(4) DEFAULT 0,
  `useddate` timestamp NULL DEFAULT NULL,
  `cc` int(11) DEFAULT NULL,
  `question_gid` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `rec_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sar_recovary_master`
--

DROP TABLE IF EXISTS `sar_recovary_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sar_recovary_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_no` varchar(100) DEFAULT NULL,
  `no` int(11) DEFAULT NULL,
  `duedate` int(11) DEFAULT NULL,
  `facility_type` varchar(255) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `cus_name` varchar(255) DEFAULT NULL,
  `grant_amt` double DEFAULT NULL,
  `monthly_rent` double DEFAULT NULL,
  `no_rent_arrears` double DEFAULT NULL,
  `total_arrears` double DEFAULT NULL,
  `total_receive` double DEFAULT NULL,
  `cus_number` int(11) DEFAULT NULL,
  `secondary_no` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `upload_datetime` datetime DEFAULT NULL,
  `upload_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=21833 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(45) NOT NULL,
  `form_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `send_fax`
--

DROP TABLE IF EXISTS `send_fax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `send_fax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `number` char(10) NOT NULL,
  `file` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms campaign`
--

DROP TABLE IF EXISTS `sms campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_cmpgn_mst`
--

DROP TABLE IF EXISTS `sms_cmpgn_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_cmpgn_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_cmpgn_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_fromnumbers`
--

DROP TABLE IF EXISTS `sms_fromnumbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_fromnumbers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_log`
--

DROP TABLE IF EXISTS `sms_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `sip_id` int(10) unsigned NOT NULL,
  `from_number` varchar(20) NOT NULL,
  `number` varchar(20) NOT NULL,
  `msg` varchar(200) NOT NULL,
  `smsid` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT 0,
  `result` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_log_data`
--

DROP TABLE IF EXISTS `sms_log_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_log_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `count` int(11) NOT NULL,
  `file` varchar(200) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_templates`
--

DROP TABLE IF EXISTS `sms_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` text NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_list`
--

DROP TABLE IF EXISTS `status_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  `type_flag` enum('LOGOUT','BREAK','SYSTEM','OUTGOING','DISPOSITION','CALLTAG','OFFLINE') DEFAULT NULL,
  `time_limit` int(11) DEFAULT 0,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscriber`
--

DROP TABLE IF EXISTS `subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber` (
  `msisdn` int(11) NOT NULL,
  `contact_phone` int(11) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`msisdn`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tast`
--

DROP TABLE IF EXISTS `tast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_abn_hourly_report`
--

DROP TABLE IF EXISTS `tbl_abn_hourly_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_abn_hourly_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `queue_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `1-hour` int(11) DEFAULT NULL,
  `2-hour` varchar(20) DEFAULT NULL,
  `3-hour` int(11) DEFAULT NULL,
  `4-hour` int(11) DEFAULT NULL,
  `5-hour` int(11) DEFAULT NULL,
  `6-hour` int(11) DEFAULT NULL,
  `7-hour` int(11) DEFAULT NULL,
  `8-hour` int(11) DEFAULT NULL,
  `9-hour` int(11) DEFAULT NULL,
  `10-hour` int(11) DEFAULT NULL,
  `11-hour` int(11) DEFAULT NULL,
  `12-hour` int(11) DEFAULT NULL,
  `13-hour` int(11) DEFAULT NULL,
  `14-hour` int(11) DEFAULT NULL,
  `15-hour` int(11) DEFAULT NULL,
  `16-hour` int(11) DEFAULT NULL,
  `17-hour` int(11) DEFAULT NULL,
  `18-hour` int(11) DEFAULT NULL,
  `19-hour` int(11) DEFAULT NULL,
  `20-hour` int(11) DEFAULT NULL,
  `21-hour` int(11) DEFAULT NULL,
  `22-hour` int(11) DEFAULT NULL,
  `23-hour` int(11) DEFAULT NULL,
  `24-hour` int(11) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12760 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_acw_time`
--

DROP TABLE IF EXISTS `tbl_acw_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_acw_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queueid` int(11) DEFAULT NULL,
  `agnt_queue_typeid` varchar(30) DEFAULT NULL,
  `acw_sec` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_acw_tmp`
--

DROP TABLE IF EXISTS `tbl_acw_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_acw_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sip_id` int(11) DEFAULT NULL,
  `linkid` varchar(50) DEFAULT NULL,
  `from_caller_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5583 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_agent_allocation`
--

DROP TABLE IF EXISTS `tbl_agent_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agent_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allo_agent_id` int(11) DEFAULT NULL,
  `lvlone_id` int(11) DEFAULT NULL,
  `lvltwo_id` int(11) DEFAULT NULL,
  `lvlthr_id` int(11) DEFAULT NULL,
  `add_dt` datetime DEFAULT NULL,
  `add_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tbl_agent_allocation_ibfk_1` (`lvlone_id`) USING BTREE,
  KEY `tbl_agent_allocation_ibfk_2` (`lvltwo_id`) USING BTREE,
  KEY `tbl_agent_allocation_ibfk_3` (`lvlthr_id`) USING BTREE,
  KEY `allo_user` (`allo_agent_id`) USING BTREE,
  CONSTRAINT `tbl_agent_allocation_ibfk_1` FOREIGN KEY (`lvlone_id`) REFERENCES `tbl_levelone` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_agent_allocation_ibfk_2` FOREIGN KEY (`lvltwo_id`) REFERENCES `tbl_leveltwo` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_agent_allocation_ibfk_3` FOREIGN KEY (`lvlthr_id`) REFERENCES `tbl_levelthr` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_agent_allocation_ibfk_4` FOREIGN KEY (`allo_agent_id`) REFERENCES `user_master` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_agent_detail_report`
--

DROP TABLE IF EXISTS `tbl_agent_detail_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agent_detail_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `str_evntid` varchar(50) DEFAULT NULL,
  `end_event` varchar(50) DEFAULT NULL,
  `login_duration_dec` varchar(20) DEFAULT NULL,
  `login_duration` varchar(20) DEFAULT NULL,
  `break_time_sec` varchar(20) DEFAULT NULL,
  `online_time_sec` varchar(20) DEFAULT NULL,
  `break_time` varchar(20) DEFAULT NULL,
  `answer_sec` varchar(20) DEFAULT NULL,
  `answer_tot_calls` varchar(20) DEFAULT NULL,
  `answer_ind_calls` varchar(20) DEFAULT NULL,
  `answer_out_calls` varchar(20) DEFAULT NULL,
  `acw_sec` varchar(20) DEFAULT NULL,
  `acw_times` varchar(20) DEFAULT NULL,
  `logout_datetime` datetime DEFAULT NULL,
  `login_datetime` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `active_time` varchar(30) DEFAULT NULL,
  `idle_time` varchar(30) DEFAULT NULL,
  `notready_time` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31236 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_agent_pre_remarks`
--

DROP TABLE IF EXISTS `tbl_agent_pre_remarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agent_pre_remarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_rmark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_agent_ring_event`
--

DROP TABLE IF EXISTS `tbl_agent_ring_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agent_ring_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agnt_sipid` int(10) unsigned DEFAULT NULL,
  `agnt_userid` int(11) DEFAULT NULL,
  `ringing_start` datetime DEFAULT NULL,
  `ringing_end` datetime DEFAULT NULL,
  `linkedid` varchar(40) DEFAULT NULL,
  `frm_caller_num` int(10) unsigned DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `ring_sec_count` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=270299 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_agnt_evnt`
--

DROP TABLE IF EXISTS `tbl_agnt_evnt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agnt_evnt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agnt_event` enum('Log In Time','Log Out Time','Online','Offline','Break Start','Break End','Outbound On','Outbound Off') DEFAULT NULL,
  `date` date DEFAULT NULL,
  `agnt_userid` int(11) DEFAULT NULL,
  `agnt_sipid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `evnt_min_count` int(11) DEFAULT NULL,
  `id_of_prtone` int(11) DEFAULT NULL,
  `agnt_desc` varchar(200) DEFAULT NULL,
  `time_exceeded_reason` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `agnt_event_agnt_userid` (`agnt_event`,`agnt_userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56515 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_agnt_queue_types`
--

DROP TABLE IF EXISTS `tbl_agnt_queue_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_agnt_queue_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `queueid` int(11) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_br_req_pre_remark`
--

DROP TABLE IF EXISTS `tbl_br_req_pre_remark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_br_req_pre_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_break_request`
--

DROP TABLE IF EXISTS `tbl_break_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_break_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sip_id` int(11) DEFAULT NULL,
  `break_type` varchar(255) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `sup_userid` int(11) DEFAULT NULL,
  `sup_act_datetime` datetime DEFAULT NULL,
  `sup_action` varchar(255) DEFAULT NULL,
  `sup_penalty_time` int(11) DEFAULT NULL,
  `sup_remark` varchar(255) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=189 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_break_schedule_master`
--

DROP TABLE IF EXISTS `tbl_break_schedule_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_break_schedule_master` (
  `rec_date_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `record_day` varchar(100) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_call_rating`
--

DROP TABLE IF EXISTS `tbl_call_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_call_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_datetime` datetime NOT NULL,
  `cli_num` varchar(20) NOT NULL,
  `queue` varchar(20) NOT NULL,
  `rate_num` varchar(20) NOT NULL,
  `call_duration` varchar(20) NOT NULL,
  `sip_id` varchar(20) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `unique_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique_id` (`unique_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_callback_mst`
--

DROP TABLE IF EXISTS `tbl_callback_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_callback_mst` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `lang` varchar(45) NOT NULL,
  `number` varchar(45) NOT NULL,
  `did` varchar(45) NOT NULL,
  `queue` varchar(45) NOT NULL,
  `uniqueid` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `out_uniqueid` varchar(45) NOT NULL,
  `in_uniqueid` varchar(45) NOT NULL,
  `in_datetime` datetime NOT NULL,
  `out_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uniqueid` (`uniqueid`) USING BTREE,
  KEY `did` (`did`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11785 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_calls_evnt`
--

DROP TABLE IF EXISTS `tbl_calls_evnt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_calls_evnt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_type` enum('Inbound','outbound','Internalcall') DEFAULT NULL,
  `frm_caller_num` varchar(40) DEFAULT NULL,
  `to_caller_num` varchar(40) DEFAULT NULL,
  `did_num` varchar(20) DEFAULT NULL,
  `uniqueid` varchar(40) DEFAULT NULL,
  `dest_uniqueid` varchar(40) DEFAULT NULL,
  `linkedid` varchar(40) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `agnt_userid` int(11) DEFAULT NULL,
  `agnt_sipid` varchar(20) DEFAULT NULL,
  `agnt_queueid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `ring_sec_count` int(11) DEFAULT NULL,
  `answer_datetime` datetime DEFAULT NULL,
  `answer_sec_count` varchar(255) DEFAULT NULL,
  `hangup_datatime` datetime DEFAULT NULL,
  `acwend_datatime` datetime DEFAULT NULL,
  `outoacw_sec_count` int(11) DEFAULT NULL,
  `acw_sec_count` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `to_trans_no` varchar(20) DEFAULT NULL,
  `to_trans_linkedid` varchar(30) DEFAULT NULL,
  `in_uniqueid` varchar(45) DEFAULT NULL,
  `in_datetime` datetime NOT NULL,
  `cbstatus` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `agnt_queueid` (`agnt_queueid`) USING BTREE,
  KEY `date` (`date`) USING BTREE,
  KEY `answer_sec_count` (`answer_sec_count`) USING BTREE,
  KEY `acwend_datatime` (`acwend_datatime`) USING BTREE,
  KEY `ring_sec_count` (`ring_sec_count`) USING BTREE,
  KEY `desc` (`desc`) USING BTREE,
  KEY `dest_uniqueid` (`dest_uniqueid`) USING BTREE,
  KEY `uniqueid` (`uniqueid`) USING BTREE,
  KEY `cbstatus` (`cbstatus`) USING BTREE,
  KEY `cre_datetime_linkedid` (`cre_datetime`,`linkedid`) USING BTREE,
  KEY `linkedid` (`linkedid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=176617 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_calls_hold_evnts`
--

DROP TABLE IF EXISTS `tbl_calls_hold_evnts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_calls_hold_evnts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueid` varchar(40) DEFAULT NULL,
  `linkedid` varchar(40) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `hold_datetime` datetime DEFAULT NULL,
  `hold_sec_count` varchar(255) DEFAULT NULL,
  `unhold_datatime` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `uniqueid` (`uniqueid`) USING BTREE,
  KEY `linkedid` (`linkedid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14787 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cl_abndon_event`
--

DROP TABLE IF EXISTS `tbl_cl_abndon_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cl_abndon_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_type` enum('Inbound','outbound','Internalcall') DEFAULT NULL,
  `frm_caller_num` varchar(14) DEFAULT NULL,
  `to_caller_num` varchar(14) DEFAULT NULL,
  `did_num` varchar(14) DEFAULT NULL,
  `uniqueid` varchar(40) DEFAULT NULL,
  `dest_uniqueid` varchar(40) DEFAULT NULL,
  `linkedid` varchar(40) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `agnt_userid` int(11) DEFAULT NULL,
  `agnt_sipid` int(11) DEFAULT NULL,
  `agnt_queueid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `sup_status` enum('YES','NO') DEFAULT NULL,
  `sup_comment` text DEFAULT NULL,
  `sup_userid` int(11) DEFAULT NULL,
  `cmnt_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cl_back_evnt`
--

DROP TABLE IF EXISTS `tbl_cl_back_evnt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cl_back_evnt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_type` enum('Inbound','outbound','Internalcall') DEFAULT NULL,
  `frm_caller_num` varchar(14) DEFAULT NULL,
  `to_caller_num` varchar(14) DEFAULT NULL,
  `did_num` varchar(14) DEFAULT NULL,
  `uniqueid` varchar(40) DEFAULT NULL,
  `dest_uniqueid` varchar(40) DEFAULT NULL,
  `linkedid` varchar(40) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `agnt_userid` int(11) DEFAULT NULL,
  `agnt_sipid` int(11) DEFAULT NULL,
  `agnt_queueid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `sup_status` enum('YES','NO') DEFAULT 'YES',
  `sup_comment` text DEFAULT NULL,
  `sup_userid` int(11) DEFAULT NULL,
  `cmnt_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cltypes`
--

DROP TABLE IF EXISTS `tbl_cltypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cltypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_com_mst`
--

DROP TABLE IF EXISTS `tbl_com_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_com_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_name` varchar(100) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `call_prompt` enum('yes','no') DEFAULT 'yes',
  `arogya` enum('yes','no') DEFAULT 'no',
  `ad_auth` enum('yes','no') DEFAULT 'no',
  `ad_serverip` varchar(20) DEFAULT NULL,
  `ringnoanswer` enum('yes','no') DEFAULT 'no',
  `livedb_url` varchar(30) DEFAULT NULL,
  `livequeue_url` varchar(30) DEFAULT NULL,
  `out_context` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_cus_inq_comment`
--

DROP TABLE IF EXISTS `tbl_cus_inq_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cus_inq_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_docverify`
--

DROP TABLE IF EXISTS `tbl_docverify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_docverify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_type` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_donotcl_act`
--

DROP TABLE IF EXISTS `tbl_donotcl_act`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_donotcl_act` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `campaign` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_dontcl_nos`
--

DROP TABLE IF EXISTS `tbl_dontcl_nos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dontcl_nos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_no` int(11) DEFAULT NULL,
  `campaign` int(11) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_email_attach`
--

DROP TABLE IF EXISTS `tbl_email_attach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_email_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emailcat_id` int(11) DEFAULT NULL,
  `attach_file` varchar(500) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_email_cat`
--

DROP TABLE IF EXISTS `tbl_email_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_email_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_cat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `email_cat` (`email_cat`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_email_templts`
--

DROP TABLE IF EXISTS `tbl_email_templts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_email_templts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startup` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_emlsrv_receive`
--

DROP TABLE IF EXISTS `tbl_emlsrv_receive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_emlsrv_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(15) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `ref_no_num` int(11) DEFAULT NULL,
  `e_from` varchar(255) DEFAULT NULL,
  `e_to` text DEFAULT NULL,
  `e_cc` text DEFAULT NULL,
  `e_sub` text CHARACTER SET utf8 DEFAULT NULL,
  `e_msg` text CHARACTER SET utf8 DEFAULT NULL,
  `e_attach` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rec_datetime` datetime DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Fresh',
  `update_userid` int(11) DEFAULT NULL,
  `update_datetime` timestamp NULL DEFAULT NULL,
  `com_id` char(1) DEFAULT NULL,
  `emlbox_id` int(11) DEFAULT NULL,
  `e_bcc` text DEFAULT NULL,
  `e_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3897 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_emlsrv_send`
--

DROP TABLE IF EXISTS `tbl_emlsrv_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_emlsrv_send` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(15) DEFAULT NULL,
  `e_to` text DEFAULT NULL,
  `e_sub` text CHARACTER SET utf8 DEFAULT NULL,
  `e_msg` text CHARACTER SET utf8 DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `e_cc` text DEFAULT NULL,
  `e_bcc` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2500 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_agnt_ans_det`
--

DROP TABLE IF EXISTS `tbl_eval_agnt_ans_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_agnt_ans_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_ans_id` int(11) DEFAULT NULL,
  `rec_id` int(11) DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `eval_ans_weight` int(11) DEFAULT NULL,
  `criteria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `eval_ans_id` (`eval_ans_id`) USING BTREE,
  KEY `criteria_id` (`criteria_id`) USING BTREE,
  KEY `record_id` (`rec_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_agnt_ans_mst`
--

DROP TABLE IF EXISTS `tbl_eval_agnt_ans_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_agnt_ans_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_agnt_id` int(11) DEFAULT NULL,
  `eval_id` int(11) DEFAULT NULL,
  `eval_contxt_id` int(11) DEFAULT NULL,
  `total_marks` int(11) DEFAULT NULL,
  `cre_user` varchar(255) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `eval_id` (`eval_id`) USING BTREE,
  KEY `eval_context_id` (`eval_contxt_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_agnt_mst`
--

DROP TABLE IF EXISTS `tbl_eval_agnt_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_agnt_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_name` varchar(50) DEFAULT NULL,
  `eval_contxt_id` int(11) NOT NULL,
  `frm_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `rec_num` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  `rec_option` char(12) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_agnt_rec`
--

DROP TABLE IF EXISTS `tbl_eval_agnt_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_agnt_rec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_id` int(11) DEFAULT NULL,
  `eval_agent_userid` int(11) DEFAULT NULL,
  `rec_name` varchar(255) DEFAULT NULL,
  `linked_id` varchar(35) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `evaluation_agnt_id` (`eval_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_agnt_usr`
--

DROP TABLE IF EXISTS `tbl_eval_agnt_usr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_agnt_usr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_id` int(11) NOT NULL,
  `eval_agent_userid` int(11) NOT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`eval_id`,`eval_agent_userid`) USING BTREE,
  KEY `evaluation_agnt_id` (`eval_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=259 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_contxt_det`
--

DROP TABLE IF EXISTS `tbl_eval_contxt_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_contxt_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_contxt_id` int(11) NOT NULL,
  `eval_crit_id` int(11) NOT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`eval_contxt_id`,`eval_crit_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_contxt_mst`
--

DROP TABLE IF EXISTS `tbl_eval_contxt_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_contxt_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_contxt_name` varchar(50) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_crit_mst`
--

DROP TABLE IF EXISTS `tbl_eval_crit_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_crit_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_crit_name` varchar(50) NOT NULL,
  `eval_crit_type_id` int(11) NOT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  `descript` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_crit_type`
--

DROP TABLE IF EXISTS `tbl_eval_crit_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_crit_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_type_name` varchar(50) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_rate_card`
--

DROP TABLE IF EXISTS `tbl_eval_rate_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_rate_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `colour` varchar(255) DEFAULT NULL,
  `tot_marks_from` int(11) DEFAULT NULL,
  `tot_marks_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_eval_weight`
--

DROP TABLE IF EXISTS `tbl_eval_weight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_eval_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eval_id` int(11) DEFAULT NULL,
  `eval_contxt_id` int(11) DEFAULT NULL,
  `eval_crit_id` int(11) DEFAULT NULL,
  `eval_crit_type_id` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `cre_user` varchar(255) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_exten_allocation`
--

DROP TABLE IF EXISTS `tbl_exten_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_exten_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `exten_id` varchar(40) DEFAULT NULL,
  `cre_user_id` int(11) DEFAULT NULL,
  `cre_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `exten_id` (`exten_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_faxsrv_receive`
--

DROP TABLE IF EXISTS `tbl_faxsrv_receive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_faxsrv_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(15) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `ref_no_num` varchar(11) DEFAULT NULL,
  `from_num` varchar(255) DEFAULT NULL,
  `to_num` text DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `fx_filepath` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rec_datetime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `cre_datetime` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Fresh',
  `update_datetime` datetime DEFAULT NULL,
  `com_id` char(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_faxsrv_send`
--

DROP TABLE IF EXISTS `tbl_faxsrv_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_faxsrv_send` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(15) DEFAULT NULL,
  `to_num` text DEFAULT NULL,
  `msg` text DEFAULT NULL,
  `fx_filepath` varchar(255) DEFAULT NULL,
  `fx_des` varchar(255) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `update_datetime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ivr_event`
--

DROP TABLE IF EXISTS `tbl_ivr_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ivr_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calldate` datetime NOT NULL,
  `duration` int(11) NOT NULL,
  `lastapp` varchar(80) NOT NULL,
  `did` varchar(50) NOT NULL,
  `uniqueid` varchar(32) NOT NULL,
  `lastdata` varchar(80) NOT NULL,
  `src` varchar(80) NOT NULL,
  `dst` varchar(80) NOT NULL,
  `billsec` int(11) NOT NULL,
  `recordingfile` varchar(225) NOT NULL,
  `disposition` varchar(45) NOT NULL,
  `channel` varchar(80) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=222721 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_kpilevel_info`
--

DROP TABLE IF EXISTS `tbl_kpilevel_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_kpilevel_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kpilevel_name` varchar(1000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `lvlone_id` int(11) NOT NULL,
  `lvltwo_id` int(11) NOT NULL,
  `lvlthr_id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `status` float DEFAULT NULL,
  `l1_kpi` int(11) DEFAULT NULL,
  `l2_kpi` int(11) DEFAULT NULL,
  `l3_kpi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `lvlone_id` (`lvlone_id`) USING BTREE,
  KEY `lvltwo_id` (`lvltwo_id`) USING BTREE,
  KEY `lvlthr_id` (`lvlthr_id`) USING BTREE,
  CONSTRAINT `tbl_kpilevel_info_ibfk_1` FOREIGN KEY (`lvlone_id`) REFERENCES `tbl_levelone` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_kpilevel_info_ibfk_2` FOREIGN KEY (`lvltwo_id`) REFERENCES `tbl_leveltwo` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_kpilevel_info_ibfk_3` FOREIGN KEY (`lvlthr_id`) REFERENCES `tbl_levelthr` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_kpilevel_user`
--

DROP TABLE IF EXISTS `tbl_kpilevel_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_kpilevel_user` (
  `lvlone_id` int(11) DEFAULT NULL,
  `lvltwo_id` int(11) DEFAULT NULL,
  `lvlthr_id` int(11) DEFAULT NULL,
  `kpilevel_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` float DEFAULT NULL,
  KEY `lvlone_id` (`lvlone_id`) USING BTREE,
  KEY `lvltwo_id` (`lvltwo_id`) USING BTREE,
  KEY `lvlthr_id` (`lvlthr_id`) USING BTREE,
  KEY `kpilevel_id` (`kpilevel_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_levelone`
--

DROP TABLE IF EXISTS `tbl_levelone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_levelone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `com_id` int(11) NOT NULL,
  `cre_userid` int(11) NOT NULL,
  `cre_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_levelthr`
--

DROP TABLE IF EXISTS `tbl_levelthr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_levelthr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lvlone_id` int(11) NOT NULL,
  `lvltwo_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `lvlone_id` (`lvlone_id`) USING BTREE,
  KEY `lvltwo_id` (`lvltwo_id`) USING BTREE,
  CONSTRAINT `tbl_levelthr_ibfk_1` FOREIGN KEY (`lvltwo_id`) REFERENCES `tbl_leveltwo` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_levelthr_ibfk_2` FOREIGN KEY (`lvlone_id`) REFERENCES `tbl_levelone` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=493 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_leveltwo`
--

DROP TABLE IF EXISTS `tbl_leveltwo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_leveltwo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lvlone_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `lvlone_id` (`lvlone_id`) USING BTREE,
  CONSTRAINT `tbl_leveltwo_ibfk_1` FOREIGN KEY (`lvlone_id`) REFERENCES `tbl_levelone` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_onhold_operation`
--

DROP TABLE IF EXISTS `tbl_onhold_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_onhold_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) DEFAULT NULL,
  `start_description` text DEFAULT NULL,
  `on_hold_start` datetime DEFAULT NULL,
  `start_user` int(11) DEFAULT NULL,
  `on_hold_end` datetime DEFAULT NULL,
  `end_user` int(11) DEFAULT NULL,
  `end_description` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_outbound_apicall_mst`
--

DROP TABLE IF EXISTS `tbl_outbound_apicall_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_outbound_apicall_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_userid` int(11) DEFAULT NULL,
  `agent_sipid` int(11) DEFAULT NULL,
  `cus_id` bigint(20) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `channel_name` varchar(200) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `source` varchar(200) DEFAULT NULL,
  `product` varchar(200) DEFAULT NULL,
  `disposition` varchar(200) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `rmk_datetime` datetime DEFAULT NULL,
  `call_status` varchar(200) DEFAULT NULL,
  `linkedid` varchar(45) DEFAULT NULL,
  `uniqueid` varchar(45) DEFAULT NULL,
  `com_id` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_pay_opt`
--

DROP TABLE IF EXISTS `tbl_pay_opt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pay_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_opt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_phone_bk`
--

DROP TABLE IF EXISTS `tbl_phone_bk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_phone_bk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_id` int(11) DEFAULT NULL,
  `contact_type` varchar(20) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `contact_one` int(11) DEFAULT NULL,
  `contact_two` int(11) DEFAULT NULL,
  `contact_three` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cre_user_id` int(11) DEFAULT NULL,
  `cre_date_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_prefix_notification`
--

DROP TABLE IF EXISTS `tbl_prefix_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_prefix_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `from` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `greet_content` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qn_mst`
--

DROP TABLE IF EXISTS `tbl_qn_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qn_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qn_name` varchar(255) DEFAULT NULL,
  `qntype_id` int(11) DEFAULT NULL,
  `tags` text DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `modified_userid` int(11) DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qnans_det`
--

DROP TABLE IF EXISTS `tbl_qnans_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qnans_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qnans_id` int(11) DEFAULT NULL,
  `qn_id` int(11) DEFAULT NULL,
  `qn_ans` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qnans_mst`
--

DROP TABLE IF EXISTS `tbl_qnans_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qnans_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_id` int(11) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `primary_contact` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `disp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qnopt_det`
--

DROP TABLE IF EXISTS `tbl_qnopt_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qnopt_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qn_id` int(11) DEFAULT NULL,
  `qnopt_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qnsrvy_det`
--

DROP TABLE IF EXISTS `tbl_qnsrvy_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qnsrvy_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qnsrvy_id` int(11) NOT NULL,
  `qn_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qnsrvy_mst`
--

DROP TABLE IF EXISTS `tbl_qnsrvy_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qnsrvy_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qnsrvy_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `modified_userid` int(11) DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_qntype_mst`
--

DROP TABLE IF EXISTS `tbl_qntype_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qntype_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qntype_name` varchar(255) DEFAULT NULL,
  `elmt_type` varchar(100) DEFAULT NULL,
  `minoptcount` int(11) DEFAULT NULL,
  `maxoptcount` int(11) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_queue_day_report`
--

DROP TABLE IF EXISTS `tbl_queue_day_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_queue_day_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(10) DEFAULT NULL,
  `date_or_hour` date DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `queueid` int(11) DEFAULT NULL,
  `cur_date` date DEFAULT NULL,
  `descr` varchar(50) DEFAULT NULL,
  `offerd_calls` int(11) DEFAULT NULL,
  `answer_calls` int(11) DEFAULT NULL,
  `q_breakout_calls` int(11) DEFAULT NULL,
  `answer_times` varchar(10) DEFAULT NULL,
  `avg_answer_times` varchar(10) DEFAULT NULL,
  `answer_acwtimes` varchar(10) DEFAULT NULL,
  `avg_answer_acwtimes` varchar(10) DEFAULT NULL,
  `tot_ht` varchar(20) DEFAULT NULL,
  `answer_call_sl` int(11) DEFAULT NULL,
  `tot_abn_calls` int(11) DEFAULT NULL,
  `tot_abn_calls_sl` int(11) DEFAULT NULL,
  `service_level` varchar(20) DEFAULT NULL,
  `tot_abn_rate` varchar(20) DEFAULT NULL,
  `tot_abn_rate_sl` varchar(20) DEFAULT NULL,
  `tot_abn_rate_outofsl` varchar(20) DEFAULT NULL,
  `hold_calls` int(11) DEFAULT NULL,
  `hold_time` varchar(10) DEFAULT NULL,
  `avg_hold_time` varchar(10) DEFAULT NULL,
  `transout_calls` int(11) DEFAULT NULL,
  `asa` varchar(20) DEFAULT NULL,
  `cur_datetime` datetime DEFAULT NULL,
  `hour` int(11) DEFAULT NULL,
  `ans_calls` int(11) DEFAULT NULL,
  `ring_sec_count` varchar(20) DEFAULT NULL,
  `answer_sec_count` int(10) unsigned DEFAULT NULL,
  `acw_sec_count` int(10) unsigned DEFAULT NULL,
  `hold_sec_count` int(10) unsigned DEFAULT NULL,
  `ring_sec_count_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30818 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_queue_hrly_report`
--

DROP TABLE IF EXISTS `tbl_queue_hrly_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_queue_hrly_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(10) DEFAULT NULL,
  `date_or_hour` varchar(200) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `queueid` int(11) DEFAULT NULL,
  `cur_date` varchar(20) DEFAULT NULL,
  `descr` varchar(50) DEFAULT NULL,
  `offerd_calls` int(11) DEFAULT NULL,
  `answer_calls` int(11) DEFAULT NULL,
  `q_breakout_calls` int(11) DEFAULT NULL,
  `answer_times` varchar(10) DEFAULT NULL,
  `avg_answer_times` varchar(10) DEFAULT NULL,
  `answer_acwtimes` varchar(10) DEFAULT NULL,
  `avg_answer_acwtimes` varchar(10) DEFAULT NULL,
  `tot_ht` varchar(20) DEFAULT NULL,
  `answer_call_sl` int(11) DEFAULT NULL,
  `tot_abn_calls` int(11) DEFAULT NULL,
  `tot_abn_calls_sl` int(11) DEFAULT NULL,
  `service_level` varchar(20) DEFAULT NULL,
  `tot_abn_rate` varchar(20) DEFAULT NULL,
  `tot_abn_rate_sl` varchar(20) DEFAULT NULL,
  `tot_abn_rate_outofsl` varchar(20) DEFAULT NULL,
  `hold_calls` int(11) DEFAULT NULL,
  `hold_time` varchar(10) DEFAULT NULL,
  `avg_hold_time` varchar(10) DEFAULT NULL,
  `transout_calls` int(11) DEFAULT NULL,
  `asa` varchar(20) DEFAULT NULL,
  `cur_datetime` datetime DEFAULT NULL,
  `hour` int(11) DEFAULT NULL,
  `ans_calls` int(11) DEFAULT NULL,
  `ring_sec_count` varchar(20) DEFAULT NULL,
  `answer_sec_count` int(10) unsigned DEFAULT NULL,
  `acw_sec_count` int(10) unsigned DEFAULT NULL,
  `hold_sec_count` int(10) unsigned DEFAULT NULL,
  `ring_sec_count_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41917 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_queue_summary_report`
--

DROP TABLE IF EXISTS `tbl_queue_summary_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_queue_summary_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(10) DEFAULT NULL,
  `date_or_hour` date DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `queueid` int(11) DEFAULT NULL,
  `cur_date` date DEFAULT NULL,
  `descr` varchar(50) DEFAULT NULL,
  `offerd_calls` int(11) DEFAULT NULL,
  `answer_calls` int(11) DEFAULT NULL,
  `q_breakout_calls` int(11) DEFAULT NULL,
  `answer_times` varchar(10) DEFAULT NULL,
  `avg_answer_times` varchar(10) DEFAULT NULL,
  `answer_acwtimes` varchar(10) DEFAULT NULL,
  `avg_answer_acwtimes` varchar(10) DEFAULT NULL,
  `tot_ht` varchar(20) DEFAULT NULL,
  `answer_call_sl` int(11) DEFAULT NULL,
  `tot_abn_calls` int(11) DEFAULT NULL,
  `tot_abn_calls_sl` int(11) DEFAULT NULL,
  `service_level` varchar(20) DEFAULT NULL,
  `tot_abn_rate` varchar(20) DEFAULT NULL,
  `tot_abn_rate_sl` varchar(20) DEFAULT NULL,
  `tot_abn_rate_outofsl` varchar(20) DEFAULT NULL,
  `hold_calls` int(11) DEFAULT NULL,
  `hold_time` varchar(10) DEFAULT NULL,
  `avg_hold_time` varchar(10) DEFAULT NULL,
  `transout_calls` int(11) DEFAULT NULL,
  `asa` varchar(20) DEFAULT NULL,
  `cur_datetime` datetime DEFAULT NULL,
  `hour` int(11) DEFAULT NULL,
  `ans_calls` int(11) DEFAULT NULL,
  `ring_sec_count` varchar(20) DEFAULT NULL,
  `answer_sec_count` int(10) unsigned DEFAULT NULL,
  `acw_sec_count` int(10) unsigned DEFAULT NULL,
  `hold_sec_count` int(10) unsigned DEFAULT NULL,
  `ring_sec_count_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38009 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_remind_mst`
--

DROP TABLE IF EXISTS `tbl_remind_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_remind_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) DEFAULT NULL,
  `rem_datetime` datetime DEFAULT NULL,
  `rem_des` varchar(255) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ren_send_opt`
--

DROP TABLE IF EXISTS `tbl_ren_send_opt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ren_send_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_meth` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sendsms_mst`
--

DROP TABLE IF EXISTS `tbl_sendsms_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sendsms_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(30) DEFAULT NULL,
  `customer_id` varchar(30) DEFAULT NULL,
  `to` varchar(30) DEFAULT NULL,
  `message` varchar(200) DEFAULT NULL,
  `remark` varchar(200) NOT NULL,
  `com_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sms_batch_det`
--

DROP TABLE IF EXISTS `tbl_sms_batch_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sms_batch_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sms_batch_mst`
--

DROP TABLE IF EXISTS `tbl_sms_batch_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sms_batch_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `cre_user` varchar(255) DEFAULT NULL,
  `cre_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sms_camp_mst`
--

DROP TABLE IF EXISTS `tbl_sms_camp_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sms_camp_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_cmpgn_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cre_date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sms_cmpgn_det`
--

DROP TABLE IF EXISTS `tbl_sms_cmpgn_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sms_cmpgn_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmpign_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT '0',
  `attempt` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `sms_batch_det_id` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_smssrv_receive`
--

DROP TABLE IF EXISTS `tbl_smssrv_receive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_smssrv_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(15) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `ref_no_num` varchar(11) DEFAULT NULL,
  `from_num` varchar(255) DEFAULT NULL,
  `to_num` text DEFAULT NULL,
  `msg` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rec_datetime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `cre_datetime` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Fresh',
  `update_datetime` datetime DEFAULT NULL,
  `com_id` char(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_smssrv_send`
--

DROP TABLE IF EXISTS `tbl_smssrv_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_smssrv_send` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(15) DEFAULT NULL,
  `to_num` text DEFAULT NULL,
  `msg` text DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_softlogiclife_disp_remark`
--

DROP TABLE IF EXISTS `tbl_softlogiclife_disp_remark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_softlogiclife_disp_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_softlogiclife_prod_remark`
--

DROP TABLE IF EXISTS `tbl_softlogiclife_prod_remark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_softlogiclife_prod_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_softlogiclife_src_remark`
--

DROP TABLE IF EXISTS `tbl_softlogiclife_src_remark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_softlogiclife_src_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srv_usrs`
--

DROP TABLE IF EXISTS `tbl_srv_usrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srv_usrs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `srvbox_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `com_id` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvbox_mst`
--

DROP TABLE IF EXISTS `tbl_srvbox_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvbox_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `box_name` text NOT NULL,
  `EmailPassword` varchar(60) NOT NULL,
  `r_EmailProtocol` varchar(60) NOT NULL,
  `r_EmailHost` varchar(60) NOT NULL,
  `r_EmailPort` int(11) NOT NULL,
  `s_SMTPAuth` varchar(10) DEFAULT NULL,
  `s_SMTPSecure` varchar(50) DEFAULT NULL,
  `s_Host` varchar(50) DEFAULT NULL,
  `s_Port` int(11) DEFAULT NULL,
  `AttachmentLocations` text NOT NULL,
  `com_id` int(11) NOT NULL,
  `cat_id` char(1) DEFAULT NULL,
  `twilioaccountsid` varchar(35) DEFAULT NULL,
  `twilioauthToken` varchar(35) DEFAULT NULL,
  `twiliochatserviceid` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cat_id` (`cat_id`) USING BTREE,
  KEY `com_id` (`com_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvchat_det`
--

DROP TABLE IF EXISTS `tbl_srvchat_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvchat_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_RefNo` varchar(11) DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `author` varchar(40) DEFAULT NULL,
  `channel` varchar(30) DEFAULT NULL,
  `Direction` varchar(15) DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `AccountSid` varchar(100) DEFAULT NULL,
  `channelID` varchar(100) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Messageid` varchar(100) DEFAULT NULL,
  `DateSent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=887 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvchat_mst`
--

DROP TABLE IF EXISTS `tbl_srvchat_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvchat_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_RefNo` varchar(20) NOT NULL,
  `from_chat_num` varchar(30) NOT NULL,
  `Customer_id` int(11) DEFAULT NULL,
  `Chat_Subject` varchar(100) DEFAULT NULL,
  `chat_Status` varchar(15) NOT NULL,
  `chat_comment` varchar(50) DEFAULT NULL,
  `t_status` text DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `cre_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `update_userid` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT 1,
  `viewStatus` varchar(20) DEFAULT 'Seen',
  `author` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvmessenger_det`
--

DROP TABLE IF EXISTS `tbl_srvmessenger_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvmessenger_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_RefNo` varchar(11) DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `NumMedia` int(11) DEFAULT NULL,
  `Name` text DEFAULT NULL,
  `Extension` varchar(20) DEFAULT NULL,
  `From` varchar(30) NOT NULL,
  `Direction` varchar(15) NOT NULL,
  `DateUpdated` datetime NOT NULL,
  `AccountSid` varchar(100) NOT NULL,
  `To` varchar(30) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Status` varchar(15) NOT NULL,
  `Messageid` varchar(100) NOT NULL,
  `DateSent` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvmessenger_mst`
--

DROP TABLE IF EXISTS `tbl_srvmessenger_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvmessenger_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_RefNo` varchar(20) NOT NULL,
  `from` varchar(30) NOT NULL,
  `fb_Status` varchar(15) NOT NULL,
  `fb_comment` varchar(50) NOT NULL,
  `t_status` text NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `cre_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_userid` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL DEFAULT 1,
  `viewStatus` varchar(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvsms_det`
--

DROP TABLE IF EXISTS `tbl_srvsms_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvsms_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m_RefNo` varchar(11) DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `From` varchar(30) NOT NULL,
  `To` varchar(30) NOT NULL,
  `Direction` varchar(15) NOT NULL,
  `DateUpdated` datetime NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Status` varchar(15) NOT NULL,
  `Messageid` varchar(100) NOT NULL,
  `DateSent` datetime NOT NULL,
  `from_sms_num` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvsms_mst`
--

DROP TABLE IF EXISTS `tbl_srvsms_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvsms_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m_RefNo` varchar(20) NOT NULL,
  `from_sms_num` varchar(30) NOT NULL,
  `Customer_id` int(11) NOT NULL,
  `m_Status` varchar(15) NOT NULL,
  `m_comment` varchar(50) NOT NULL,
  `t_status` text NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `cre_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_userid` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL DEFAULT 1,
  `viewStatus` varchar(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvwac_det`
--

DROP TABLE IF EXISTS `tbl_srvwac_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvwac_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `w_RefNo` varchar(11) DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `NumMedia` int(11) DEFAULT NULL,
  `Name` text DEFAULT NULL,
  `Extension` varchar(20) DEFAULT NULL,
  `From` varchar(30) NOT NULL,
  `Direction` varchar(15) NOT NULL,
  `DateUpdated` datetime NOT NULL,
  `AccountSid` varchar(100) NOT NULL,
  `To` varchar(30) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Status` varchar(15) NOT NULL,
  `Messageid` varchar(100) NOT NULL,
  `DateSent` datetime NOT NULL,
  `from_wac_num` varchar(30) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4023 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_srvwac_mst`
--

DROP TABLE IF EXISTS `tbl_srvwac_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_srvwac_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `w_RefNo` varchar(20) NOT NULL,
  `from_wac_num` varchar(30) NOT NULL,
  `Customer_id` int(11) NOT NULL,
  `w_Subject` varchar(100) NOT NULL,
  `w_Status` varchar(15) NOT NULL,
  `w_comment` varchar(50) NOT NULL,
  `t_status` text NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `cre_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_userid` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL DEFAULT 1,
  `viewStatus` varchar(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_stremarks`
--

DROP TABLE IF EXISTS `tbl_stremarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stremarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `st_remarks` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `add_user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  `add_dt` datetime DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_subremarks`
--

DROP TABLE IF EXISTS `tbl_subremarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subremarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `sub_remark` varchar(300) NOT NULL,
  `status` char(1) NOT NULL,
  `add_user` int(11) NOT NULL,
  `update_user` int(11) NOT NULL,
  `create_data` datetime NOT NULL,
  `update date` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sup_com_allo`
--

DROP TABLE IF EXISTS `tbl_sup_com_allo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sup_com_allo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sup_id` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_supusr_com_allo`
--

DROP TABLE IF EXISTS `tbl_supusr_com_allo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_supusr_com_allo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supusr_userid` int(11) NOT NULL,
  `com_id` int(11) NOT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`supusr_userid`,`com_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_sync_client_info`
--

DROP TABLE IF EXISTS `tbl_sync_client_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sync_client_info` (
  `Id` int(11) NOT NULL,
  `Client_ID_No` int(11) DEFAULT NULL,
  `Client_Title` varchar(15) DEFAULT NULL,
  `Client_First_Name` varchar(100) DEFAULT NULL,
  `Client_Last_Name` varchar(100) DEFAULT NULL,
  `Client_Address` text DEFAULT NULL,
  `Phone _No` varchar(15) DEFAULT NULL,
  `Client_Type` int(11) DEFAULT NULL,
  `Facility_Number` int(11) DEFAULT NULL,
  `Next_Rental_Amount` varchar(15) DEFAULT NULL,
  `Next_Due_Date` date DEFAULT NULL,
  `No_of_Arrears` int(11) DEFAULT NULL,
  `Arrears_Amount` varchar(20) DEFAULT NULL,
  `To_be_Paid_Amount` varchar(20) DEFAULT NULL,
  `Last Pay Date` date DEFAULT NULL,
  `Last_Pay_Date` date DEFAULT NULL,
  `Last_Pay_Amount` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ticket_comment`
--

DROP TABLE IF EXISTS `tbl_ticket_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ticket_comment` (
  `ticket_id` int(11) NOT NULL,
  `lvltwo_id` int(11) NOT NULL,
  `lvlthr_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('Fresh','Work In Progress','Closed','Return','Cancelled') NOT NULL,
  `comment` text DEFAULT NULL,
  `date_time` datetime NOT NULL,
  `returnStatus` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `uploadPath` varchar(500) DEFAULT NULL,
  KEY `ticket_id` (`ticket_id`) USING BTREE,
  KEY `lvltwo_id` (`lvltwo_id`) USING BTREE,
  KEY `lvlthr_id` (`lvlthr_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `tbl_ticket_comment_ibfk_1` FOREIGN KEY (`ticket_id`) REFERENCES `tbl_ticket_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_comment_ibfk_2` FOREIGN KEY (`lvltwo_id`) REFERENCES `tbl_leveltwo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_comment_ibfk_3` FOREIGN KEY (`lvlthr_id`) REFERENCES `tbl_levelthr` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_comment_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ticket_master`
--

DROP TABLE IF EXISTS `tbl_ticket_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ticket_master` (
  `ticket_num` varchar(15) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `lvltwo_id` int(11) NOT NULL,
  `lvlthr_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lvlone_id` int(11) NOT NULL,
  `remark` text NOT NULL,
  `status` enum('Fresh','Closed','Work In Progress','Return','Cancelled','Complete','On Hold') NOT NULL,
  `priority` enum('Normal','High','Critical','Minimal') NOT NULL,
  `kpilevel_id` int(11) DEFAULT NULL,
  `kpil_des` varchar(255) DEFAULT NULL,
  `l1_kpi` int(11) DEFAULT NULL,
  `l2_kpi` int(11) DEFAULT NULL,
  `l3_kpi` int(11) DEFAULT NULL,
  `tktUser` int(11) DEFAULT NULL,
  `complete_remark` text DEFAULT NULL,
  `completeUser` int(11) DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `secondary_contact2` int(11) DEFAULT NULL,
  `secondary_contact` int(11) DEFAULT NULL,
  `primary_contact` int(11) DEFAULT NULL,
  `tickettype` varchar(255) DEFAULT NULL,
  `ticketcapturemode` varchar(255) DEFAULT NULL,
  `orderreference` varchar(255) DEFAULT '',
  `referenceperson` varchar(255) DEFAULT '',
  `call_uid` varchar(50) DEFAULT NULL,
  `returnstatus` char(1) DEFAULT '0',
  `escalated_usr` varchar(255) DEFAULT NULL,
  `esc_datetime` datetime DEFAULT NULL,
  `status_of_esc` enum('YES','NO') DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `ticket_num` (`ticket_num`) USING BTREE,
  KEY `fk_ticket_master_department_master1_idx` (`lvltwo_id`) USING BTREE,
  KEY `fk_ticket_master_subdepartment_master1_idx` (`lvlthr_id`) USING BTREE,
  KEY `fk_ticket_master_contact_master1_idx` (`contact_id`) USING BTREE,
  KEY `fk_csp_ticket_master_user1_idx` (`user_id`) USING BTREE,
  KEY `fk_category` (`lvlone_id`) USING BTREE,
  KEY `kpilevel_id` (`kpilevel_id`) USING BTREE,
  CONSTRAINT `tbl_ticket_master_ibfk_1` FOREIGN KEY (`lvltwo_id`) REFERENCES `tbl_leveltwo` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_master_ibfk_2` FOREIGN KEY (`lvlthr_id`) REFERENCES `tbl_levelthr` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_master_ibfk_3` FOREIGN KEY (`lvlone_id`) REFERENCES `tbl_levelone` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_master_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_ticket_master_ibfk_5` FOREIGN KEY (`kpilevel_id`) REFERENCES `tbl_kpilevel_info` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tmldr_agnt_allo`
--

DROP TABLE IF EXISTS `tbl_tmldr_agnt_allo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tmldr_agnt_allo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmldr_userid` int(11) NOT NULL,
  `agnt_userid` int(11) NOT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`tmldr_userid`,`agnt_userid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tmldr_queue_allo`
--

DROP TABLE IF EXISTS `tbl_tmldr_queue_allo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tmldr_queue_allo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmldr_userid` int(11) NOT NULL,
  `queue_id` int(11) NOT NULL,
  `cre_datetime` datetime DEFAULT NULL,
  `cre_userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`tmldr_userid`,`queue_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_type_one`
--

DROP TABLE IF EXISTS `tbl_type_one`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_type_one` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_type_two`
--

DROP TABLE IF EXISTS `tbl_type_two`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_type_two` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_user_allocation`
--

DROP TABLE IF EXISTS `tbl_user_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allo_user` int(11) DEFAULT NULL,
  `level_one` varchar(1000) DEFAULT NULL,
  `level_two` varchar(1000) DEFAULT NULL,
  `level_three` varchar(1000) DEFAULT NULL,
  `add_dt` datetime DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `update_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `allo_user` (`allo_user`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_user_auth_log`
--

DROP TABLE IF EXISTS `tbl_user_auth_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_auth_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event` varchar(100) NOT NULL,
  `des` text NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=283082 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test campaign`
--

DROP TABLE IF EXISTS `test campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `contact_no` varchar(45) DEFAULT NULL,
  `contact_no2` varchar(45) DEFAULT NULL,
  `contact_no3` varchar(45) DEFAULT NULL,
  `branch` varchar(50) NOT NULL,
  `fac_no` varchar(45) NOT NULL,
  `clicode` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `c_name` varchar(100) DEFAULT NULL,
  `fac_type` varchar(45) DEFAULT NULL,
  `fac_amount` varchar(45) DEFAULT NULL,
  `rent_amount` varchar(45) DEFAULT NULL,
  `tenor` varchar(45) DEFAULT NULL,
  `rent_due_date` varchar(45) DEFAULT NULL,
  `fac_status` varchar(45) DEFAULT NULL,
  `v_no` varchar(45) DEFAULT NULL,
  `ast_type` varchar(45) DEFAULT NULL,
  `m_officer` varchar(45) DEFAULT NULL,
  `lease_age` varchar(45) DEFAULT NULL,
  `rent_due` varchar(45) DEFAULT NULL,
  `other_charge` varchar(45) DEFAULT NULL,
  `in_charges` varchar(45) DEFAULT NULL,
  `tot_ar` varchar(45) DEFAULT NULL,
  `last_pay_date` varchar(45) DEFAULT NULL,
  `last_pay_amt` varchar(45) DEFAULT NULL,
  `mat_rent` varchar(45) DEFAULT NULL,
  `g1name` varchar(100) DEFAULT NULL,
  `g1code` varchar(45) DEFAULT NULL,
  `g1phone1` varchar(45) DEFAULT NULL,
  `g1phone2` varchar(45) DEFAULT NULL,
  `g2name` varchar(100) DEFAULT NULL,
  `g2phone1` varchar(45) DEFAULT NULL,
  `g2phone2` varchar(45) DEFAULT NULL,
  `g2code` varchar(45) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `camp_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fac_no`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transfer_log`
--

DROP TABLE IF EXISTS `transfer_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transfer_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sip_id` int(11) NOT NULL,
  `cli` int(11) NOT NULL,
  `callid_original` varchar(45) NOT NULL,
  `transfer_cli` int(11) NOT NULL,
  `callid_transfer` varchar(45) NOT NULL,
  `transfer_time` datetime NOT NULL,
  `direction` enum('IN','OUT') NOT NULL,
  `queueid` int(11) NOT NULL,
  `campaign_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_transfer_log_user_master_idx` (`user_id`) USING BTREE,
  CONSTRAINT `transfer_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_master`
--

DROP TABLE IF EXISTS `user_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_master` (
  `userstatus` char(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `user_type_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `login_status` int(10) unsigned DEFAULT 1,
  `live_status` int(10) unsigned NOT NULL DEFAULT 0,
  `recordble` int(11) NOT NULL,
  `sin` tinyint(4) DEFAULT 0,
  `tam` tinyint(4) DEFAULT 0,
  `eng` tinyint(4) DEFAULT 0,
  `com_id` int(10) unsigned DEFAULT 0,
  `email` varchar(255) DEFAULT NULL,
  `contactno` int(11) DEFAULT NULL,
  `auth_method` int(11) DEFAULT NULL,
  `auto_answer` enum('sup_yes','sup_no','agnt_op') DEFAULT 'agnt_op',
  `ringing_type` enum('silence','normal','laud') DEFAULT 'laud',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_user_master_user_type_idx` (`user_type_id`) USING BTREE,
  CONSTRAINT `fk_user_master_user_type` FOREIGN KEY (`user_type_id`) REFERENCES `user_type_list` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_privilege`
--

DROP TABLE IF EXISTS `user_privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_privilege` (
  `user_type_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `view` tinyint(1) DEFAULT NULL,
  `add` tinyint(1) DEFAULT NULL,
  `edit` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  `print` int(11) NOT NULL,
  PRIMARY KEY (`user_type_id`,`section_id`) USING BTREE,
  KEY `fk_user_type_list_has_section_section1_idx` (`section_id`) USING BTREE,
  KEY `fk_user_type_list_has_section_user_type_list1_idx` (`user_type_id`) USING BTREE,
  CONSTRAINT `user_privilege_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_privilege_ibfk_2` FOREIGN KEY (`user_type_id`) REFERENCES `user_type_list` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_type_list`
--

DROP TABLE IF EXISTS `user_type_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `description` varchar(1000) NOT NULL,
  `csp_agent` char(1) DEFAULT '0',
  `csp_sup` char(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `working_hours`
--

DROP TABLE IF EXISTS `working_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `working_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `days` varchar(50) DEFAULT NULL,
  `noofhours` double(5,2) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-04 14:35:23
