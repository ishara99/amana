<?php
/**
 * Created by PhpStorm.
 * User: Prashan
 * Date: 24-Jul-17
 * Time: 9:21 AM
 */
namespace App\Http\Controllers;
use App\Http\Util;
use Carbon\Carbon;
use Session;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class NewCallLogController
{

public function addcalllog_for_num_console(){
        if(Util::isAuthorized("addcalllog")=='LOGGEDOUT'){
            return redirect('/');
        }
        /*if(Util::isAuthorized("addcalllog")=='DENIED'){
            return view('permissiondenide');
        }*/
        Util::log("Add Call Log for Number Console","View");
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User open the Add Call Log for Number Form from Console",$username,"View Add Call log for Number Console");

        $get_details = DB::table('tbl_calls_evnt')
                    ->select('*')
                    ->where('linkedid',$_GET['linkedid'])
                    ->orderBy('id','desc')
                    ->first();
    
        if($get_details){
            $call_type=$get_details->call_type;

            if($call_type=='outbound'){
                $number=$get_details->to_caller_num;
            }else{
                $number=$get_details->frm_caller_num;
            }
        }else{
            $call_type='';
            $number='';
        }


        return view('AddCallLog_for_num_console')
            ->with('presets',(new PresetRemarkController())->retrieveAll())
            ->with('number',$number)
            ->with('call_type',$call_type)
            ->with('linkedid',$_GET['linkedid'])
        ->with('type_one',(new PresetRemarkController())->retrieveTypeOneData())
        ->with('type_two',(new PresetRemarkController())->retrieveTypeTwoData());
    }

public function Save_Call_Log_for_num_console(Request $request){
     
          
    
    $callType =  $request->input('callType');
    $preset1 =  $request->input('category');
    $preset2 =  $request->input('inquiry');
    $remark =  $request->input('remark');
    $linkedid =  $request->input('linkedid');
    $contact_num =  $request->input('contact_num');
    // print_r($cus_id); exit();
    $datentime = Carbon::now();
    $userid=session('userid');
    $endpoint = session('endpoint');

    $com_id = util::get_com_id_by_user($userid);

    if($preset1 == "All"){

        $preset1 = "";
    }
    
    if($preset2 == "All"){

        $preset2 = "";
    }

    $get_hist = DB::table('csp_callhistory')
                    ->select('*')
                    ->where('unq_id',$linkedid)
                    ->where('sipid',$endpoint)
                    ->orderBy('id','desc')
                    ->first();

    if($get_hist){
            $id=$get_hist->id;

            DB::table('csp_callhistory')
                    ->where('id',$id)
                    ->update([
                           'cat_one_prerem_id' => $preset1,
                           'cat_two_prerem_id' => $preset2,
                           'call_log' => "$remark",
                           'updated_datetime' => NOW()
                    ]);

    }else{
            $saveinfo=DB::table('csp_callhistory')->insert(
                ['log_type' => $callType,
                'unq_id' => $linkedid,
                'sipid' => $endpoint,
                'cat_one_prerem_id' => $preset1,
                'cat_two_prerem_id' => $preset2,
                'call_log' => "$remark",
                'pho_number' => "$contact_num",
                'created_datetime' => $datentime,
                'created_userid' => $userid,
                'com_id' => $com_id,
                ]

        );

    }

    $get_hist_detail = DB::table('csp_callhistory_detail')
                    ->select('*')
                    ->where('unq_id',$linkedid)
                    ->where('sipid',$endpoint)
                    ->orderBy('id','desc')
                    ->first();

    if($get_hist_detail){
            $id=$get_hist_detail->id;

            DB::table('csp_callhistory_detail')
                    ->where('id',$id)
                    ->update([
                           'cat_one_prerem_id' => $preset1,
                           'cat_two_prerem_id' => $preset2,
                           'call_log' => "$remark",
                           'updated_datetime' => NOW()
                    ]);

    }else{

                $datalog = array(
                    'pho_number' => "$contact_num",
                    'log_type'=>$callType,
                    'unq_id' => $linkedid,
                    'sipid' => $endpoint,
                    'cat_one_prerem_id' => $preset1,
                    'cat_two_prerem_id' => $preset2,
                    'call_log'=>"$remark",
                    'created_userid' => $userid,
                    'created_datetime' => NOW(),
                    'com_id' => $com_id,
                    
                );
            
                $insert=DB::table('csp_callhistory_detail')
                            ->insert($datalog);
    }

    if(true){
        Session::put('flash_message','successfully saved.');
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Add Call Log for Number",$username,"Add Call Log for Number");

        return redirect()->back();
        
          
        }
    }

public function check_call_befor_log() {
    $endpoint = session('endpoint');

    $getCallInfo = DB::select("SELECT * FROM `asterisk`.`ps_contacts` WHERE LENGTH(`linkedid`)>0 AND `endpoint`='$endpoint' " );

    if($getCallInfo){
        echo $getCallInfo[0]->linkedid;
    }else{
        echo "ERROR";
    }
}




}