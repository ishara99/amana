<!DOCTYPE html>
@include('header_new')
{{--{{dd($contact)}}--}}
<style>
    .rem-pad {
        padding-right: 0px;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="form_caption">Add Call Log for Number</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                <form class="form-horizontal" method="POST" id="form_new" name="form_new" action="Save_Call_Log_for_num_console" autocomplete="off">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('flash_message'))

                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <input type="hidden" id="sessionMessage" data-initial-value="" name="sessionMessage" value="{!! session('flash_message') !!}">
                        {{ Session::forget('flash_message') }}
                        @endif      

                        @if ($errors->any())
                            <div class="alert alert-danger">

                                <ul>

                                    @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>
                        @endif
    
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{csrf_field()}}
						</div>
					
                        <div class="col-lg-8 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Call Type</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                        <select class="form-control custom-select-value" readonly style="width:120%" id="callType" name="callType" >
                                            <option value="">Select a Call Type</option>
                                            <option <?php if($call_type=='Inbound'){ echo "selected"; } ?> value="Inbound">Inbound</option>
                                            <option <?php if($call_type=='outbound'){ echo "selected"; } ?> value="Outbound">Outbound</option>
                                        </select>  
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required">Contact Number</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                        <input type="hidden" class="form-control" name="linkedid" id="linkedid" value="{{$linkedid}}" />
                                        <input type="text" class="form-control" style="width:120%" name="contact_num" id="contact_num" readonly required value="{{$number}}" />
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Preset Remark 1</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					                    <select class="form-control custom-select-value" style="width:120%" id="category" name="category" onchange = "addPreset('category')">
                                            <option value="All">Select Preset Remark 1</option>
						                    @foreach($type_one as $category)
                                                <option value="{{$category->id}}">{{$category->remark}}</option>
                                            @endforeach
							            </select>  
                                    </div>
                               </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro">Preset Remark 2</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					                    <select class="form-control custom-select-value" style="width:120%" id="inquiry" name="inquiry" onchange = "addPreset('inquiry')">
                                            <option value="All">Select Preset Remark 2</option>
						                    @foreach($type_two as $inquiry)
                                                <option value="{{$inquiry->id}}">{{$inquiry->remark}}</option>
                                            @endforeach
							            </select>
                                    </div>
                                   
                                </div>
                            </div>
                           <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro required">Remark</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                        <textarea class="form-control" style="padding-bottom: 10px ; width:120%" rows="4" name="remark" id="remark" required></textarea>
                                    </div>
                               </div>
                            </div> 
                            
                        </div>
                        
                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                        </div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" style="margin-top:30px; margin-left:225px;" >
                                <div class="button-style-four btn-mg-b-10">
                                    <button type="sumbit" class="btn btn-custon-four btn-success attr_btn" name ="submit">Save</button>
                                    <button type="button" class="btn btn-custon-four btn-danger attr_btn" onclick="cancelform()">Cancel</button>   
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-2 col-sm-12 col-xs-12">
                            </div>
                        </div>                                         
                        </form>
                    </div>      
                </div>                                               
            
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->









<script>
   
    
    function refresh() {
        location.reload(); 
    }
    function cancelform(){
    $.ajax({
        url: '{{url('cancelform')}}',
        type: 'GET',
        data: {formName:'Add Call Log'},
        success: function (response)
        {
           if(response==1){
            location.href='{{url("contacts")}}'+'?contactid=all&status=All' ;
           }else{
               //
           }
        }
           
           
        });
    }

    function addPreset(remark){
        var totalRemark = document.getElementById('remark');
        var secondrem = $("#"+remark).find('option:selected').text();

        totalRemark.value = totalRemark.value
            + ((totalRemark.value=='') ? '' : ' - ')
            + secondrem;
            // document.getElementById('preset1').value="";
            // document.getElementById('preset2').value="";
    }
 
    
</script>

@include('footer')

</body>
</html>

