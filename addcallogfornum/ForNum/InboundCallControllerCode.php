<?php
/**
 * Created by PhpStorm.
 * User: Prashan
 * Date: 24-Jul-17
 * Time: 9:21 AM
 */


public function addcalllog_for_num(){
        if(Util::isAuthorized("addcalllog")=='LOGGEDOUT'){
            return redirect('/');
        }
        /*if(Util::isAuthorized("addcalllog")=='DENIED'){
            return view('permissiondenide');
        }*/
        Util::log("Add Call Log for Number","View");
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User open the Add Call Log for Number Form",$username,"View Add Call log for Number");

        return view('AddCallLog_for_num')
            ->with('presets',(new PresetRemarkController())->retrieveAll())
        ->with('type_one',(new PresetRemarkController())->retrieveTypeOneData())
        ->with('type_two',(new PresetRemarkController())->retrieveTypeTwoData());
    }

public function Save_Call_Log_for_num(Request $request){
     
          
    
    $callType =  $request->input('callType');
    $preset1 =  $request->input('category');
    $preset2 =  $request->input('inquiry');
    $remark =  $request->input('remark');
    $contact_num =  $request->input('contact_num');
    // print_r($cus_id); exit();
    $datentime = Carbon::now();
    $userid=session('userid');

    $com_id = util::get_com_id_by_user($userid);

    if($preset1 == "All"){

        $preset1 = "";
    }
    
    if($preset2 == "All"){

        $preset2 = "";
    }

    $saveinfo=DB::table('csp_callhistory')->insert(
        ['log_type' => $callType,
        'cat_one_prerem_id' => $preset1,
        'cat_two_prerem_id' => $preset2,
        'call_log' => "$remark",
        'pho_number' => "$contact_num",
        'created_datetime' => $datentime,
        'created_userid' => $userid,
        'com_id' => $com_id,
        ]

    );

        $datalog = array(
                    'pho_number' => "$contact_num",
                    'log_type'=>$callType,
                    'cat_one_prerem_id' => $preset1,
                    'cat_two_prerem_id' => $preset2,
                    'call_log'=>"$remark",
                    'created_userid' => $userid,
                    'created_datetime' => NOW(),
                    'com_id' => $com_id,
                    
                );
      
            
                $insert=DB::table('csp_callhistory_detail')
                            ->insert($datalog);

    if($saveinfo){
        Session::put('flash_message','successfully saved.');
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Add Call Log for Number",$username,"Add Call Log for Number");

        return redirect()->back();
        
          
    }

    
    }


