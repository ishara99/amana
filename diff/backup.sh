#!/bin/bash
#find /home/dbbackup/* -type d -ctime +3 -exec rm -rf {} \;
find /home/dbbackup/* -type d -ctime +7 -exec rm -rf {} \;
cd /home/dbbackup

FL=$(date +"%Y-%m-%d")
mkdir $FL
cd $FL
mysqldump --lock-tables=false --routines=true --databases asterisk > asterisk.sql -ppassw0rd
mysqldump --lock-tables=false --routines=true --databases asteriskcdrdb > asteriskcdrdb.sql -ppassw0rd
mysqldump --lock-tables=false --routines=true --databases phonikip_db > phonikip_db.sql -ppassw0rd


