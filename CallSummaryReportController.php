<?php
/**
 * Created by PhpStorm.
 * User: Prashan
 * Date: 02-Aug-17
 * Time: 11:12 AM
 */

namespace App\Http\Controllers;
use App\Http\Util;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CallSummaryReportController extends Controller
{
    public function view_call_sum_repo(){
         if(Util::isAuthorized("view_call_sum_repo")=='LOGGEDOUT'){
             return redirect('/');
         }
         if(Util::isAuthorized("view_call_sum_repo")=='DENIED'){
             return view('permissiondenide');
         }
         Util::log("View Call Summary Report","View");
        $userid=session('userid');
       
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getdndstatus  = DB::table('asterisk.queues_config')
                            ->select('extension','descr') 
                            ->Where('queues_config.com_id',$get_com_id->com_id)
                            ->get();

        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $users = DB::select("SELECT a.`id`,a.`username` 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where c.`title`= 'Csp_Agent' and a.`com_id`= $get_com_id->com_id group by a.`username`   ;");
      
        return view('view_call_summary_report',compact('users','getdndstatus','get_com_data'));

    }
    public function get_queue_data()
    {
		$com_id=$_GET['com_id'];
		$data =DB::SELECT('SELECT * FROM asterisk.queues_config WHERE com_id='. $com_id);
		return compact('data');
	
    }

    public function get_agent_data()
    {
        
        $com_id=$_GET['com_id'];
        $data =DB::SELECT('SELECT * FROM phonikip_db.user_master WHERE com_id='. $com_id);
        
		return compact('data');
	
    }

    public function search_call_sum_rep_by_company(Request $request)
    {  
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
        ->where('id',$userid)
        ->first();

        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $agent_id= $request->input('agent_id');
        $com_id= $request->input('com_id');
        $group_by= $request->input('groupby');

        // search by company
        if($group_by = 'company')
        {
            // check if company list all or one
            if($request->input('com_id') !== 'All') 
            {
                $condition['tbl_com_mst.id'] = $com_id;
            }else if($request->input('com_id') == 'All'){
				$condition['tbl_com_mst.id'] = $get_com_id->com_id;
			}else
            {
                $condition = array();
            }
        }

        $data = $dataq = DB::table('tbl_com_mst')
            ->select(
                'id AS agnt_queueid',
                'id AS id',
                'id AS agnt_userid',
                'tbl_com_mst.com_name',
                'tbl_com_mst.id AS company_id',
                'id AS cre_datetime',
                'id AS descr',
                'id AS username',

                // offerd_calls
                DB::raw("(SELECT COUNT( tbl_calls_evnt.id ) AS offerd_calls 
                            FROM phonikip_db.tbl_calls_evnt 
                            WHERE ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                            AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                            AND tbl_calls_evnt.agnt_queueid IN ( SELECT extension FROM asterisk.queues_config WHERE com_id = company_id ) 
                            ) AS offerd_calls"),
    
                //Answer calls           
                DB::raw("(SELECT
                COUNT( tbl_calls_evnt.id ) AS answer_calls 
                FROM
                    phonikip_db.tbl_calls_evnt 
                WHERE
                ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
                AND tbl_calls_evnt.agnt_queueid IN ( SELECT extension FROM asterisk.queues_config WHERE com_id = company_id ) 
                ) AS answer_calls"),

                //total abandon calls          
                DB::raw("(SELECT
                COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
                FROM
                    phonikip_db.tbl_calls_evnt 
                WHERE
                ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                AND tbl_calls_evnt.DESC = 'ABANDON' 
                AND tbl_calls_evnt.agnt_queueid IN ( SELECT extension FROM asterisk.queues_config WHERE com_id = company_id ) 
                ) AS tot_abn_calls"),

                //total reached abandon callback calls          
                DB::raw("(SELECT
                COUNT( ev.id ) AS tot_abn_callb_calls 
                FROM
                    phonikip_db.tbl_calls_evnt ev
                JOIN csp_callhistory csp ON csp.unq_id=ev.linkedid   
                JOIN asteriskcdrdb.cdr ON cdr.uniqueid=csp.callback_unq_id   
                WHERE
                ( ev.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                AND ev.DESC = 'ABANDON' 
                AND cdr.disposition = 'ANSWERED' 
                AND ev.agnt_queueid IN ( SELECT extension FROM asterisk.queues_config WHERE com_id = company_id ) 
                ) AS tot_abn_callb_calls"),

                //reached abandon callback rate
                DB::raw("(select ROUND(SUM((tot_abn_callb_calls)/(tot_abn_calls)*100))) as tot_abn_callb_rate"),
                
                //total abandon rate
                DB::raw("(select ROUND(SUM((tot_abn_calls)/(offerd_calls)*100))) as tot_abn_rate"),
    
                //total outbound calls
                DB::raw("(SELECT
                COUNT( tbl_calls_evnt.id ) AS tot_ob_calls 
                FROM
                    `tbl_calls_evnt` 
                WHERE
                ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                AND tbl_calls_evnt.call_type = 'outbound' 
                AND agnt_userid IN ( SELECT id FROM `user_master` WHERE com_id = company_id ) 
                ) AS tot_ob_calls"),
            
                //total outbound answer call
                DB::raw("(SELECT
                COUNT( tbl_calls_evnt.id ) AS tot_ob_ans_calls 
                FROM
                    phonikip_db.tbl_calls_evnt 
                WHERE
                ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                AND tbl_calls_evnt.call_type = 'outbound' 
                AND tbl_calls_evnt.STATUS = 'ANSWER' 
                AND agnt_userid IN ( SELECT id FROM `user_master` WHERE com_id = company_id ) 
                ) AS tot_ob_ans_calls"),
    
                //total cda calls
                DB::raw("(SELECT
                COUNT( tbl_calls_evnt.id ) AS tot_cda_calls 
                FROM
                    phonikip_db.tbl_calls_evnt 
                WHERE
                ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
                AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                AND tbl_calls_evnt.DESC = 'COMPLETEAGENT' 
                AND tbl_calls_evnt.agnt_queueid IN ( SELECT extension FROM asterisk.queues_config WHERE com_id = company_id ) 
                ) AS tot_cda_calls "));
                
                if(!empty($condition))
                {
                    $dataq = $dataq->where($condition);
                }
                    $data = $dataq ->get();
                   
		        return compact('data',$data);

    }
    
    public function search_call_sum_rep_by_queue(Request $request)
    {  
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
        ->where('id',$userid)
        ->first();

        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $agent_id= $request->input('agent_id');
        $com_id= $request->input('com_id');
        $group_by= $request->input('groupby');
        
        // search by company
        if($group_by = 'queue')
        {
            // check if company list all or one
            if($request->input('com_id') !== 'All') 
            {
                $condition['queues_config.com_id'] = $com_id;
            }else if($request->input('com_id') == 'All'){
				$condition['tbl_com_mst.id'] = $get_com_id->com_id;
			}else
            {
                $condition = array();
            }
            if($request->input('queue_id') !== 'All') 
            {
                $condition2['queues_config.extension'] = $queue_id;
            }else
            {
                $condition2 = array();
            }
        }
            // sql
        $data = $dataq = DB::table('asterisk.queues_config')
            ->select(
                'extension AS queueid',
                'extension AS id',
                'extension AS agnt_userid',
                'extension AS com_name',
                'queues_config.com_id AS company_id',
                'phonikip_db.tbl_com_mst.com_name',
                'extension AS cre_datetime',
                'descr AS descr',
                'extension AS username',
               
                // offerd_calls
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS offerd_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
            AND tbl_calls_evnt.agnt_queueid = queueid 
            ) AS offerd_calls"),

            //Answer calls           
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS answer_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
            AND tbl_calls_evnt.agnt_queueid = queueid 
            ) AS answer_calls"),

            //total abandon calls          
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND tbl_calls_evnt.DESC = 'ABANDON' 
            AND tbl_calls_evnt.agnt_queueid = queueid 
            ) AS tot_abn_calls"),

            //total abandon rate
            DB::raw("(select ROUND(SUM((tot_abn_calls)/(offerd_calls)*100))) as tot_abn_rate"),


            //total reached abandon callback calls          
            DB::raw("(SELECT
            COUNT( ev.id ) AS tot_abn_callb_calls 
            FROM
                phonikip_db.tbl_calls_evnt ev
            JOIN csp_callhistory csp ON csp.unq_id=ev.linkedid   
            JOIN asteriskcdrdb.cdr ON cdr.uniqueid=csp.callback_unq_id   
            WHERE
            ( ev.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND ev.DESC = 'ABANDON' 
            AND cdr.disposition = 'ANSWERED' 
            AND ev.agnt_queueid = queueid 
            ) AS tot_abn_callb_calls"),

            //reached abandon callback rate
            DB::raw("(select ROUND(SUM((tot_abn_callb_calls)/(tot_abn_calls)*100))) as tot_abn_callb_rate"),

             //total outbound calls
             DB::raw("0 AS tot_ob_calls"),
         
             //total outbound answer call
             DB::raw("0 AS tot_ob_ans_calls"),                

            //total cda calls
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS tot_cda_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
            AND tbl_calls_evnt.DESC = 'COMPLETEAGENT' 
            AND tbl_calls_evnt.agnt_queueid = queueid 
            ) AS tot_cda_calls"))

            ->join('phonikip_db.tbl_com_mst','tbl_com_mst.id','=','queues_config.com_id');
                
                if(!empty($condition))
                {
                    $dataq = $dataq->where($condition);
                }
                if(!empty($condition2))
                {
                    $dataq = $dataq->where($condition2);
                }
                    $data = $dataq ->get();
                   
                // end of company sql --------------------------------------------
		        return compact('data',$data);

    }
    public function search_call_sum_rep_by_agent(Request $request)
    {  
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
        ->where('id',$userid)
        ->first();

        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $agent_id= $request->input('agent_id');
        $com_id= $request->input('com_id');
        $group_by= $request->input('groupby');
        $users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where c.`title`= 'Csp_Agent' and a.`com_id`= $get_com_id->com_id ;");
		$user_array = explode(",",$users[0]->userList); 
        // search by company
        if($group_by = 'agent')
        {
            // check if company list all or one
            if($request->input('com_id') !== 'All') 
            {
                $condition['user_master.com_id'] = $com_id;
            }else if($request->input('com_id') == 'All'){
				$condition['tbl_com_mst.id'] = $get_com_id->com_id;
			}else
            {
                $condition = array();
            }
            
        }
            // sql
        $data = $dataq = DB::table('user_master')
            ->select(
                'user_master.id AS agnt_queueid',
            'user_master.id AS id',
            'user_master.id AS userid',
            'tbl_com_mst.com_name',
            'tbl_com_mst.id AS company_id',
            'user_master.id AS cre_datetime',
            'user_master.id AS descr',
            'user_master.username',
            'user_master.id AS offerd_calls',
               
            //Answer calls           
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS answer_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND ( tbl_calls_evnt.status = 'ANSWER' ) 
            AND tbl_calls_evnt.agnt_userid = userid
            ) AS answer_calls"),

            'user_master.id AS tot_abn_calls',
            'user_master.id as tot_abn_rate',

            //total outbound calls
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS tot_ob_calls 
            FROM
            `tbl_calls_evnt` 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND tbl_calls_evnt.call_type = 'outbound' 
            AND agnt_userid = userid
            ) AS tot_ob_calls"),

            //total outbound answer call
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS tot_ob_ans_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND tbl_calls_evnt.call_type = 'outbound' 
            AND tbl_calls_evnt.STATUS = 'ANSWER' 
            AND agnt_userid = userid
            ) AS tot_ob_ans_calls"),

            //total cda calls
            DB::raw("(SELECT
            COUNT( tbl_calls_evnt.id ) AS tot_cda_calls 
            FROM
                phonikip_db.tbl_calls_evnt 
            WHERE
            ( tbl_calls_evnt.cre_datetime BETWEEN '$frm_date' AND '$to_date' ) 
            AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
            AND tbl_calls_evnt.DESC = 'COMPLETEAGENT' 
            AND tbl_calls_evnt.agnt_userid = userid
            ) AS tot_cda_calls"))

            ->join('phonikip_db.tbl_com_mst','tbl_com_mst.id','=','user_master.com_id');
                
                if(!empty($condition))
                {
                    $dataq = $dataq->where($condition);
                }
                if($request->input('agent_id') !== 'All') 
					{	
						$dataq = $dataq->where('user_master.id',$agent_id);
						
					}
				if($request->input('agent_id') == 'All'){
					$dataq = $dataq->whereIn('user_master.id',$user_array);
					}
                    $data = $dataq ->get();

		        return compact('data',$data);
    }
   
}
