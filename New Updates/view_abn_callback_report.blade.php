<!DOCTYPE html>
<html>

@include('header_new')
<script>
$(document).ready(function() {
searchdata();
});
</script>

<!-- Content Wrapper. Contains page content -->
<!-- Start body -->
<div class="content-wrapper">
<div class="col-lg-12 ">
    <h1 class="form_caption">Abandon Callback Report</h1>
</div>
<div class="container-fluid" style="margin-left: 33px; margin-top: 7px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
            <form class="form-horizontal" action="{{url('searchContact')}}" method="post" id="form">
                <div class="form-group-inner">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="com_id" name="com_id" onchange="loadQeues();"
                                            required>
                                            <option value="All">Company</option>
                                            <?php 
                                            foreach($get_com_data as $com_value){ ?>
                                            <option value="<?php echo $com_value->id ?>">
                                                <?php echo $com_value->com_name ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="user" name="user" value="{{ Session::get('username')}}" >
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds"  id="queue_id" name="queue_id"
                                            required>
                                            <option value="All">Queue</option>
                                            <?php 
                                            foreach($getdndstatus as $value){ ?>
                                            <option value="<?php echo $value->extension ?>">
                                                <?php echo $value->descr ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 30px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">From</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 00:00:00'); ?>" name="frm_date"
                                            id="frm_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 5px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">To</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 23:00:00'); ?>" name="to_date"
                                            id="to_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="button-style-four btn-mg-b-10">
                                            <button type="button" class="btn btn-custon-four btn-success attr_btn"
                                                style="width:78px; " onclick="searchdata()">Search &nbsp</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    <div class="sparkline13-list">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
                <table id="queue_report" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                    width="100%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Date Time</p></th>
                            <th><p class="text-center">Queue</p></th>
                            <th><p class="text-center">Queue Name</p></th>
                            <th><p class="text-center">CLI</p></th>
                            <th><p class="text-center">Customer Name</p></th>
                            <th><p class="text-center">DID</p></th>
                            <th><p class="text-center">Agent ID</p></th>
                            <th><p class="text-center">Agent Name</p></th>
                            <th><p class="text-center">Duration</p></th>
                            <th><p class="text-center">Preset One</p></th>
                            <th><p class="text-center">Preset Two</p></th>
                            <th><p class="text-center">Comment</p></th>          
                            <th><p class="text-center">Status</p></th>
                            <th><p class="text-center">Updated User</p></th>
                            <th><p class="text-center">Callback time</p></th>
                            <th><p class="text-center">Callback Call Status</p></th>
                            <th><p class="text-center">In Date/Time</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">



    <div class="modal fade bd-example-modal-sm" style="width:100%;" id="addcbstatusdashboard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width:700px; margin-left:-300px;">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Call Log</h5>
                    <button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="ticket_model">
                    <div class="content-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <form action="#">
                                        <input type="hidden" name="agent" id="agent" value="" />
                                        <input type="hidden" name="linkid" id="linkid" value="" />
                                        <div class="form-group-inner">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                <div class="row">
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                                <label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important;">Contact Number</label>
                                                            </div>
                                                            <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
                                                                <input class="form-control textfeilds" name="phonumber" id="phonumber" style="width: 120%; height: 26px;" value="" readonly>
                                                                <!-- <input class="form-control textfeilds" name="statusCalltext" id="statusCalltext" style="width: 120%; height: 26px;" value="" readonly> -->
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                                <div class="row">
                                                                    <input type="hidden" name="linkunid" id="linkunid" value="">
                                                                    <input type="hidden" name="status" id="status" value="">
                                                                    <input type="hidden" name="id" id="id" value="">
                                                                    <input type="hidden" name="qextension" id="qextension" value="">
                                                                    <input type="hidden" name="cus_id" id="cus_id" value="">
                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                                                <label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important; margin-left:-2px;">Preset Remark 1</label>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                                                                                <select class="form-control custom-select-value" id="categories" style="margin-left:7px; width:95%;" name="categories" onchange = "addPreset('categories')">
                                                                                    <option value="">Category</option>
                                                                                    
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                                <div class="row">
                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                                                <label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important; margin-left:-2px;">Preset Remark 2</label>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                                                                                <select class="form-control custom-select-value" id="inquiries" style="margin-left:7px; width:95%;" name="inquiries" onchange = "addPreset('inquiries')">
                                                                                    <option value="">Inquiry</option>
                                                                                    
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                                <div class="row">
                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                                                                <label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important; margin-left:-2px;">Remark</label>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12" style="margin-left:7px;">
                                                                                <textarea name="setremark" cols="54" rows="5"  id="setremark" ></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                                <div class="button-style-four btn-mg-b-10" style="float:right">
                                                                    <button type="button" class="btn btn-custon-four btn-success attr_btn" style="width:78px; " onclick="SaveCallback()">Save &nbsp</button>
                                                                    <button type="button" class="btn btn-custon-four btn-danger attr_btn" class="close" data-dismiss="modal" style="width:78px;" onclick="refresh()"> Close </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ./col -->
</div>
<!-- /.row -->
</div>
<br><br>
<script>

    $(document).ready(function () {

        $.ajax({
                url: 'getInquaryList',
                type: 'GET',
                success: function (response)
                {
                    var model = $('#inquiries');
                    model.empty();
                    var i = 0

                    var datalist;
                    datalist += "<option value=''>Select Inquiry</option>";
                    $.each(response, function (index, element) {
                        // $.each(element, function (colIndex, c) {
                        // console.log (c);

                            datalist += "<option value='" + element.id + "'>" + element.remark+ "</option>";
                        // });
                    });
                    $('#inquiries').html(datalist);
                }
            });

            $.ajax({
                url: 'getCategoryList',
                type: 'GET',
                success: function (response)
                {
                    var model = $('#categories');
                    model.empty();
                    var i = 0

                    var datalist;
                    datalist += "<option value=''>Select Category</option>";
                    $.each(response, function (index, element) {
                        // $.each(element, function (colIndex, c) {
                            datalist += "<option value='" + element.id + "'>" + element.remark+ "</option>";
                        // });
                    });
                    $('#categories').html(datalist);
                }
            });

        searchdata();
        
    });

    function searchdata() {
        var queue_id = document.getElementById("queue_id").value;
        var queue_id_ = $('#queue_id option:selected').text();
        queue_id_1 = queue_id_.trim();
        var to_date = document.getElementById("to_date").value;
        var frm_date = document.getElementById("frm_date").value;
        var com_id = document.getElementById("com_id").value;
        var com_id_ = $('#com_id option:selected').text();
        com_id_1 = com_id_.trim();
        var user = document.getElementById("user").value;
        var report_name = "Abandon Callback Report";
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds(); 

        $("#queue_report").dataTable().fnDestroy();

        $('#queue_report').DataTable({
            "processing": true,
            "scrollX": true,
            "ajax": {
                "url": "viewcallbackdata",

                "type": "GET",
                data: {
                    queue_id: queue_id,
                    to_date: to_date,
                    frm_date: frm_date,
                    com_id:com_id,

                },
            },
            "columns": [
                { "data": "incoming_date" },
                { "data": "extension" },
                { "data": "descr" },
                { "data": "incoming_num" },
                { "data": "fullName" },
                { "data": "did_num" },
                { "data": "agnt_sipid" },
                { "data": "agnt_name" },
                { "data": "waiting_time" },
                { "data": "cat_one_prerem_id" },
                { "data": "cat_two_prerem_id" },
                { "data": "call_log" },             
                {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                     var ID = full.id;
                     var status = full.cbstatus;
                     var incoming_num = full.incoming_num;
                     var rec_linkedid = full.rec_linkedid;
                     var qextension = full.extension;
                     var cus_id = full.cus_id;
                     var agnt_name = full.agnt_name;
if(status=='1'){
    return '<input onclick="update(1,'+ID+')" type="checkbox" checked  id='+ID+' value='+status+'>';
}else{
    return '<input onclick="update(0,'+ID+')" type="checkbox"  id='+ID+' value='+status+'>';
}
                     
                 }
                },
                { "data": "update_user" },
                { "data": "callback_datetime" },
                { "data": "callbackStatus" },
                { "data": "in_datetime" }

            ],
            "columnDefs": [{
                className: "text-center",
                "targets": [0,1,2,3,4,5,6,7,8]
            },
            {  }
            ],
            "order": [
                [0, "desc"]
            ],
            "pageLength": 25,

            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'pdfHtml5',
            orientation: 'landscape', 
            pageSize: 'A3',
            title: '',
            filename: report_name.toString(),
           
            customize: function (doc) 
            {
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',              
                            },
                            {
                                alignment: 'left',
                                text: ['To Date: ',{ text:to_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',                                              
                            },
                            {
                                alignment: 'left',                                          
                                text: ['From Date: ',{ text: frm_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     });  
                     doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['Date Time: ',{ text: datetime.toString() }],              
                            },
                            {
                                alignment: 'left',
                                text: ['Queue: ',{ text: queue_id_1.toString() }]                       
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['User: ',{ text: user.toString() }],                                              
                            },
                            {
                                alignment: 'left',                                          
                                text: ['Company: ',{ text: com_id_1.toString() }],                    
                            }
                        ],
                            margin: 5
                    });  
                    doc['header']=(function() {
                        return {
                            columns: [
                                {
                                    alignment: 'center',  
                                    fontSize: 12,                        
                                    text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                }

                            ],
                            margin: 20
                        }
                    });
        }  
        },
        {extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',  title:report_name.toString(),
                
            customize: function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var numrows = 3;
                var clR = $('row', sheet);

                //update Row
                clR.each(function () {
                    var attr = $(this).attr('r');
                    var ind = parseInt(attr);
                    ind = ind + numrows;
                    $(this).attr("r",ind);
                });

                // Create row before data
                $('row c ', sheet).each(function () {
                    var attr = $(this).attr('r');
                    var pre = attr.substring(0, 1);
                    var ind = parseInt(attr.substring(1, attr.length));
                    ind = ind + numrows;
                    $(this).attr("r", pre + ind);
                });

                function Addrow(index,data) {
                    msg='<row r="'+index+'">'
                    for(i=0;i<data.length;i++){
                        var key=data[i].key;
                        var value=data[i].value;
                        msg += '<c t="inlineStr" r="' + key + index + '">';
                        msg += '<is>';
                        msg +=  '<t>'+value+'</t>';
                        msg+=  '</is>';
                        msg+='</c>';
                    }
                    msg += '</row>';
                    return msg;
                }

                //insert
                var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                    { key: 'B', value: 'Date Time :'+datetime },
                                    { key: 'C', value: 'Report Generated User :'+user }
                                ]);

                var r2 = Addrow(2, [{ key: 'A', value: 'Company :'+com_id_1 }, 
                                    { key: 'B', value: 'Queue:'+queue_id_1},
                                    { key: 'C', value: 'From Date :'+frm_date},
                                    { key: 'D', value: 'To Date :'+to_date}
                                    ]);
                sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
            }
        } 
        ]

        });
            $('input[type="search"]').css({
                'width': '350px',
                'display': 'inline-block'
            });
    }
function loadQeues(){
    var com_id = document.getElementById("com_id").value;
    $.ajax({
            url: 'getQeueData',
            type: 'GET',
            data: {com_id: com_id},
            success: function (response)
            {
                var model = $('#queue_id');
                model.empty();

                var datalist;
                datalist += "<option value='All'>Queue</option>";
                $.each(response, function (index, element) {
                    $.each(element, function (colIndex, c) {
                        datalist += "<option value='" + c.extension + "'>" + c.descr+ "</option>";
                    });
                });
                $('#queue_id').html(datalist);
            }
        });
    }
</script>

<!-- return '<input onclick="update(0,'+ID+','+incoming_num+','+rec_linkedid+','+agnt_name+','+qextension+',"'+cus_id+'")" type="checkbox"  id='+ID+' value='+status+'>'; -->
<script>

function update(status,id){

        var status = status;
        var id = id;


        $.ajax({
            url: 'abanupdatecallback',
            type: 'GET',
            data: {id: id,status:status},
            success: function (response) {
                window.alert("Updated");
            }
          });



    }


    function update_abc(status,id,phonumber,linkunid,agnt_name,qextension,cus_id){

        var status = status;
        var id = id;
        var agnt_name = agnt_name;

        document.getElementById("phonumber").value=phonumber;
        document.getElementById("linkunid").value=linkunid;
        document.getElementById("status").value=status;
        document.getElementById("id").value=id;
        document.getElementById("qextension").value=qextension;
        document.getElementById("cus_id").value=cus_id;

        if(status==0){
          if(!agnt_name){
            
            $('#addcbstatusdashboard').modal('show');
          }else{
            window.alert("Can't Update");
            console.log('asdasdas');
          }  
        }else{
            console.log('22222222222');
          $.ajax({
            url: 'abanupdatecallback',
            type: 'GET',
            data: {id: id,status:status},
            success: function (response) {
                window.alert("Updated");
            }
          });
        }

    }

function SaveCallback() {
            var pho_number = document.getElementById("phonumber").value;
            var category = document.getElementById("categories").value;
            var inquiry = document.getElementById("inquiries").value;
            var remark = document.getElementById("setremark").value;
            var linkid = document.getElementById("linkunid").value;

            var status = document.getElementById("status").value;
            var id = document.getElementById("id").value;

            var qextension = document.getElementById("qextension").value;
            var cus_id = document.getElementById("cus_id").value;

            $.ajax({
                url: 'SaveCallbackLog_report',
                type: 'GET',
                data: {pho_number: pho_number, category: category, inquiry: inquiry,
                 remark: remark, linkid:linkid, status: status, id:id, qextension:qextension, cus_id:cus_id},
                success: function (response)
                {
                //alert(response);
                if (response == "Call Log Added Successfully!") {
                    alert(response);
                    document.getElementById("phonumber").value = "";
                    document.getElementById("categories").value = "";
                    document.getElementById("inquiries").value = "";
                    document.getElementById("setremark").value = "";
                    document.getElementById("linkunid").value = "";
                    document.getElementById("status").value = "";
                    document.getElementById("id").value = "";
                    document.getElementById("qextension").value = "";
                    document.getElementById("cus_id").value = "";
                    $('#addcbstatusdashboard').modal('hide');
                    searchdata();

                } else if (response == "Data Saving Error!") {
                    alert(response);
                    searchdata();


                }
                }
            });
        }

function change_rp_type(type) {

    var rp_type = document.getElementById('rp_type');
    rp_type.innerHTML = '';
    if(type=="day")
    {
        $('#rp_type').append('<p class="text-center">Date</p>');
    }else if(type=="hr")
    {
        $('#rp_type').append('<p class="text-center">Hour</p>');
    }else if(type=="summary")
    {
        $('#rp_type').append('<p class="text-center">#</p>');
    }
}

function addPreset(remark){
    var totalRemark = document.getElementById('setremark');
    var secondrem = $("#"+remark).find('option:selected').text();

    totalRemark.value = totalRemark.value
        + ((totalRemark.value=='') ? '' : ' - ')
        + secondrem;
        
}

function init() {
    var element = getsubdeps(document.getElementById('deps').value);
}

function refresh(){
    searchdata();
}

</script>
<script>
//$.datetimepicker.setLocale('en');
$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

</script>
<script>
$('#queue_report').DataTable({
    "processing": true,
    "pageLength": 25,
    "scrollX": true,
});
</script>
@include('footer')

</body>

</html>