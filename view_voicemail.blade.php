<!DOCTYPE html>
<html>

@include('header_new')
<link href="{{ asset('public/dist_01/skin/blue.monday/css/jplayer.blue.monday.min.css') }}" rel="stylesheet">  
<script src="{{ asset('public/dist_01/js/jquery.jplayer.min.js') }}"></script>
<script>
$(document).ready(function() {
    searchdata();
});
</script>
<style type="text/css">
    /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
/*.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .1 ) 
                url('public/ring.gif') 
                50% 50% 
                no-repeat;
}*/

  .form-inline{
      margin-top: 10px !important;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<!-- Start body -->
<div class="content-wrapper">
<div class="col-lg-12 ">
    <h1 class="form_caption">Voicemail Detail Report</h1>
</div>
<div class="container-fluid" style="margin-left: 33px; margin-top: 7px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
            <form class="form-horizontal" action="{{url('searchContact')}}" method="post" id="form">
                <div class="form-group-inner">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="com_id" name="com_id" onchange="loadQeues();"
                                            required>
                                            <option value="All">Company</option>
                                            <?php 
                                            foreach($get_com_data as $com_value){ ?>
                                            <option value="<?php echo $com_value->id ?>">
                                                <?php echo $com_value->com_name ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="user" name="user" value="{{ Session::get('username')}}" >
                         
                            
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 30px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">From</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 00:00:00'); ?>" name="frm_date"
                                            id="frm_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 30px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">To</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 23:00:00'); ?>" name="to_date"
                                            id="to_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="button-style-four btn-mg-b-10">
                                            <button id="search_btn" type="button" class="btn btn-custon-four btn-success attr_btn"
                                                style="width:78px; " onclick="searchdata()">Search &nbsp</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
       
    <div class="sparkline13-list">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
            <br>
            <table>
            <tr>
            <td style="padding-left: 15px !important;">
            <div id="jquery_jplayer_1" class="jp-jplayer" style="width:20px"></div>
            <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
                <div class="jp-type-single">
                    <div class="jp-gui jp-interface" style="height: 42px">
                        <div class="jp-controls" style="padding: 0px">
                            <button class="jp-play" role="button" tabindex="0">play</button>
                            <button class="jp-stop" role="button" tabindex="0">stop</button>
                        
                        <div class="jp-progress jpprogressupdate" style="top: 13px;">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar"></div>
                            </div>
                        </div>
                        <div class="jp-volume-controls jpvolumecontrolsupdate" style="top: 13px;">
                            <button class="jp-mute" role="button" tabindex="0">mute</button>
                            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                            <div class="jp-volume-bar">
                                <div class="jp-volume-bar-value"></div>
                            </div>
                        </div>
                        <div class="jp-time-holder jptimeholderupate" style="top: 26px;">
                            <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                            <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                            
                        </div>
                        </div>
                    </div>
            </div>
           </div>    
             </td>

           </tr></table>       


        <div class="nav-tabs-custom" style="margin-top:8px">
                                <ul class="nav nav-tabs ">
                                     <li class="active" id="tab_div1" style="padding-right: 0px;">
                                        <a id="tab1_li" href="#tab_1" onclick="searchdata()" style="background-color:#d7eceb;border-color:#E0E3D8" data-toggle="tab">Fresh</a>
                                    </li>
                                    <li>
                                        <a id="tab2_li" href="#tab_2" onclick="searchdata_archived()" style="border-color:#E0E3D8" data-toggle="tab">Archived</a>
                                    </li>          
                                </ul>
                                <div class="tab-content" >
                                    <div id="tab_1" class="tab-pane active">
                                       <table id="voicemail_fresh" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                                                width="100%">
                                            <thead class="table_head">
                                                <tr>
                                                    <th><p class="text-center">Received Date/Time</p></th>
                                                    <th><p class="text-center">Message Id</p></th>
                                                    <th><p class="text-center">Caller Id</p></th>
                                                    <th><p class="text-center">Duration</p></th>
                                                    <th><p class="text-center">Recording</p></th>
                                                    <th><p class="text-center">Download</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div id="tab_2" class="tab-pane">
                                        <table id="voicemail_archived" class="table table-bordered table-striped tablerowsize">
                                            <thead class="table_head">
                                                <tr>
                                                    <th><p class="text-center">Received Date/Time</p></th>
                                                    <th><p class="text-center">Message Id</p></th>
                                                    <th><p class="text-center">Caller Id</p></th>
                                                    <th><p class="text-center">Duration</p></th>
                                                    <th><p class="text-center">Remark</p></th>
                                                    <th><p class="text-center">Viewed Date/Time</p></th>
                                                    <th><p class="text-center">Viewed User</p></th>
                                                    <th><p class="text-center">Deleted User</p></th>
                                                    <th><p class="text-center">Deleted Date/Time</p></th>

                                                    <th><p class="text-center">Delete Voicemail</p></th>
                                                    <th><p class="text-center">Recording</p></th>
                                                    <th><p class="text-center">Download</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>

              

                
            
            </div>
        </div>
    </div>
    <br>
    <br>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">

</div>

<!-- Small modal -->
<div id="myConfModal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
           <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Voicemail Deletion</h5>
            <input type="hidden" id="mainid" value="">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="mod_body">
            <h3>Are you sure you want to delete this Voicemail?</h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" id='button_save' onclick="confOk()" class="btn btn-primary">Yes</button>
          </div>
    </div>
  </div>
</div>

<!-- Small modal -->
<div id="myRemarkModal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
           <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Voicemail Deletion</h5>
            <input type="hidden" id="mainIdtoDel" value="">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="mod_body">
            Enter Remark &nbsp;<span id="warn" style="color: red;display: none">Please enter remark!</span><br>
            <textarea rows="3" cols="35" id="remark"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            <button type="button" id='button_save' onclick="saveAndDel()" class="btn btn-primary">Save Remark and Delete VM</button>
          </div>
    </div>
  </div>
</div>

<!-- ./col -->
</div>
<!-- /.row -->
</div>
<script>
$( function() {
  $('#tab1_li').click( function() {
            $(this).css('background', '#d7eceb');
            $('#tab2_li').css('background', '#fff');
            $('#search_btn').attr('onclick', 'searchdata()');
  } );
  $('#tab2_li').click( function() {
            $('#tab1_li').css('background', '#fff');
            $(this).css('background', '#d7eceb');
            $('#search_btn').attr('onclick', 'searchdata_archived()');

  } );
} );



</script>
<script>


function searchdata() {
   
    var agnt_id_ = "N/A";
    agnt_id_1 = agnt_id_.trim();
    var type = "Fresh";
    type_ = type.trim();
    var to_date = document.getElementById("to_date").value;
    var frm_date = document.getElementById("frm_date").value;
    var com_id = document.getElementById("com_id").value;
    var com_id_ = $('#com_id option:selected').text();
    com_id_1 = com_id_.trim();
    var user = document.getElementById("user").value;
    var report_name = "Voicemail Detail Report";
    var currentdate = new Date(); 
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
    $("#voicemail_fresh").dataTable().fnDestroy();

    $('#voicemail_fresh').DataTable({
        "processing": true,
        // "scrollX": true,
        "ajax": {
            "url": "search_voicemail",

            "type": "GET",
            data: {
                to_date: to_date,
                frm_date: frm_date,
                type: type,
                com_id:com_id,

            },
        },
        "columns": [
	    {
                "data": "datetime"
            },
            {
                "data": "msg_id"
            },
            {
                "data": "callerid"
            },
            {
                "data": "duration"
            },
            
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                    if(full.audio_file != "")
                    {
                        var origmailbox=full.origmailbox;
                        var buttonID =  "default/"+origmailbox+"/INBOX/"+ full.audio_file;
                        //alert(buttonID);
                         //alert(full.recordingfile);
                        var id = full.id;
                        var vmtype = "Fresh";
                        return '<a id='+buttonID+' class="btn btn-info btn-flat btnlengthedit" onclick="playvalue(this.id,\'' + id + '\',\'' + vmtype + '\')" role="button"><span class="fa fa-fw fa-play"></span></a>';
                    }else
                    {
                        return '<td>Record not found!!<td>'; 
                    }
                }
                  
            },
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                   if(full.audio_file != "")
                    {

                     var origmailbox=full.origmailbox;
                     var buttonID =  "default/"+origmailbox+"/INBOX/"+ full.audio_file;
                     var id = full.id; 
                     var vmtype = "Fresh"; 
                     return '<a id='+buttonID+' onclick="updateDB(\'' + id + '\',\'' + vmtype + '\')" href="downloadcallrecord2?thisd='+buttonID+'&id='+id+'"  download><span class= "fa fa-fw fa-download"></span></a>';
                     }else
                    {
                        return '<td>record not found!!<td>';
                    }
                }

		},

        ],
        "columnDefs": [{
            className: "text-center",
            "targets": [0,1,2,3,4,5]
        },
        { "width": "12%", "targets": [3] }
        ],
        "order": [
            [0, "desc"]
        ],
        "pageLength": 25,

        dom: 'Bfrtip',

        buttons: 
        [
            {
                extend: 'pdfHtml5',
                orientation: 'landscape', 
                pageSize: 'A3',
                title: '',
                filename: report_name.toString(),
            
                customize: function (doc) 
                {
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['To date: ',{ text: to_date.toString() }],                    
                                }
                            ],
                                margin: 5
                        });  
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',              
                                },
                                {
                                    alignment: 'left',
                                    text: ['From date: ',{ text: frm_date.toString() }]                       
                                }
                            ],
                                margin: 5
                        });
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['Agent: ',{ text:agnt_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        }); 
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: ['Date Time: ',{ text: datetime.toString() }],              
                                },
                                {
                                    alignment: 'left',
                                    text: ['Type: ',{ text: type_.toString() }]                       
                                }
                            ],
                                margin: 5
                        });
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: ['User: ',{ text: user.toString() }],                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['Company: ',{ text: com_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        });      

                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        alignment: 'center',  
                                        fontSize: 12,                        
                                        text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                    }

                                ],
                                margin: 20
                            }
                        });      
                }        
            },
            {           
                extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',
                    
                customize: function (xlsx) 
                {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var numrows = 3;
                    var clR = $('row', sheet);

                    //update Row
                    clR.each(function () {
                        var attr = $(this).attr('r');
                        var ind = parseInt(attr);
                        ind = ind + numrows;
                        $(this).attr("r",ind);
                    });

                    // Create row before data
                    $('row c ', sheet).each(function () {
                        var attr = $(this).attr('r');
                        var pre = attr.substring(0, 1);
                        var ind = parseInt(attr.substring(1, attr.length));
                        ind = ind + numrows;
                        $(this).attr("r", pre + ind);
                    });

                    function Addrow(index,data) {
                        msg='<row r="'+index+'">'
                        for(i=0;i<data.length;i++){
                            var key=data[i].key;
                            var value=data[i].value;
                            msg += '<c t="inlineStr" r="' + key + index + '">';
                            msg += '<is>';
                            msg +=  '<t>'+value+'</t>';
                            msg+=  '</is>';
                            msg+='</c>';
                        }
                        msg += '</row>';
                        return msg;
                    }
                    //insert
                    var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                        { key: 'B', value: 'Date Time :'+datetime },
                                        { key: 'C', value: 'Report Generated User :'+user }
                                    ]);

                    var r2 = Addrow(2, [{ key: 'A', value: 'Company:'+com_id_1},
                                        { key: 'B', value: 'Type :'+type_},
                                        { key: 'C', value: 'Agent :'+agnt_id_1},
                                        { key: 'D', value: 'From Date :'+frm_date},
                                        { key: 'E', value: 'To Date :'+to_date}
                                        ]);
                    sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
                }
		    } 
        ]
    });

$('input[type="search"]').css({
        'width': '350px',
        'display': 'inline-block'
    });

}

function searchdata_archived() {
   
    var agnt_id_ = "N/A";
    agnt_id_1 = agnt_id_.trim();
    var type = "Archived";
    type_ = type.trim();
    var to_date = document.getElementById("to_date").value;
    var frm_date = document.getElementById("frm_date").value;
    var com_id = document.getElementById("com_id").value;
    var com_id_ = $('#com_id option:selected').text();
    com_id_1 = com_id_.trim();
    var user = document.getElementById("user").value;
    var report_name = "Voicemail Detail Report";
    var currentdate = new Date(); 
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
    $("#voicemail_archived").dataTable().fnDestroy();

    $('#voicemail_archived').DataTable({
        "processing": true,
        // "scrollX": true,
        "ajax": {
            "url": "search_voicemail",

            "type": "GET",
            data: {
                to_date: to_date,
                frm_date: frm_date,
                type: type,
                com_id:com_id,

            },
        },
        "columns": [
        {
                "data": "datetime"
            },
            {
                "data": "msg_id"
            },
            {
                "data": "callerid"
            },
            {
                "data": "duration"
            },
            {
                "data": "remark"
            },
             {
                "data": "viewed_at"
            },
             {
                "data": "viewed_user"
            },
             {
                "data": "deleted_user"
            },
             {
                "data": "deleted_at"
            },
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                    if(full.status == "1" && full.allow_deletion == "YES")
                    {
                        var mainid=full.id;
                        return '<a class="btn btn-danger btn-flat btnlengthedit" onclick="delete_vm('+mainid+')" role="button"><span class="fa fa-trash"></span></a>';
                    }else if(full.status == "1" && full.allow_deletion == "NO")
                    {
                        return '<td>Permission Denied<td>'; 
                    }else
                    {
                        return '<td>Deleted<td>'; 
                    }
                   
                 }
            },
            
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                    if(full.audio_file != "")
                    {
                        var origmailbox=full.origmailbox;
                        var buttonID =  "default/"+origmailbox+"/INBOX/"+ full.audio_file;
                        //alert(buttonID);
                         //alert(full.recordingfile);
                        var id = full.id;
                        var vmtype = "Archived";
                        return '<a id='+buttonID+' class="btn btn-info btn-flat btnlengthedit" onclick="playvalue(this.id,\'' + id + '\',\'' + vmtype + '\')" role="button"><span class="fa fa-fw fa-play"></span></a>';
                    }else
                    {
                        return '<td>record not found!!<td>' 
                    }
                   
                 }
            },
            {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                   if(full.audio_file != "")
                    {

                     var origmailbox=full.origmailbox;
                     var buttonID =  "default/"+origmailbox+"/INBOX/"+ full.audio_file;
                     var id = full.id; 
                     var vmtype = "Archived";
                     return '<a id='+buttonID+' onclick="updateDB(\'' + id + '\',\'' + vmtype + '\')" href="downloadcallrecord2?thisd='+buttonID+'&id='+id+'"  download><span class= "fa fa-fw fa-download"></span></a>';
                     }else
                    {
                        return '<td>record not found!!<td>';
                    }

                 }
        },

        ],
        "columnDefs": [{
            className: "text-center",
            "targets": [0,1,2,3,4,5,6,7,8,9,10,11]
        },
        { "width": "12%", "targets": [5] }
        ],
        "order": [
            [0, "desc"]
        ],
        "pageLength": 25,

        dom: 'Bfrtip',

        buttons: 
        [
            {
                extend: 'pdfHtml5',
                orientation: 'landscape', 
                pageSize: 'A3',
                title: '',
                filename: report_name.toString(),
            
                customize: function (doc) 
                {
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['To date: ',{ text: to_date.toString() }],                    
                                }
                            ],
                                margin: 5
                        });  
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',              
                                },
                                {
                                    alignment: 'left',
                                    text: ['From date: ',{ text: frm_date.toString() }]                       
                                }
                            ],
                                margin: 5
                        });
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['Agent: ',{ text:agnt_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        }); 
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: ['Date Time: ',{ text: datetime.toString() }],              
                                },
                                {
                                    alignment: 'left',
                                    text: ['Type: ',{ text: type_.toString() }]                       
                                }
                            ],
                                margin: 5
                        });
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: ['User: ',{ text: user.toString() }],                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['Company: ',{ text: com_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        });      

                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        alignment: 'center',  
                                        fontSize: 12,                        
                                        text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                    }

                                ],
                                margin: 20
                            }
                        });      
                }        
            },
            {           
                extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',
                    
                customize: function (xlsx) 
                {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var numrows = 3;
                    var clR = $('row', sheet);

                    //update Row
                    clR.each(function () {
                        var attr = $(this).attr('r');
                        var ind = parseInt(attr);
                        ind = ind + numrows;
                        $(this).attr("r",ind);
                    });

                    // Create row before data
                    $('row c ', sheet).each(function () {
                        var attr = $(this).attr('r');
                        var pre = attr.substring(0, 1);
                        var ind = parseInt(attr.substring(1, attr.length));
                        ind = ind + numrows;
                        $(this).attr("r", pre + ind);
                    });

                    function Addrow(index,data) {
                        msg='<row r="'+index+'">'
                        for(i=0;i<data.length;i++){
                            var key=data[i].key;
                            var value=data[i].value;
                            msg += '<c t="inlineStr" r="' + key + index + '">';
                            msg += '<is>';
                            msg +=  '<t>'+value+'</t>';
                            msg+=  '</is>';
                            msg+='</c>';
                        }
                        msg += '</row>';
                        return msg;
                    }
                    //insert
                    var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                        { key: 'B', value: 'Date Time :'+datetime },
                                        { key: 'C', value: 'Report Generated User :'+user }
                                    ]);

                    var r2 = Addrow(2, [{ key: 'A', value: 'Company:'+com_id_1},
                                        { key: 'B', value: 'Type :'+type_},
                                        { key: 'C', value: 'Agent :'+agnt_id_1},
                                        { key: 'D', value: 'From Date :'+frm_date},
                                        { key: 'E', value: 'To Date :'+to_date}
                                        ]);
                    sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
                }
            } 
        ]
    });

$('input[type="search"]').css({
        'width': '350px',
        'display': 'inline-block'
    });

}


$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }    
});


$('#myRemarkModal').on('hidden.bs.modal', function (e) {
  $('#remark').val("");
})

function delete_vm(mainid){
    var mainid=mainid;

    $('#myConfModal').modal('show');
    $('#myConfModal').find('#mainid').val(mainid);

    }

function confOk(){
    var mainid = $("#mainid").val();

    $('#myConfModal').modal('hide');

    $('#myRemarkModal').modal('show');
    $('#myRemarkModal').find('#mainIdtoDel').val(mainid);

    $('#warn').hide();
}
function saveAndDel(){
    var mainid = $("#mainIdtoDel").val();
    var remark = $("#remark").val().trim();

    if(remark.toString().length==0){
        $("#warn").show();
    }else{
        $('#warn').hide();

        $.ajax({
                url: 'deleteVoiceMail',
                type: 'GET',
                data: {mainid: mainid,remark: remark},
                success: function (response)
                {
                    $('#myRemarkModal').modal('hide');
                    searchdata_archived();
                }
            });

    }

}

function updateDB(id,vmtype){
    var vmtype=vmtype.toString();
    console.log(vmtype);
            $.ajax({
                url: 'CheckedVoiceMail',
                type: 'GET',
                data: {mainid: id,type: vmtype},
                success: function (response)
                {
                    console.log("Updated to DB");
                }
            });
}

function playvalue(url,id,vmtype){

        updateDB(id,vmtype); 

        var table = $('#voicemail_fresh').DataTable();

            var ipadd="<?php echo $_SERVER['SERVER_ADDR'];?>";
            var company="<?php echo config('app.company_name');?>";
            var files="http://"+ipadd+"/"+company+"/callrecord_mount/"+url ;

            $.ajax({
                url: 'playrecordfile2',
                type: 'GET',
                data: {record_url:files,id:'125'},
                success: function(response)
                {
                    //alert(response);
                    var file="<?php echo session()->get('playfile');?>";
                    $("#jquery_jplayer_1").jPlayer("destroy"); 
                    var ipadd="<?php echo $_SERVER['SERVER_ADDR'];?>";
                    // alert(files);
                    var files="http://"+ipadd+"/"+company+"/callrecord_temp/"+response ;
                    
                    $("#jquery_jplayer_1").jPlayer({
                        ready: function (event) {
                            $(this).jPlayer("setMedia", {
                                mp3: files
                            }).jPlayer("play"); 
                        },
                        swfPath: "../../dist/jplayer",
                        supplied: "mp3,m4a, oga",
                        wmode: "window",
                        useStateClassSkin: true,
                        autoBlur: false,
                        smoothPlayBar: true,
                        keyEnabled: true,
                        remainingDuration: true,
                        toggleDuration: true
                    });
                    
                    //$('#something').html(response);
                }
            });
            
     
    }
function init() {
    var element = getsubdeps(document.getElementById('deps').value);
}
</script>
<script>
//$.datetimepicker.setLocale('en');
$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

</script>
<script>
$('#voicemail_fresh').DataTable({
    "processing": true,
    "pageLength": 25,
    // "scrollX": true,
});
</script>
@include('footer')

</body>

</html>