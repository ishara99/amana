<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Carbon\Carbon;
use Session;

require app_path() . '/Http/Helpers/helpers.php';
require app_path() . '/../vendor/autoload.php';

class OutApiController extends Controller {

    public function index() {

	if (Util::isAuthorized("Agentdashboard") == 'LOGGEDOUT') {
	    return redirect('/');
	}
	if (Util::isAuthorized("Agentdashboard") == 'DENIED') {
	    return view('permissiondenide');
	}
	$endpoint = session()->get('endpoint');
	$linkedid ="";
	$getdndstatus = DB::table('asterisk.ps_contacts')
		->select('status', 'status_des')
		->where('endpoint', $endpoint)
		->first();
		
		$getlogout_reasons  = DB::table('phonikip_db.status_list')
			->select('status') 
			->where('type_flag', 'LOGOUT')
			->get();
		
		$getbreak_reasons  = DB::table('phonikip_db.status_list')
			->select('status') 
			->where('type_flag', 'BREAK')
			->get();

		$getoffline_reasons  = DB::table('phonikip_db.status_list')
			->select('status') 
			->where('type_flag', 'OFFLINE')
			->get();
			$outbound_st = "Online";

			$get_startpart = DB::table('tbl_agnt_evnt')
				->select('tbl_agnt_evnt.*')
				->where('tbl_agnt_evnt.agnt_sipid', '=', $endpoint)
				->where('tbl_agnt_evnt.agnt_event', '=', "Outbound On")
				->orderBy('tbl_agnt_evnt.id', 'desc')
				->first();
			if (!empty($get_startpart)) 
			{
				$check_softphone_rec = DB::table('tbl_agnt_evnt')
					->select('tbl_agnt_evnt.*')
					->where('tbl_agnt_evnt.id_of_prtone', '=', $get_startpart->id)
					->where('tbl_agnt_evnt.agnt_event', '=', "Outbound Off")
					->orderBy('tbl_agnt_evnt.id', 'desc')
					->first();

					if (!empty($check_softphone_rec)) 
					{
						
						$outbound_st = "Outbound Off";
					}
					else
					{
						$outbound_st = "Outbound On";
					}
			}
			else
			{
				$outbound_st = "Online";
			}

			$categories = (new CallHistoryController())->getCategoryList();
			$inquiries = (new CallHistoryController())->getInquaryList();
			
			// dd($categories);

		return view('Agentdashboard',compact('getdndstatus','getlogout_reasons','getbreak_reasons','outbound_st','linkedid','getoffline_reasons','categories','inquiries'));


    }

public function check_out_api(){	
		   
           $endpoint=$_GET['endpoint'];

           $get_result= DB::table('asterisk.ps_contacts')
                ->select('camp_status')
                ->where('ps_contacts.endpoint', '=', $endpoint)
                ->first();

              if($get_result){
	          	$status=$get_result->camp_status; 

		          if($status=='Out Api Call'){
		                return "Yes";
		          }else{
		                return "No";
		          }
		      }else{
		      	 return "No";
		      }   
	}

public function getdetailsofcaller_Out_Api() {
	$endpoint = $_GET['endpoint'];
	$cl_number = $_GET['cl_number'];
	$linkedid = $_GET['linkedid'];

	$contact = (new ContactController())->retrieveAllContact($cl_number);
	//$this->saveCallLog($cl_number, $linkedid);
	//	dd($contact);

	if (count($contact) >= 1 || count($contact) == 0) {

	    return view('CallList_agntdb_Out_Api')
			    ->with('contact', $contact)
			    ->with('sipid', $endpoint)
			    ->with('number', $cl_number)->with('linkedid', $linkedid);
	    echo "success";
	} else if (count($contact) == 1) {

	    $card = '<div class="col-md-12">';
	    $card .= '<div class="box box-danger">';
	    $card .= '<div class="box-header with-border">';
	    $card .= '<input type="hidden" name="incoming_num" id="incoming_num" value="' . $cl_number . '" />';
	    $card .= '<h3 class="box-title">Customer Info OB Api call - ' . $cl_number . '</h3>';
	    $card .= '<div class="box-tools pull-right">';
	    $card .= '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
	    $card .= '</button>';
	    $card .= '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>';
	    $card .= '</div>';
	    $card .= '</div>';
	    $card .= '<div class="box-body" id="pieChartContent">';
	    $card .= '<table style="margin-top:10px;width:100%">';
	    $card .= '<tr>';
	    $card .= '<td><strong>Contact Name: ' . $contact[0]->title . ' ' . $contact[0]->firstname . ' ' . $contact[0]->lastname . '</strong></td>';
	    $card .= '<td><strong>Primary Contact: ' . $contact[0]->primary_contact . '</strong></td>';
	    $card .= '<td><strong>Secondary Contact 1: ' . $contact[0]->secondary_contact . '</strong></td>';
	    $card .= '<td><strong>Secondary Contact 2: ' . $contact[0]->secondary_contact2 . '</strong></td>';
	    $card .= '<td><strong>Email: ' . $contact[0]->email . '</strong></td>';
	    $card .= '<td><a style="color: #000000" target="_blank" onclick="viewinboundform('.$contact[0]->primary_contact.','.$contact[0]->id.','.$cl_number.','.$endpoint.','.$linkedid.')">';
	    $card .= '<span class="glyphicon glyphicon-eye-open"></span></a></td>';
	    $card .= '</tr>';
	    $card .= '</table>';
	    $card .= '</div>';
	    $card .= '</div>';
	    $card .= '</div>';
	    return $card;
	}
	//return Redirect::to(url('frmdb_inboundCallUpdateRecordInfo/'.$cl_number .'/'.$contact_id.'/'.$primary_number.'/'.$endpoint));
    }

    public function open_agntdb_addcusview_Out_Api(){
        $cl_number = $_GET['cl_number'];
        $endpoint = $_GET['endpoint'];
        $linkedId = $_GET['linkedid'];


        return view("addcontact_agntdb_Out_Api")
        ->with('number',$cl_number)
        ->with('sipid',$endpoint)
        ->with('linkedId',$linkedId)
        ->with('prefList',(new ContactTypeController())->retrieveAllRefference());
    }

    public function retrieveSourceRemark(){
        return DB::table('tbl_softlogiclife_src_remark')->get();
	}

	public function retrieveProductRemark(){
	        return DB::table('tbl_softlogiclife_prod_remark')->get();
	}

	public function retrieveDispRemark(){
	        return DB::table('tbl_softlogiclife_disp_remark')->get();
	}

public function inboundCallUpdateRecordInfo_Out_Api($number=null){
        $output=[];
        $primary_number = request()->segment(2);
        $cusid = request()->segment(3);
         // dd($cusid);
        $incoming_number = request()->segment(4);
        $sipid = request()->segment(5);
        $linkedid = request()->segment(6);

        $user=session('userid');

        if($primary_number=="")
        {
            $primary_number = $_GET['primaryNum'];    
        }
        if($cusid=="")
        {
            $cusid = $_GET['id'];    
        }
        if($incoming_number=="")
        {
            $incoming_number = $_GET['Number'];    
        }
        if($sipid=="")
        {
            $sipid = $_GET['sipid'];    
        }
        if($linkedid=="")
        {
            $linkedid = $_GET['linkedid'];    
        }

        Util::log("Inbound Call OB Api Call","View");
                
        // $checkduplicate = DB::table('csp_callhistory')
        //             ->select('csp_callhistory.call_log')
        //             ->where('unq_id',$linkedid)
        //             ->first();
    
        // if($checkduplicate){

	       //  if($checkduplicate->call_log == "Automate Call Log"){

	       //      DB::table('csp_callhistory')
	       //          ->where('unq_id',$linkedid)
	       //          ->where('sipid',$sipid)
	       //          ->update([
	       //              'cus_id'=>$cusid,
	       //              'call_log'=> "Call Answered"
	       //          ]);

	       //  }  
        // }        
        
        $contact1='';

        $emptyContact=(object)array(
            "id"=>"",
            "registration_no"=>"",
            "title"=>"",
            "firstname"=>"",
            "lastname"=>"",
            "primary_contact"=>"",
            "secondary_contact"=>"",
            "secondary_contact2"=>"",
            "email"=>"",
            "gender"=>"",
            "address_line1"=>"",
            "address_line2"=>"",
            "city_state_province"=>"",
            "zip"=>"",
            "country"=>"",
            "contact_type_id"=>"",
            "remark"=>"",
            "mediumS"=>"",
            "mediumT"=>"",
            "mediumE"=>""
        );


        $callhistoryController = new CallHistoryController();
        $contacttypes=(new ContactTypeController())->retrieveAll();         //Retrieve contact types

        $workingProgress=(new TicketController())->retrieveTicketsOfContact($cusid,"Work In Progress");
	     
	    $freshtickets=(new TicketController())->retrieveTicketsOfContact($cusid,"Fresh");
        //print_r($freshtickets);//Retrieve known contact's open tickets
        $closedickets=(new TicketController())->retrieveTicketsOfContact($cusid,"Closed");  //Retrieve known contact's closed tickets
	    //print_r($callhistory1); 
        // dd($cusid);
        // $callhistory=$callhistoryController->retrieveCallhistory($cusid,$linkedid);  //Retrieve known contact's call history
        $callhistory=$callhistoryController->retrieveCallhistoryDetail($cusid,$linkedid);  //Retrieve known contact's call history
          // dd($callhistory);
        if($contact1==""){
            //$get_contact=(new ContactController())->retrieveContactByNumber($segment1);
            $get_contact=(new ContactController())->retrieveContactById($cusid);

            $get_email_det = DB::table('tbl_emlsrv_receive')
                            ->select(   'tbl_emlsrv_receive.id',
                                        'tbl_emlsrv_receive.ref_no',
                                        'tbl_emlsrv_receive.e_sub',
                                        'tbl_emlsrv_receive.e_from',
                                        'tbl_emlsrv_receive.rec_datetime',
                                        'tbl_emlsrv_receive.e_to')
                            ->where('tbl_emlsrv_receive.e_from',$get_contact->email)
                            ->where('user_id',"=",$user)
                            ->orderBy('rec_datetime','desc')
                            ->get();  
            $selectAuthNo = DB::select("SELECT * FROM `tbl_srvbox_mst` WHERE `com_id` = (SELECT `com_id` FROM `user_master` WHERE `id` = $user) AND `cat_id` = '4'"); //WHERE company and agent
        
            $Numbers = array();

            if($selectAuthNo){

                foreach ($selectAuthNo as $selectAuthNo) {

                    array_push($Numbers, $selectAuthNo->box_name);
                }

            }        

            $WhatsappData = DB::select("SELECT *  FROM `tbl_srvwac_mst` where Customer_id = $cusid ");
            $get_whatsapp_data =array();

            if($WhatsappData){

                foreach ($WhatsappData as $Data) {

                    if(!in_array($Data->from_wac_num, $Numbers)){

                        $w_RefNo = $Data->w_RefNo;
                        $From = $Data->from_wac_num;
                        $w_Subject = $Data->w_Subject;
                        $status = $Data->w_Status;

                        $LastDate = DB::select("SELECT MAX(`DateSent`) AS MaxDate, MIN(`DateSent`) AS RcDate FROM `tbl_srvwac_det` WHERE `from_wac_num` = '$From'");

                        $lastReply = $LastDate[0]->MaxDate;
                        $received = $LastDate[0]->RcDate;


                        $get_whatsapp_data[] = [

                            'w_RefNo' => $w_RefNo,
                            'From' => $From,
                            'status' => $status,
                            'lastReply' => $lastReply,
                            'received' => $received
                        ];

                    }

                }
                // dd($get_whatsapp_data);
            }

             $get_sms_data = DB::table('tbl_smssrv_receive')
                            ->select('tbl_smssrv_receive.id',
                                    'tbl_smssrv_receive.from_num',
                                    'tbl_smssrv_receive.ref_no',
                                    'tbl_smssrv_receive.msg',
                                    'tbl_smssrv_receive.rec_datetime',
                                    'tbl_smssrv_receive.to_num')
                            ->where('tbl_smssrv_receive.from_num',$incoming_number)
                            ->where('user_id',"=",$user)
                            ->orderBy('rec_datetime','desc')
                            ->get();

            $get_fax_det = DB::table('tbl_faxsrv_receive')
                            ->select('tbl_faxsrv_receive.id',
                                     'tbl_faxsrv_receive.from_num',
                                     'tbl_faxsrv_receive.ref_no',
                                     'tbl_faxsrv_receive.msg',
                                     'tbl_faxsrv_receive.rec_datetime',
                                     'tbl_faxsrv_receive.to_num')
                            ->where('tbl_faxsrv_receive.from_num',$incoming_number)
                            ->where('user_id',"=",$user)
                            ->orderBy('rec_datetime','desc')
                            ->get();
            
            if(!empty($get_contact)) {

                $contact1=array(
                    "id"=>"$get_contact->id",
                    "registration_no"=>"$get_contact->registration_no",
                    "title"=>"$get_contact->title",
                    "firstname"=>"$get_contact->firstname",
                    "lastname"=>"$get_contact->lastname",
                    "primary_contact"=>"$get_contact->primary_contact",
                    "secondary_contact"=>"$get_contact->secondary_contact",
                    "secondary_contact2"=>"$get_contact->secondary_contact2",
                    "email"=>"$get_contact->email",
                    "gender"=>"$get_contact->gender",
                    "address_line1"=>"$get_contact->address_line1",
                    "address_line2"=>"$get_contact->address_line2",
                    "city_state_province"=>"$get_contact->city_state_province",
                    "zip"=>"$get_contact->zip",
                    "country"=>"$get_contact->country",
                    "contact_type_id"=>"$get_contact->contact_type_id",
                    "CRMNo"=>'123',
                    "remark"=>"$get_contact->remark",
                    "mediumS"=>"$get_contact->mediumS",
                    "mediumT"=>"$get_contact->mediumT",
                    "mediumE"=>"$get_contact->mediumE"
                );
            }else {

                $contact1=array(
                    "id"=>"",
                    "registration_no"=>"",
                    "title"=>"",
                    "firstname"=>"",
                    "lastname"=>"",
                    "primary_contact"=>"",
                    "secondary_contact"=>"",
                    "secondary_contact2"=>"",
                    "email"=>"",
                    "gender"=>"",
                    "address_line1"=>"",
                    "address_line2"=>"",
                    "city_state_province"=>"",
                    "zip"=>"",
                    "country"=>"",
                    "contact_type_id"=>"",
                    "remark"=>"",
                    "CRMNo"=>'',
                    "mediumS"=>"",
                    "mediumT"=>"",
                    "mediumE"=>""
                );
            }

            if(count($callhistory)!=0){
                return view('Csp_InboundCall_Out_Api')
                ->with('number',$incoming_number)
                ->with('sipid',$sipid)
                ->with('contact',$contact1)
                ->with('callhistory',$callhistory)
                ->with('workingProgress',$workingProgress)
                ->with('freshtickets',$freshtickets)
                ->with('closedtickets',$closedickets)
                ->with('autohistoryid',null)
                ->with('contacttypes',$contacttypes) 
                ->with('source',$this->retrieveSourceRemark())
                ->with('product',$this->retrieveProductRemark())
                ->with('disposition',$this->retrieveDispRemark())
                ->with('presets',(new PresetRemarkController())->retrieveAll())
                ->with('type_one',(new PresetRemarkController())->retrieveTypeOneData())
                ->with('type_two',(new PresetRemarkController())->retrieveTypeTwoData())
                ->with('level1s',(new LevelOneController())->retrievelvlone())
                ->with('linkedid',$linkedid)
                ->with('contact',$contact1)
                ->with('get_email_det',$get_email_det)
                ->with('get_whatsapp_data',$get_whatsapp_data)
                ->with('get_sms_data',$get_sms_data)
                ->with('get_fax_det',$get_fax_det);
            }else{

                return view('Csp_InboundCall_Out_Api')
                ->with('number',$incoming_number)
                ->with('sipid',$sipid)
                ->with('contact',$contact1)
                ->with('callhistory',null)
                ->with('workingProgress',$workingProgress)
                ->with('freshtickets',$freshtickets)
                ->with('closedtickets',$closedickets)
                ->with('autohistoryid',null)
                ->with('contacttypes',$contacttypes) 
                ->with('source',$this->retrieveSourceRemark())
                ->with('product',$this->retrieveProductRemark())
                ->with('disposition',$this->retrieveDispRemark())
                ->with('presets',(new PresetRemarkController())->retrieveAll())
        		->with('type_one',(new PresetRemarkController())->retrieveTypeOneData())
        		->with('type_two',(new PresetRemarkController())->retrieveTypeTwoData())
                ->with('level1s',(new LevelOneController())->retrievelvlone())
                ->with('contact',$contact1)
                ->with('linkedid',$linkedid)
                ->with('get_email_det',$get_email_det)
                ->with('get_whatsapp_data',$get_whatsapp_data)
                ->with('get_sms_data',$get_sms_data)
                ->with('get_fax_det',$get_fax_det);
            }
        }else {

            if(count($callhistory)!=0){

                return view('InboundCall_Out_Api')
                ->with('number',$incoming_number)
                ->with('sipid',$sipid)
                ->with('contact',$contact1)
                ->with('callhistory',$callhistory)
                ->with('workingProgress',$workingProgress)
                ->with('freshtickets',$freshtickets)
                ->with('closedtickets',$closedickets)
                ->with('autohistoryid',null)
                ->with('contacttypes',$contacttypes) 
                ->with('source',$this->retrieveSourceRemark())
                ->with('product',$this->retrieveProductRemark())
                ->with('disposition',$this->retrieveDispRemark())
                ->with('presets',(new PresetRemarkController())->retrieveAll())
    		    ->with('type_one',(new PresetRemarkController())->retrieveTypeOneData())
    		    ->with('type_two',(new PresetRemarkController())->retrieveTypeTwoData())
                ->with('level1s',(new LevelOneController())->retrievelvlone())
                ->with('contact',$contact1);

            }else{

                return view('InboundCall_Out_Api')
                ->with('number',$incoming_number)
                ->with('sipid',$sipid)
                ->with('contact',$contact1)
                ->with('callhistory',null)
                ->with('workingProgress',$workingProgress)
                ->with('freshtickets',$freshtickets)
                ->with('closedtickets',$closedickets)
                ->with('autohistoryid',null)
                ->with('contacttypes',$contacttypes) 
                ->with('source',$this->retrieveSourceRemark())
                ->with('product',$this->retrieveProductRemark())
                ->with('disposition',$this->retrieveDispRemark())
                ->with('presets',(new PresetRemarkController())->retrieveAll())
    		    ->with('type_one',(new PresetRemarkController())->retrieveTypeOneData())
    		    ->with('type_two',(new PresetRemarkController())->retrieveTypeTwoData())
                ->with('level1s',(new LevelOneController())->retrievelvlone())
                ->with('contact',$contact1);
            }
       }
	}

function SaveCallLog_Out_Api(){
	$pho_number = $_GET['pho_number'];
	
	$source = $_GET['source'];
	$product = $_GET['product'];
	$disposition = $_GET['disposition'];
	
	$remark = $_GET['remark'];
	$cusid = $_GET['cusid'];
	$userid=session('userid');
	$com_id = util::get_com_id_by_user($userid);
	$endpoint=session('endpoint');
	$today=date("Y-m-d");
	if(strlen($_GET['linkid'])==0){
		$linkedid=session('linkedid');
	}else{
		$linkedid=$_GET['linkid'];
	}
	// dd($linkedid);
	$agent_sipid="";

		$get_status= DB::table('asterisk.ps_contacts')
                ->select('camp_status')
                ->where('ps_contacts.status', '=', 'Online')
                ->where('ps_contacts.endpoint', '=', $endpoint)
                ->first();

        if(!$get_status){
	        return  'In Call';  
		}

		$get_call_status= DB::table('phonikip_db.tbl_calls_evnt')
                ->select('uniqueid','status')
                ->where('tbl_calls_evnt.linkedid', '=', $linkedid)
                ->first();
        $uniqueid=$get_call_status->uniqueid;  
        $status=$get_call_status->status;  

	    DB::UPDATE("UPDATE `tbl_outbound_apicall_mst` 
	    	SET `cus_id` = '$cusid', `source` = '$source',`product` = '$product',`disposition` = '$disposition',
	    	`remark` = '$remark',`rmk_datetime` = NOW(),`com_id`='$com_id',`call_status`='$status',`uniqueid`='$uniqueid'
	    	WHERE 
	    	`linkedid` = '$linkedid' ");

	    DB::UPDATE("UPDATE `asterisk`.`ps_contacts` 
	    	SET `status_des` = '', `camp_status` = ''
	    	WHERE `endpoint` = '$endpoint' ");

		if(true){
			
			$date_time=date('Y-m-d H:i:s');
		
			$updateSuccess= DB::update('UPDATE csp_callhistory set
			call_log=?,updated_datetime=?,cus_id=? where unq_id = ?  ORDER BY call_datetime DESC LIMIT 1',
		    [$remark,$date_time,$cusid,$linkedid]);
			 // dd($updateSuccess);

		 }

			$data = array(
					'unq_id' => $linkedid,
					'call_datetime' => NOW(),
					'pho_number' => $pho_number,
					'sipid' => $endpoint,
					'cus_id'=>$cusid,
					'log_type'=>"OB API Call",
					'call_log'=> $remark,
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			
			$insertSuccess=DB::table('csp_callhistory_detail')
				->insert($data);
	
			if($updateSuccess){
				return  'Call Log Added Successfully!';
			}else{
				return 'Data Saving Error! ';
			}	
			
		}

function SaveCallLog_Auto_OB_Api(){
	$pho_number = $_GET['pho_number'];
	
	
	$remark = $_GET['remark'];
	$userid=session('userid');
	$com_id = util::get_com_id_by_user($userid);
	$endpoint=session('endpoint');

	if(strlen($_GET['linkid'])==0){
		$linkedid=session('linkedid');
	}else{
		$linkedid=$_GET['linkid'];
	}
 	
 	  $checkduplicate = DB::table('csp_callhistory')
                    ->select('csp_callhistory.call_log')
                    ->where('unq_id',$linkedid)
                    ->where('call_log','Automate Call Log')
                    ->first();
    
       if(!$checkduplicate){
        	 $data = array(
					'unq_id' => $linkedid,
					'call_datetime' => NOW(),
					'pho_number' => $pho_number,
					'sipid' => $endpoint,
					'log_type'=>"OB API Call",
					'call_log'=> $remark,
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			$insertSuccess1=DB::table('csp_callhistory')
				->insert($data);

        }     

		   
        $checkduplicateDetail = DB::table('csp_callhistory_detail')
                    ->select('csp_callhistory_detail.call_log')
                    ->where('unq_id',$linkedid)
                    ->where('call_log','Automate Call Log')
                    ->first();
    
        if(!$checkduplicateDetail){

			$dataDetail = array(
					'unq_id' => $linkedid,
					'call_datetime' => NOW(),
					'pho_number' => $pho_number,
					'sipid' => $endpoint,
					'log_type'=>"OB API Call",
					'call_log'=> $remark,
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			
			$insertSuccess2=DB::table('csp_callhistory_detail')
				->insert($dataDetail);
		}


	
	}



}
