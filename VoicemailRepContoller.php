<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;

require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class VoicemailRepContoller extends Controller
{
  
    
    public function index(){	
		
		if(Util::isAuthorized("view_voicemail_detail_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_voicemail_detail_report")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log('Voicemail Detail Report','View');
        $userid=session('userid');
        $usertypeid=session('usertypeid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

 
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Voicemail Detail Report",$username,"View Voicemail Detail Report");

        return view('view_voicemail',compact('get_com_data'));   

	}

	public function search_voicemail(Request $request){  

        $userid=session('userid');
		$usertypeid=session('usertypeid');
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();
        $user_com_id=$get_com_id->com_id; 
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');
        $type= $request->input('type');

        if($usertypeid=='1' || $usertypeid=='17'){
        	$allow_deletion='YES';
        }else{
        	$allow_deletion='NO';
        }

        $where="";
        if($type=='Fresh'){
        	$where="AND vm.viewed='0'";	
        }else{
        	$where="AND vm.viewed='1'";	
        }	

		$data = DB::select("SELECT vm.msg_id,
									'$allow_deletion' as `allow_deletion`,
									vm.remark,
									vm.viewed_at,
                                    vm.viewed_user,
                                    vm.deleted_user,
									IF(vm.deleted_at='0000-00-00 00:00:00','',vm.deleted_at) as deleted_at,
									vm.status,
									vm.id,
									vm.callerid,
									vm.origmailbox,
									SEC_TO_TIME(vm.duration) as duration,
									vm.audio_file,
									vm.`datetime`
									From `phonikip_db`.`tbl_voicemail_mst` vm 
									JOIN `phonikip_db`.`tbl_srvbox_mst` box ON box.box_name=vm.origmailbox
									WHERE (vm.`datetime` between '$frm_date' and '$to_date')
									AND box.com_id='$user_com_id' 
									$where ;");
                        
                        return compact('data',$data);                 
    }

public function deleteVoiceMail(Request $request){  

        $id= $request->input('mainid');
        $remark= $request->input('remark');

        $data=array('status'=>'0',
        			'remark'=>$remark,
                    'deleted_user'=>session()->get('username'),
        			'deleted_at'=> DB::raw('NOW()')
		);

        DB::table('tbl_voicemail_mst')
            ->where('id', $id)
            ->update($data); 


        $files = DB::table('tbl_voicemail_mst')
					->where('id', $id) 
					->first();

		$fileSet=str_replace(".wav","",$files->audio_file);
		$origmailbox=$files->origmailbox;
		shell_exec("rm -rf /var/spool/asterisk/voicemail/default/".$origmailbox."/INBOX/".$fileSet.".*");

		$ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Deleted a Voicemail Record;rec_id=$id",$username,"Delete Voicemail Record");

	}

public function CheckedVoiceMail(Request $request){
	$userid=session('userid');
	$usertypeid=session('usertypeid');
    $id= $request->input('mainid');
    $type= $request->input('type');

    if($type=='Fresh'){

    	$data=array('viewed'=>'1',
    				'viewed_user'=>session()->get('username'),
        			'viewed_at'=> DB::raw('NOW()')
		);

        DB::table('tbl_voicemail_mst')
            ->where('id', $id)
            ->update($data);

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Viewed a Fresh Voicemail Record;rec_id=$id",$username,"Viewed Fresh Voicemail Record");

    }else{

    	$ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Viewed a Archived Voicemail Record;rec_id=$id",$username,"Viewed Archived Voicemail Record");
    }


}

}