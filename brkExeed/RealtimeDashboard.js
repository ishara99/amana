import React, { useState, useEffect } from 'react';
import './App.css';
import Timer from './Timer.js';
import Timer_new from './Timer_new.js';
import AutoACW from './AutoACW.js';
import Tiles from './Tiles.js';
import Tiles_sub from './Tiles_sub.js';
import SrvTiles from './SrvTiles.js';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    
  },
  paper: {
    padding: theme.spacing(4),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  tiles: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingTop: '15px',
    textAlign: 'center',
    marginBottom: '20px',
    color: theme.palette.text.secondary,
    
  },
  tiles_summery: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingTop: '35px',
    textAlign: 'center',
    color: theme.palette.text.secondary,
    
  },
  
  srvtiles: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  tiles_srv: {
    paddingTop: '10px',
    paddingBottom: '15px',
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  tablerow: {
    paddingTop: '5px !important',
    paddingBottom: '5px !important',
    fontWeight: 700,
    fontSize: '15px',

  },
  tblrowpending: {
    background: '#f0cc97'
   

  },
  tblrowerror: {
    background: '#fa5c43'
   

  },
  tblrowbreack: {
    background: '#e4c5fc'
   

  },
  tblrowacw: {
    background: '#7ec485'
   

  },
  
  tblrowconnect: {
    background: '#c5fccb'
   

  },
  tblrowoffline: {
    background: '#bdbab5'
   

  },
  
  tblrowoutbound: {
    background: '#a5daf2'
   

  },
  tablehead: {
    color: "pink",
    background: '#001536',
    color: '#ffffff !important',
  },
  tablehead_txt: {
    background: '#001536',
    color: '#ffffff !important',
  },
  tile_connect: {
    background: '#ffffff',
    border: '1px solid #08590d',
    color: '#08590d !important',
    paddingLeft: '8px',
  },
  tile_pending: {
    background: '#ffffff',
    border: '1px solid #000000',
    color: '#000000 !important',
    paddingLeft: '8px',
  },
  tile_abandon: {
    background: '#ffffff',
    border: '1px solid #c22f06',
    color: '#c22f06 !important',
    paddingLeft: '8px',
  },
  tile_incoming: {
    background: '#ffffff',
    border: '1px solid #021052',
    color: '#021052 !important',
    paddingLeft: '8px',
  },
  tile_seconrow: {
    paddingLeft: '8px',
  }

}));



function RealtimeDashboard(com_id) {

  const [ nests, setNests ] = useState([]);
  const [ queue_status, setqueue_status ] = useState([]);
  const [ queue_dt, setqueue_dt ] = useState([]);
  const [ offerd_calls,setofferd_calls] = useState([]);
  const [ answer_calls,setanswer_calls] = useState([]);
  const [ tot_abn_calls,settot_abn_calls] = useState([]);
  const [ tot_clbk_calls,settot_clbk_calls] = useState([]);
  const [ tot_connect_calls,settot_connect_calls] = useState([]);
  const [ tot_pending_calls,settot_pending_calls] = useState([]);
  const [ tot_abn_rate,settot_abn_rate] = useState([]);
  const [ tot_ob_calls,settot_ob_calls] = useState([]);
  const [ tot_ob_ans_calls,settot_ob_ans_calls] = useState([]);
  const [ tot_cda_calls,settot_cda_calls] = useState([]);
  const [ tot_eml_count,settot_eml_count] = useState([]);
  const [ fsh_eml_count,setfsh_eml_count] = useState([]);
  const [ tot_fax_count,settot_fax_count] = useState([]);
  const [ fsh_fax_count,setfsh_fax_count] = useState([]);
  const [ tot_sms_count,settot_sms_count] = useState([]);
  const [ fsh_sms_count,setfsh_sms_count] = useState([]);
  const [ tot_wac_count,settot_wac_count] = useState([]);
  const [ fsh_wac_count,setfsh_wac_count] = useState([]);
  const [ tot_webc_count,settot_webc_count] = useState([]);
  const [ fsh_webc_count,setfsh_webc_count] = useState([]);
  const [ tot_rate_calls,settot_rate_calls] = useState([]);
  const [ tot_tran_rate_calls,settot_tran_rate_calls] = useState([]);
  const [ tot_sl_rate,settot_sl_rate] = useState([]);

  const [ tot_completed_callbk_req,settot_completed_callbk_req] = useState([]);
  const [ tot_pending_callbk_req,settot_pending_callbk_req] = useState([]);
  const [ tot_pending_abancallbk,settot_pending_abancallbk] = useState([]);
  const [ tot_completed_abancallbk,settot_completed_abancallbk] = useState([]);



  const [ listening, setListening ] = useState(false);
  const classes = useStyles();
  useEffect( () => {
   // const timeoutID = window.setTimeout(() => {
    if (!listening) {
      const events = new EventSource('http://192.168.120.231:3005/events');
      events.onmessage = (event) => {
        const parsedData = JSON.parse(event.data);
        
        if(parsedData!="")
        {
            const agent_date = parsedData[0];
            const queue_data = parsedData[3];
            const summery_data = parsedData[1];
            const ob_summery_data = parsedData[2];
            const srv_data = parsedData[4];
            const queue_details = parsedData[5];
            //console.log(queue_details)
     
            if(Array.isArray(parsedData[0]))
            {
              var length = Object.keys(agent_date).length;
            }else if(typeof agent_date === "undefined")
            {
             
              var length = 0;
            }else
            {
              var length = 1;
            }

            if(Array.isArray(parsedData[1]))
            {
              //console.log("answer_calls")
              var length_summery= Object.keys(parsedData[1]).length;
            }else
            {
              var length_summery = 1;
            }
            //console.log(length_summery)
            if(Array.isArray(parsedData[2]))
            {
             
              var length_ob_summery= Object.keys(parsedData[2]).length;
            }else
            {
              var length_ob_summery = 1;
            }

            if(Array.isArray(parsedData[4]))
            {
             
              var length_srv= Object.keys(parsedData[4]).length;
            }else
            {
              var length_srv = 1;
            }

            if(Array.isArray(parsedData[3]))
            {
             
              var length_queue = Object.keys(queue_data).length;
            }else
            {
              var length_queue = 1;
            }

            if(Array.isArray(parsedData[5]))
            {
             
              var length_queue_dt = Object.keys(queue_details).length;
            }else if(typeof queue_details === "undefined")
            {
             
              var length_queue_dt = 0;
            }else
            {
              var length_queue_dt = 1;
            }

            
            
            if(summery_data!=null)
            {
               //console.log(summery_data) ;
              // console.log(length_summery) ;
              for (var index=0; index < length_summery; index++) {
                  //const summery_date= JSON.parse(parsedData[1]);
                  const summery_date= summery_data;
                  if(length_summery == 1)
                    {
                      
                      if(JSON.parse(summery_data)['com_id']==com_id.com_id)
                      {
                        //console.log(summery_date.offerd_calls) ;
                        setofferd_calls(JSON.parse(summery_date).offerd_calls);
                        setanswer_calls(JSON.parse(summery_date).answer_calls);
                        settot_abn_calls(JSON.parse(summery_date).tot_abn_calls);
                        settot_clbk_calls(JSON.parse(summery_date).tot_clbk_calls);
                        settot_connect_calls(JSON.parse(summery_date).tot_connect_calls);
                        settot_pending_calls(JSON.parse(summery_date).tot_pending_calls);
                        settot_abn_rate(JSON.parse(summery_date).tot_abn_rate);
                        settot_cda_calls(JSON.parse(summery_date).tot_cda_calls);
                        settot_rate_calls(JSON.parse(summery_date).tot_rate_calls);
                        settot_tran_rate_calls(JSON.parse(summery_date).tot_tran_rate_calls);
                        settot_sl_rate(JSON.parse(summery_date).tot_sl_rate);
                        
                        settot_completed_callbk_req(JSON.parse(summery_date).tot_completed_callbk_req);
                        settot_pending_callbk_req(JSON.parse(summery_date).tot_pending_callbk_req);
                        settot_pending_abancallbk(JSON.parse(summery_date).tot_pending_abancallbk);
                        settot_completed_abancallbk(JSON.parse(summery_date).tot_completed_abancallbk);
                      }
                    }else
                    { 
                      //console.log(JSON.parse(summery_data[index]));
                      if(JSON.parse(summery_data[index])['com_id']==com_id.com_id)
                      {
                        setofferd_calls(JSON.parse(summery_data[index]).offerd_calls);
                        setanswer_calls(JSON.parse(summery_data[index]).answer_calls);
                        settot_abn_calls(JSON.parse(summery_data[index]).tot_abn_calls);
                        settot_clbk_calls(JSON.parse(summery_data[index]).tot_clbk_calls);
                        settot_connect_calls(JSON.parse(summery_data[index]).tot_connect_calls);
                        settot_pending_calls(JSON.parse(summery_data[index]).tot_pending_calls);
                        settot_abn_rate(JSON.parse(summery_data[index]).tot_abn_rate);
                        settot_cda_calls(JSON.parse(summery_data[index]).tot_cda_calls);
                        settot_rate_calls(JSON.parse(summery_data[index]).tot_rate_calls);
                        settot_tran_rate_calls(JSON.parse(summery_data[index]).tot_tran_rate_calls);
                        settot_sl_rate(JSON.parse(summery_data[index]).tot_sl_rate);

                        settot_completed_callbk_req(JSON.parse(summery_data[index]).tot_completed_callbk_req);
                        settot_pending_callbk_req(JSON.parse(summery_data[index]).tot_pending_callbk_req);
                        settot_pending_abancallbk(JSON.parse(summery_data[index]).tot_pending_abancallbk);
                        settot_completed_abancallbk(JSON.parse(summery_data[index]).tot_completed_abancallbk);
                      }
                    }
                 

                  }
            }

            if(ob_summery_data!=null)
            {
               //console.log(ob_summery_data) ;
              //console.log(com_id.com_id) ;
              for (var index=0; index < length_ob_summery; index++) {
                  //const summery_date= JSON.parse(parsedData[1]);
                  const ob_summery_data_val= ob_summery_data;
                  if(length_ob_summery == 1)
                    {
                     
                      if(JSON.parse(ob_summery_data_val)['com_id']===com_id.com_id)
                      {
                        settot_ob_calls(JSON.parse(ob_summery_data_val).tot_ob_calls);
                        settot_ob_ans_calls(JSON.parse(ob_summery_data_val).tot_ob_ans_calls);
                       
                      }
                    }else
                    { 
                      //console.log(JSON.parse(summery_data[index]));
                      if(JSON.parse(ob_summery_data_val[index])['com_id']==com_id.com_id)
                      {
                        settot_ob_calls(JSON.parse(ob_summery_data_val[index]).tot_ob_calls);
                        settot_ob_ans_calls(JSON.parse(ob_summery_data_val[index]).tot_ob_ans_calls);
                       
                      }
                    }
                 

                  }
            }

            if(srv_data!=null)
            {
               //console.log(ob_summery_data) ;
              //console.log(com_id.com_id) ;
              for (var index=0; index < length_srv; index++) {
                  //const summery_date= JSON.parse(parsedData[1]);
                  const srv_data_val= srv_data;
                  if(length_srv == 1)
                    {
                      //console.log(JSON.parse(srv_data_val)['com_id']);
                      if(JSON.parse(srv_data_val)['com_id']===com_id.com_id)
                      {
                        console.log(JSON.parse(srv_data_val).tot_eml_count);
                        settot_eml_count(JSON.parse(srv_data_val).tot_eml_count);
                        setfsh_eml_count(JSON.parse(srv_data_val).fsh_eml_count);
                        settot_fax_count(JSON.parse(srv_data_val).tot_fax_count);
                        setfsh_fax_count(JSON.parse(srv_data_val).fsh_fax_count);
                        settot_sms_count(JSON.parse(srv_data_val).tot_sms_count);
                        setfsh_sms_count(JSON.parse(srv_data_val).fsh_sms_count);
                        settot_wac_count(JSON.parse(srv_data_val).tot_wac_count);
                        setfsh_wac_count(JSON.parse(srv_data_val).fsh_wac_count);
                        settot_webc_count(JSON.parse(srv_data_val).tot_webc_count);
                        setfsh_webc_count(JSON.parse(srv_data_val).fsh_webc_count);
                       
                      }
                    }else
                    { 
                      //console.log(JSON.parse(summery_data[index]));
                      if(JSON.parse(srv_data_val[index])['com_id']==com_id.com_id)
                      {
                        settot_eml_count(JSON.parse(srv_data_val[index]).tot_eml_count);
                        setfsh_eml_count(JSON.parse(srv_data_val[index]).fsh_eml_count);
                        settot_fax_count(JSON.parse(srv_data_val[index]).tot_fax_count);
                        setfsh_fax_count(JSON.parse(srv_data_val[index]).fsh_fax_count);
                        settot_sms_count(JSON.parse(srv_data_val[index]).tot_sms_count);
                        setfsh_sms_count(JSON.parse(srv_data_val[index]).fsh_sms_count);
                        settot_wac_count(JSON.parse(srv_data_val[index]).tot_wac_count);
                        setfsh_wac_count(JSON.parse(srv_data_val[index]).fsh_wac_count);
                        settot_webc_count(JSON.parse(srv_data_val[index]).tot_webc_count);
                        setfsh_webc_count(JSON.parse(srv_data_val[index]).fsh_webc_count);
                       
                      }
                    }
                 

                  }
            }
           
            setNests([]);
            setqueue_status([]);
            setqueue_dt([]);
           
            for (var index=0; index < length_queue; index++) {
              
              if(length_queue == 1)
              {
                if(JSON.parse(queue_data)['com_id']===com_id.com_id)
                {
                  setqueue_status((queue_status) => queue_status.concat(JSON.parse(queue_data)));
                }
              }else
              { 
                if(JSON.parse(queue_data[index])['com_id']==com_id.com_id)
                {
                  setqueue_status((queue_status) => queue_status.concat(JSON.parse(queue_data[index])));
                }
              }

                
            } 
            //console.log(queue_details);
            for (var index=0; index < length_queue_dt; index++) {
              
              if(length_queue_dt == 1)
              {
                if(JSON.parse(queue_details)['com_id']===com_id.com_id)
                {
                  setqueue_dt((queue_dt) => queue_dt.concat(JSON.parse(queue_details)));
                }
              }else
              { 
                if(JSON.parse(queue_details[index])['com_id']==com_id.com_id)
                {
                  setqueue_dt((queue_dt) => queue_dt.concat(JSON.parse(queue_details[index])));
                }
              }

                
            } 
            for (var index=0; index < length; index++) {
              
              if(length == 1)
              {
              
                if(JSON.parse(agent_date)['com_id']===com_id.com_id)
                {
                  setNests((nests) => nests.concat(JSON.parse(agent_date)));
                }
                
              }else
              {
                if(JSON.parse(agent_date[index])['com_id']===com_id.com_id)
                {
                  setNests((nests) => nests.concat(JSON.parse(agent_date[index])));
                }
              }
              
            } 
        }
        
      };
      setListening(true);
    }
  //}, 1000);
  }, [listening,nests]);
  

  return (
    <div className={classes.root}>
    <Grid container spacing={4}>

    <Grid className={classes.tiles_summery}
        container
        spacing={2}
      >
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles background_color={'#0997e3'} className={classes.tile_incoming}
           title={'OFFERED CALLS'}
           value={Array.isArray(offerd_calls)? 0:offerd_calls}
           />
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles background_color={'#0997e3'} className={classes.tile_incoming}
           title={'ANSWERD CALLS'}
           value={Array.isArray(answer_calls)? 0:answer_calls}
           />
        </Grid>
       
       {/*
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles_sub background_color={'#8a0703'} className={classes.tile_abandon}
          title={'ABANDON CALLS'}
          value={Array.isArray(tot_abn_calls)? 0:tot_abn_calls}
          //value_sub={( 0 +" - " +0 )}
          value_sub={(Array.isArray(tot_completed_abancallbk)? 0:tot_completed_abancallbk)+ " - " +(Array.isArray(tot_pending_abancallbk)? 0:tot_pending_abancallbk)  }
          />
        </Grid>

      */}

         <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles background_color={'#c22f06'} className={classes.tile_abandon}
          title={'GEN. ABANDON'}
          value={Array.isArray(tot_abn_calls)? 0:tot_abn_calls}
          />
        </Grid>
        
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles  background_color={'#8a0703'} className={classes.tile_abandon}
          title={'GEN. ABANDON %'}
          value={Array.isArray(tot_abn_rate)? 0:tot_abn_rate}
          />
        </Grid>
       
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles  className={classes.tile_connect}
          background_color={'#ffffff'}
          title={'CONNECTED CALLS'}
          value={Array.isArray(tot_connect_calls)? 0:tot_connect_calls}
          />
        </Grid>

        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles  background_color={'#fffff'}  className={classes.tile_pending}
          title={'PENDING'}
          value={Array.isArray(tot_pending_calls)? 0:tot_pending_calls}
          />
        </Grid>

      </Grid>





      <Grid className={classes.tiles}
        container
        spacing={2}
      >
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles background_color={'#22b14c'}  className={classes.tile_seconrow}
          title={'OB CALLS'}
          value={Array.isArray(tot_ob_calls)? 0:tot_ob_calls }
          />
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          <Tiles  background_color={'#22b14c'} className={classes.tile_seconrow}
          title={'OB ANSWERD CALLS'}
          value={Array.isArray(tot_ob_ans_calls)? 0:tot_ob_ans_calls }
          />
        </Grid>

        {/*
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
              <Tiles_sub  background_color={'#80490b'} className={classes.tile_seconrow}
          title={'CALL BACK'}
          value={Array.isArray(tot_clbk_calls)? 0:tot_clbk_calls }
          value_sub={(Array.isArray(tot_completed_callbk_req)? 0:tot_completed_callbk_req) + " - " +(Array.isArray(tot_pending_callbk_req)? 0:tot_pending_callbk_req) }
          //value_sub={( 0 +" - " +0 )}
          /> 
        */}

         <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          
        <Tiles  background_color={'#3c8dbc'} 
          title={'TRAN.TO RATING IVR'}
          value={Array.isArray(tot_tran_rate_calls)? 0:tot_tran_rate_calls }
          />
  
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
            <Tiles  background_color={'#3c8dbc'} 
          title={'RATED IVR CALLS'}
          value={Array.isArray(tot_rate_calls)? 0:tot_rate_calls }
          />
  
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
              <Tiles  background_color={'#f39c12'} 
          title={'SLA %'}
          value={Array.isArray(tot_sl_rate)? 0:tot_sl_rate }
          /> 
  
        </Grid>

        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
          {/* <Tiles  background_color={'#000000'} 
          title={'CDBA'}
          value={Array.isArray(tot_cda_calls)? 0:tot_cda_calls }
          /> */}
         <Tiles  background_color={'#646496'} 
          title={'VOICEMAIL'}
          value={Array.isArray(tot_clbk_calls)? 0:tot_clbk_calls }
          /> 
        </Grid>

      

      </Grid>

{/*
      <Grid className={classes.tiles_srv}
        container
        spacing={1}
      >
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
         <Grid spacing={1} className={classes.srvtiles}>
                <SrvTiles  background_color={'#e30997'} 
                  title={'email'}
                  value={(Array.isArray(tot_eml_count)? 0:tot_eml_count) + " - " + (Array.isArray(fsh_eml_count)? 0:fsh_eml_count)}
                  spacing={1}
                  />
            </Grid>
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
         <Grid  spacing={1} className={classes.srvtiles}>
                  <SrvTiles  background_color={'#e30997'} 
                title={'whatsapp'}
                value={(Array.isArray(tot_wac_count)? 0:tot_wac_count) + " - " + (Array.isArray(fsh_wac_count)? 0:fsh_wac_count)}
                />
            </Grid>
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
            <Grid spacing={1} className={classes.srvtiles}>
                <SrvTiles  background_color={'#e30997'} 
                  title={'fax'}
                  value={(Array.isArray(tot_fax_count)? 0:tot_fax_count) + " - " + (Array.isArray(fsh_fax_count)? 0:fsh_fax_count)}
                  spacing={1}
                  />
            </Grid>
    
  
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
            <Grid  spacing={1} className={classes.srvtiles}>
                  <SrvTiles  background_color={'#e30997'} 
                title={'webchat'}
                value={(Array.isArray(tot_webc_count)? 0:tot_webc_count) + " - " + (Array.isArray(fsh_webc_count)? 0:fsh_webc_count)}
                />
            </Grid>
  
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
               <Grid spacing={1} className={classes.srvtiles}>
                <SrvTiles  background_color={'#e30997'} 
                  title={'sms'}
                  value={(Array.isArray(tot_sms_count)? 0:tot_sms_count) + " - " + (Array.isArray(fsh_sms_count)? 0:fsh_sms_count)}
                  spacing={1}
                  />
            </Grid>
           
  
        </Grid>
        <Grid
          item
          lg={2}
          sm={6}
          xl={2}
          xs={12}
        >
  
        </Grid>

      

      </Grid>
     
      */ }


    </Grid>


      <Grid container spacing={3}>
      
        <Grid item xs={6} sm={6} md={6}>
         
            <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead className={classes.tablehead} >
                <TableRow  >
                  <TableCell className={classes.tablehead_txt}>SipId</TableCell>
                  <TableCell className={classes.tablehead_txt}>Agent Name</TableCell>
                  <TableCell className={classes.tablehead_txt}>CLI</TableCell>
                  <TableCell className={classes.tablehead_txt} align="right">Status</TableCell>
                  <TableCell className={classes.tablehead_txt}align="right">Status Desc.</TableCell>
                  <TableCell className={classes.tablehead_txt}align="right">Time</TableCell>
                </TableRow>
              </TableHead>
              <TableBody pt={1}>
                {nests.map((nest, i) => (
                  <TableRow   className={ nest.status =="Online"? 
                                            classes.tablerow 
                                            : (nest.status =="In Call" || nest.status =="Out Call"|| nest.status =="Busy") ?
                                              classes.tblrowconnect
                                            : (nest.status =="Break" && nest.status_des =="Exceeded") ?
                                              classes.tblrowerror 
                                            : (nest.status =="ACW" && nest.status_des =="Call") ?
                                              classes.tblrowconnect
                                            : (nest.status =="ACW" && nest.status_des =="") ?
                                              classes.tblrowacw
                                            : (nest.status =="Break") ?
                                              classes.tblrowbreack
                                            : (nest.status =="Break" && nest.status_des =="RINGNOANSWER") ?
                                              classes.tblrowbreack
                                            : (nest.status =="Outbound") ?
                                              classes.tblrowoutbound
                                            : (nest.status =="Offline") ?
                                              classes.tblrowoffline
                                            : (nest.status =="Ringing" || nest.status =="Dialing") ?
                                              classes.tblrowpending
                                            : classes.tblrowerror
                                            } key={i}>
                    <TableCell className={classes.tablerow}>{nest.endpoint}</TableCell>
                    <TableCell className={classes.tablerow}>{nest.username}</TableCell>
                    <TableCell className={classes.tablerow}>{nest.call_cli !== null? nest.call_cli:  nest.out_call_cli }</TableCell>
                    <TableCell className={classes.tablerow} align="right">{nest.status?  nest.status: "System Issue"}</TableCell>
                    <TableCell  className={classes.tablerow}align="right">{nest.status_des}<AutoACW  datetime={nest.datetime}
                                                                        acw_sec_count={nest.acw_sec_count} 
                                                                        linkedid={nest.linkedid}  
                                                                        endpoint={nest.endpoint} 
                                                                        outoacw_sec_count={nest.outoacw_sec_count}
                                                                        timestamp={nest.timestamp}>
                                                                          </AutoACW></TableCell>
                    <TableCell className={classes.tablerow} align="right" width="10%">{nest.status?   <Timer_new datetime={nest.datetime} timestamp={nest.timestamp} ></Timer_new> : "--/--/--"}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>    
        
        </Grid>

    
        <Grid item xs={6} sm={6} md={6}>
          {/* <Paper className={classes.paper}>
          { */}
            <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead className={classes.tablehead} >
                <TableRow>
                <TableCell className={classes.tablehead_txt}>Queue Name</TableCell>
                <TableCell className={classes.tablehead_txt} >Agent</TableCell>
                  <TableCell className={classes.tablehead_txt}>CLI</TableCell>
                  <TableCell className={classes.tablehead_txt}>Event</TableCell>
                  <TableCell className={classes.tablehead_txt}>Duration</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>
                {queue_dt.map((value, i) => (
                  <TableRow className={ value.call_status=== "PENDING" ? classes.tblrowpending : classes.tblrowconnect }  key={i}>
                    <TableCell className={classes.tablerow}>{value.queue_name}</TableCell>
                    <TableCell className={classes.tablerow}>{value.username}</TableCell>
                    <TableCell className={classes.tablerow} >{value.frm_caller_num}</TableCell>
                    <TableCell className={classes.tablerow} >{value.call_status}</TableCell>
                    <TableCell className={classes.tablerow} ><Timer_new datetime={value.cre_datetime} timestamp={value.timestamp} ></Timer_new></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>    

          {/* <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead className={classes.tablehead} >
                <TableRow>
                <TableCell className={classes.tablehead_txt}>Queue Name</TableCell>
                  <TableCell className={classes.tablehead_txt}>Current Queue calls</TableCell>
                  <TableCell className={classes.tablehead_txt}>Connected Calls</TableCell>
                  <TableCell className={classes.tablehead_txt} >Pending Calls</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>
                {queue_status.map((value, i) => (
                  <TableRow key={i}>
                    <TableCell className={classes.tablerow}>{value.queue_name}</TableCell>
                    <TableCell className={classes.tablerow}>{value.tot_queue_calls}</TableCell>
                    <TableCell className={classes.tablerow} >{value.tot_connect_calls}</TableCell>
                    <TableCell className={classes.tablerow} >{value.tot_pending_calls}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>     */}
          {/* }
          </Paper> */}
        </Grid>
        
      </Grid>
    </div>
    
  );

}
export default RealtimeDashboard;
