<!DOCTYPE html>
<html>


<body>
@include('header_new')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>test Edit Call Log Detail - {{"$contact->title . $contact->firstname $contact->lastname"}} </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-8" >
                <div class="box box-primary" >
                    <div class="box-header with-border" id="heders">
                    </div>
                    <!-- form start -->
                        <div  style="margin-left: -1.2cm">
                            <div class="box-body" >
                                <table style="margin-left: 1.5cm; width:80%">
                                    <form class="form-horizontal" method="POST" id="form"
                                          action="updatemanualcalllogDetail">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cusid" value="{{$_GET['id']}}">
										<input type="hidden" name="pho_number" value="{{$_GET['number']}}">
										<input type="hidden" name="callid" value="{{$_GET['callid']}}">
                                    
                                    
                                    <tr>
                                        <td>
                                            <div class="form-group marginelement">
                                                <label for="department" class="col-md-4 control-label labels"
                                                       style="padding-top: 10px; text-align: right">
                                                    Preset Remark 1</label>
                                                <div class="col-md-6 paddingmin" >

                                                <select class="form-control   textfeilds" style="width: 118%;" id="preset1" name="preset1" required
                                                        onchange="addPreset('preset1')">
                                                        <option value="All">Select Preset Remark 1</option>
                                                        <?php 
                                                            foreach($presets as $value){ ?>
                                                            @if($value->type==1)
                                                                <option value="<?php echo $value->id ?>"><?php echo $value->remark ?></option>
                                                            @endif
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                               
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group marginelement">
                                                <label for="department" class="col-md-4 control-label labels"
                                                       style="padding-top: 10px; text-align: right" >
                                                    Preset Remark 2</label>
                                                <div class="col-md-6 paddingmin" >

                                                <select class="form-control   textfeilds" style="width: 118%;" id="preset2" name="preset2" 
                                                        onchange="addPreset('preset2')">
                                                        <option value="All">Select Preset Remark 2</option>
                                                        <?php 
                                                            foreach($presets as $value){ ?>
                                                            @if($value->type==2)
                                                                <option value="<?php echo $value->id ?>"><?php echo $value->remark ?></option>
                                                            @endif
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                               
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group marginelement ">
                                                <label for="remark" class="col-md-4 control-label labels required" style="text-align: right">
                                                    Remark</label>
                                                <div class="col-md-7 paddingmin">
                                                    <textarea class="form-control" style="padding-bottom: 10px;height: 100%" rows="4"
                                                              name="remark" id="remark"></textarea>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
									<tr>
                                        <td>
                                            <div class="form-group marginelement">
                                               
                                                </div>
                                               
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
									<tr>
									<td>
                                            <div class="form-group marginelement">
                                                <label for="remark" class="col-md-4 control-label labels" style="text-align: right">
                                                    Previous Remark</label>
                                                <div class="col-md-7 paddingmin">
                                                    <textarea class="form-control" style="padding-bottom: 10px;height: 100%" rows="4"
                                                              name="remark_old" readonly id="remark_old" required><?php echo str_replace('<br>', "\n", $callhistory[0]->call_log); ?></textarea>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <div class="form-group marginelement" style="padding-top: 10px; padding-left: 70%" >
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-block btn-success btn-flat btnlength pull-right">Save</button>
                                                </div>
                                                <div class="col-md-6" >
                                                    <button type="button" class="btn btn-block btn-danger btn-flat btnlength"
                                                            onclick="cancelform()">Cancel</button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </form>
                                </table>
                            </div>
                        </div><br><br>
                        <!-- /.box-body -->
                        <!-- /.box-footer -->
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('footer')
<script>
function cancelform(){
    $.ajax({
            url: '{{url('cancelform')}}',
            type: 'GET',
            data: {formName:'addcalllog'},
            success: function (response)
            {
               if(response==1){
                location.href='{{url("contacts")}}'+'?contactid=all&status=All' ;
               }else{
                   //
               }
            }

                
            });
}


</script>

<script>
    $.datetimepicker.setLocale('en');

    $('.some_class').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss'
    });

    $('#datetimepicker_dark').datetimepicker({theme: 'dark'});

    $(".awesomplete").on('keyup', function (event) {
        if(event.keyCode==13) {
            addPreset(event.target.value);
        }
    });

    $(document).ready(function(){
        $(".awesomplete").attr("style","width:100%");
    });
</script>

<script>
 function addPreset(remark){
var totalRemark = document.getElementById('remark');
var secondrem = $("#"+remark).find('option:selected').text();

        totalRemark.value = totalRemark.value
            + ((totalRemark.value=='') ? '' : ' - ')
            + secondrem;
            // document.getElementById('preset1').value="";
            // document.getElementById('preset2').value="";
           
    }
</script>

</body>
</html>
      
