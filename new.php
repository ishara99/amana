    
          
    
    $callType =  $request->input('callType');
    $preset1 =  $request->input('category');
    $preset2 =  $request->input('inquiry');
    $remark =  $request->input('remark');
    $contact_num =  $request->input('contact_num');
    $cus_id = $request->input('cus_id');
    // print_r($cus_id); exit();
    $datentime = Carbon::now();
    $userid=session('userid');

    $com_id = util::get_com_id_by_user($userid);

    if($preset1 == "All"){

        $preset1 = "";
    }
    
    if($preset2 == "All"){

        $preset2 = "";
    }

    $saveinfo=DB::table('csp_callhistory')->insert(
        ['log_type' => $callType,
        'cat_one_prerem_id' => $preset1,
        'cat_two_prerem_id' => $preset2,
        'call_log' => "$remark",
        'pho_number' => "$contact_num",
        'created_datetime' => $datentime,
        'created_userid' => $userid,
        'cus_id'=>$cus_id
        ]

    );

        $datalog = array(
                    'pho_number' => "$contact_num",
                    'cus_id'=>$cus_id,
                    'log_type'=>$callType,
                    'cat_one_prerem_id' => $preset1,
                    'cat_two_prerem_id' => $preset2,
                    'call_log'=>"$remark",
                    'created_userid' => $userid,
                    'created_datetime' => NOW(),
                    'com_id' => $com_id,
                    
                );
      
            
                $insert=DB::table('csp_callhistory_detail')
                            ->insert($datalog);

    if($saveinfo){
        Session::flash('flash_message','successfully saved.');
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Add Call Log",$username,"Add Call Log");

        return redirect()->back();
        
          
    }

    // ==============================================

    public function retrieveCallhistoryDetail($contactid){
        return DB::table('csp_callhistory_detail')
            ->select('csp_callhistory_detail.*', 'user_master.username')
            ->join('user_master', 'user_master.id', '=', 'csp_callhistory_detail.created_userid')
            ->where('cus_id', $contactid)
            ->orderBy('created_datetime', 'desc')
            ->get();
    }
// ===========================================
document.getElementById("save_calllog_btn").disabled = true;
      setTimeout(function(){document.getElementById("save_calllog_btn").disabled = false;},5000);

	//document.getElementById('form_new').submit();
	var cusid=document.getElementById("cusid_calllog").value;
		var sipid=document.getElementById("sipid").value;
		var pho_number=document.getElementById("incomingcall_callLog").value;
		var remark=document.getElementById("remark").value;
		var category=document.getElementById("category").value;
		var inquiry=document.getElementById("inquiry").value;
		var firstname=document.getElementById("firstname").value;
		var lastname=document.getElementById("lastname").value;
        var agent=document.getElementById("agent").value;

        <?php if(isset($_GET['linkedid'])){ ?>
        	var linkid="{{$_GET['linkedid']}}";
        <?php }else{ ?>
	        var linkid=document.getElementById("linkid").value;
        <?php } ?>
    // ==============================================

    	$pho_number = $_GET['pho_number'];
	$category = $_GET['category'];
	$inquiry = $_GET['inquiry'];
	$remark = $_GET['remark'];
	$cusid = $_GET['cusid'];
	$userid=session('userid');
	$com_id = util::get_com_id_by_user($userid);
	$endpoint=session('endpoint');
	$today=date("Y-m-d");
	$linkedid=$_GET['linkid'];
	// dd($linkedid);
	$agent_sipid="";
		$getCallStatusInfo = DB::select("SELECT
											tbl_calls_evnt.linkedid,
											tbl_calls_evnt.`status`,
											tbl_calls_evnt.`agnt_sipid`
											FROM
											tbl_calls_evnt
											WHERE
											tbl_calls_evnt.frm_caller_num = '".$pho_number."'  AND tbl_calls_evnt.date='".$today."'
											ORDER BY
											tbl_calls_evnt.cre_datetime DESC
											LIMIT 1" );
											$linkedid=$linkedid;
											//$agent_sipid=$getCallStatusInfo[0]->agnt_sipid;

		$getLogInfo = DB::select("SELECT
											csp_callhistory.id,
											csp_callhistory.unq_id,
											csp_callhistory.pho_number,
											csp_callhistory.call_log
												FROM
											csp_callhistory
												WHERE
											csp_callhistory.unq_id ='".$linkedid."' AND
												csp_callhistory.pho_number like '%".$pho_number."%'" );

		

		if(!empty($getLogInfo)){

			$getQueueInfo = DB::select("SELECT agnt_queueid FROM `tbl_calls_evnt` WHERE linkedid = '".$linkedid."'" );
		 // dd($getQueueInfo);
			if(!empty($getLogInfo))
			{
				$queueid = $getQueueInfo[0]->agnt_queueid;
			}else
			{
				$queueid = "";
			}
			
			$date_time=date('Y-m-d H:i:s');
		
			$updateSuccess= DB::update('update csp_callhistory set
			cat_one_prerem_id=?,cat_two_prerem_id=?,call_log=?,queue_id=?,updated_datetime=? where unq_id = ? AND pho_number like ? ORDER BY call_datetime DESC',
		   [$category,$inquiry,$getLogInfo[0]->call_log." - ".$remark,$queueid,$date_time,$linkedid,"%".$pho_number."%"]);
			 // dd($updateSuccess);


			$data = array(
					'unq_id' => $linkedid,
					'call_datetime' => NOW(),
					'pho_number' => $pho_number,
					'sipid' => $endpoint,
					'queue_id'=>$queueid,
					'cus_id'=>$cusid,
					'log_type'=>"Inbound",
					'call_log'=> $remark,
					'cat_one_prerem_id'=>$category,
					'cat_two_prerem_id'=>$inquiry,
					'created_userid' => $userid,
					'created_datetime' => NOW(),
					'com_id' => $com_id,
					
				);
	  
			
			$insertSuccess=DB::table('csp_callhistory_detail')
				->insert($data);
	
			if($updateSuccess){
				return  'Call Log Added Successfully!';
			}else{
				return 'Data Saving Error! ';
			}	
		}

// =-======================================================
function SaveCallbackLog(){

		$pho_number = $_GET['pho_number'];
		$category = $_GET['category'];
		$inquiry = $_GET['inquiry'];
		$remark = $_GET['remark'];
		// $logtypetext = $_GET['logtypetext'];

		$userid=session('userid');
		$com_id = util::get_com_id_by_user($userid);
		$endpoint=session('endpoint');
		$today=date("Y-m-d");
		$linkedid=$_GET['linkid'];
		$agent_sipid="";

		$getCallStatusInfo = DB::select("SELECT tbl_calls_evnt.linkedid, tbl_calls_evnt.`status`, tbl_calls_evnt.`agnt_sipid` FROM tbl_calls_evnt WHERE tbl_calls_evnt.frm_caller_num = '$pho_number'  AND tbl_calls_evnt.date='$today' ORDER BY tbl_calls_evnt.cre_datetime DESC LIMIT 1" );

		$linkedid=$linkedid;
		//$agent_sipid=$getCallStatusInfo[0]->agnt_sipid;

		$getLogInfo = DB::select("SELECT csp_callhistory.id, csp_callhistory.unq_id, csp_callhistory.pho_number FROM csp_callhistory WHERE csp_callhistory.unq_id ='$linkedid' AND csp_callhistory.pho_number = '$pho_number'" ); 

		$selectStatus = DB::select("SELECT * FROM `csp_callhistory` WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND (`csp_callhistory`.`call_datetime` BETWEEN CONCAT(CURDATE()+ '00:00:00') AND NOW())"); 


		// $data = array(
		// 			'unq_id' => $linkedid,
		// 			'call_datetime' => NOW(),
		// 			'pho_number' => $pho_number,
		// 			'sipid' => $endpoint,
		// 			'queue_id'=>"",
		// 			'cus_id'=>"",
		// 			'log_type'=>"",
		// 			'call_log'=> $remark,
		// 			'cat_one_prerem_id'=>$category,
		// 			'cat_two_prerem_id'=>$inquiry,
		// 			'created_userid' => $userid,
		// 			'created_datetime' => NOW(),
		// 			'com_id' => $com_id,
					
		// 		);
	  
			
		// 	$insertSuccess=DB::table('csp_callhistory_detail')
		// 		->insert($data);

		if($selectStatus){

			if(!$selectStatus[0]->cat_one_prerem_id){

				$returnValue = DB::UPDATE("UPDATE `csp_callhistory`
					SET `csp_callhistory`.`cat_one_prerem_id` = '$category', `csp_callhistory`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory`.`callback_Status` = 'Dialed With Comment', `csp_callhistory`.`call_log` = '$remark' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND (`csp_callhistory`.`call_datetime` BETWEEN CONCAT(CURDATE()+ '00:00:00') AND NOW())");

				$r = DB::UPDATE("UPDATE `csp_callhistory_detail`
					SET `csp_callhistory_detail`.`cat_one_prerem_id` = '$category', `csp_callhistory_detail`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory_detail`.`callback_Status` = 'Dialed With Comment', `csp_callhistory_detail`.`call_log` = '$remark' 
					WHERE `csp_callhistory_detail`.`unq_id` = '$linkedid' and (`csp_callhistory_detail`.`log_type` ='CallbackRequest' OR `csp_callhistory_detail`.`log_type` ='AbandonCallback')");


				if($returnValue != 0){

					echo 'Call Log Added Successfully!';

				}else{

					echo 'Data Saving Error!1 ';
				}

			}else{

				$returnValue = DB::UPDATE("UPDATE `csp_callhistory`
					INNER JOIN `tbl_calls_evnt`
					ON `csp_callhistory`.unq_id =  `tbl_calls_evnt`.`linkedid`
					SET `csp_callhistory`.`cat_one_prerem_id` = '$category', `csp_callhistory`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory`.`callback_Status` = 'Dialed With Comment', `csp_callhistory`.`call_log` = '$remark' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND `csp_callhistory`.`unq_id` = '$linkedid' ");

				$r = DB::UPDATE("UPDATE `csp_callhistory_detail`
					SET `csp_callhistory_detail`.`cat_one_prerem_id` = '$category', `csp_callhistory_detail`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory_detail`.`callback_Status` = 'Dialed With Comment', `csp_callhistory_detail`.`call_log` = '$remark' 
					WHERE `csp_callhistory_detail`.`unq_id` = '$linkedid' and (`csp_callhistory_detail`.`log_type` ='CallbackRequest' OR `csp_callhistory_detail`.`log_type` ='AbandonCallback')");

				$returns = DB::UPDATE("UPDATE `csp_callhistory`
					SET `csp_callhistory`.`callback_Status` = 'Dialed With Comment' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND (`csp_callhistory`.`call_datetime` BETWEEN CONCAT(CURDATE()+ '00:00:00') AND NOW())");


				if($returnValue != 0){

					echo 'Call Log Added Successfully!';

				}else{

					echo 'Data Saving Error!2 ';
				}
			}

		}else{

			$returnValue = DB::UPDATE("UPDATE `csp_callhistory`
					INNER JOIN `tbl_calls_evnt`
					ON `csp_callhistory`.unq_id =  `tbl_calls_evnt`.`linkedid`
					SET `csp_callhistory`.`cat_one_prerem_id` = '$category', `csp_callhistory`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory`.`callback_Status` = 'Dialed With Comment', `csp_callhistory`.`call_log` = '$remark' 
					WHERE `csp_callhistory`.`pho_number` = '$pho_number' AND `csp_callhistory`.`unq_id` = '$linkedid' ");

			$r = DB::UPDATE("UPDATE `csp_callhistory_detail`
					SET `csp_callhistory_detail`.`cat_one_prerem_id` = '$category', `csp_callhistory_detail`.`cat_two_prerem_id` = '$inquiry', `csp_callhistory_detail`.`callback_Status` = 'Dialed With Comment', `csp_callhistory_detail`.`call_log` = '$remark' 
					WHERE `csp_callhistory_detail`.`unq_id` = '$linkedid' and (`csp_callhistory_detail`.`log_type` ='CallbackRequest' OR `csp_callhistory_detail`.`log_type` ='AbandonCallback') ");


			if($returnValue != 0){

				echo 'Call Log Added Successfully!';

			}else{

				echo 'Data Saving Error!3 ';
			}

		}

		

	}
// ================================================
    public function retrieveCallhistoryRecordDetail($callid) {
    return DB::table('csp_callhistory_detail')
            ->select('csp_callhistory_detail.*', 'user_master.username')
            ->join('user_master', 'user_master.id', '=', 'csp_callhistory_detail.created_userid')
            ->where('csp_callhistory_detail.id', $callid)
            ->orderBy('created_datetime', 'desc')
            ->get();
    }
// ===========================================
    public function updatemanualcalllogDetail(Request $request) {

    $cusid = $request->input('cusid');
    $pho_number = $request->input('pho_number');
    $remark = $request->input('remark');
    $remark_old = $request->input('remark_old');
    $callid = $request->input('callid');
    $userid = session('userid');
     $com_id = util::get_com_id_by_user($userid);
    $preset1 = $request->input('preset1');
    $cur_date = date("Y-m-d h:i:s");
    $updatedRemark = $cur_date . '-' . $remark . "<br>"."Previous" . $remark_old;

    if ($preset1 == "All") {
        $preset1 = "";
    }
    $preset2 = $request->input('preset2');
    if ($preset2 == "All") {
        $preset2 = "";
    }

    $saveRecord = DB::table('csp_callhistory_detail')
        ->where('id', $callid)
        ->update([
        'cus_id' => $cusid,
        'cat_one_prerem_id' => $preset1,
        'cat_two_prerem_id' => $preset2,
        'call_log' => "$updatedRemark",
        'update_user' => $userid,
        'updated_datetime' => $cur_date,
    ]);



    $ipaddress = (new UsersController())->get_client_ip();
    $username = session()->get('username');
    Util::user_auth_log($ipaddress, "Supervisor Update Call Log", $username, "Update Call Log");
    return redirect()->back();
    }

	============================
	Route::get('view_callhistory_detail_report','CallHistoryDetReportController@view_callhistory_detail_report');
Route::get('search_callhistory_detail_report','CallHistoryDetReportController@search_callhistory_detail_report');
