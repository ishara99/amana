<!DOCTYPE html>
<html>

@include('header_new')
<script>
$(document).ready(function() {
    $('input[type="search"]').css({
        'width': '350px',
        'display': 'inline-block'
    });
});
</script>

<!-- Content Wrapper. Contains page content -->
<!-- Start body -->
<div class="col-lg-12 ">
    <h1 class="form_caption">Call Summary Report</h1>
</div>
<div class="container-fluid" style="margin-top: 7px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
            <form class="form-horizontal" action="{{url('searchContact')}}" method="post" id="form">
                <div class="form-group-inner">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="padding-left:0px;">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" >From</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 00:00:00'); ?>" name="frm_date"
                                            id="frm_date">

                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="user" name="user" value="{{ Session::get('username')}}" >
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <label class="login2 pull-right pull-right-pro" >To</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 23:00:00'); ?>" name="to_date"
                                            id="to_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-2 col-sm-12 col-xs-12" style="padding-left:0px;">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:0px">
                                  
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left:0px">
                                            <input  type="radio" id="company"  name="groupby" value="company" style="width: 100%;" onclick="byCompany(); "> 
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-left:0px"">
                                            <label >By Company</label>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:0px">
                                  
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left:0px">
                                            <input  type="radio" id="queue"  name="groupby" value="queue" style="width: 100%;" onclick="byQueue()"> 
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-left:0px"">
                                            <label >By Queue</label>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:0px">
                                  
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-left:0px">
                                            <input  type="radio" id="agent"  name="groupby" value="agent" style="width: 100%;" onclick="byAgent()"> 
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-left:0px"">
                                            <label >By Agent</label>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="com_id" name="com_id" onchange="load_qeues();load_agent();"
                                            required disabled>
                                            <option value="All">All Company</option>
                                            <?php 
                                            foreach($get_com_data as $com_value){ ?>
                                            <option value="<?php echo $com_value->id ?>">
                                                <?php echo $com_value->com_name ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="queue_id" name="queue_id" 
                                            required disabled>
                                            <option value="All">All Queue</option>
                                            <?php 
                                            foreach($getdndstatus as $value){ ?>
                                            <option value="<?php echo $value->extension ?>">
                                                <?php echo $value->descr ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="agent_id" name="agent_id"
                                            required disabled>
                                            <option value="All">All Agent</option>
                                            <?php 
                                            foreach($users as $value){ ?>
                                            <option value="<?php echo $value->id ?>"> <?php echo $value->username ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="button-style-four btn-mg-b-10">
                                            <button type="button" class="btn btn-custon-four btn-success attr_btn"
                                                style="width:78px; " onclick="search_form_data();   ">Search &nbsp</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                </div>
                            </div>
                        </div>
                        <br> 
                    </div> 
            </form>
        </div>
    </div>
    <div class="sparkline13-list" id="div_company">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
                <table id="call_sum_report_com" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                    width="100%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Company</p></th>
                            <th><p class="text-center">Offered Calls</p></th>
                            <th><p class="text-center">Answered Calls</p></th>
                            <th><p class="text-center">Abandon Calls</p></th>
                            <th><p class="text-center"> Abandon Rate</p></th>
                            <th><p class="text-center">Total Aba: Callback Reached</p></th>
                            <th><p class="text-center">Total Aba: Callback Rate</p></th>
                            <th><p class="text-center">Outbound Calls</p></th>
                            <th><p class="text-center">Outbound Answered calls</p></th>
                            <th><p class="text-center">CDBA - Call Disconnected By Agent</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="sparkline13-list" id="div_queue">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
                <table id="call_sum_report_queue" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                    width="100%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Company</p></th>
                            <th><p class="text-center">Queue Name</p></th>
                            <th><p class="text-center">Offered Calls</p></th>
                            <th><p class="text-center">Answered Calls</p></th>
                            <th><p class="text-center">Abandand Calls</p></th>
                            <th><p class="text-center">Abandand Rate</p></th>
                            <th><p class="text-center">Total Aba: Callback Reached</p></th>
                            <th><p class="text-center">Total Aba: Callback Rate</p></th>
                            <th><p class="text-center">CDBA - Call Disconnected By Agent</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="sparkline13-list" id="div_agent">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
                <table id="call_sum_report_agent" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                    width="100%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Company</p></th>
                            <th><p class="text-center">Agent Name</p></th>
                            <th><p class="text-center">Offered Calls</p></th>
                            <th><p class="text-center">Answered Calls</p></th>
                            <th><p class="text-center">Outbound Calls</p></th>
                            <th><p class="text-center">Outbound Answered calls</p></th>
                            <th><p class="text-center">CDBA - Call Disconnected By Agent</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">

</div>


<!-- ./col -->
</div>
<!-- /.row -->
</div>
<br>
<br>
<br>
<script>
function load_qeues(){
	var com_id = document.getElementById("com_id").value;
	$.ajax({
            url: 'get_queue_data',
            type: 'GET',
            data: {com_id: com_id},
            success: function (response)
            {
                var model = $('#queue_id');
                model.empty();

                var datalist;
                datalist += "<option value='All'>All Queue</option>";
                $.each(response, function (index, element) {
                    $.each(element, function (colIndex, c) {
                        datalist += "<option value='" + c.extension + "'>" + c.descr+ "</option>";
                    });
                });
                $('#queue_id').html(datalist);
            }        
        });
    }
</script>
<script>
    function load_agent(){
        
	var com_id = document.getElementById("com_id").value;

    //alert(com_id);

	$.ajax({
        url: 'get_agent_data',
            type: 'GET',
            data: {com_id: com_id},
            success: function (response)
            {
                var model = $('#agent_id');
                model.empty();

                var agent_datalist;
                agent_datalist += "<option value='All'>All Agent</option>";
                $.each(response, function (index, element) {
                    $.each(element, function (colIndex, c) {
                        agent_datalist += "<option value='" + c.id + "'>" + c.username+ "</option>";
                    });
                });
                $('#agent_id').html(agent_datalist);
            }       
        });
    }
</script>
<script>
function validateNumber(obj) 
{
    var regex = /^[0-9]+$/;
    if (!obj.match(regex)) 
    {
        document.getElementById('contactnumber').value = '';
    } else 
    {
    }
}

function settodate(obj) 
{
 
    var fromdate = new Date(obj);
    fromdate.setDate(fromdate.getDate() + 7);
    var CurrentDateTime = fromdate.getFullYear().toString() + "-" + ((fromdate.getMonth() + 1).toString().length == 2 ?
            (fromdate.getMonth() + 1).toString() : "0" + (fromdate.getMonth() + 1).toString()) + "-" + (fromdate
            .getDate().toString().length == 2 ? fromdate.getDate().toString() : "0" + fromdate.getDate().toString()) +
        " " + (fromdate.getHours().toString().length == 2 ? fromdate.getHours().toString() : "0" + fromdate.getHours()
            .toString()) + ":" + ((parseInt(fromdate.getMinutes() / 5) * 5).toString().length == 2 ? (parseInt(fromdate
            .getMinutes() / 5) * 5).toString() : "0" + (parseInt(fromdate.getMinutes() / 5) * 5).toString()) + ":00";

    $('#enddate').val(CurrentDateTime);


}

function cancelform() 
{
    history.back();
}

function search_form_data() 
{
    var com_id = document.getElementById("com_id").value;
    var com_id_ = $('#com_id option:selected').text();
    com_id_1 = com_id_.trim();
    var to_date = document.getElementById("to_date").value;
    var frm_date = document.getElementById("frm_date").value;
    var user = document.getElementById("user").value;
    var report_name = "Call Summary Report - Company";
    var currentdate = new Date(); 
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds(); 
    if (document.getElementById('company').checked) {
        
        search_by = document.getElementById("company").value;
    // company table

    $("#call_sum_report_com").dataTable().fnDestroy();

    $('#call_sum_report_com').DataTable({
    "processing": true,
    "ajax": {
        "url": 'search_call_sum_rep_by_company',

        "type": "GET",
        data: {
            agent_id: agent_id,
            to_date: to_date,
            frm_date: frm_date,
            com_id: com_id,
            queue_id: queue_id,
            
        },
    },
    "columns": [
        {
            "data": "com_name"
        },
        {
            "data": "offerd_calls"
        },
        {
            "data": "answer_calls"
        },
        {
            "data": "tot_abn_calls"
        },
        {
            "data": "tot_abn_rate"
        },

        {
            "data": "tot_abn_callb_calls"
        },
        {
            "data": "tot_abn_callb_rate"
        },
        {
            "data": "tot_ob_calls"
        },
        {
            "data": "tot_ob_ans_calls"
        },
        {
            "data": "tot_cda_calls"
        }

        
    ],
    "columnDefs": [{
        className: "text-center",
        "targets": [0]
    },
    { "width": "10%", "targets": [0] }
    ],
    "order": [
        [1, "desc"]
    ],
    "pageLength": 25,

    dom: 'Bfrtip',

    buttons: [
        {
            extend: 'pdfHtml5',
            orientation: 'landscape', 
            pageSize: 'A4',
            title: '',
            filename: report_name.toString(),
           
            customize: function (doc) 
            {
               
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',                      
                            },
                            {
                                alignment: 'left',
                                text: ['To Date: ',{ text: to_date.toString() }]            
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['Date Time: ',{ text: datetime.toString() }],                  
                            },
                            {
                                alignment: 'left',
                                text: ['From Date: ',{ text: frm_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     }); 
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['User: ',{ text: user.toString() }],                                              
                            },
                            {
                                alignment: 'left',                                          
                                text: ['Company: ',{ text: com_id_1.toString() }],                    
                            }
                        ],
                            margin: 5
                     });    

                     doc['header']=(function() {
                        return {
                            columns: [
                                {
                                    alignment: 'center',  
                                    fontSize: 12,                        
                                    text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                }

                            ],
                            margin: 20
                        }
                    });                 
            }       
        },
        {             
            extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',
                
            customize: function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var numrows = 3;
                var clR = $('row', sheet);

                //update Row
                clR.each(function () {
                    var attr = $(this).attr('r');
                    var ind = parseInt(attr);
                    ind = ind + numrows;
                    $(this).attr("r",ind);
                });

                // Create row before data
                $('row c ', sheet).each(function () {
                    var attr = $(this).attr('r');
                    var pre = attr.substring(0, 1);
                    var ind = parseInt(attr.substring(1, attr.length));
                    ind = ind + numrows;
                    $(this).attr("r", pre + ind);
                });

                function Addrow(index,data) {
                    msg='<row r="'+index+'">'
                    for(i=0;i<data.length;i++){
                        var key=data[i].key;
                        var value=data[i].value;
                        msg += '<c t="inlineStr" r="' + key + index + '">';
                        msg += '<is>';
                        msg +=  '<t>'+value+'</t>';
                        msg+=  '</is>';
                        msg+='</c>';
                    }
                    msg += '</row>';
                    return msg;
                }

                //insert
                var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                    { key: 'B', value: 'Date Time :'+datetime },
                                    { key: 'C', value: 'Report Generated User :'+user }
                                ]);

                var r2 = Addrow(2, [{ key: 'A', value: 'Company :'+com_id_1 }, 
                                    { key: 'B', value: 'From Date :'+frm_date},
                                    { key: 'C', value: 'To Date :'+to_date}
                                    ]);
                sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
            }
		} 
        ]
});
    }

    if (document.getElementById('queue').checked) {
        search_by = document.getElementById("queue").value;
        var queue_id = document.getElementById("queue_id").value;
        var queue_id_ = $('#queue_id option:selected').text();
        queue_id_1 = queue_id_.trim();
        var to_date = document.getElementById("to_date").value;
        var frm_date = document.getElementById("frm_date").value;
        var user = document.getElementById("user").value;
        var report_name = "Call Summary Report - Queue";
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds(); 
    $("#call_sum_report_queue").dataTable().fnDestroy();
    $('#call_sum_report_queue').DataTable({
        "processing": true,
        "ajax": {
            "url": 'search_call_sum_rep_by_queue',

            "type": "GET",
            data: {
                agent_id: agent_id,
                to_date: to_date,
                frm_date: frm_date,
                com_id: com_id,
                queue_id: queue_id,
                
            },
        },
        "columns": [
            {
                "data": "com_name"
            },
             {
                "data": "descr"
            },
            {
                "data": "offerd_calls"
            },
            {
                "data": "answer_calls"
            },
            {
                "data": "tot_abn_calls"
            },
            {
                "data": "tot_abn_rate"
            },
            {
                "data": "tot_abn_callb_calls"
            },
            {
                "data": "tot_abn_callb_rate"
            },
            {
                "data": "tot_cda_calls"
            }

            
        ],
        "columnDefs": [{
            className: "text-center",
            "targets": [0]
        },
        { "width": "10%", "targets": [0] }
        ],
        "order": [
            [1, "desc"]
        ],
        "pageLength": 25,

        dom: 'Bfrtip',

        buttons: [
        {
            extend: 'pdfHtml5',
            orientation: 'landscape', 
            pageSize: 'A4',
            title: '',
            filename: report_name.toString(),
           
            customize: function (doc) 
            {          
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',                      
                            },
                            {
                                alignment: 'left',
                                text: ['To Date: ',{ text: to_date.toString() }]            
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['Date Time: ',{ text: datetime.toString() }],            
                            },
                            {
                                alignment: 'left',
                                text: ['From Date: ',{ text: frm_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     }); 
                     doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['User: ',{ text: user.toString() }],              
                            },
                            {
                                alignment: 'left',
                                text: ['Queue: ',{ text: queue_id_1.toString() }]                       
                            }
                        ],
                            margin: 5
                     });
                     doc['header']=(function() {
                        return {
                            columns: [
                                {
                                    alignment: 'center',  
                                    fontSize: 12,                        
                                    text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                }

                            ],
                            margin: 20
                        }
                    });                 
            }       
        },
        {             
            extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',
                
            customize: function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var numrows = 3;
                var clR = $('row', sheet);

                //update Row
                clR.each(function () {
                    var attr = $(this).attr('r');
                    var ind = parseInt(attr);
                    ind = ind + numrows;
                    $(this).attr("r",ind);
                });

                // Create row before data
                $('row c ', sheet).each(function () {
                    var attr = $(this).attr('r');
                    var pre = attr.substring(0, 1);
                    var ind = parseInt(attr.substring(1, attr.length));
                    ind = ind + numrows;
                    $(this).attr("r", pre + ind);
                });

                function Addrow(index,data) {
                    msg='<row r="'+index+'">'
                    for(i=0;i<data.length;i++){
                        var key=data[i].key;
                        var value=data[i].value;
                        msg += '<c t="inlineStr" r="' + key + index + '">';
                        msg += '<is>';
                        msg +=  '<t>'+value+'</t>';
                        msg+=  '</is>';
                        msg+='</c>';
                    }
                    msg += '</row>';
                    return msg;
                }

                //insert
                var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                    { key: 'B', value: 'Date Time :'+datetime },
                                    { key: 'C', value: 'Report Generated User :'+user }
                                ]);

                var r2 = Addrow(2, [ 
                                    { key: 'A', value: 'Queue:'+queue_id_1},
                                    { key: 'B', value: 'From Date :'+frm_date},
                                    { key: 'C', value: 'To Date :'+to_date}
                                    ]);
                sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
            }
		} 
        ]

    });
    }

    if (document.getElementById('agent').checked) {
        search_by = document.getElementById("agent").value;
        var agent_id = document.getElementById("agent_id").value;
        var agent_id_ = $('#agent_id option:selected').text();
        agent_id_1 = agent_id_.trim();
        var to_date = document.getElementById("to_date").value;
        var frm_date = document.getElementById("frm_date").value;
        var user = document.getElementById("user").value;
        var report_name = "Call Summary Report - Agent";
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds(); 
        $("#call_sum_report_agent").dataTable().fnDestroy();

        $('#call_sum_report_agent').DataTable({
        "processing": true,
        "ajax": {
            "url": 'search_call_sum_rep_by_agent',

            "type": "GET",
            data: {
                agent_id: agent_id,
                to_date: to_date,
                frm_date: frm_date,
                com_id: com_id,
                queue_id: queue_id,
                
            },
        },
        "columns": [
            {
                "data": "com_name"
            },
            {
                "data": "username"
            },
            {
                "data": "offerd_calls"
            },
            {
                "data": "answer_calls"
            },
            {
                "data": "tot_ob_calls"
            },
            {
                "data": "tot_ob_ans_calls"
            },
            {
                "data": "tot_cda_calls"
            }

            
        ],
        "columnDefs": [{
            className: "text-center",
            "targets": [0]
        },
        { "width": "10%", "targets": [0] }
        ],
        "order": [
            [1, "desc"]
        ],
        "pageLength": 25,

        dom: 'Bfrtip',

        buttons: [
        {
            extend: 'pdfHtml5',
            orientation: 'landscape', 
            pageSize: 'A4',
            title: '',
            filename: report_name.toString(),
           
            customize: function (doc) 
            {
               
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',                      
                            },
                            {
                                alignment: 'left',
                                text: ['To Date: ',{ text: to_date.toString() }]            
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['Date Time: ',{ text: datetime.toString() }],                
                            },
                            {
                                alignment: 'left',
                                text: ['From Date: ',{ text: frm_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     }); 
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['User: ',{ text: user.toString() }],                                              
                            },
                            {
                                alignment: 'left',                                          
                                text: ['Agent: ',{ text: agent_id_1.toString() }],                    
                            }
                        ],
                            margin: 5
                     });    

                     doc['header']=(function() {
                        return {
                            columns: [
                                {
                                    alignment: 'center',  
                                    fontSize: 12,                        
                                    text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                }

                            ],
                            margin: 20
                        }
                    });                 
            }       
        },
        {             
            extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',
                
            customize: function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var numrows = 3;
                var clR = $('row', sheet);

                //update Row
                clR.each(function () {
                    var attr = $(this).attr('r');
                    var ind = parseInt(attr);
                    ind = ind + numrows;
                    $(this).attr("r",ind);
                });

                // Create row before data
                $('row c ', sheet).each(function () {
                    var attr = $(this).attr('r');
                    var pre = attr.substring(0, 1);
                    var ind = parseInt(attr.substring(1, attr.length));
                    ind = ind + numrows;
                    $(this).attr("r", pre + ind);
                });

                function Addrow(index,data) {
                    msg='<row r="'+index+'">'
                    for(i=0;i<data.length;i++){
                        var key=data[i].key;
                        var value=data[i].value;
                        msg += '<c t="inlineStr" r="' + key + index + '">';
                        msg += '<is>';
                        msg +=  '<t>'+value+'</t>';
                        msg+=  '</is>';
                        msg+='</c>';
                    }
                    msg += '</row>';
                    return msg;
                }

                //insert
                var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                    { key: 'B', value: 'Date Time :'+datetime },
                                    { key: 'C', value: 'Report Generated User :'+user }
                                ]);

                var r2 = Addrow(2, [{ key: 'A', value: 'Agent :'+ agent_id_1 }, 
                                    { key: 'B', value: 'From Date :'+frm_date},
                                    { key: 'C', value: 'To Date :'+to_date}
                                    ]);
                sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
            }
		} 
        ]

    });
    }   
    
}
function init() {
    var element = getsubdeps(document.getElementById('deps').value);
}
</script>
<script>
//$.datetimepicker.setLocale('en');
$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

</script>
<script>
$('#call_sum_report_com').DataTable({
    "processing": true,
    "pageLength": 25,
    "scrollX": true,
});
</script>
<script>
$('#call_sum_report_queue').DataTable({
    "processing": true,
    "pageLength": 25,
    "scrollX": true,
});
</script>
<script>
$('#call_sum_report_agent').DataTable({
    "processing": true,
    "pageLength": 25,
    "scrollX": true,
});

</script>
<script>
function fnShowHide( iCol )
{
    /* Get the DataTables object again - this is not a recreation, just a get of the object */
    var oTable = $('#call_sum_report').dataTable();
     
    var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
    oTable.fnSetColumnVis( iCol, bVis ? false : true );
   // fnShowHide(1);
}

function byCompany()
{
    //fnShowHide(1);
    document.getElementById("com_id").disabled = false;
    document.getElementById("queue_id").disabled = true;
    document.getElementById("agent_id").disabled = true;
    //Hide tables
    var company_table = document.getElementById("div_company");
    company_table.style.display = "block";

    var queue_table = document.getElementById("div_queue");
    queue_table.style.display = "none";

    var agent_table = document.getElementById("div_agent");
    agent_table.style.display = "none";
}
function byQueue() 
{
    document.getElementById("com_id").disabled = false;
    document.getElementById("queue_id").disabled = false;
    document.getElementById("agent_id").disabled = true;
    //hide tables
    var company_table = document.getElementById("div_company");
    company_table.style.display = "none";

    var queue_table = document.getElementById("div_queue");
    queue_table.style.display = "block";

    var agent_table = document.getElementById("div_agent");
    agent_table.style.display = "none";

}
function byAgent() 
{
    document.getElementById("com_id").disabled = false;
    document.getElementById("queue_id").disabled = false;
    document.getElementById("agent_id").disabled = false;

    //hide tables
    var company_table = document.getElementById("div_company");
    company_table.style.display = "none";

    var queue_table = document.getElementById("div_queue");
    queue_table.style.display = "none";

    var agent_table = document.getElementById("div_agent");
    agent_table.style.display = "block";

}
 
    //Hide tables when form loading
    var company_table = document.getElementById("div_company");
    company_table.style.display = "block";

    var queue_table = document.getElementById("div_queue");
    queue_table.style.display = "none";

    var agent_table = document.getElementById("div_agent");
    agent_table.style.display = "none";

</script>
@include('footer')

</body>

</html>