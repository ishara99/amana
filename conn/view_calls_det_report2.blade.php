<!DOCTYPE html>
<html>

@include('header_new')
<link href="{{ asset('public/dist_01/skin/blue.monday/css/jplayer.blue.monday.min.css') }}" rel="stylesheet">  
<script src="{{ asset('public/dist_01/js/jquery.jplayer.min.js') }}"></script>
<script>

</script>

<!-- Content Wrapper. Contains page content -->
<!-- Start body -->
<div class="content-wrapper">
<div class="col-lg-12 ">
    <h1 class="form_caption">Download Call Details Report</h1>
</div>
<div class="container-fluid" style="margin-left: 33px; margin-top: 7px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
            <form class="form-horizontal" action="{{url('searchContact')}}" method="post" id="form">
                <div class="form-group-inner">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="com_id" name="com_id" onchange="loadQeues();"
                                            required>
                                            <option value="All">Company</option>
                                            <?php 
                                            foreach($get_com_data as $com_value){ ?>
                                            <option value="<?php echo $com_value->id ?>">
                                                <?php echo $com_value->com_name ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="user" name="user" value="{{ Session::get('username')}}" >
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds"  id="queue_id" name="queue_id"
                                            required>
                                            <option value="All">Queue</option>
                                            <?php 
                                            foreach($getqueues as $value){ ?>
                                            <option value="<?php echo $value->extension ?>">
                                                <?php echo $value->extension.'-'.$value->descr ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds"  id="agnt_id" name="agnt_id"
                                            required>
                                            <option value="All">User</option>
                                            <?php 
                                            foreach($users as $value){ ?>
                                            <option value="<?php echo $value->id ?>">
                                                <?php echo $value->username?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 30px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">From</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 00:00:00'); ?>" name="frm_date"
                                            id="frm_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 30px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">To</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 23:00:00'); ?>" name="to_date"
                                            id="to_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="button-style-four btn-mg-b-10"  style="display: inline-flex;">
                                            <button type="button" class="btn btn-custon-four btn-success attr_btn"
                                                style="width:78px; " onclick="searchdata()">Search &nbsp</button>

                                            <button type="button" onclick="download_rep()" class="btn btn-custon-four btn-success attr_btn" style="width:116px; margin-left: 5px" >Download Excel &nbsp</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
       
    <div class="sparkline13-list">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
            <br>
            <table style="display: none">
            <tr>
            <td style="padding-left: 15px !important;">
            <div id="jquery_jplayer_1" class="jp-jplayer" style="width:20px"></div>
            <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
                <div class="jp-type-single">
                    <div class="jp-gui jp-interface" style="height: 42px">
                        <div class="jp-controls" style="padding: 0px">
                            <button class="jp-play" role="button" tabindex="0">play</button>
                            <button class="jp-stop" role="button" tabindex="0">stop</button>
                        
                        <div class="jp-progress jpprogressupdate" style="top: 13px;">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar"></div>
                            </div>
                        </div>
                        <div class="jp-volume-controls jpvolumecontrolsupdate" style="top: 13px;">
                            <button class="jp-mute" role="button" tabindex="0">mute</button>
                            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                            <div class="jp-volume-bar">
                                <div class="jp-volume-bar-value"></div>
                            </div>
                        </div>
                        <div class="jp-time-holder jptimeholderupate">
                            <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                            <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                            
                        </div>
                        </div>
                    </div>
            </div>
           </div>    
             </td>

           </tr></table>     <br><br>  
                <table id="queue_report" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                    width="150%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Company</p></th>
                            <th><p class="text-center">Call Date Time</p></th>
                            <th><p class="text-center">Hangup Date Time</p></th>
                            <th><p class="text-center">Call Type</p></th>
                            <th> <p class="text-center">Unique ID</p></th>
                            <th><p class="text-center">CLI</p></th>
                            <th><p class="text-center">Dial Number</p></th>
                            <th><p class="text-center">Status</p></th>
                            <th><p class="text-center">Description</p></th>
                            <th><p class="text-center">Queue</p></th>
                            <th><p class="text-center">Agent</p></th>
                            <th><p class="text-center">Sip Id</p></th>
                            <th><p class="text-center">Waiting Time(Ringing)</p></th>
                            <th><p class="text-center">Duration</p></th>
                            <th><p class="text-center">Talk Time</p></th>
                            <th><p class="text-center">Held Time</p></th>
                            <th><p class="text-center">Acw Time</p></th>
                            <!-- <th><p class="text-center">Recording</p></th> -->
                            <!-- <th><p class="text-center">Download</p></th> -->

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br>
    <br>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">

</div>


<!-- ./col -->
</div>
<!-- /.row -->
</div>
<script>
function loadQeues(){
	var com_id = document.getElementById("com_id").value;
	$.ajax({
            url: 'getQeueData',
            type: 'GET',
            data: {com_id: com_id},
            success: function (response)
            {
                var model = $('#queue_id');
                model.empty();

                var datalist;
                datalist += "<option value='All'>Queue</option>";
                $.each(response, function (index, element) {
                    $.each(element, function (colIndex, c) {
                        datalist += "<option value='" + c.extension + "'>" + c.extension+"-"+c.descr+ "</option>";
                    });
                });
                $('#queue_id').html(datalist);
            }
        });
    }
</script>
<script>


function searchdata() {

     var agnt_id = document.getElementById("agnt_id").value;
    var agnt_id_ = $('#agnt_id option:selected').text();
    agnt_id_1 = agnt_id_.trim();
    var queue_id = document.getElementById("queue_id").value;
    var queue_id_ = $('#queue_id option:selected').text();
    queue_id_1 = queue_id_.trim();
    var to_date = document.getElementById("to_date").value;
    var frm_date = document.getElementById("frm_date").value;
    var com_id = document.getElementById("com_id").value;
    var com_id_ = $('#com_id option:selected').text();
    com_id_1 = com_id_.trim();
    var user = document.getElementById("user").value;
    var report_name = "Call Details Report";
    var currentdate = new Date(); 
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    $("#queue_report").dataTable().fnDestroy();

    $('#queue_report').DataTable({
        "processing": true,
        "scrollX": true,
        "ajax": {
            "url": "search_call_det_report2",

            "type": "GET",
            data: {
                queue_id: queue_id,
                to_date: to_date,
                frm_date: frm_date,
                agnt_id:agnt_id,
                com_id:com_id,

            },
        },
        "columns": [
	    {
                "data": "com_name"
            },
            {
                "data": "cre_datetime"
            },
            {
                "data": "hangup_datatime"
            },
            {
                "data": "call_type"
            },
            {
                "data": "uniqueid"
            },
            {
                "data": "frm_caller_num"
            },
            {
                "data": "to_caller_num"
            },
            {
                "data": "status"
            },
            {
                "data": "cdb_desc"
            },
            {
                "data": "agnt_queueid"
            },
            {
                "data": "username"
            },
            {
                "data": "agnt_sipid"
            },
            {
                "data": "waiting"
            },
            {
                "data": "tot_time"
            },
            {
                "data": "talk_time"
            },
            {
                "data": "hold_time"
            },
            {
                "data": "acw"
            }
            

        ],
        "columnDefs": [{
            className: "text-center",
            "targets": [1,2,3,4,5,6,7,8,10,11,12,13]
        },
        { "width": "15%", "targets": [9] }
        ],
        "order": [
            [0, "asc"]
        ],
        "pageLength": 25,

        dom: 'Bfrtip',

       buttons: 
        [
            {
                extend: 'pdfHtml5',
                orientation: 'landscape', 
                pageSize: 'A3',
                title: '',
                filename: report_name.toString(),
            
                customize: function (doc) 
                {
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['To date: ',{ text: to_date.toString() }],                    
                                }
                            ],
                                margin: 5
                        });  
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',              
                                },
                                {
                                    alignment: 'left',
                                    text: ['From date: ',{ text: frm_date.toString() }]                       
                                }
                            ],
                                margin: 5
                        });
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: '',                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['Agent: ',{ text:agnt_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        }); 
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: ['Date Time: ',{ text: datetime.toString() }],              
                                },
                                {
                                    alignment: 'left',
                                    text: ['Queue: ',{ text: queue_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        });
                        doc.content.splice( 0,0,{
                        columns:
                            [ 
                                {
                                    alignment: 'left',
                                    text: ['User: ',{ text: user.toString() }],                                              
                                },
                                {
                                    alignment: 'left',                                          
                                    text: ['Company: ',{ text: com_id_1.toString() }]                       
                                }
                            ],
                                margin: 5
                        });      

                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        alignment: 'center',  
                                        fontSize: 12,                        
                                        text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                    }

                                ],
                                margin: 20
                            }
                        });      
                }        
            },
            {           
                extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',  title:report_name.toString(),
                    
                customize: function (xlsx) 
                {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    var numrows = 3;
                    var clR = $('row', sheet);

                    //update Row
                    clR.each(function () {
                        var attr = $(this).attr('r');
                        var ind = parseInt(attr);
                        ind = ind + numrows;
                        $(this).attr("r",ind);
                    });

                    // Create row before data
                    $('row c ', sheet).each(function () {
                        var attr = $(this).attr('r');
                        var pre = attr.substring(0, 1);
                        var ind = parseInt(attr.substring(1, attr.length));
                        ind = ind + numrows;
                        $(this).attr("r", pre + ind);
                    });

                    function Addrow(index,data) {
                        msg='<row r="'+index+'">'
                        for(i=0;i<data.length;i++){
                            var key=data[i].key;
                            var value=data[i].value;
                            msg += '<c t="inlineStr" r="' + key + index + '">';
                            msg += '<is>';
                            msg +=  '<t>'+value+'</t>';
                            msg+=  '</is>';
                            msg+='</c>';
                        }
                        msg += '</row>';
                        return msg;
                    }
                    //insert
                    var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                        { key: 'B', value: 'Date Time :'+datetime },
                                        { key: 'C', value: 'Report Generated User :'+user }
                                    ]);

                    var r2 = Addrow(2, [{ key: 'A', value: 'Company:'+com_id_1},
                                        { key: 'B', value: 'Queue :'+queue_id_1},
                                        { key: 'C', value: 'Agent :'+agnt_id_1},
                                        { key: 'D', value: 'From Date :'+frm_date},
                                        { key: 'E', value: 'To Date :'+to_date}
                                        ]);
                    sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
                }
            } 
        ]
    });

    $('input[type="search"]').css({
        'width': '350px',
        'display': 'inline-block'
    });
}

function playvalue(url,id,all){
        var table = $('#queue_report').DataTable();

        // var rowid=all.parentNode.parentNode.getElementsByClassName('index')[0].innerText;
        // var newrow = all.parentNode.parentNode;

        // if(all.parentNode.parentNode!== previd && previd!='')
        // {    
        //     previd.removeAttribute("style");
        // // x.style.backgroundColor =  '#97e2a6';  
        // }

        // var x = all.parentNode.parentNode;
        // x.style.backgroundColor =  '#97e2a6'; 
        // previd= newrow;
             

            var ipadd="<?php echo $_SERVER['SERVER_ADDR'];?>";
            var company="<?php echo config('app.company_name');?>";
            var files="http://"+ipadd+"/"+company+"/callrecord_mount/"+url ;
            // alert(id);
            // $(document).ready(function(){
            //         $("#callplayer").attr("src", 'files');
            //     });
            // });
            $.ajax({
                url: 'playrecordfile2',
                type: 'GET',
                data: {record_url:files,id:id},
                success: function(response)
                {
                    //alert(response);
                    var file="<?php echo session()->get('playfile');?>";
                    $("#jquery_jplayer_1").jPlayer("destroy"); 
                    var ipadd="<?php echo $_SERVER['SERVER_ADDR'];?>";
                    // alert(files);
                    var files="http://"+ipadd+"/"+company+"/callrecord_temp/"+response ;
                    
                    $("#jquery_jplayer_1").jPlayer({
                        ready: function (event) {
                            $(this).jPlayer("setMedia", {
                                mp3: files
                            }).jPlayer("play"); 
                        },
                        swfPath: "../../dist/jplayer",
                        supplied: "mp3,m4a, oga",
                        wmode: "window",
                        useStateClassSkin: true,
                        autoBlur: false,
                        smoothPlayBar: true,
                        keyEnabled: true,
                        remainingDuration: true,
                        toggleDuration: true
                    });
                    
                    //$('#something').html(response);
                }
            });
            
     
    }
function init() {
    var element = getsubdeps(document.getElementById('deps').value);
}
</script>
<script>
//$.datetimepicker.setLocale('en');
$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

function download_rep(){

    var agnt_id = document.getElementById("agnt_id").value;
    var queue_id = document.getElementById("queue_id").value;
    var to_date = document.getElementById("to_date").value;
    var frm_date = document.getElementById("frm_date").value;
    var com_id = document.getElementById("com_id").value;
          
          // console.log(queue_id);  
            
            $.ajax({
            url: 'download_rep',
            type: 'GET',
            data: {   queue_id: queue_id,
                to_date: to_date,
                frm_date: frm_date,
                agnt_id:agnt_id,
                com_id:com_id,},
            success: function(response)
            {
                location.href=response;
            }
        });

        }

</script>
<script>
$('#queue_report').DataTable({
    "processing": true,
    "pageLength": 25,
    "scrollX": true,
});
</script>
@include('footer')

</body>

</html>