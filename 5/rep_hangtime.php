<?php
        $host = "localhost";
        $username = "root";
        $password = "passw0rd";
        $database_name = "phonikip_db";
        $remark= "cron job";
        $date=date('Y-m-d H:i:s');
        $date1=date('YmdHis');
        $massage="";
        // Get connection object and set the charset
        $conn = mysqli_connect($host, $username, $password, $database_name);
     
        //echo "$date <br>";
        if ($conn->connect_error) 
        {
            die("Connection failed: " . $conn->connect_error);
            
        }
        
        $getstartedcmpgns = "SELECT
                                tbl_calls_evnt.uniqueid as call_linkedid, 
                                tbl_calls_evnt.answer_datetime as call_anstime, 
                                tbl_calls_evnt.`status`, 
                                tbl_calls_evnt.id, 
                                tbl_calls_evnt.call_type, 
                                tbl_calls_evnt.hangup_datatime,
                                (
                                SELECT
                                asteriskcdrdb.cel.eventtime
                                FROM
                                asteriskcdrdb.cel
                                WHERE
                                asteriskcdrdb.cel.uniqueid = call_linkedid AND
                                asteriskcdrdb.cel.eventtype = 'HANGUP' AND
                                (asteriskcdrdb.cel.context =  'from-internal' or asteriskcdrdb.cel.context =  'ext-queues') ORDER BY
                                asteriskcdrdb.cel.id DESC limit 1
                                
                                ) as call_hanguptime,
                                (SELECT TIMESTAMPDIFF(SECOND, call_anstime , call_hanguptime)) as ans_sec
                            FROM
                                tbl_calls_evnt
                            WHERE
                                tbl_calls_evnt.`status` = 'ANSWER' AND
                                 tbl_calls_evnt.`date` = DATE(NOW()) AND 
                                (tbl_calls_evnt.hangup_datatime IS NULL OR tbl_calls_evnt.hangup_datatime='0000-00-00 00:00:00') ";
        //echo $getstartedcmpgns;
        $result = mysqli_query($conn, $getstartedcmpgns);
        //var_dump($result);
        if (mysqli_num_rows($result) > 0) 
        {
            while($row = mysqli_fetch_assoc($result)) 
            {
                
                $call_linkedid = $row["call_linkedid"];
                $call_hanguptime = $row["call_hanguptime"];
                $ans_sec = $row["ans_sec"];
                
    
                $cmpgn_mst_update = "UPDATE tbl_calls_evnt SET tbl_calls_evnt.hangup_datatime = '$call_hanguptime',
                                    tbl_calls_evnt.answer_sec_count = '$ans_sec' 
                                    WHERE tbl_calls_evnt.uniqueid='$call_linkedid' 
                                    and tbl_calls_evnt.status='ANSWER'";
   
                //echo "$cmpgn_mst_update";
                if (mysqli_query($conn, $cmpgn_mst_update)) 
                {
                   echo "Record updated successfully";
                }else 
                {
                   echo "Error updating record: " . mysqli_error($conn);
                }

                
            }
        } 
        else 
        {
            echo "0 results";
        }

        $main = "SELECT
                                *
                                FROM tbl_calls_evnt where
                                 tbl_calls_evnt.`date` = DATE(NOW()) AND 
                                 tbl_calls_evnt.`status` = 'ANSWER' AND
                                tbl_calls_evnt.answer_sec_count IS NULL";
        //echo $getstartedcmpgns;
        $result = mysqli_query($conn, $main);
        //var_dump($result);
        if (mysqli_num_rows($result) > 0) 
        {
            while($row = mysqli_fetch_assoc($result)) 
            {
                
                $id = $row["id"];
                $answer_datetime = $row["answer_datetime"];
                $hangup_datatime = $row["hangup_datatime"];
                
                if($hangup_datatime>$answer_datetime){

                    $answer_sec_count=strtotime($hangup_datatime) - strtotime($answer_datetime);
        
                    $cmpgn_mst_update = "UPDATE tbl_calls_evnt SET tbl_calls_evnt.answer_sec_count = '$answer_sec_count'
                                        WHERE 
                                        tbl_calls_evnt.id='$id' ";
       
                    //echo "$cmpgn_mst_update";
                    if (mysqli_query($conn, $cmpgn_mst_update)) 
                    {
                       echo "Record updated successfully";
                    }else 
                    {
                       echo "Error updating record: " . mysqli_error($conn);
                    }
                }

                
            }
        } 
        else 
        {
            echo "0 results";
        }

        $sql = "SELECT * from tbl_calls_evnt where tbl_calls_evnt.acwend_datatime IS NOT NULL AND hangup_datatime IS NOT NULL ";
        $res = mysqli_query($conn, $sql);

    while($row = mysqli_fetch_assoc($res)) 
    {
        $id=$row['id'];

                $hangup_datatime = $row["hangup_datatime"];
                $acwend_datatime = $row["acwend_datatime"];
                
        if($hangup_datatime!='0000-00-00 00:00:00'){
            

            if($acwend_datatime>$hangup_datatime){

                        $acw_sec_count=strtotime($acwend_datatime) - strtotime($hangup_datatime);

                $sql2 = "UPDATE tbl_calls_evnt set acw_sec_count='$acw_sec_count',to_trans_no=2 where id = '$id'";
                $res2 = mysqli_query($conn, $sql2);

            }

        }
        
        echo $i."<br>";
        $i++;
    }

        //var_dump($result);
        mysqli_close($conn);
        
        // /var/www/html/dpmc_new_cc/cronjobs/rep_hangtime.php

        ?>
