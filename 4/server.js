// Require needed modules and initialize Express app
//https://alligator.io/nodejs/server-sent-events-build-realtime-app/

/*
add these lines to bellow file path
/etc/my.cnf 
[mysqld]
log-bin=mysql-bin
expire-logs-days=7
log-bin-index=bin-log.index
max_binlog_size=100M
binlog_format=row
*/
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

var axios = require('axios'); 
const app = express();
//const app_agent = express();
const mysql = require('mysql');
const MySQLEvents = require('@rodrigogs/mysql-events');
const ora = require('ora'); // cool spinner
const json = require('json');
var moment = require('moment'); 
var ip = require('ip');
var count =1;
//console.log(ip.address()); 
const spinner = ora({
  text: 'ðŸ›¸',
  color: 'blue',
  spinner: 'dots2'
});

const new_conn = mysql.createConnection({
  host: ip.address(),
  user: 'root',
  password: 'passw0rd',
  database : 'asterisk'
});

//--- part 1
//const program = async () => {
  const connection = mysql.createConnection({
    host: ip.address(),
    user: 'root',
    password: 'passw0rd',
    database : 'asterisk'
  });

  // const instance = new MySQLEvents(connection, {
  //   startAtEnd: true // to record only the new binary logs, if set to false or you didn'y provide it all the events will be console.logged after you start the app
  // });

//   const db = mysql.createConnection({
//     host     : ip.address(),
//     user     : 'root',
//     password : 'passw0rd',
//     database : 'asterisk'
// });
 // await instance.start();

//   instance.addTrigger({
//     name: 'monitoring all statments',
//     expression: 'asterisk.ps_contacts', // listen to TEST database !!!
//     statement: MySQLEvents.STATEMENTS.ALL, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
//     onEvent: e => {
// if(count % 2 == 0){
      
      setInterval(function(){
      const current_datetime = moment().format('Y-MM-DD');
          // let sql = "SELECT phonikip_db.tbl_agnt_evnt.id,";
          // sql += " asterisk.ps_contacts.`status`, ";
          // sql += " asterisk.ps_contacts.status_des, ";
          // sql += " asterisk.ps_contacts.endpoint, ";
          // sql += " asterisk.ps_contacts.update_datetime,"; 
          // sql += " asterisk.ps_contacts.linkedid as cli_linkedid,";
          // sql += " phonikip_db.tbl_calls_evnt.outoacw_sec_count,";
          // sql += " phonikip_db.tbl_calls_evnt.acw_sec_count, ";
          // sql += " phonikip_db.tbl_calls_evnt.linkedid,";
          // sql += " phonikip_db.user_master.username, ";
          // sql += " phonikip_db.user_master.com_id,";
          // sql += "( SELECT tbl_calls_evnt.frm_caller_num FROM phonikip_db.tbl_calls_evnt WHERE  tbl_calls_evnt.linkedid = cli_linkedid AND tbl_calls_evnt.status = 'ENTERQUEUE' AND date = '"+current_datetime+"' limit 1) AS call_cli,";
          // sql += "( SELECT tbl_calls_evnt.to_caller_num FROM phonikip_db.tbl_calls_evnt WHERE  tbl_calls_evnt.linkedid = cli_linkedid AND tbl_calls_evnt.call_type = 'outbound' AND date = '"+current_datetime+"' limit 1) AS out_call_cli";
          // sql += " from phonikip_db.tbl_agnt_evnt";
          // sql += " LEFT JOIN asterisk.ps_contacts  ON asterisk.ps_contacts.endpoint = phonikip_db.tbl_agnt_evnt.agnt_sipid";
          // sql += " Left JOIN phonikip_db.tbl_calls_evnt ON asterisk.ps_contacts.update_datetime = phonikip_db.tbl_calls_evnt.hangup_datatime ";
          // sql += " JOIN phonikip_db.user_master ON tbl_agnt_evnt.agnt_userid = phonikip_db.user_master.id";
          // sql += " where phonikip_db.tbl_agnt_evnt.agnt_event= 'Log Out Time' ";
          // sql += " and  phonikip_db.tbl_agnt_evnt.date = '"+current_datetime+"' and  phonikip_db.tbl_agnt_evnt.evnt_min_count = '0' and phonikip_db.tbl_agnt_evnt.agnt_sipid != '0' ";
          // sql += " Group by asterisk.ps_contacts.endpoint";

          let sql = "SELECT phonikip_db.tbl_agnt_evnt.id,";
          sql += " asterisk.ps_contacts.`status`, ";
          sql += " asterisk.ps_contacts.status_des, ";
          sql += " asterisk.ps_contacts.endpoint, ";
          sql += " asterisk.ps_contacts.update_datetime,"; 
          sql += " asterisk.ps_contacts.linkedid as cli_linkedid,";
          sql += " phonikip_db.tbl_calls_evnt.outoacw_sec_count,";
          sql += " phonikip_db.tbl_calls_evnt.acw_sec_count, ";
          sql += " phonikip_db.tbl_calls_evnt.linkedid,";
          sql += " phonikip_db.user_master.username, ";
          sql += " phonikip_db.user_master.com_id,";
          sql += "( SELECT tbl_calls_evnt.frm_caller_num FROM phonikip_db.tbl_calls_evnt WHERE  tbl_calls_evnt.linkedid = cli_linkedid AND tbl_calls_evnt.status = 'ENTERQUEUE' and phonikip_db.tbl_agnt_evnt.date = '"+current_datetime+"' limit 1) AS call_cli,";
          sql += "( SELECT tbl_calls_evnt.to_caller_num FROM phonikip_db.tbl_calls_evnt WHERE  tbl_calls_evnt.linkedid = cli_linkedid AND tbl_calls_evnt.call_type = 'outbound' and phonikip_db.tbl_agnt_evnt.date = '"+current_datetime+"' limit 1) AS out_call_cli";
          sql += " from phonikip_db.tbl_agnt_evnt";
          sql += " LEFT JOIN asterisk.ps_contacts  ON asterisk.ps_contacts.endpoint = phonikip_db.tbl_agnt_evnt.agnt_sipid";
          sql += " Left JOIN phonikip_db.tbl_calls_evnt ON asterisk.ps_contacts.update_datetime = phonikip_db.tbl_calls_evnt.hangup_datatime ";
          sql += " JOIN phonikip_db.user_master ON tbl_agnt_evnt.agnt_userid = phonikip_db.user_master.id";
          sql += " where phonikip_db.tbl_agnt_evnt.agnt_event= 'Log Out Time' ";
          sql += " and  phonikip_db.tbl_agnt_evnt.evnt_min_count = '0' and  phonikip_db.tbl_agnt_evnt.date = '"+current_datetime+"' and phonikip_db.tbl_agnt_evnt.agnt_sipid != '0' ";
          sql += " Group by asterisk.ps_contacts.endpoint";


  let query = connection.query(sql, (err, result) => {
 
    var data_row = {};
    var data_arr = new Array();
    var data = new Array();
      if(err) throw err;
      var i = 0;
      Object.keys(result).forEach(function(key) {
        var row = result[key];
        var endpoint = result[key]['endpoint'];
        var status = result[key]['status'];
        var status_des = result[key]['status_des'];
        var username = result[key]['username'];
        var datetime = result[key]['update_datetime'];
        var outoacw_sec_count = result[key]['outoacw_sec_count'];
        var cli_linkedid = result[key]['cli_linkedid'];
        var acw_sec_count = result[key]['acw_sec_count'];
        var call_cli = result[key]['call_cli'];
        var out_call_cli = result[key]['out_call_cli'];
        var com_id = result[key]['com_id'];
        
        var timestamp = new Date().getTime();
        
    
        
        data_row  = JSON.stringify({"endpoint":endpoint,
                                    "status" : status ,
                                    "status_des":status_des,
                                    "username":username,
                                    "datetime":datetime,
                                    "outoacw_sec_count":outoacw_sec_count,
                                    "cli_linkedid":cli_linkedid,
                                    "acw_sec_count":acw_sec_count,
                                    "call_cli":call_cli,
                                    "timestamp":timestamp,
                                    "out_call_cli":out_call_cli,
                                    "com_id":com_id
                                    
                                    });
       
        data_arr.push(data_row);
        
      });

      data[0] =data_arr;

      //     var data_sent_agent = JSON.stringify(data);
      //     sleep(1000).then(() => { 
      //   // setTimeout(function() {     
      //       var config = {
      //         method: 'post',
      //         url: 'http://'+ip.address()+':3008/nest_agent',
      //         headers: { 
      //           'Content-Type': 'application/json'
      //         },
      //         data : data_sent_agent
      //       };
            
      //       axios(config)
      //       .then(function (response) {
      //         //console.log(JSON.stringify(response.data));
      //       })
      //       .catch(function (error) {
      //         console.log(error);
      //       });

      //     //}, 3000);
      //   //curl.end();
      // });

  
        //---------------------- fetch data of summnery of the day -----------------------------
      const current_datetime = moment().format('Y-MM-DD');
      let sql_tiles = "SELECT";
          sql_tiles += " tbl_calls_evnt.id,";
          sql_tiles += " queues_config.com_id AS company_id,";
          sql_tiles += " tbl_calls_evnt.date AS currnt_date,";

          sql_tiles += " ( SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS answer_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' OR tbl_calls_evnt.DESC = 'CONNECT' ) AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS answer_calls, ";

          // sql_tiles += " (SELECT data as sl_x FROM phonikip_db.func_data where type='x') as sl_x,";
          // sql_tiles += " (SELECT data as abn_sl_y FROM phonikip_db.func_data where type='y') as abn_sl_y, ";

          sql_tiles += " ( SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS answer_calls_sl ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' ";
          sql_tiles += " AND queues_config.com_id = company_id AND tbl_calls_evnt.ring_sec_count < 21 ";
          sql_tiles += " ) AS answer_calls_sl, (";

          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS offerd_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS offerd_calls, (";

          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_abn_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.DESC = 'ABANDON' ";
          sql_tiles += " AND tbl_calls_evnt.ring_sec_count > 10 ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS tot_abn_calls,(";

          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.DESC = 'ABANDON' ";
          sql_tiles += " AND queues_config.com_id = company_id AND tbl_calls_evnt.ring_sec_count < 5";
          sql_tiles += " ) AS tot_abn_calls_sl,(";

          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_clbk_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.DESC = 'EXITWITHKEY' ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS tot_clbk_calls,(";
          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_connect_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' ";
          sql_tiles += " AND tbl_calls_evnt.DESC = 'CONNECT' ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS tot_connect_calls,(";
          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_pending_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' ";
          sql_tiles += " AND ( tbl_calls_evnt.DESC = 'RINGNOANSWER' OR tbl_calls_evnt.DESC is null ) ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS tot_pending_calls,(";
          sql_tiles += " SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_cda_calls ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = currnt_date ";
          sql_tiles += " AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' ";
          sql_tiles += " AND tbl_calls_evnt.DESC = 'COMPLETEAGENT'  ";
          sql_tiles += " AND queues_config.com_id = company_id ";
          sql_tiles += " ) AS tot_cda_calls,(";

      
          sql_tiles += " SELECT";
          sql_tiles += " COUNT(queue) as tot_rate_calls";
          sql_tiles += " FROM phonikip_db.tbl_call_rating";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_call_rating.call_datetime between '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00' ";
          sql_tiles += " AND rate_num IN (1,2,3,4,5)";
          sql_tiles += " ) AS tot_rate_calls,(";

          sql_tiles += " SELECT";
          sql_tiles += " COUNT(queue) as tot_tran_rate_calls";
          sql_tiles += " FROM phonikip_db.tbl_call_rating";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_call_rating.call_datetime between '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00' ";
          sql_tiles += " ) AS tot_tran_rate_calls,";

          sql_tiles += " ( SELECT";
          sql_tiles += " COUNT( tbl_callback_mst.id ) AS tot_completed_callbk_req ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_callback_mst";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_callback_mst.queue = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " DATE(tbl_callback_mst.datetime) = '"+current_datetime+"' ";
          sql_tiles += " AND (queues_config.com_id = company_id OR tbl_callback_mst.did IN (SELECT box_name from phonikip_db.tbl_srvbox_mst Where cat_id=2 and com_id=company_id)) ";
          sql_tiles += " AND tbl_callback_mst.status = 1 ";
          sql_tiles += " ) AS tot_completed_callbk_req, ";

          sql_tiles += " ( SELECT";
          sql_tiles += " COUNT( tbl_callback_mst.id ) AS tot_pending_callbk_req ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_callback_mst";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_callback_mst.queue = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " DATE(tbl_callback_mst.datetime) = '"+current_datetime+"' ";
          sql_tiles += " AND (queues_config.com_id = company_id OR tbl_callback_mst.did IN (SELECT box_name from phonikip_db.tbl_srvbox_mst Where cat_id=2 and com_id=company_id)) ";
          sql_tiles += " AND tbl_callback_mst.status = 0 ";
          sql_tiles += " ) AS tot_pending_callbk_req, ";

          sql_tiles += " ( SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_pending_abancallbk ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = '"+current_datetime+"' ";
          sql_tiles += " AND queues_config.com_id = company_id  ";
          sql_tiles += " AND tbl_calls_evnt.cbstatus = 0 ";
          sql_tiles += " AND tbl_calls_evnt.desc = 'ABANDON' ";
          sql_tiles += " ) AS tot_pending_abancallbk, ";

          sql_tiles += " ( SELECT";
          sql_tiles += " COUNT( tbl_calls_evnt.id ) AS tot_completed_abancallbk ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = '"+current_datetime+"' ";
          sql_tiles += " AND queues_config.com_id = company_id  ";
          sql_tiles += " AND tbl_calls_evnt.cbstatus = 1 ";
          sql_tiles += " AND tbl_calls_evnt.desc = 'ABANDON' ";
          sql_tiles += " ) AS tot_completed_abancallbk, ";

          sql_tiles += " ( SELECT ROUND(( tot_abn_calls / offerd_calls )* 100 , 1) AS tot_abn_rate ) AS tot_abn_rate, ";
          sql_tiles += " ( SELECT ROUND(( answer_calls_sl / (offerd_calls-tot_abn_calls_sl-tot_clbk_calls) )* 100 , 1) AS tot_sl_rate ) AS tot_sl_rate ";
          sql_tiles += " FROM";
          sql_tiles += " phonikip_db.tbl_calls_evnt";
          sql_tiles += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid = queues_config.extension ";
          sql_tiles += " WHERE";
          sql_tiles += " tbl_calls_evnt.date = '"+current_datetime+"' ";
          sql_tiles += " GROUP BY";
          sql_tiles += " queues_config.com_id";
       
       

        //console.log(sql_tiles);
        let query = connection.query(sql_tiles, (err, result_tiles) => {
          var data_summery = {};
          var data_arr_sum = new Array();
            if(err) throw err;
            var i = 0;
            Object.keys(result_tiles).forEach(function(key) {
             // console.log(result_tiles);
              var row = result_tiles[key];
              var offerd_calls = result_tiles[key]['offerd_calls'];
              var answer_calls = result_tiles[key]['answer_calls'];
              var tot_abn_calls = result_tiles[key]['tot_abn_calls'];
              var tot_connect_calls = result_tiles[key]['tot_connect_calls'];
              var tot_pending_calls = result_tiles[key]['tot_pending_calls'];
              var tot_abn_rate = result_tiles[key]['tot_abn_rate'];
              var company_id = result_tiles[key]['company_id'];
              var tot_ob_calls = result_tiles[key]['tot_ob_calls'];
              var tot_ob_ans_calls = result_tiles[key]['tot_ob_ans_calls'];
              var tot_cda_calls = result_tiles[key]['tot_cda_calls'];
              var tot_clbk_calls = result_tiles[key]['tot_clbk_calls'];
              var tot_rate_calls = result_tiles[key]['tot_rate_calls'];
              var tot_tran_rate_calls = result_tiles[key]['tot_tran_rate_calls'];
              var tot_sl_rate = result_tiles[key]['tot_sl_rate'];

              var tot_completed_callbk_req = result_tiles[key]['tot_completed_callbk_req'];
              var tot_pending_callbk_req = result_tiles[key]['tot_pending_callbk_req'];
              var tot_pending_abancallbk = result_tiles[key]['tot_pending_abancallbk'];
              var tot_completed_abancallbk = result_tiles[key]['tot_completed_abancallbk'];
             
              
              data_summery  = JSON.stringify({"offerd_calls":offerd_calls,
                                              "answer_calls" : answer_calls ,
                                              "tot_abn_calls":tot_abn_calls,
                                              "tot_connect_calls":tot_connect_calls,
                                              "tot_pending_calls":tot_pending_calls,
                                              "tot_abn_rate":tot_abn_rate,
                                              "tot_ob_calls":tot_ob_calls,
                                              "tot_ob_ans_calls":tot_ob_ans_calls,
                                              "tot_cda_calls":tot_cda_calls,
                                              "tot_clbk_calls":tot_clbk_calls,
                                              "tot_rate_calls":tot_rate_calls,
                                              "tot_tran_rate_calls":tot_tran_rate_calls,
                                              "tot_sl_rate":tot_sl_rate,
                                              
                                              "tot_completed_callbk_req":tot_completed_callbk_req,
                                              "tot_pending_callbk_req":tot_pending_callbk_req,
                                              "tot_pending_abancallbk":tot_pending_abancallbk,
                                              "tot_completed_abancallbk":tot_completed_abancallbk,
                                              "com_id":company_id
                                              });
            
              data_arr_sum.push(data_summery);
              
            });

    
            data[1] = data_arr_sum;

          //---------------------- fetch data of outbound calls -----------------------------
          const current_datetime = moment().format('Y-MM-DD');
          let sql_outbound = "SELECT user_master.com_id AS company_id,";
              sql_outbound += " tbl_calls_evnt.date AS currnt_date,";
              sql_outbound += " COUNT( tbl_calls_evnt.id ) AS tot_ob_calls,";
              sql_outbound += " (SELECT COUNT( tbl_calls_evnt.id ) AS tot_ob_ans_calls";
              sql_outbound += " FROM phonikip_db.tbl_calls_evnt ";
              sql_outbound += " INNER JOIN phonikip_db.user_master ON tbl_calls_evnt.agnt_userid= user_master.id";
              sql_outbound += " WHERE tbl_calls_evnt.date = currnt_date ";
              sql_outbound += " AND tbl_calls_evnt.call_type= 'outbound' ";
              sql_outbound += " AND user_master.com_id = company_id  AND tbl_calls_evnt.status= 'ANSWER'";
              sql_outbound += " ) AS tot_ob_ans_calls FROM";
              sql_outbound += " phonikip_db.tbl_calls_evnt ";
              sql_outbound += " INNER JOIN phonikip_db.user_master ON tbl_calls_evnt.agnt_userid= user_master.id";
              sql_outbound += " WHERE";
              sql_outbound += " tbl_calls_evnt.date = '"+current_datetime+"' AND tbl_calls_evnt.call_type= 'outbound' ";
              sql_outbound += " GROUP BY";
              sql_outbound += " user_master.com_id";


          //console.log(sql_outbound);
            query_outbound = connection.query(sql_outbound, (err, result_outbound) => {

            var data_outbound = {};
            var data_arr_outbound = new Array();
              if(err) throw err;
              var i = 0;
              Object.keys(result_outbound).forEach(function(key) {
                //console.log(result_outbound);
                var row = result_outbound[key];
                var tot_ob_calls = result_outbound[key]['tot_ob_calls'];
                var tot_ob_ans_calls = result_outbound[key]['tot_ob_ans_calls'];
                var com_id = result_outbound[key]['company_id'];
                

              
                
                data_outbound  = JSON.stringify({"tot_ob_calls" : tot_ob_calls ,
                                              "tot_ob_ans_calls":tot_ob_ans_calls,
                                              "com_id":com_id
                                            });
              
                data_arr_outbound.push(data_outbound);
                
              });

              

              data[2] = data_arr_outbound;


          });
        //---------------------- fetch data of Queue summery -----------------------------

        // let sql_queue = "SELECT asterisk.queues_config.extension as queue_id,";
        //     sql_queue += "asterisk.queues_config.descr as description, ";
        //     sql_queue += "asterisk.queues_config.com_id, ";
        //     sql_queue += "(SELECT CONCAT_WS('-', queue_id, description) as queue_name)as queue_name, ";
        //     sql_queue += "( SELECT  COUNT( tbl_calls_evnt.id ) AS tot_connect_calls FROM phonikip_db.tbl_calls_evnt WHERE tbl_calls_evnt.date = '"+current_datetime+"' and tbl_calls_evnt.agnt_queueid = queue_id ";
        //     sql_queue += "AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' AND tbl_calls_evnt.DESC = 'CONNECT' ) AS tot_connect_calls,";
        //     sql_queue += "( SELECT COUNT( tbl_calls_evnt.id ) AS tot_pending_calls FROM  phonikip_db.tbl_calls_evnt  WHERE  tbl_calls_evnt.date = '"+current_datetime+"' and tbl_calls_evnt.agnt_queueid = queue_id "; 
        //     sql_queue += "AND tbl_calls_evnt.STATUS = 'ENTERQUEUE'  AND (tbl_calls_evnt.DESC = 'RINGNOANSWER' or tbl_calls_evnt.DESC is null) ) AS tot_pending_calls,";
        //     sql_queue += "(select ROUND((tot_pending_calls + tot_connect_calls)) as tot_queue_calls )as tot_queue_calls ";
        //     sql_queue += "FROM asterisk.queues_config ";
            
       let sql_queue ="SELECT '' as queue_name,";
        sql_queue +=" '0' as tot_connect_calls,";
        sql_queue +=" '0' as tot_pending_calls,";
        sql_queue +=" '0' as tot_queue_calls,";
        sql_queue +=" '0' as com_id";

        //console.log(sql_queue);
        let query = connection.query(sql_queue, (err, result_queue) => {
      
          var data_queue = {};
          var data_arr_qu = new Array();
            if(err) throw err;
            var i = 0;
            Object.keys(result_queue).forEach(function(key) {
             // console.log(result_tiles);
              var row = result_queue[key];
              var queue_name = result_queue[key]['queue_name'];
              var tot_connect_calls = result_queue[key]['tot_connect_calls'];
              var tot_pending_calls = result_queue[key]['tot_pending_calls'];
              var tot_queue_calls = result_queue[key]['tot_queue_calls'];
              var com_id = result_queue[key]['com_id'];
              

             
              
              data_queue  = JSON.stringify({"queue_name":queue_name,
                                          "tot_connect_calls" : tot_connect_calls ,
                                          "tot_pending_calls":tot_pending_calls,
                                          "tot_queue_calls":tot_queue_calls,
                                          "com_id":com_id
                                          });
            
              data_arr_qu.push(data_queue);
              
            });



            data[3] = data_arr_qu;

    }); 


          // //---------------------- fetch data of Srv Module -----------------------------


          // let sql_srv = "SELECT tbl_com_mst.id AS company_id,";
          // sql_srv += " tbl_com_mst.com_name,";

          // sql_srv += " (SELECT Count(tbl_emlsrv_receive.id) as tot_eml_count FROM phonikip_db.tbl_emlsrv_receive";
          // sql_srv += " where tbl_emlsrv_receive.`com_id` = company_id and tbl_emlsrv_receive.cre_datetime BETWEEN '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00') AS tot_eml_count,";

          // sql_srv += " (SELECT Count(tbl_emlsrv_receive.id) as fsh_eml_count FROM phonikip_db.tbl_emlsrv_receive";
          // sql_srv += " where tbl_emlsrv_receive.`com_id` = company_id  and tbl_emlsrv_receive.`status` = 'FRESH') AS fsh_eml_count,";

          // sql_srv += " (SELECT Count(tbl_faxsrv_receive.id) as tot_fax_count FROM phonikip_db.tbl_faxsrv_receive";
          // sql_srv += " where tbl_faxsrv_receive.`com_id` = company_id and tbl_faxsrv_receive.cre_datetime BETWEEN '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00') AS tot_fax_count,";

          // sql_srv += " (SELECT Count(tbl_faxsrv_receive.id) as fsh_fax_count FROM phonikip_db.tbl_faxsrv_receive";
          // sql_srv += " where tbl_faxsrv_receive.`com_id` = company_id  and tbl_faxsrv_receive.`status` = 'FRESH') AS fsh_fax_count,";

          // sql_srv += " (SELECT Count(tbl_smssrv_receive.id) as tot_sms_count FROM phonikip_db.tbl_smssrv_receive";
          // sql_srv += " where tbl_smssrv_receive.`com_id` = company_id and tbl_smssrv_receive.cre_datetime BETWEEN '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00') AS tot_sms_count,";

          // sql_srv += " (SELECT Count(tbl_smssrv_receive.id) as fsh_sms_count FROM phonikip_db.tbl_smssrv_receive";
          // sql_srv += " where tbl_smssrv_receive.`com_id` = company_id  and tbl_smssrv_receive.`status` = 'FRESH') AS fsh_sms_count,";

          // sql_srv += " (SELECT Count(tbl_srvwac_mst.id) as tot_wac_count FROM phonikip_db.tbl_srvwac_mst ";
          // sql_srv += " where tbl_srvwac_mst.cre_datetime BETWEEN '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00') AS tot_wac_count,";

          // sql_srv += " (SELECT Count(tbl_srvwac_mst.id) as fsh_wac_count FROM phonikip_db.tbl_srvwac_mst";
          // sql_srv += " where tbl_srvwac_mst.`com_id` = company_id  and tbl_srvwac_mst.`w_Status` = 'FRESH') AS fsh_wac_count,";

          // sql_srv += " (SELECT Count(tbl_srvchat_mst.id) as tot_webc_count FROM phonikip_db.tbl_srvchat_mst";
          // sql_srv += " where tbl_srvchat_mst.`com_id` = company_id and tbl_srvchat_mst.cre_datetime BETWEEN '"+current_datetime+" 00:00:00' and '"+current_datetime+" 23:00:00') AS tot_webc_count,";

          // sql_srv += " (SELECT Count(tbl_srvchat_mst.id) as fsh_webc_count FROM phonikip_db.tbl_srvchat_mst";
          // sql_srv += " where tbl_srvchat_mst.`com_id` = company_id  and tbl_srvchat_mst.`chat_Status` = 'FRESH') AS fsh_webc_count";

          // sql_srv += " FROM phonikip_db.tbl_com_mst GROUP BY tbl_com_mst.id";

          let sql_srv="";
          sql_srv+="SELECT '0' as tot_eml_count,";
          sql_srv+=" '0' as fsh_eml_count,";
          sql_srv+=" '0' as tot_fax_count,";
          sql_srv+=" '0' as fsh_fax_count,";
          sql_srv+=" '0' as tot_sms_count,";
          sql_srv+=" '0' as fsh_sms_count,";
          sql_srv+=" '0' as tot_wac_count,";
          sql_srv+=" '0' as fsh_wac_count,";
          sql_srv+=" '0' as tot_webc_count,";
          sql_srv+=" '0' as fsh_webc_count,";
          sql_srv+=" '0' as company_id";


      //console.log(sql_srv);
      let query_srv = connection.query(sql_srv, (err, result_srv) => {

        var data_srv = {};
        var data_arr_srv = new Array();
          if(err) throw err;
          var i = 0;
          //console.log(result_srv);
          Object.keys(result_srv).forEach(function(key) {
           
            var row = result_srv[key];
            var tot_eml_count = result_srv[key]['tot_eml_count'];
            var fsh_eml_count = result_srv[key]['fsh_eml_count'];
            var tot_fax_count = result_srv[key]['tot_fax_count'];
            var fsh_fax_count = result_srv[key]['fsh_fax_count'];
            var tot_sms_count = result_srv[key]['tot_sms_count'];
            var fsh_sms_count = result_srv[key]['fsh_sms_count'];
            var tot_wac_count = result_srv[key]['tot_wac_count'];
            var fsh_wac_count = result_srv[key]['fsh_wac_count'];
            var tot_webc_count= result_srv[key]['tot_webc_count'];
            var fsh_webc_count= result_srv[key]['fsh_webc_count'];
            var com_id = result_srv[key]['company_id'];
            

          
            
            data_srv = JSON.stringify({"tot_eml_count":tot_eml_count,
                                        "fsh_eml_count" : fsh_eml_count ,
                                        "tot_fax_count":tot_fax_count,
                                        "fsh_fax_count":fsh_fax_count,
                                        "tot_sms_count":tot_sms_count,
                                        "fsh_sms_count":fsh_sms_count,
                                        "tot_wac_count":tot_wac_count,
                                        "fsh_wac_count":fsh_wac_count,
                                        "tot_webc_count":tot_webc_count,
                                        "fsh_webc_count":fsh_webc_count,
                                        "com_id":com_id
                                        });
          
            data_arr_srv.push(data_srv);
            
          });



          data[4] = data_arr_srv;

           //---------------------- fetch data of Queue status -----------------------------


        let sql_queue_status = "SELECT CASE WHEN tbl_calls_evnt.DESC = 'CONNECT' THEN 'CONNECT'";
            sql_queue_status += " WHEN tbl_calls_evnt.DESC  = 'RINGNOANSWER' THEN 'PENDING'";
            sql_queue_status += " WHEN tbl_calls_evnt.DESC  is null THEN 'PENDING'";
            sql_queue_status += " END AS call_status,";
            sql_queue_status += " tbl_calls_evnt.frm_caller_num,";
            sql_queue_status += " tbl_calls_evnt.agnt_queueid,";
            sql_queue_status += " tbl_calls_evnt.linkedid as call_linkedid,";
            sql_queue_status += " tbl_calls_evnt.uniqueid as call_uniqueid,";
            sql_queue_status += " tbl_calls_evnt.cre_datetime,";
            sql_queue_status += " asterisk.queues_config.com_id, ";
            sql_queue_status += " CONCAT(queues_config.extension, '-', queues_config.descr) as queue_name, ";
            sql_queue_status += " (SELECT user_master.username FROM phonikip_db.tbl_calls_evnt";
            sql_queue_status += " INNER JOIN phonikip_db.user_master ON tbl_calls_evnt.agnt_userid = user_master.id";
            sql_queue_status += " WHERE tbl_calls_evnt.uniqueid = call_uniqueid and tbl_calls_evnt.status = 'ANSWER' limit 1) as username";
            sql_queue_status += " FROM phonikip_db.tbl_calls_evnt ";
            sql_queue_status += " LEFT JOIN phonikip_db.user_master ON tbl_calls_evnt.agnt_userid = user_master.id";
            sql_queue_status += " INNER JOIN asterisk.queues_config ON tbl_calls_evnt.agnt_queueid= queues_config.extension";
            sql_queue_status += " WHERE tbl_calls_evnt.date = '"+current_datetime+"'";
            sql_queue_status += " AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' AND ( tbl_calls_evnt.DESC = 'CONNECT' or tbl_calls_evnt.DESC = 'RINGNOANSWER' or tbl_calls_evnt.DESC is null)";

    
        //console.log(sql_queue);
        let query = connection.query(sql_queue_status, (err, result_queue_status) => {
      
          var data_queue_status = {};
          var data_arr_qu_st = new Array();
            if(err) throw err;
            var i = 0;
            Object.keys(result_queue_status).forEach(function(key) {
            // console.log(result_tiles);
              var row = result_queue_status[key];
              var frm_caller_num = result_queue_status[key]['frm_caller_num'];
              var queue_name = result_queue_status[key]['queue_name'];
              var call_status = result_queue_status[key]['call_status'];
              var cre_datetime = result_queue_status[key]['cre_datetime'];
              var username = result_queue_status[key]['username'];
              var com_id = result_queue_status[key]['com_id'];
              var timestamp = new Date().getTime();
              

            
              
              data_queue_status  = JSON.stringify({"frm_caller_num":frm_caller_num,
                                          "call_status":call_status,
                                          "queue_name" : queue_name ,
                                          "cre_datetime":cre_datetime,
                                          "username":username,
                                          "com_id":com_id,
                                          "timestamp":timestamp
                                          });
            
              data_arr_qu_st.push(data_queue_status);
              
            });



            data[5] = data_arr_qu_st;
            //console.log(data_arr_qu_st);



            //---------------------- live queue data-----------------------------


            let sql_live_status = " SELECT";
            sql_live_status += " descr as `Queue_Name`,";
            sql_live_status += " com_id as `com_id`,";
            sql_live_status += " (select CONCAT('Local/',ps.id, '@from-queue/n,0')) as sip_num_str,";
            sql_live_status += " count(qd.id)  AS `Allocated_agents`,";
            sql_live_status += " (select count(distinct(q.data)) from asterisk.ps_contacts p join asterisk.queues_details q ON CONCAT('Local/',p.endpoint, '@from-queue/n,0')=q.data where q.id=extension)  AS `Agents_Logged`,";
            sql_live_status += " (select count(distinct(q.data)) from asterisk.ps_contacts p join asterisk.queues_details q ON CONCAT('Local/',p.endpoint, '@from-queue/n,0')=q.data Where q.id=extension and status='Online')  AS `Agents_Free`,";
            sql_live_status += " (select count(distinct(q.data)) from asterisk.ps_contacts p join asterisk.queues_details q ON CONCAT('Local/',p.endpoint, '@from-queue/n,0')=q.data Where q.id=extension and status!='Online')  AS `Agents_Busy`,";
            sql_live_status += " (select count(distinct(q.data)) from asterisk.ps_contacts p join asterisk.queues_details q ON CONCAT('Local/',p.endpoint, '@from-queue/n,0')=q.data Where q.id=extension and status='In Call')  AS `Agents_Occupied`,";
            sql_live_status += " (SELECT COUNT( tbl_calls_evnt.id ) AS offerd_calls";
            sql_live_status += " FROM";
            sql_live_status += " phonikip_db.tbl_calls_evnt";
            sql_live_status += " WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND tbl_calls_evnt.STATUS = 'ENTERQUEUE'";
            sql_live_status += " AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'  ";
            sql_live_status += " ) AS offerd_calls,";
            sql_live_status += " (SELECT";
            sql_live_status += " COUNT( tbl_calls_evnt.id ) AS answer_calls";
            sql_live_status += " FROM";
            sql_live_status += " phonikip_db.tbl_calls_evnt";
            sql_live_status += " WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT')            ";                    
            sql_live_status += " AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'";
            sql_live_status += " ) AS answer_calls,";
            sql_live_status += " (SELECT";
            sql_live_status += " COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl";
            sql_live_status += " FROM";
            sql_live_status += " phonikip_db.tbl_calls_evnt";
            sql_live_status += " WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND tbl_calls_evnt.DESC = 'ABANDON'";
            sql_live_status += " AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'";
            sql_live_status += " ) AS tot_abn_calls,";
            sql_live_status += " (SELECT";
            sql_live_status += " COUNT( tbl_calls_evnt.id ) AS answer_call_sl";
            sql_live_status += " FROM";
            sql_live_status += " phonikip_db.tbl_calls_evnt";
            sql_live_status += " WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' )";
            sql_live_status += " AND tbl_calls_evnt.ring_sec_count < (SELECT data FROM `phonikip_db`.`func_data` where type='x')";
            sql_live_status += " AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'";
            sql_live_status += " ) AS answer_call_sl,";
            sql_live_status += " (SELECT";
            sql_live_status += " COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl";
            sql_live_status += " FROM";
            sql_live_status += " phonikip_db.tbl_calls_evnt";
            sql_live_status += " WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND tbl_calls_evnt.DESC = 'ABANDON'";
            sql_live_status += " AND tbl_calls_evnt.ring_sec_count < (SELECT data FROM `phonikip_db`.`func_data` where type='y')";
            sql_live_status += " AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'";
            sql_live_status += " ) AS tot_abn_calls_sl,";
            sql_live_status += " (SELECT";
            sql_live_status += " COUNT( tbl_calls_evnt.id ) AS q_breakout_calls";
            sql_live_status += " FROM";
            sql_live_status += " phonikip_db.tbl_calls_evnt";
            sql_live_status += " WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND tbl_calls_evnt.desc = 'EXITWITHKEY'";
            sql_live_status += " AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'";
            sql_live_status += " ) AS q_breakout_calls,";
            
            sql_live_status += " (SELECT";
            sql_live_status += " SUM( tbl_calls_evnt.answer_sec_count) AS handling_time";
            sql_live_status += " FROM phonikip_db.tbl_calls_evnt  WHERE";
            sql_live_status += " tbl_calls_evnt.agnt_queueid = extension";
            sql_live_status += " AND tbl_calls_evnt.STATUS = 'ANSWER'               ";                 
            sql_live_status += "  AND `tbl_calls_evnt`.`date` ='"+current_datetime+"'";
            sql_live_status += "  ) AS handling_time,";
            
            sql_live_status += " (select ROUND((answer_call_sl/(offerd_calls-tot_abn_calls_sl-q_breakout_calls))*100 , 2)) as SLA,";
            sql_live_status += " (select ROUND((tot_abn_calls/offerd_calls)*100, 2)) as AbanRate,";
            sql_live_status += " (select SEC_TO_TIME(ROUND((handling_time/offerd_calls),0))) as AHT";
            
            sql_live_status += " from  asterisk.queues_config qc";
            sql_live_status += " left join asterisk.queues_details qd on qc.extension=qd.id";
            sql_live_status += " left join asterisk.ps_endpoints ps on qd.data =CONCAT('Local/',ps.id, '@from-queue/n,0')";
            sql_live_status += " where qd.keyword='member' group by qc.extension";



    //console.log(sql_live_status);
    let query = connection.query(sql_live_status, (err, result_live_status) => {
  
      var data_live_status = {};
      var data_arr_lv_st = new Array();
        if(err) throw err;
        var i = 0;
        Object.keys(result_live_status).forEach(function(key) {
        // console.log(result_tiles);
          var row = result_live_status[key];
          var Queue_Name = result_live_status[key]['Queue_Name'];
          var Allocated_agents = result_live_status[key]['Allocated_agents'];
          var Agents_Logged = result_live_status[key]['Agents_Logged'];
          var Agents_Free = result_live_status[key]['Agents_Free'];
          var Agents_Busy = result_live_status[key]['Agents_Busy'];
          var Agents_Occupied = result_live_status[key]['Agents_Occupied'];

          var offerd_calls = result_live_status[key]['offerd_calls'];
          var answer_calls = result_live_status[key]['answer_calls'];
          var tot_abn_calls_sl = result_live_status[key]['tot_abn_calls_sl'];
          var tot_abn_calls = result_live_status[key]['tot_abn_calls'];
          var SLA = result_live_status[key]['SLA'];
          var AbanRate = result_live_status[key]['AbanRate'];
          var AHT = result_live_status[key]['AHT'];
          var com_id = result_live_status[key]['com_id'];
          

        
          
          data_live_status  = JSON.stringify({"Queue_Name":Queue_Name,
                                      "Allocated_agents":Allocated_agents,
                                      "Agents_Logged" : Agents_Logged ,
                                      "Agents_Free":Agents_Free,
                                      "Agents_Busy":Agents_Busy,
                                      "Agents_Occupied":Agents_Occupied,
                                      "offerd_calls":offerd_calls,
                                      "answer_calls":answer_calls,
                                      "tot_abn_calls_sl":tot_abn_calls_sl,
                                      "tot_abn_calls":tot_abn_calls,
                                      "SLA":SLA,
                                      "AHT":AHT,
                                      "AbanRate":AbanRate,
                                      "com_id":com_id
                                      });
        
          data_arr_lv_st.push(data_live_status);
          
        });



        data[6] = data_arr_lv_st;
        //console.log(data);
        //await sleep(2000);
       var data_sent = JSON.stringify(data);
       console.clear();
       //Buffer.clear();
       //sleep(2000).then(() => { 
      // setTimeout(function() { 
        console.log(count)
        
          var config = {
            method: 'post',
            url: 'http://'+ip.address()+':3005/nest',
            headers: { 
              'Content-Type': 'application/json'
            },
            data : data_sent
          };
          
          axios(config)
          .then(function (response) {
            //console.log(JSON.stringify(response.data));
          })
          .catch(function (error) {
            console.log(error);
          });
        //}  
          

        //}, 3000);
      //curl.end();
    //});
      }); 
    });
}); 
    }); 
}); 
  
  },3000)
      //console.log(e);
      //spinner.succeed('ðŸ‘½ _EVENT_ ðŸ‘½');
//       spinner.start();
//     }
// } 
 
//   });

//   instance.on(MySQLEvents.EVENTS.CONNECTION_ERROR, console.error);
//   instance.on(MySQLEvents.EVENTS.ZONGJI_ERROR, console.error);



// };

// program()
//   .then(spinner.start.bind(spinner))
//   .catch(console.error);

//-- end part 1

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
} 


// Middleware for GET /events endpoint
function eventsHandler(req, res, next) {
  // Mandatory headers and http status to keep connection open
  const headers = {
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache'
  };
  res.writeHead(200, headers);

  // After client opens connection send all nests as string
  const data = `data: ${JSON.stringify(nests)}\n\n`;

  res.write(data);

  // Generate an id based on timestamp and save res
  // object of client connection on clients list
  // Later we'll iterate it and send updates to each client
  const clientId = Date.now();
  const newClient = {
    id: clientId,
    res
  };
  clients.push(newClient);

  // When client closes connection we update the clients list
  // avoiding the disconnected one
  req.on('close', () => {
    console.log(`${clientId} Connection closed`);
    clients = clients.filter(c => c.id !== clientId);
  });
}



function sendEventsToAll(newNest) {
  clients.forEach(c => c.res.write(`data: ${JSON.stringify(newNest)}\n\n`))
}

// Middleware for POST /nest endpoint
async function addNest(req, res, next) {
  const newNest = req.body;
  nests =newNest;

  // Send recently added nest as POST result
  res.json(newNest)

  // Invoke iterate and send function
  return sendEventsToAll(newNest);
}




// Middleware for POST /nest endpoint
async function updateacw(req, res, next) {

    const {endpoint, status,cli_linkedid,outoacw_sec_count} = req.body;
    // Update data

    if(cli_linkedid!="" || cli_linkedid!="undefined")
    {
      const update_datetime = moment().format('Y-M-D H:m:s');
  
      //var sql = "UPDATE asterisk.ps_contacts SET status = '"+status+"' , update_datetime = '"+update_datetime+"', status_des = '' WHERE endpoint ="+endpoint;
      var sql = "UPDATE asterisk.ps_contacts SET status = 'Online' , update_datetime = '"+update_datetime+"', status_des = '', linkedid = '' WHERE endpoint ="+endpoint;
      //console.log(sql);

      new_conn.query(sql, function (err, result) {
        if (err) throw err;
        console.log(result);
      });

 
      var sql_02 = "UPDATE phonikip_db.tbl_calls_evnt SET acw_sec_count = "+outoacw_sec_count+" , acwend_datatime = '"+update_datetime+"' WHERE linkedid ='"+cli_linkedid+"'";
      //console.log(sql_02);
      new_conn.query(sql_02, function (err, result_02) {
        if (err) throw err;
        console.log(result_02);
      });

    }

    


     
  

  //console.log(endpoint+" - "+status);

}

// Set cors and bodyParser middlewares
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Define endpoints
app.post('/nest', addNest);
//app.post('/add_call', add_call);
app.post('/updateacw', updateacw);
app.get('/events', eventsHandler);
//app.get('/call_events', call_eventsHandler);
app.get('/status', (req, res) => res.json({clients: clients.length}));
//app.get('/call_status', (req, res) => res.json({call_clients: call_clients.length}));

const PORT = 3005;

let clients = [];
//let call_clients = [];
let nests = [];

//let call_nests = [];

// Start server on 3000 port
app.listen(PORT, () =>"");






// // Middleware for GET /events endpoint
// function eventsHandler_agent(req, res, next) {
//   // Mandatory headers and http status to keep connection open
//   const headers = {
//     'Content-Type': 'text/event-stream',
//     'Connection': 'keep-alive',
//     'Cache-Control': 'no-cache'
//   };
//   res.writeHead(200, headers);

//   // After client opens connection send all nests as string
//   const data_agent = `data: ${JSON.stringify(nests_agent)}\n\n`;

//   res.write(data_agent);

//   // Generate an id based on timestamp and save res
//   // object of client connection on clients list
//   // Later we'll iterate it and send updates to each client
//   const clientId_agent = Date.now();
//   const newClient_agent = {
//     id: clientId_agent,
//     res
//   };
//   clients.push(newClient_agent);

//   // When client closes connection we update the clients list
//   // avoiding the disconnected one
//   req.on('close', () => {
//     console.log(`${clientId_agent} Connection closed`);
//     clients_agent = clients_agent.filter(c => c.id !== clientId_agent);
//   });
// }



// function sendEventsToAll_agent(newNest) {
//   clients_agent.forEach(c => c.res.write(`data: ${JSON.stringify(newNest)}\n\n`))
// }

// // Middleware for POST /nest endpoint
// async function addNest_agent(req, res, next) {
//   const newNest_agent = req.body;
//   nests_agent =newNest_agent;

//   // Send recently added nest as POST result
//   res.json(newNest_agent)

//   // Invoke iterate and send function
//   return sendEventsToAll_agent(newNest_agent);
// }






// app_agent.use(cors());
// app_agent.use(bodyParser.json());
// app_agent.use(bodyParser.urlencoded({extended: false}));

// // Define endpoints
// app_agent.post('/nest_agent', addNest_agent);
// //app.post('/add_call', add_call);
// app_agent.get('/events_agent', eventsHandler_agent);
// //app.get('/call_events', call_eventsHandler);
// app_agent.get('/status_agent', (req, res) => res.json({clients_agent: clients_agent.length}));
// //app.get('/call_status', (req, res) => res.json({call_clients: call_clients.length}));

// const PORT_agent = 3008;

// let clients_agent = [];
// //let call_clients = [];
// let nests_agent = [];
// //let call_nests = [];

// // Start server on 3000 port
// app_agent.listen(PORT_agent, "");
