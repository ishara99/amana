<!DOCTYPE html>
<html>
@include('header_new')
<script>
$(document).ready(function() {
    $('input[type="search"]').css({
        'width': '350px',
        'display': 'inline-block'
    });
});
</script>
<!-- Content Wrapper. Contains page content -->
<!-- Start body -->
<div class="content-wrapper">
<section class="content-header">
    <div class="col-lg-12 ">
		<h1 class="form_caption">Queue Allocation</h1>
    </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
        <form class="form-horizontal" action="" method="post" id="form" name="form">
   
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="tblTransfer" style="padding-left:0px;">
              <div class="form-group-inner">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-right: 0px;">
                    <select class="form-control   textfeilds" id="sip_id" name="sip_id" onchange="get_in_queue()" required>
                      <option @if($selected_sip=="0") selected @endif value="">Extension</option>
                      <?php foreach($get_sip_ids as $value){  ?>      
                      <option value="<?php echo $value->id ?>"

                       @if($selected_sip==$value->id) selected @endif >

                       <?php echo $value->id ?>
                         
                       </option>
                      <?php } ?>
                    </select>

                  </div>
                </div>
              </div>
              <!-- <table id="qn_tbl" width="100%" class="table table-bordered table-dragable"> -->
            </div>
            <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
            </div>
            <br><br><br><br>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="queue_allo" >
              <table width="100%" border="0" cellspacing="2" cellpadding="2" id="tbl_que">
                  <tr>
                      <input type="button" onclick='selectAll()' value="Select All" class="btn btn-custon-four btn-primary attr_btn"/>
                      <input type="button" onclick='UnSelectAll()' value="Unselect All" style="width: 90px;margin-left: 3px;" class="btn btn-custon-four btn-warning attr_btn"/>
                    <br><br>
                    <td> <table cellpadding="2" cellspacing="2" border="0" width="100%">
                    <tbody>
                    <?php
                    // print_r($getqueue);
                      $count = count($getqueue) ; 
                      $counter = 0;
                      // echo $count;
                        for ($i=0; $i < $count; $i++)
                        {  
                          if($i % 5 === 0)
                            {
                            echo "<tr style='border:  20px solid white;'>";
                            }
                          //  echo $counter;

                            echo "<td style='border:  20px solid white; width:15%' > ".$getqueue[$counter]->descr ,' (',$getqueue[$counter]->extension, ') '."";
                            
                            

                            echo "<input type='checkbox' name='getqueuelist[]'  value=".$getqueue[$counter]->extension." id='getqueuelist'"; 

                                          if($inqus!=null){            
                                              foreach($inqus as $inqu){

                                                if($inqu->id==$getqueue[$counter]->extension){

                                                  echo 'checked';

                                                }

                                                }

                                          } 

                            echo " />";




                            echo "</td>";
                              if($i % 5 === 4)
                              { 
                                echo "</tr>";
                              }
                            $counter ++; 
                        }                         
                      ?>
                      </tbody>
                      </table>                    
                    </td>
                  </tr>
                </table>  
            </div>
              <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="margin-top:29px;" >
                <div class="button-style-four btn-mg-b-10">
                    <button type="button" name="submit" class="btn btn-custon-four btn-success attr_btn" onclick="save_que_allo();" >Submit</button>
                    <button type="button" class="btn btn-custon-four btn-danger attr_btn" onclick="window.history.back();">Cancel</button>   
                </div>
              </div>
            {{csrf_field()}}
        </form>
      </div>
    </div>
  </div>
</section>
</div>
<br><br>
<script>
function save_que_allo(){
  var checkedq = []; 
  var uncheckedq = []; 
  var items=document.getElementsByName('getqueuelist[]');
  // alert(items);
  for(var i=0; items[i]; ++i){
      if(items[i].checked)
        {
          
          checkedq.push(items[i].value);
          // alert(checkedq);
        }
      else
      {
          uncheckedq.push(items[i].value);
          // alert(uncheckedq);
      }
    } 
    // alert(checkedq);
  var sip_id = document.getElementById('sip_id').value;
  if(sip_id==''){
    alert("Please select Extension!");
  }else{

  $.ajax({
      url: '../save_que_allo',
      type: 'GET',
      data: {sip_id: sip_id, checkedq:checkedq, uncheckedq:uncheckedq },
      success: function (response)
      {
        if(response == "OK")
        {
          alert("Changed Successfully!");
          location.href = "{{url ('view_que_allo')}}/"+sip_id;

        }else if(response == "Error_queue_is_missing"){
          alert("Error.. Some Queues are missing in config file!");
          location.href = "{{url ('view_que_allo')}}/"+sip_id;
        }
      }
    });
  }

  }

  function selectAll(){
    var items=document.getElementsByName('getqueuelist[]');
    // alert(items);
    for(var i=0; i<items.length; i++){
      if(items[i].type=='checkbox')
        items[i].checked=true;
    }
  } 
  function UnSelectAll(){
    var items=document.getElementsByName('getqueuelist[]');
    for(var i=0; i<items.length; i++){
      if(items[i].type=='checkbox')
        items[i].checked=false;
    }
  } 

function get_in_queue() 
{
  var get_sip = document.getElementById('sip_id').value;
            var url=window.location.href;
            if(url.match(/view_que_allo\//g)){
                get_sip="../view_que_allo/"+get_sip;
                window.location.href = get_sip;
            }
            else{
                get_sip="view_que_allo/"+get_sip;
                        window.location.href = get_sip;
            }
}


</script>
@include('footer')

</body>

</html>