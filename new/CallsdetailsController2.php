<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;
use PHPExcel;
use PHPExcel_IOFactory;

require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class CallsdetailsController2 extends Controller
{
  
    
    public function view_calls_det_report2(){	
		
		if(Util::isAuthorized("view_calls_det_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_calls_det_report")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log('Queue Details Report','View');
        $userid=session('userid');
        $usertypeid=session('usertypeid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getqueues  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();

                if($usertypeid=='17' || $usertypeid=='1'){
                    $users = DB::select("SELECT a.`allo_agent_id`,b.`id`,b.`username` 
                                         FROM `tbl_agent_allocation` as a 
                                         right join `user_master` as b ON a.`allo_agent_id`=b.`id`
                                         inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
                                         where c.`title`= 'Csp_Agent' and b.`com_id`= $get_com_id->com_id group by b.`username`  ;");
                        }else{
                            $users = DB::select("SELECT a.`allo_agent_id`,b.`id`,b.`username` 
                                               FROM `tbl_agent_allocation` as a 
                                               right join `user_master` as b ON a.`allo_agent_id`=b.`id`
                                               inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
                                               where c.`title`= 'Csp_Agent' and b.`com_id`= $get_com_id->com_id and b.id=".$userid." group by b.`username`  ;");
                        }
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Call Details Report Dashboard",$username,"View Call Details Report");

        return view('view_calls_det_report2',compact('getqueues','users','get_com_data'));   

	}

    public function search_call_det_report_old(Request $request){  
   
        $userid=session('userid');    
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first(); 
        $queue_id= $request->input('queue_id');
        $agnt_id= $request->input('agnt_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');


        $data = $dataq = DB::table('tbl_calls_evnt')
        ->select(
                 'user_master.username',
                 'tbl_calls_evnt.agnt_sipid',
                 'tbl_calls_evnt.date',
                 'tbl_calls_evnt.call_type',
                 'tbl_calls_evnt.to_caller_num',
                 'tbl_calls_evnt.frm_caller_num',
                 'tbl_calls_evnt.uniqueid',
                 'tbl_calls_evnt.cre_datetime',
                 /*'tbl_calls_evnt.agnt_queueid',*/
		DB::raw('CONCAT_WS("-",tbl_calls_evnt.agnt_queueid,asterisk.queues_config.descr) AS agnt_queueid'),
                 'tbl_calls_evnt.agnt_userid',
                 'tbl_calls_evnt.status',
                 'tbl_calls_evnt.desc',
                 'tbl_calls_evnt.linkedid as cur_linkedid',
		'tbl_com_mst.com_name',
                 DB::raw("(select SEC_TO_TIME(ROUND(tbl_calls_evnt.answer_sec_count))) as tot_time"),
                 
                 DB::raw("(SELECT tbl_calls_evnt.desc as cdb_desc FROM tbl_calls_evnt 
                                WHERE tbl_calls_evnt.linkedid=cur_linkedid and status = 'ENTERQUEUE'
                                 order by id desc limit 1 ) as cdb_desc"),

                 DB::raw("(SELECT ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count)) as hold_sec FROM tbl_calls_hold_evnts 
                                WHERE tbl_calls_hold_evnts.linkedid=tbl_calls_evnt.linkedid) as hold_sec"),
                
                 DB::raw("(SELECT tbl_calls_evnt.hangup_datatime as hangup_datatime FROM tbl_calls_evnt 
                                WHERE tbl_calls_evnt.linkedid=cur_linkedid
                                 order by id desc limit 1 ) as hangup_datatime"),
                DB::raw("(SELECT asteriskcdrdb.cdr.recordingfile as recordingfile FROM asteriskcdrdb.cdr 
                                WHERE cdr.linkedid=cur_linkedid and cdr.recordingfile!=''
                                group by calldate  limit 1 ) as recordingfile"),

                
                                 
                 DB::raw("(select SEC_TO_TIME(ROUND(tbl_calls_evnt.answer_sec_count-IFNULL(hold_sec,0))) )as talk_time"),

                 DB::raw("(SELECT SEC_TO_TIME(ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count))) as hold_time FROM tbl_calls_hold_evnts 
                         WHERE tbl_calls_hold_evnts.linkedid=tbl_calls_evnt.linkedid) as hold_time")
                 ) 
        ->join('user_master','user_master.id','=','tbl_calls_evnt.agnt_userid')
        ->join('tbl_com_mst','tbl_com_mst.id','=','user_master.com_id')
	->leftjoin('asterisk.queues_config','queues_config.extension','=','tbl_calls_evnt.agnt_queueid')
        ->whereBetween('tbl_calls_evnt.cre_datetime', array($frm_date, $to_date));
        //->Where('user_master.com_id',$get_com_id->com_id);
 	if($com_id!="All")
        {

        	$data = $dataq->Where('user_master.com_id',$com_id);
	}

        
        if($queue_id!="All")
        {
            $data = $dataq->Where('tbl_calls_evnt.agnt_queueid',$queue_id); 
        }

        if($agnt_id!="All")
        {
            $data = $dataq->Where('tbl_calls_evnt.agnt_userid',$agnt_id); 
        }
        $data = $dataq->groupby('tbl_calls_evnt.uniqueid'); 
        $data = $dataq->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Call Details Report",$username,"Search Call Details Report");

        return compact('data',$data);
        
    }
	public function search_call_det_report(Request $request){  

        $userid=session('userid');
	$usertypeid=session('usertypeid');
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first(); 
        $queue_id= $request->input('queue_id');
        $agnt_id= $request->input('agnt_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');
        $get_team_id  = DB::table('tbl_team_mst')
            ->select('tbl_team_mst.id as team_id','user_master.user_type_id')
            ->join('user_master','user_master.id','=','tbl_team_mst.tm_lead_userid')
            ->where('user_master.id',$userid)
            ->first();
        //dd($get_team_id);

        if(!empty($get_team_id))
        {
            if($get_team_id->user_type_id == '19')
            {
                $team_id = $get_team_id->team_id;
                $where4=" AND `tbl_calls_evnt`.`call_tm_id` = $team_id";
            }
        }else
        {
            $team_id = "";
            $where4 ="";
        }

	   if($usertypeid=='17' || $usertypeid=='1'){
            $users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where (c.`title`= 'Csp_Agent' OR c.`title`= 'Csp_Supervisor') and a.`com_id`= $get_com_id->com_id  ;");
		}else if ($usertypeid=='20'){
			$users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
								FROM `tbl_tmldr_agnt_allo` as a 
								right join `user_master` as b ON a.`agnt_userid`=b.`id`
								inner join `user_type_list` as c ON b.`user_type_id`=c.`id`
								where a.`tmldr_userid` = $userid ;");		
		}else
        {
            		$users = DB::select("SELECT GROUP_CONCAT(a.`id`) as userList 
                               FROM `user_master` as a 
                               inner join `user_type_list` as c ON a.`user_type_id`=c.`id`
                               where c.`title`= 'Csp_Agent' and a.`com_id`= $get_com_id->com_id and a.id=".$userid."  ;");
       	} 



        
		
		$queue_list=DB::select("SELECT GROUP_CONCAT(extension) AS extension_list FROM asterisk.queues_config WHERE `com_id`= $get_com_id->com_id"); 

		$user_array = $users[0]->userList;
		$extension_array = $queue_list[0]->extension_list;
		$where="";
		$where1="";
		$where2="";
		$where3="";
		$where="`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' AND '$to_date'";

        if($com_id!="All")
        {
			$where1=" AND `user_master`.`com_id` = $com_id ";
		}else if($usertypeid=='19'){
            $where1="";
        }
        else
        {
			$where1=" AND `user_master`.`com_id` = $get_com_id->com_id ";
		}

		if($queue_id!="All")
        {
			$where2=" AND tbl_calls_evnt.agnt_queueid = $queue_id ";           
        }

       	if($agnt_id!="All")			
		{ 
			$where3=" AND tbl_calls_evnt.agnt_userid =$agnt_id";
		}else if($usertypeid=='19'){
            $where3="";
        }
		else
		{	
			$where3=" AND tbl_calls_evnt.agnt_userid IN ($user_array) ";
		}


		$data = DB::select("SELECT
                    `tbl_calls_evnt`.`agnt_sipid`,
                    `tbl_calls_evnt`.`date`,
                    `tbl_calls_evnt`.`call_type`,
                    `tbl_calls_evnt`.`to_caller_num`,
                    `tbl_calls_evnt`.`frm_caller_num`,
                    `tbl_calls_evnt`.`uniqueid`,
                    max(`tbl_calls_evnt`.`cre_datetime`) AS cre_datetime,
                    `tbl_calls_evnt`.`linkedid` AS `cur_linkedid`,
		    `tbl_com_mst`.`com_name`,
                    (SELECT SEC_TO_TIME(`tbl_calls_evnt`.`ring_sec_count`) as waiting FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS waiting,
                    (SELECT `tbl_calls_evnt`.`status` FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS status,
                    (SELECT CONCAT_WS('-',`tbl_calls_evnt`.`agnt_queueid`,`asterisk`.`queues_config`.`descr`) FROM tbl_calls_evnt INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid`  WHERE `tbl_calls_evnt`.`cre_datetime` = cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1 ) AS agnt_queueid,
                    (SELECT `tbl_calls_evnt`.`agnt_userid` FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS agnt_userid,
                    (SELECT `tbl_calls_evnt`.`desc` FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS `desc`,
                    (SELECT `user_master`.`username` FROM tbl_calls_evnt INNER JOIN `user_master` ON `user_master`.`id` = `tbl_calls_evnt`.`agnt_userid` 
                    WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY user_master.id DESC LIMIT 1) AS username,
                    (
                    SELECT
                        SEC_TO_TIME(
                        ROUND( tbl_calls_evnt.answer_sec_count )) FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS tot_time,
                    (
                    SELECT tbl_calls_evnt.answer_sec_count FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS tot_time_sec,
                    ( SELECT tbl_calls_evnt.DESC AS cdb_desc FROM tbl_calls_evnt WHERE tbl_calls_evnt.linkedid = cur_linkedid AND STATUS = 'ENTERQUEUE' ORDER BY id DESC LIMIT 1 ) AS cdb_desc,
                    ( SELECT ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec FROM tbl_calls_hold_evnts WHERE tbl_calls_hold_evnts.linkedid = tbl_calls_evnt.linkedid ) AS hold_sec,
                    ( SELECT tbl_calls_evnt.hangup_datatime AS hangup_datatime FROM tbl_calls_evnt WHERE tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1 ) AS hangup_datatime,
                    '1' AS recordingfile,
                    ( SELECT SEC_TO_TIME( ROUND( tbl_calls_evnt.answer_sec_count - IFNULL( hold_sec, 0 ))) FROM tbl_calls_evnt WHERE `tbl_calls_evnt`.`cre_datetime`=cre_datetime AND tbl_calls_evnt.linkedid = cur_linkedid ORDER BY id DESC LIMIT 1) AS talk_time,
                    ( SELECT SEC_TO_TIME( ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time FROM tbl_calls_hold_evnts WHERE tbl_calls_hold_evnts.linkedid = tbl_calls_evnt.linkedid ) AS hold_time 
                FROM
                    `tbl_calls_evnt`
                    INNER JOIN `user_master` ON `user_master`.`id` = `tbl_calls_evnt`.`agnt_userid`
		          INNER JOIN `tbl_com_mst` ON `tbl_com_mst`.`id` =`user_master`.`com_id` 
                WHERE
                    $where $where1 $where2 $where3 $where4
                GROUP BY
                    `tbl_calls_evnt`.`uniqueid`
                ORDER BY 
                    `tbl_calls_evnt`.`cre_datetime` DESC,
                    `tbl_calls_evnt`.`uniqueid` DESC,
                    `tbl_calls_evnt`.`linkedid` DESC");
                        


                        return compact('data',$data);
                        
    }

 public function download(Request $request){



    $result = $this->search_call_det_report($request);



    // print_r($result);
$objPHPExcel = new PHPExcel();

             // Set document properties
                $objPHPExcel->getProperties()->setCreator("PhonikIP Callcenter")->setLastModifiedBy("PhonikIP Callcenter")->setTitle("CC Report")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);

                // Add column headers
                $objPHPExcel->getActiveSheet()
                            ->setCellValue('A1', 'Company')
                            ->setCellValue('B1', 'Call Date Time')
                            ->setCellValue('C1', 'Hangup Date Time')
                            ->setCellValue('D1', 'Call Type')
                            ->setCellValue('E1', 'Unique ID')
                            ->setCellValue('F1', 'CLI')
                            ->setCellValue('G1', 'Dial Number')
                            ->setCellValue('H1', 'Status')
                            ->setCellValue('I1', 'Description')
                            ->setCellValue('J1', 'Queue')
                            ->setCellValue('K1', 'Agent')
                            ->setCellValue('L1', 'Ringing Time')
                            ->setCellValue('M1', 'Duration')
                            ->setCellValue('N1', 'Talk Time')
                            ->setCellValue('O1', 'Held Time')
                            ;

                            $fileName="CC_report".$userid=session('userid').date("Y-m-d_H-i-s");
        $i=2;                

// print_r($result['data']);
        
   foreach ($result['data'] as $key=>$value) {
    // foreach ($value2 as $value) {
            //Put each record in a new cell
                
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $value->com_name);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value->cre_datetime);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value->hangup_datatime);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value->call_type);
                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $value->uniqueid);
                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value->frm_caller_num);
                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $value->to_caller_num);
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $value->status);
                    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $value->cdb_desc);
                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $value->agnt_queueid);
                    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $value->username);
                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $value->waiting);
                    $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $value->tot_time);
                    $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $value->talk_time);
                    $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $value->hold_time);
                
                
                // Set worksheet title
                $objPHPExcel->getActiveSheet()->setTitle($fileName);

        $i++;  

    // }
}

     //save the file to the server (Excel2007)
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        exec('rm -rf /var/www/html/'.config('app.company_name').'/downloadexcell/CC_report'.$userid=session('userid').'* ');

        $objWriter->save('/var/www/html/'.config('app.company_name').'/downloadexcell/' . $fileName.".xlsx");
        $fileName2=$fileName.".xlsx";

        // Get path from storage directory
        $path = 'http://'.$_SERVER['SERVER_ADDR'].'/'.config('app.company_name').'/downloadexcell/' . $fileName.".xlsx";

        // Download file with custom headers
        echo $path;

 }


}