<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'UsersController@loginuser');

Route::get('usertype', 'UsertypeController@index');

Route::get('users', 'UsersController@index');
Route::get('resetpassword', 'UsersController@viewResetPassword');
Route::post('resetpassword', 'UsersController@resetPassword');

Route::get('insertusertype',function(){
	return view('insertusertype');
});

Route::post('insertusertype','UsertypeController@insert');


Route::get('insertusers', 'UsersController@insertuser');

Route::post('insertusers','UsersController@insert');

Route::get('updateusers/{id}', 'UsersController@updateusers');

Route::post('updateusers','UsersController@edit');
//delete user
Route::get('deleteusers/{id}', 'UsersController@deleteuser');

Route::post('deleteusers','UsersController@delete');
Route::get('deleteusertypes','UsertypeController@delete');

Route::get('agentedit', 'UsersController@changepass');
Route::post('updateagents','UsersController@changeuserpass');

Route::get('privilagetype', 'PrivilagetypeController@index');
Route::get('insertprivilagetypes/{moduleid}', 'PrivilagetypeController@inertprivilage');
Route::get('updateprivilagetypes/{moduleid}', 'PrivilagetypeController@updateprivilage');
Route::get('viewprivilagetypes/{moduleid}', 'PrivilagetypeController@viewprivilage');

Route::get('loginusers', 'UsersController@loginuser');
Route::get('dashboards', 'DashboardController@index');
Route::get('liveupdatequeue', 'DashboardController@updatelivequeue');

Route::get('checkloginusers', 'UsersController@checkuser');

Route::get('livereports', 'LivereportController@index');

Route::get('updateusertypes/{id}', 'UsertypeController@updateusertype');

Route::post('updateusertypes','UsertypeController@edit');
Route::get('resetusers', 'UsersController@resetuser');

Route::get('permissiondenide', 'UsersController@notpermission');
Route::get('sessionexpire', 'UsersController@sessionhasexpire');


Route::get('live', 'LivereportController@view');
//Route::get('livereportrefresh', 'livereport@reload');
//Route::get('queuerefresh', 'livereport@queue');

Route::get('agentreportrefresh', 'LivereportController@reloadagentstatus');
Route::get('queueportrefresh', 'LivereportController@reloadqueuestatus');
// Route::get('agentmoniterrefrech', 'LivereportController@reloadagentcountstatus');
// Route::get('queuemoniterrefrech', 'LivereportController@reloadqueuemoniter');
Route::get('queuemoniterrefrech', 'LivereportController@reloadagentcountstatus');
Route::get('setlivedate', 'LivereportController@setsessiondate');
Route::get('error404', 'ErrorController@pagenotfound');

Route::get('updatesms', 'UsersController@updatesmsstate');
Route::get('updateautodial', 'UsersController@updateautodial');
Route::get('updateacw', 'UsersController@updateacw');
Route::get('setlivestatus', 'UsersController@setlivestate');
Route::get('setusertypes', 'UsertypeController@setusertype');
Route::get('viewusertypes', 'PrivilagetypeController@viewsetusertype');
Route::get('slareports', 'SLAReportController@index');
Route::get('callbackreport', 'CallbackReportController@index');
Route::get('searchcallbackdata', 'CallbackReportController@searchCallbackData');
Route::get('searchslareportdata', 'SLAReportController@searchdata');
Route::get('queuedetailreports', 'QueueDetailController@index');
Route::get('searchqueuedetailreports', 'QueueDetailController@searchqueuedata');

Route::get('calltraffic', 'CallTrafficReportController@index');
Route::get('searchdata', 'CallTrafficReportController@searchdata');
//sachith
Route::get('dailycall', 'DailyCallTrafficReportController@index');
Route::get('searchdata_daily', 'DailyCallTrafficReportController@searchdata_daily');
//Nadee
Route::get('breakreport', 'breakReportController@index');
Route::get('timerange', 'breakReportController@timerange');
Route::post('breakreport', 'breakReportController@timerange');
//sachith 
Route::get('callagent', 'callCenterAgentController@index');
Route::get('searchdata_agent', 'callCenterAgentController@searchdata_agent');

Route::get('callagentsummary', 'CCASummaryController@index');
Route::get('searchdata_agent_s', 'CCASummaryController@searchdata_agent_s');

Route::get('queueprofile', 'QueueProfileController@index');
Route::get('queueprofilesearch', 'QueueProfileController@searchqueuedata');


Route::get('abandonreport', 'AbandonReportController@index');
Route::get('viewdata', 'AbandonReportController@viewdata');
// activityreport
Route::get('activityreport', 'ActivityReportContoller@index');
Route::get('activitydate', 'ActivityReportContoller@searchdata');
// DetailCallLog
Route::get('detailcalllog', 'DetailCallLogContoller@index');
Route::get('detailcalldata', 'DetailCallLogContoller@searchdata');

//CallByCaller
Route::get('callbycaller', 'CallByCallers@index');
Route::get('callbycallerdata', 'CallByCallers@searchdata');

//Disposition
Route::get('disposition', 'DispositionReport@index');
Route::get('dispositiondata', 'DispositionReport@searchdata');

//test
Route::get('table', 'DtController@index');
Route::get('dt', 'DtController@searchdata');

Route::get('loginlogout', 'LoginLogoutController@index');
Route::get('loginlogoutseach', 'LoginLogoutController@searchdata');

Route::get('hanguprepost', 'HangupreportController@index');
Route::get('hangup', 'HangupreportController@searchdata');


Route::get('inboundoutbound', 'inboundoutboundController@index');
Route::get('inboundoutboundsearch', 'inboundoutboundController@searchdata');

Route::get('queuelogin', 'QueueloginContoller@index');
Route::get('queueloginsearch', 'QueueloginContoller@searchdata');

Route::get('functiondata', 'funcdataController@index');
Route::get('updatefunctiondata','funcdataController@updatefuncdata');


Route::get('insertcamp', 'CampaginContoller@index');
Route::post('savecamp', 'CampaginContoller@save');
Route::get('campaigns', 'CampaginContoller@view');
Route::get('updatecampaignid', 'CampaginContoller@updatestatus');
Route::get('viewcampaigndata', 'CampaginContoller@serchdata');
Route::get('editcamp/{id}', 'CampaginContoller@edit');
Route::post('savecampedit', 'CampaginContoller@editsave');

Route::get('crm', 'CrmContoller@index');
Route::get('crm2', 'CrmContoller@view');
//------- added by thili 07/31/2020------//
Route::post('savecusfeedback', 'CrmContoller@savecusfeedback');
Route::get('searchCustomerData','CrmContoller@searchCustomerData');
Route::get('searchCustomerHistry','CrmContoller@searchCustomerHistry');
//------- added by thili 07/31/2020------//
Route::post('insertcustomerdata','CrmContoller@save');
Route::get('deletecsv','upload_csv_controller@delete');
Route::get('uploadcsv','upload_csv_controller@index');
Route::post('uploadcsv','upload_csv_controller@uploadcsv');
Route::get('getresults','upload_csv_controller@getresults');
Route::get('getwarn','upload_csv_controller@getwarns');
Route::get('getamounts','upload_csv_controller@getamount');
Route::get('gettotalupload','upload_csv_controller@gettotalupload');

Route::get('datadownload', 'DatadownloadController@index');
Route::get('getdatadownload', 'DatadownloadController@view');




Route::get('scheme', 'SchemeContoller@index');
Route::post('savescheme', 'SchemeContoller@save');
Route::get('schemeins', 'SchemeContoller@insert');
Route::get('editscheme/{id}', 'SchemeContoller@edit');
Route::get('deletescheme/{id}', 'SchemeContoller@delete');
Route::post('updatescheme', 'SchemeContoller@saveedit');

Route::get('reshudulecustomer', 'CampaginContoller@reshedulecus');

Route::post('savereshedule', 'CampaginContoller@savereshedule');

Route::get('callrecords', 'CallrecordingController@index');
Route::get('searchcallrecords', 'CallrecordingController@search');
Route::get('downloadcallrecord', 'CallrecordingController@download');
Route::post('downloadcallrecordsel', 'CallrecordingController@downloadsel');
Route::post('downloadcallrecordall', 'CallrecordingController@downloadall');
Route::get('playrecordfile', 'CallrecordingController@playrecord');

//
//CSP ROUTES BEGIN
//

Route::get('inboundcalls', 'InboundCallController@inboundCall');
Route::get('inboundcalls/{number}/{sipid}', 'InboundCallController@inboundCall')->name('inboundcalls');
Route::get('addcalllog','InboundCallController@addCallLog');
Route::get('addcalllog_for_num','InboundCallController@addcalllog_for_num');
Route::get('addcalllog_for_num_console','NewCallLogController@addcalllog_for_num_console');

Route::post('editcalllog','CallHistoryController@editCallLog');
Route::post('editCallLogDetail','CallHistoryController@editCallLogDetail');
Route::post('savedata','CallHistoryController@savedata');
Route::get('callhistory', 'call_history_controller@index');
Route::get('callhistorysearch', 'call_history_controller@search');
Route::get('editCallLog','CallHistoryController@editCallLog');
Route::get('editCallLogDetail','CallHistoryController@editCallLogDetail');

Route::get('popupcallhistory', 'CallHistoryController@index');
Route::get('popupcallhistorydetail', 'CallHistoryController@popupcallhistorydetail');
Route::post('updatemanualcalllog','CallHistoryController@updatemanualcalllog');
Route::post('updatemanualcalllogDetail','CallHistoryController@updatemanualcalllogDetail');

Route::get('tickets', 'TicketController@viewTickets');
Route::get('ticket/{id}', 'TicketController@viewTicket');
Route::get('ticketcategories', 'TicketCategoryController@index');
Route::get('searchtickets', 'TicketController@searchTickets');
Route::get('getusersofcategory', 'TicketCategoryController@usersOfCategory');
Route::post('insertticket', 'TicketController@insertTicket');

Route::get('gettransferrablesubdeps', 'TicketController@transferrableSubDeps');
Route::get('viewtransferticket/{id}','TicketController@viewTransferTicket');
Route::post('transferticket','TicketController@transferTicket');
Route::get('viewaddticket/{contactid}','TicketController@viewAddTicket');
Route::get('viewaddticketcategory','TicketCategoryController@viewAddTicketCategory');
Route::post('addticketcategory','TicketCategoryController@addTicketCategory');
Route::get('depsofcat','TicketCategoryController@depsOfCategory');
Route::post('updatestatus','TicketController@updateStatus');
Route::get('gettfusers', 'TicketController@retrieveTransferableUsers');
Route::get('viewsendsms','SmsMsgController@ViewSendSms');
Route::post('sendsmstocontact','SmsMsgController@SendSmsToContact');

Route::get('contacts', 'ContactController@index');
Route::post('insertcontact', 'ContactController@insertWithCallLog');
Route::post('updatecontact/{number}/{id}', 'ContactController@update');

Route::post('addcon', 'ContactController@insertOnly');
Route::get('editcon','ContactController@viewEditContact');
Route::get('addcontact', 'ContactController@viewAddContact');

Route::get('addphonebookcontact', 'PhonebookController@viewAddPhonebookContact');

Route::get('contacttypes', 'ContactTypeController@index');
Route::get('getcontacttypes', 'ContactTypeController@retrieveAll');
Route::get('viewaddcontacttype', 'ContactTypeController@viewAddContactType');
Route::post('addcontacttype', 'ContactTypeController@insert');

Route::get('getusersofsubdep', 'SubdepartmentController@retrieveUsersOfSubdep');
Route::get('getusersofsubdep2', 'SubdepartmentController@retrieveUsersOfSubdep2');
Route::get('getnonusersofsubdep', 'SubdepartmentController@retrieveNonUsersOfSubdep');
Route::get('getsubdeps', 'SubdepartmentController@subsOfDep');
Route::post('editsubdep', 'SubdepartmentController@update');
Route::get('editsubdep', 'SubdepartmentController@viewEdit');
Route::get('agentview', 'SubdepartmentController@view');
Route::get('viewsubdep', 'SubdepartmentController@index');
//Route::post('addsubdeps', 'SubdepartmentController@insert');
Route::get('addsubdep', 'SubdepartmentController@viewAdd');
Route::get('agentallocation', 'SubdepartmentController@viewAgentAllocation')->name('agentallocation');
Route::get('deleteagent', 'SubdepartmentController@deleteagent');
Route::post('allocateagent', 'SubdepartmentController@allocate');
Route::get('getagentallocationstring', 'SubdepartmentController@getAllocationString');

Route::get('adddep', 'DepartmentController@viewAdd');
Route::post('adddeps', 'DepartmentController@insert');
//Route::get('editdep', 'DepartmentController@viewEditDepartment');
Route::post('updatedepartment', 'DepartmentController@update');
Route::get('viewdep', 'DepartmentController@index');
Route::get('agentdata','DepartmentController@viewagentdepdata');
Route::get('headallocation', 'DepartmentController@viewHeadAllocation')->name('headallocation');
Route::get('deleteheads', 'DepartmentController@deleteheads');
Route::post('allocatehead', 'DepartmentController@allocateHead');
Route::get('getheadallocationstring', 'DepartmentController@getAllocationString');
Route::get('userallocation', 'DepartmentController@viewUserAllocation');

Route::get('presetremarks', 'PresetRemarkController@index');
Route::get('viewaddpresetremark', 'PresetRemarkController@viewAdd');
Route::post('addpresetremark', 'PresetRemarkController@add');
Route::get('deletepresetremark', 'PresetRemarkController@delete');

Route::get('contactupload', 'ContactUploadController@index');
Route::post('uploadcontact','ContactUploadController@upload');


Route::get('getAllContactRecords', 'ContactController@getAllContactRecords');


Route::resource('phonebookcontact', 'PhonebookContactController');

Route::get('calltransfer', 'CallTransferReportController@index');
Route::get('transferdata', 'CallTransferReportController@searchdata');


//Route::get('addnewcontact/{number}/{reclocation}', 'ContactController@addNewContact');
Route::get('inboundCallUpdateRecordInfo/{primary}/{cusid}/{cli}/{sipid}/{linkedid}','InboundCallController@inboundCallUpdateRecordInfo');
Route::get('inboundCallUpdateRecordInfo/{number}','InboundCallController@inboundCallUpdateRecordInfo');


Route::get('outgoing', 'OutgoingReportController@index');
Route::get('outgoingdata', 'OutgoingReportController@searchdata');

//AUX-Break
Route::get('auxbreak', 'AuxBreakController@index');
Route::get('auxbreaksearch', 'AuxBreakController@searchdata');

Route::get('getlastlogin', 'UsersController@getlastlogin');

Route::get('get_aststatus', 'UsersController@checkExtenStatus');

Route::get('sdashboardunit1', 'SupervisorDashbController@index');
Route::get('sdashboardunit2', 'SupervisorDashbController@sdashboardunit2');
Route::get('sdashboardmaster', 'SupervisorDashbController@sdashboardmaster');
Route::get('queuemonitor', 'SupervisorDashbController@queuemonitor');
Route::get('queuemonitor_misscall', 'SupervisorDashbController@queuemonitor_misscall');
Route::get('queuemonitor_overall', 'SupervisorDashbController@queuemonitor_overall');
Route::get('agentmonitor', 'SupervisorDashbController@agentmonitor');

Route::get('qdashboard', 'QueueMonitorMasterController@index');
Route::get('queuemonitormaster', 'QueueMonitorMasterController@queuemonitormaster');
Route::get('callrecords2', 'CallrecordingController2@index');
Route::get('searchcallrecords2', 'CallrecordingController2@search');
Route::get('downloadcallrecord2', 'CallrecordingController2@download');
Route::post('downloadcallrecordsel2', 'CallrecordingController2@downloadsel');
Route::post('downloadcallrecordall2', 'CallrecordingController2@downloadall');

//AgentPerformanceIBOB chinthaka
Route::get('agentperformanceibob', 'AgentPerformanceIBOBController@index');
Route::get('agentperformanceibobsearch', 'AgentPerformanceIBOBController@searchdata');

Route::get('agentperformanceob', 'AgentPerformanceOBController@index');
Route::get('agentperformanceobsearch', 'AgentPerformanceOBController@searchdata');

Route::get('obreport', 'OBController@index');
Route::get('obsearch', 'OBController@searchdata');


Route::get('oblive', 'OBLiveController@index');
Route::get('oblivemonitor', 'OBLiveController@oblivemonitor');

// Route::get('ivrreport', 'IVRController@index');
// Route::get('ivrsearch', 'IVRController@searchdata');

Route::get('queuemonitor_credit_overall', 'SupervisorDashbController@queuemonitor_credit_overall');

// agentHistory
Route::get('agenthistory', 'AgentHistoryContoller@index');
Route::get('aghsearch', 'AgentHistoryContoller@searchdata');
//
//CSP ROUTES END
//

Route::get('callrecords2', 'CallrecordingController2@index');
Route::get('searchcallrecords2', 'CallrecordingController2@search');
Route::get('downloadcallrecord2', 'CallrecordingController2@download');
Route::post('downloadcallrecordsel2', 'CallrecordingController2@downloadsel');
Route::post('downloadcallrecordall2', 'CallrecordingController2@downloadall');
Route::get('playrecordfile2', 'CallrecordingController2@playrecord');
Route::get('live2', 'Livereport2Controller@view');
Route::get('live3', 'Livereport2Controller@view3');

// Route::get('callrecords2', 'CallrecordingController2@index');
// Route::get('searchcallrecords2', 'CallrecordingController2@search');
// Route::get('downloadcallrecord2', 'CallrecordingController2@download');
// Route::post('downloadcallrecordsel2', 'CallrecordingController2@downloadsel');
// Route::post('downloadcallrecordall2', 'CallrecordingController2@downloadall');
// Route::get('playrecordfile2', 'CallrecordingController2@playrecord');


Route::get('addSubject', 'SubjectController@viewAddSubject');
Route::get('subject/{id}', 'SubjectController@subjects');
Route::get('subject', 'SubjectController@index');
Route::get('searchsubject', 'SubjectController@searchTickets');
Route::get('getusersofsubject', 'SubjectController@usersOfSubject');
Route::post('insertsubject', 'SubjectController@addSubject');
Route::get('getlevel3', 'TicketCategoryController@getlevel3');
Route::get('getAgentSubDepartmentsWise','TicketCategoryController@getAgentList');

Route::get('departmentdb','DashboardController@departmentDashboard');
Route::get('subdepartmentdb','DashboardController@subdepartmentDashboard');
Route::get('userdb','DashboardController@userDashboard');
Route::get('getkpi_levelsLocationWise','SubjectController@getkpi_levelsLocationWise');
Route::get('getDepartment','DepartmentController@getDepartment');

Route::get('deptsubdept','DeptSubDeptController@viewTickets');
Route::get('returnTickets','DeptSubDeptController@viewReturnTickets');
Route::get('getAgentUserTypeWise','SubdepartmentController@getAgentUserTypeWise');
Route::get('ticketTransferDashboard', 'TicketController@viewTTDashboard');
Route::get('searchreturntickets' ,'TicketController@searchreturntickets' );
Route::get('toBeCompleteDashBoard','TicketController@toBeCompleteDashBoard');
Route::get('searchcompletetickets','TicketController@searchcompletetickets');
Route::get('viewCompleteticket/{id}','TicketController@viewCompleteticket');
Route::post('completeJob','TicketController@completeJob');

Route::get('searchdeptsubdepttickets', 'DeptSubDeptController@searchdeptsubdepttickets');
Route::get('subdeptsubdept','DeptSubDeptController@viewSubTicketsTickets');

//CSP ROUTES END
//
Route::get('locationdashboard','LocationController@viewTickets');
Route::get('searchlocationtickets', 'LocationController@searchdeptsubdepttickets');



Route::get('updatesubjectstatus', 'SubjectController@updatesubjectstatus');
Route::get('updatesubjectview/{id}', 'SubjectController@updatesubjectview');
//Route::get('assignuserview/{id}', 'SubjectController@assignuserview');
//Route::post('assignuser', 'SubjectController@assignuser');

Route::get('selectassignuserview/{id}/{kpi}', 'SubjectController@selectassignuserview');
Route::post('updatesubject', 'SubjectController@updateSubject');
Route::get('deletesubject/{id}', 'SubjectController@deletesubject');

Route::get('getsubjectsofsubdep', 'SubjectController@getsubjectsofsubdep');
Route::get('getDateWiseData', 'DashboardController@getDateWiseData');
Route::get('getDateWiseWP', 'DashboardController@getDateWiseWP');
Route::get('getTicketDataForSearchCat', 'DashboardController@getTicketDataForSearchCat');
Route::get('searchKpisforCategories', 'DashboardController@searchKpisforCategories');
Route::get('getDateWiseDataBar', 'DashboardController@getDateWiseDataBar');
Route::get('getDateWiseDatasearchCat', 'DashboardController@getDateWiseDatasearchCat');

Route::get('getsubject', 'SubdepartmentController@subsOfDep');
Route::get('addnewcontact/{number}', 'ContactController@addNewContactManual');

Route::get('searchCustomer','InboundCallController@searchCustomer');
Route::post('searchContact','InboundCallController@searchContact');
Route::get('inboundCallUpdateRecordCustomerInfo/{number}/{id}','InboundCallController@inboundCallUpdateRecordCustomerInfo');



//Route::post('assignsubjectuser', 'SubjectController@assignsubjectuser');
Route::post('addconManual', 'ContactController@insertOnlyManual');
Route::post('updatecontactnew', 'ContactController@updatenew');
//Route::get('updatedeptstatus', 'DepartmentController@updatedeptstatus');
Route::get('updatesubdeptstatus', 'SubdepartmentController@updatesubdeptstatus');

Route::get('searchsubdepttickets', 'DeptSubDeptController@searchsubdepttickets');

Route::get('addnewcontactmanual/{number}', 'ContactController@addNewContactManualwithnumber');

Route::get('filterdbcontact/{campaign_id}', 'filterdbcontactContoller@view');
Route::get('filterdbcontactdata', 'filterdbcontactContoller@filterdbcontactdata');

Route::get('addcus_to_camp', 'filterdbcontactContoller@addcus_to_camp');

Route::get('smscampaigns', 'SmsCampaginContoller@view');
Route::get('insertsmscamp', 'SmsCampaginContoller@index');
Route::post('savesmscamp', 'SmsCampaginContoller@save');
Route::get('updatesmscampaignid', 'SmsCampaginContoller@updatestatus');
Route::get('editsmscamp/{id}', 'SmsCampaginContoller@edit');
Route::post('savesmscampedit', 'SmsCampaginContoller@editsave');
Route::get('deletecamp/{id}', 'SmsCampaginContoller@deletecamp');
Route::get('smsfilterdbcontact/{campaign_id}', 'smsfilterdbcontactContoller@view');
Route::get('smsfilterdbcontactdata', 'smsfilterdbcontactContoller@smsfilterdbcontactdata');

Route::get('addcus_to_smscamp', 'smsfilterdbcontactContoller@addcus_to_smscamp');

Route::get('addmaterial/{campaign_id}', 'CampaginContoller@addmaterialview');
Route::post('savemtrl', 'CampaginContoller@savemtrl');

Route::get('deletemtrl/{id}', 'CampaginContoller@deletemtrl');


///---------------------------routes Add By Vinod 06-24-2019-------------------------------------------
Route::get('viewlevelone', 'LevelOneController@index');
Route::get('add_levelone_view', 'LevelOneController@add_levelone_view');
Route::post('add_levelone', 'LevelOneController@add_levelone');
Route::get('update_levelone_status', 'LevelOneController@update_levelone_status');
Route::get('del_levelone', 'LevelOneController@del_levelone');

Route::get('viewleveltwo', 'LevelTwoController@index');
Route::get('add_leveltwo_view', 'LevelTwoController@add_leveltwo_view');
Route::post('add_leveltwo', 'LevelTwoController@add_leveltwo');
Route::get('updatelvltwostatus', 'LevelTwoController@updatelvltwostatus');
Route::get('del_leveltwo', 'LevelTwoController@del_leveltwo');
Route::get('getleveltwodata','LevelTwoController@getleveltwodata');

Route::get('viewlevelthr', 'LevelThrController@index');
Route::get('add_levelthr_view', 'LevelThrController@add_levelthr_view');
Route::post('add_levelthr', 'LevelThrController@add_levelthr');
Route::get('updatelvlthrstatus', 'LevelThrController@updatelvlthrstatus');
Route::get('del_levelthr', 'LevelThrController@del_levelthr');

Route::get('viewkpi_info', 'KpiInfoController@index');
Route::get('add_kpi_info_view', 'KpiInfoController@add_kpi_info_view');
Route::get('getlvltwodata', 'LevelTwoController@getlvltwo_of_lvlone');
Route::get('getlvlthrdata', 'LevelThrController@getlvlthr_of_lvltwo');
Route::get('getlvlthrdataforagentallo', 'LevelThrController@getlvlthr_of_lvltwo_foragent');

Route::post('add_kpi_info', 'KpiInfoController@add_kpi_info');
Route::get('updatekpilevelstatus', 'KpiInfoController@updatekpilevelstatus');
Route::get('updatekpilevelview/{id}', 'KpiInfoController@updatekpilevelview');
Route::post('updatekpilevel', 'KpiInfoController@updatekpilevel');
Route::get('deleteKpilevelinfo/{id}', 'KpiInfoController@deleteKpilevelinfo');
Route::get('kpiassignuserview/{id}/{kpi}', 'KpiInfoController@kpiassignuserview');
Route::post('assignuser', 'KpiInfoController@assignuser');
Route::get('assignuserview/{id}', 'KpiInfoController@assignuserview');
Route::post('assignkpileveluser', 'KpiInfoController@assignkpileveluser');

Route::get('getkpilvloflvlthr', 'KpiInfoController@getkpilvloflvlthr');
Route::get('getallcontacts', 'ContactController@retrieveallcontacts');
Route::post('insertTicketmanual', 'TicketController@insertTicketmanual');

Route::get('lvlonedashboard','LevelOneController@viewTickets');
Route::get('searchlvlonetickets', 'LevelOneController@searchlvlonetickets');
Route::get('lvltwodashboard','LevelTwoController@viewTickets');

Route::get('searchlvltwotickets', 'LevelTwoController@searchlvltwotickets');
Route::get('lvlthrdashboard','LevelThrController@viewTickets');
Route::get('searchlvlthrtickets', 'LevelThrController@searchlvlthrtickets');

Route::get('levelWiseaAgentAllocation', 'agentAllocationController@assignagentview');
Route::post('assignagent', 'agentAllocationController@assignagent');

Route::get('getallocationagent', 'agentAllocationController@getallocationagent');

Route::get('getusersoflvlthr', 'LevelThrController@getusersoflvlthr');
Route::get('randomPassword', 'UsersController@randomPassword');

Route::get('TicketdetailsDashboard','TicketdetailsController@viewTickets');
Route::get('searchticketsdetails','TicketdetailsController@searchticketsdetails');
Route::get('getTicketdetailsexcel','TicketdetailsController@getTicketdetailsexcel');
Route::get('getexcelticket/{id}', 'TicketController@viewexcelTicket');

Route::get('lvlthragentdashboard','LevelThrController@viewagentTickets');
Route::get('searchlvlthragenttickets', 'LevelThrController@searchlvlthragenttickets');
Route::get('getlvlthrdata_without_agt', 'LevelThrController@getlvlthr_of_lvltwo_withoutagent');
Route::post('addcustomer_inq','TicketController@addcustomer_inq');
Route::get('ticketdetail_sendbyemail', 'TicketController@ticketdetail_sendbyemail');
Route::get('ticketdetail_sendviasms', 'TicketController@ticketdetail_sendviasms');
Route::get('playrecordfile2', 'CallrecordingController2@playrecord');

Route::get('Cspuser_allocation', 'UsertypeController@cspuser_allo');
Route::get('updatecspusertypes', 'UsertypeController@updatecspusertypes');
Route::get('getcspuserstatus', 'UsertypeController@getcspuserstatus');
Route::get('getticketforcomment', 'TicketController@getticketforcomment');
Route::get('view_findcallhistory','CallHistoryController@view_findcallhistory');
Route::get('findcallhistory','CallHistoryController@findcallhistory');
///---------------------------User Allocation Add By Badra 06-27-2019-------------------------------------------


Route::get('levelWiseUserAllocation', 'userAllocationController@index');
Route::get('saleSupervisorAllocation', 'userAllocationController@viewSaleSupervisorAllocation')->name('saleSupervisorAllocation');
Route::post('allocateLevelOneSupervisor', 'userAllocationController@allocateLevelOneSupervisor');
Route::get('allCateLocationToSalesChannal/{id}', 'userAllocationController@allCateLocationToSalesChannal');
Route::get('locationSupervisorAllocation', 'userAllocationController@locationSupervisorAllocation')->name('locationSupervisorAllocation');
Route::post('allocateLevelTwoSupervisor', 'userAllocationController@allocateLevelTwoSupervisor');
Route::get('allCateProductCatToSalesChannalLocation/{id}/{saleID}', 'userAllocationController@allCateProductCatToSalesChannalLocation');
Route::get('productcatSupervisorAllocation', 'userAllocationController@productcatSupervisorAllocation')->name('productcatSupervisorAllocation');
Route::post('allocateLevelThreeSupervisor', 'userAllocationController@allocateLevelThreeSupervisor');
Route::get('getOrderHeaderDetail','InboundCallController@getOrderHeaderDetail');
Route::get('downloadexcel/{id}','InboundCallController@downloadexcel');
Route::get('sendPromo/{contact}/{mobile}','InboundCallController@sendPromo');


Route::get('getcustomersbysearch', 'ContactController@getcustomersbysearch');
Route::get('addnewcontact/{number}/{sipid}/{linkid}', 'ContactController@addNewContactManual');
Route::post('addmanualcalllog','CallHistoryController@addmanualcalllog');
Route::get('kpiholidaycal', 'KpiInfoController@kpiholidaycal');
Route::get('addnewcontact', 'ContactController@addNewContactManual');

///---------------------------User Allocation Add By Badra 06-27-2019-------------------------------------------

Route::post('updateHoldstatus','TicketController@updateholdStatus');
Route::get('getTicketDataForSearchtypes', 'DashboardController@getTicketDataForSearchtypes');
Route::get('changeLoginStatus', 'UsersController@changeLoginStatus');
Route::get('csp_users', 'UsersController@csp_users');
Route::get('reset_cspuser_login', 'UsersController@reset_cspuser_login');
Route::get('view_firstresetpass', 'UsersController@view_firstresetpass');
Route::post('first_resetPassword', 'UsersController@first_resetPassword');
Route::get('skip_resetPassword', 'UsersController@skip_resetPassword');

Route::get('get_erpcusdata', 'ContactController@get_erpcusdata');


Route::get('emailsrvdashboard','EmailSrvController@emailsrvdashboard');
Route::get('archemailsrvdashboard','EmailSrvController@archemailsrvdashboard');
Route::get('searchsrvemail', 'EmailSrvController@searchsrvemail');
Route::get('archsearchsrvemail', 'EmailSrvController@archsearchsrvemail');
Route::get('viewemail/{id}', 'EmailSrvController@viewemail');
Route::post('disemailsrv','EmailSrvController@disemailsrv');
Route::post('inqemailsrv','EmailSrvController@inqemailsrv');
Route::get('view_emailsrvcuslist/{id}', 'EmailSrvController@view_emailsrvcuslist');
Route::get('addnewcontactbyemail/{e_id}', 'ContactController@addnewcontactbyemail');
Route::post('addconbyemail', 'ContactController@addconbyemail');
Route::get('viewaddticketbyemail/{contactid}/{e_id}','TicketController@viewaddticketbyemail');
Route::post('emailsrvinsertTicket', 'TicketController@emailsrvinsertTicket');
Route::post('sendemailsrv','EmailSrvController@sendemailsrv');
Route::get('viewoldsrvmail', 'EmailSrvController@viewoldsrvmail');
Route::get('viewoldsrvsentmail', 'EmailSrvController@viewoldsrvsentmail');
Route::get('srvemailAgentAllocation', 'EmailSrvController@srvemailAgentAllocation');
Route::post('srvassignagent', 'EmailSrvController@srvassignagent');
Route::get('getsrvallocationagent', 'EmailSrvController@getsrvallocationagent');
Route::get('getsrvboxbytype', 'EmailSrvController@getsrvboxbytype');
Route::post('forwardemailsrv','EmailSrvController@forwardemailsrv');

//----------------Agent New SMS update routes ----- Akila 19.08.2021-----------------------//
Route::get('archsmssrvdashboard','SmsSrvController@archsmssrvdashboard');
Route::get('archsearchsrvsms', 'SmsSrvController@archsearchsrvsms');
Route::post('dissmssrv','SmsSrvController@dissmssrv');
Route::get('view_smssrvcuslist/{ref_no}', 'SmsSrvController@view_smssrvcuslist');
Route::get('view_smssrvcustomerlist/{ref_no}', 'SmsSrvController@view_smssrvcustomerlist');
Route::get('cussmsAllocation/{sms_num}','SmsSrvController@cussmsAllocation');
Route::get('smssrvdashboard','SmsSrvController@smssrvdashboard');
Route::get('searchsrvsms', 'SmsSrvController@searchsrvsms');
Route::post('inqsmssrv','SmsSrvController@inqsmssrv');
Route::get('viewsms/{id}', 'SmsSrvController@viewsms');
Route::post('sendsmssrv','SmsSrvController@sendsmssrv');

Route::get('viewaddticketbynum/{contactid}/{refNo}','TicketController@viewaddticketbynum');
Route::post('smssrvinsertTicket', 'TicketController@smssrvinsertTicket');

Route::get('addnewcontactbynum/{ref_no}', 'ContactController@addnewcontactbynum');
Route::post('addconbynum', 'ContactController@addconbynum');

Route::get('ReadSmsChat/{ref_no}','SmsMsgController@ReadSmsChat');
Route::get('GetSmsChatByRefNo','SmsMsgController@GetSmsChatByRefNo');
Route::get('SendSmsMessage','SmsMsgController@SendSmsMessage');
//----------------End Agent New SMS update routes ----- Akila 19.08.2021-----------------------//

Route::get('view_srvyqn', 'SrvyQnController@index');
Route::get('view_addsrvyqn', 'SrvyQnController@view_addsrvyqn');
Route::post('add_srvyqn', 'SrvyQnController@add_srvyqn');
Route::get('getqntypedata','SrvyQnController@getqntypedata');
Route::get('updatesrvyqnview/{id}', 'SrvyQnController@updatesrvyqnview');
Route::post('update_srvyqn', 'SrvyQnController@update_srvyqn');
Route::get('update_srvyqn_status', 'SrvyQnController@update_srvyqn_status');


Route::get('view_srvyqngrp', 'SrvyQnController@view_srvyqngrp');
Route::get('view_addsrvyqngrp', 'SrvyQnController@view_addsrvyqngrp');
Route::post('add_srvyqngrp', 'SrvyQnController@add_srvyqngrp');
Route::get('updatesrvyqngrpview/{id}', 'SrvyQnController@updatesrvyqngrpview');
Route::get('getqnofsrvy','SrvyQnController@getqnofsrvy');
Route::post('update_srvyqngrp', 'SrvyQnController@update_srvyqngrp');
Route::get('update_srvyqnqrp_status', 'SrvyQnController@update_srvyqnqrp_status');
Route::get('getqnoptdata','SrvyQnController@getqnoptdata');

Route::get('checkholiday', 'TicketController@checkholiday');
Route::post('add_remainder','TicketController@add_remainder');
Route::get('remainderdashboard','RemainderController@remainderdashboard');
Route::get('searchremainder', 'RemainderController@searchremainder');
Route::get('updateremaindstatus', 'RemainderController@updateremaindstatus');

Route::get('getnumbersofsmscamp','SmsCampaginContoller@getnumbersofsmscamp');
Route::get('crm', 'CspCrmContoller@index');
Route::get('getqngrp','SrvyQnController@getqngrp');
Route::post('insertqnanswers','CspCrmContoller@insertqnanswers');
Route::get('getcallcampofcus','ContactController@getcallcampofcus');
Route::get('getsmscampofcus','ContactController@getsmscampofcus');


Route::get('faxsrvdashboard','FaxSrvController@faxsrvdashboard');
Route::get('searchsrvfax', 'FaxSrvController@searchsrvfax');
Route::get('viewfax/{id}', 'FaxSrvController@viewfax');
Route::post('disfaxsrv','FaxSrvController@disfaxsrv');
Route::get('view_faxsrvcuslist/{id}', 'FaxSrvController@view_faxsrvcuslist');
Route::get('addnewcontactbyfxnum/{e_id}', 'ContactController@addnewcontactbyfxnum');
Route::post('addconbyfxnum', 'ContactController@addconbyfxnum');
Route::get('viewaddticketbyfxnum/{contactid}/{e_id}','TicketController@viewaddticketbyfxnum');
Route::post('faxsrvinsertTicket', 'TicketController@faxsrvinsertTicket');
Route::post('inqfaxsrv','FaxSrvController@inqfaxsrv');
Route::post('sendfaxsrv','FaxSrvController@sendfaxsrv');
Route::get('archfaxsrvdashboard','FaxSrvController@archfaxsrvdashboard');
Route::get('archsearchsrvfax', 'FaxSrvController@archsearchsrvfax');

Route::post('inbound_updatecontact', 'ContactController@inbound_updatecontact');

Route::get('view_addbatchtocamp/{campaign_id}', 'CampaginContoller@view_addbatchtocamp');
Route::post('savebatchtocamp', 'CampaginContoller@savebatchtocamp');

Route::get('Agentdashboard', 'AgentdashboardController@index');
Route::get('dnd_off', 'AgentdashboardController@dnd_off');
Route::get('dnd_on', 'AgentdashboardController@dnd_on');
Route::get('getPresetDetails', 'AgentdashboardController@getPresetDetails');
Route::get('act_hold', 'AgentdashboardController@act_hold');
Route::get('opensocket', 'AgentdashboardController@opensocket');
Route::get('inboundcall', 'AgentdashboardController@inboundcall');
Route::get('getdetailsofcaller', 'AgentdashboardController@getdetailsofcaller');
Route::get('getdetailsofagnt', 'AgentdashboardController@getdetailsofagnt');

Route::get('Dial_call', 'AgentdashboardController@Dial_call');
Route::get('callback_Dial_call', 'AgentdashboardController@callback_Dial_call');
Route::get('callback_request_Dial_call', 'AgentdashboardController@callback_request_Dial_call');
Route::get('Conf_call', 'AgentdashboardController@Conf_call');
Route::get('Add_conf', 'AgentdashboardController@Add_conf');
Route::get('chanspy_channel', 'AgentdashboardController@chanspy_channel');
Route::get('hangup_channel', 'AgentdashboardController@hangup_channel');
Route::get('Blind_transfer', 'AgentdashboardController@Blind_transfer');
Route::get('Attended_transfer', 'AgentdashboardController@Attended_transfer');
Route::get('changebreakstatus', 'AgentdashboardController@changebreakstatus');
Route::get('get_clbck_det', 'AgentdashboardController@get_clbck_det');
Route::get('get_abn_det', 'AgentdashboardController@get_abn_det');


Route::get('QnreportDashboard','SrvyQnController@QnreportDashboard');
Route::get('searchqnreport','SrvyQnController@searchqnreport');
Route::get('viewansofqngrp', 'SrvyQnController@viewansofqngrp');
Route::get('getansofqngrp','SrvyQnController@getansofqngrp');

Route::get('CallcampagntDashboard','CampaginContoller@CallcampagntDashboard');
Route::get('searchclcampagntreport','CampaginContoller@searchclcampagntreport');
Route::get('CallcampagntsummryDashboard','CampaginContoller@CallcampagntsummryDashboard');
Route::get('searchclcampsummryreport','CampaginContoller@searchclcampsummryreport');


Route::get('view_agntactionhistory','AgentactionController@view_agntactionhistory');
Route::get('findagntactionhistory','AgentactionController@findagntactionhistory');

Route::get('view_sipreg', 'SipController@view_sipreg');

Route::get('start_acw', 'AgentdashboardController@start_acw');
Route::get('end_acw', 'AgentdashboardController@end_acw');

Route::get('view_queue_report','QueueController@view_queue_report');
Route::get('search_queuereport','QueueController@search_queuereport');
Route::get('view_queue_det_report','QueueController@view_queue_det_report');
Route::get('search_queue_det_report','QueueController@search_queue_det_report');
Route::get('search_queue_det_report2','QueueController@search_queue_det_report_sql');

Route::get('view_agntcallhistory','AgentcallController@view_agntcallhistory');
Route::get('search_agentcall_report','AgentcallController@search_agentcall_report');

Route::get('view_abncall_report','AbncallsController@view_abncall_report');
Route::get('search_abn_calls_report','AbncallsController@search_abn_calls_report');

Route::get('view_outboundcall_report','OutboundcallsController@view_outboundcall_report');
Route::get('search_outbound_calls_report','OutboundcallsController@search_outbound_calls_report');

Route::get('view_calls_det_report','CallsdetailsController@view_calls_det_report');
Route::get('search_call_det_report','CallsdetailsController@search_call_det_report');

Route::get('view_agnt_det_report','AgentcallController@view_agnt_det_report');
Route::get('search_agnt_det_report','AgentcallController@search_agnt_det_report');

Route::get('view_calls_trans_report','AgentcallController@view_calls_trans_report');
Route::get('search_agnt_trans_report','AgentcallController@search_agnt_trans_report');

//---------------- add predefined Email Templates ----- badra 02.06.2020-----------------------// 
Route::get('massegeTemplates', 'prDefinedDataMaintainController@viewEmailTemplates');
Route::get('addnewmessageTemplate', 'prDefinedDataMaintainController@addnewmessageTemplate');
Route::post('insertMessageTemplate','prDefinedDataMaintainController@insertMessageTemplate');
Route::get('editTemplate/{template_id}','prDefinedDataMaintainController@editTemplate');
Route::post('updateMessageTemplate','prDefinedDataMaintainController@updateMessageTemplate');
Route::get('getTemplateInfo','prDefinedDataMaintainController@getTemplateInfo');
Route::get('emailAttachment','prDefinedDataMaintainController@emailAttachment');
Route::get('addnewemailAttachment','prDefinedDataMaintainController@addnewemailAttachment');
Route::post('insertEmaiAttachment','prDefinedDataMaintainController@insertEmaiAttachment');
Route::get('editEmailAttachments/{cat_id}','prDefinedDataMaintainController@editEmailAttachments');
Route::get('deleteAttachedDocument/{rec_id}','prDefinedDataMaintainController@deleteDocuments');
Route::post('updateEmailAttachment','prDefinedDataMaintainController@updateEmailAttachment');
Route::get('getListofAttachments','prDefinedDataMaintainController@getListofAttachments');
Route::get('addscript/{camp_id}','prDefinedDataMaintainController@addscript');
Route::post('savescrpt','prDefinedDataMaintainController@savescrpt');

Route::post('logout_agent', 'UsersController@logoutuser');



//-------------------------main dashboards---------------------//
Route::get('maindashboard', 'DashboardController@index');
Route::get('getBatch','DashboardController@batchInfo');
Route::get('searchCli_data','DashboardController@getSearchData');
Route::get('viewProfile/{id}','DashboardController@viewProfile');
//-------------------------main dashboards---------------------//
//-------------------------main dashboards---------------------//
Route::get('historydashboard', 'HistoryDashboardController@index');
Route::get('historygetBatch','HistoryDashboardController@batchInfo');
Route::get('historysearchCli_data','HistoryDashboardController@getSearchData');
//Route::get('historyviewProfile/{id}','HistoryDashboardController@viewProfile');
//-------------------------main dashboards---------------------//

//-------------------------call history dashboard---------------------//
Route::get('callHistory', 'callHistoryController@index');
Route::get('searchClhis_data','callHistoryController@getSearchData');
//Route::get('viewProfile/{id}','callHistoryController@viewProfile');
Route::get('getAllContact','callHistoryController@getAllContact');
Route::get('SaveCallLog', 'CallHistoryController@SaveCallLog');
Route::get('SaveCallbackLog', 'CallHistoryController@SaveCallbackLog');

//-------------------------call history dashboard---------------------//

//-------------------------campaign Mange----------------------//

Route::get('campaigns', 'CampaginContoller@view');
Route::get('addmaterial/{campaign_id}', 'CampaginContoller@addmaterialview');
Route::post('savemtrl', 'CampaginContoller@savemtrl');
Route::get('deletemtrl/{id}', 'CampaginContoller@deletemtrl');
Route::get('editcamp/{id}', 'CampaginContoller@edit');
Route::post('savecampedit', 'CampaginContoller@editsave');


Route::get('deletecsv/{id}','upload_csv_controller@delete');
Route::get('uploadcsv','upload_csv_controller@index');
Route::post('uploadcsv','upload_csv_controller@uploadcsv');
Route::get('getresults','upload_csv_controller@getresults');
Route::get('getwarn','upload_csv_controller@getwarns');
Route::get('getamounts','upload_csv_controller@getamount');
Route::get('gettotalupload','upload_csv_controller@gettotalupload');
Route::get('checkDuplicateUpload','upload_csv_controller@checkDuplicateUpload');

Route::get('deletecsvP/{id}','upload_pay_csv_controller@delete');
Route::get('uploadcsvP','upload_pay_csv_controller@index');
Route::post('uploadcsvP','upload_pay_csv_controller@uploadcsv');
Route::get('getresultsP','upload_pay_csv_controller@getresults');
Route::get('getwarnP','upload_pay_csv_controller@getwarns');
Route::get('getamountsP','upload_pay_csv_controller@getamount');
Route::get('gettotaluploadP','upload_pay_csv_controller@gettotalupload');
Route::get('searchExcelDataP','upload_pay_csv_controller@searchExcelData');
//-------------------------campaign Mange----------------------//

//----------------------system setups-----------------------//
Route::get('standardRemarks/{campaign_id}','SystemSetupController@index');
Route::get('addRemarks/{campaign_id}','SystemSetupController@addRemarks');
Route::post('insertremark','SystemSetupController@insertremark');
Route::get('editremark/{campaign_id}/{id}','SystemSetupController@editremark');
Route::post('updateRemark','SystemSetupController@updateRemark');
Route::get('deleteremark/{campaign_id}/{id}','SystemSetupController@deleteremark');
Route::get('viewsubremarks/{campaign_id}/{id}','SystemSetupController@viewsubremarks');
Route::get('addsubremark/{campaign_id}/{remarkid}','SystemSetupController@addsubremarks');
Route::post('insertsubRemarks','SystemSetupController@insertsubRemarks');
Route::get('editsubremark/{campaign_id}/{remarkid}/{id}','SystemSetupController@editsubremark');
Route::post('updatesubRemark','SystemSetupController@updatesubRemark');
Route::get('deletesubremark/{campaign_id}/{remarkid}/{id}','SystemSetupController@deletesubremark');
//----------------------system setups-----------------------//

//----------------------access CRM----------------------------//
//Route::get('crm', 'CrmContoller@index');
Route::get('crm2', 'CrmContoller@view');
Route::post('insertcustomerdata','CrmContoller@save');
Route::get('businessTypesList/{id}','CrmContoller@businesslist');

//Route::get('viewcrm/{id}', 'CrmContoller@viewcrm');---------
//----------------------access CRM----------------------------//

//--------------------------calling campaign reports---------//
Route::get('CallcampagntDashboard','CampaginContoller@CallcampagntDashboard');
Route::get('searchclcampagntreport','CampaginContoller@searchclcampagntreport');
Route::get('getAgents','CampaginContoller@getAgents');
Route::get('updatecampaignid', 'CampaginContoller@updatestatus');
Route::get('CampaignDataDownload','CampaginDownloadController@index');
Route::get('downLoadFile/{campaign}/{batchid}','CampaginDownloadController@downLoadFile');
//--------------------------calling campaign reports---------//

//--------------------------do not call nos---------//
Route::get('donotCallNos','donotCallingController@index');
Route::get('addNumberstoDontCall','donotCallingController@addNumberstoDontCall');
Route::post('insertNumbertoDonotCallList','donotCallingController@insertNumbertoDonotCallList');
Route::get('validateMobileData','donotCallingController@validateMobileData');
Route::get('searchDontCallData','donotCallingController@searchDontCallData');
Route::get('removeFromList/{id}','donotCallingController@removeFromList');
//--------------------------do not call nos---------//
Route::get('searchExcelData','upload_csv_controller@searchExcelData');


Route::get('loadsubremarks','CrmContoller@loadsubremarks');
Route::post('linkToNextCall','CrmContoller@crm');
Route::get('livereports', 'LivereportController@index');
Route::get('agentreportrefresh', 'LivereportController@reloadagentstatus');
Route::get('queueportrefresh', 'LivereportController@reloadqueuestatus');
Route::get('agentmoniterrefrech', 'LivereportController@reloadagentcountstatus');
// Route::get('queuemoniterrefrech', 'LivereportController@reloadqueuemoniter');
Route::get('queuemoniterrefrech', 'LivereportController@reloadagentcountstatus');
Route::get('setlivedate', 'LivereportController@setsessiondate');



Route::get('checkuserbysoftphone', 'UsersController@checkuserbysoftphone');

//-------------------------add reschedule --------------------------//
Route::get('saveRescheduleData', 'CspCrmContoller@saveRescheduleData');
Route::get('searchAgentCallHistoryData','CspCrmContoller@searchAgentCallHistoryData');
//-------------------------add reschedule --------------------------//

Route::get('updateBreak','CspCrmContoller@updateBreak');
Route::get('incompleterecdashboard','CrmContoller@incompleterecdashboard');
Route::get('searchIncompleteCallList','CrmContoller@searchIncompleteCallList');
Route::get('completeTheCall/{cus_number}/{facility_no}','CrmContoller@completeTheCall');

Route::get('getQueInformation', 'AgentdashboardController@getQueInformation');
Route::get('getIncommingNo', 'AgentdashboardController@getIncommingNo');

Route::get('agentcalllogdashboard','CallHistoryController@agentcalllogdashboard');
Route::get('getAgentWiseNoList','CallHistoryController@getAgentWiseNoList');
Route::get('searchCallLogData','CallHistoryController@searchCallLogData');

Route::get('obmode_status', 'AgentdashboardController@obmode_status');
Route::get('getQueDestination','AgentdashboardController@getQueDestination');


Route::get('view_sipreg', 'SipController@view_sipreg');

//---------------- SIP Registration ----- Thilini 03.09.2020-----------------------// 
Route::post('insertextension','SipController@insert_extension');
Route::get('viewinsertuserextension','SipController@index');
Route::get('deletesip/{id}','SipController@deletesip');
//----------------End SIP Registration ----- Thilini 03.09.2020-----------------------// 

//----------------------Queue Allocation thilini 04/09/2020 --------------------------//
Route::get('viewqueueallocation/{id}', 'QueueAllocationController@index');
Route::get('displayqueue', 'QueueAllocationController@displayqueue');
Route::get('getsrvboxbytype', 'QueueAllocationController@getsrvboxbytype');
Route::get('queuAgentAllocation', 'QueueAllocationController@queuAgentAllocation');
Route::get('getqueueallocationagent', 'QueueAllocationController@getqueueallocationagent');
Route::post('queueassignagent', 'QueueAllocationController@queueassignagent');

Route::get('viewQueList','QueueAllocationController@viewQueList');
//----------------------Queue Allocation thilini 04/09/2020 --------------------------//

//----------------Extension Allocation ----- Thilini 03.09.2020-----------------------// 
Route::get('view_exten_allocation/{id}', 'UsersController@view_exten_allocation');
Route::get('get_extension_allocation', 'UsersController@get_extension_allocation');
Route::post('save_allocate_extension', 'UsersController@save_allocate_extension');
//----------------Extension Allocation ----- Thilini 03.09.2020-----------------------// 

//----------------Queue Allocation to Company ----- Thilini 09.09.2020-----------------------// 
Route::get('view_queue_allo_company', 'QueueAllocationController@view_queue_allo_company');
Route::post('save_allocate_queues', 'QueueAllocationController@save_allocate_queues');
Route::get('get_queue_allo_company', 'QueueAllocationController@get_queue_allo_company');
//----------------Queue Allocation to Company ----- Thilini 09.09.2020-----------------------// 

//----------------Phone Book ----- Thilini 11.09.2020-----------------------// 
Route::get('view_phone_bk', 'PhoneBookController@view_phone_bk');
Route::get('searchdata', 'PhoneBookController@searchdata');
Route::get('view_add_phone_bk', 'PhoneBookController@view_add_phone_bk');
Route::post('save_ph_bk_details', 'PhoneBookController@save_ph_bk_details');
Route::get('view_updating_user/{id}', 'PhoneBookController@view_updating_user');
Route::get('deleteContact', 'PhoneBookController@deleteContact');
Route::post('update_user', 'PhoneBookController@update_user');
Route::get('searchdata', 'AgentdashboardController@searchdata');
//----------------Phone Book ----- Thilini 11.09.2020-----------------------// 

//----------------Supervisor allocation to company----- Thilini 16.09.2020-----------------------// 
Route::get('view_sup_allo_company', 'SupervisorAllocationController@index');
Route::get('get_supervisor_allo_company', 'SupervisorAllocationController@get_supervisor_allo_company');
Route::post('save_allocate_supervisors', 'SupervisorAllocationController@save_allocate_supervisors');
Route::get('get_com_allo_sup', 'SupervisorAllocationController@get_com_allo_sup');
//----------------Supervisor allocation to company----- Thilini 16.09.2020-----------------------// 


Route::get('play_dtmf', 'AgentdashboardController@play_dtmf');
//----------------Type category details form----- Thilini 17.09.2020-----------------------// 
Route::get('view_cat_list', 'CategoryDetailsController@view_cat_list');
Route::get('view_add_category', 'CategoryDetailsController@view_add_category');
Route::post('add_categories', 'CategoryDetailsController@add_categories');
Route::get('search_cat', 'CategoryDetailsController@search_cat');
Route::post('update_cat_details', 'CategoryDetailsController@update_cat_details');
Route::get('view_updating_category/{id}', 'CategoryDetailsController@view_updating_category');
//----------------Type category details form----- Thilini 17.09.2020-----------------------// 

//----------------Type inquiry details form----- Thilini 17.09.2020-----------------------// 
Route::get('view_inq_list', 'InquiryDetailsController@view_inq_list');
Route::get('view_add_inquiry', 'InquiryDetailsController@view_add_inquiry');
Route::post('add_inquiries', 'InquiryDetailsController@add_inquiries');
Route::get('search_inq', 'InquiryDetailsController@search_inq');
Route::post('update_inq_details', 'InquiryDetailsController@update_inq_details');
Route::get('view_updating_inquiry/{id}', 'InquiryDetailsController@view_updating_inquiry');
//----------------Type inquiry details form----- Thilini 17.09.2020-----------------------// 

Route::get('by_softphone_Agentdashboard', 'AgentdashboardController@by_softphone_Agentdashboard');

Route::get('getQeueData', 'QueueController@getQeueData');

Route::get('checkOutboundMode', 'AgentdashboardController@checkOutboundMode');

Route::get('getCategoryList','CallHistoryController@getCategoryList');
Route::get('getInquaryList','CallHistoryController@getInquaryList');

Route::post('Save_Call_Log', 'InboundCallController@Save_Call_Log');
Route::post('Save_Call_Log_for_num', 'InboundCallController@Save_Call_Log_for_num');
Route::post('Save_Call_Log_for_num_console', 'NewCallLogController@Save_Call_Log_for_num_console');


//----------------report allocation to email form----- Thilini 09.10.2020-----------------------// 
Route::get('view_repo_allo', 'ReportAllocationController@view_repo_allo');
Route::get('add_repo_to_email', 'ReportAllocationController@add_repo_to_email');
Route::get('search_report_details', 'ReportAllocationController@search_report_details');
Route::post('save_report_info', 'ReportAllocationController@save_report_info');
//----------------report allocation to email form---- Thilini 09.10.2020-----------------------//

//----------------notification on header updating------------------------//
Route::get('count_sms_email_fax_rem','SmsSrvController@count_sms_email_fax_rem');
Route::get('getInquaryList','CallHistoryController@getInquaryList');

//----------------call summary report form---- Thilini 20.10.2020-----------------------//
Route::get('view_call_sum_repo','CallSummaryReportController@view_call_sum_repo');
Route::get('search_call_sum_rep_by_company','CallSummaryReportController@search_call_sum_rep_by_company');
Route::get('search_call_sum_rep_by_queue','CallSummaryReportController@search_call_sum_rep_by_queue');
Route::get('search_call_sum_rep_by_agent','CallSummaryReportController@search_call_sum_rep_by_agent');
Route::get('get_queue_data', 'CallSummaryReportController@get_queue_data');
Route::get('get_agent_data', 'CallSummaryReportController@get_agent_data');

Route::get('view_com_sum_repo', 'CallCompanyReportController@view_com_sum_repo');
Route::get('cancelform','ContactController@cancelform');
Route::get('resetdata','ContactController@resetdata');

//Route::get('findagntaction','AgentactionController@findagntaction');
Route::get('view_useraction','AgentactionController@index');
Route::get('searchuseractivites','AgentactionController@searchuseractivites');
Route::get('select_ring_str','QueueAllocationController@select_ring_str');

Route::get('viewQueList','QueueAllocationController@viewQueList');

//---------------------Evaluation Context Master-----Madhushika 31.12.2020------------------//
Route::get('view_evaluationContextMaster','AgentQualityEvalController@view_evaluationContextMaster');
Route::get('addSelectedCriteria','AgentQualityEvalController@addSelectedCriteria');
Route::get('updateSelectedCriteria','AgentQualityEvalController@updateSelectedCriteria');
Route::get('del_cntxt_data','AgentQualityEvalController@del_cntxt_data');
Route::post('add_crit_cntxt_to_tbl','AgentQualityEvalController@add_crit_cntxt_to_tbl');
Route::get('view_eval_cntxt','AgentQualityEvalController@view_eval_cntxt');
Route::get('view_update_cntxt/{id}', 'AgentQualityEvalController@view_update_cntxt');
Route::post('update_crit_cntxt','AgentQualityEvalController@update_crit_cntxt');

//----------------Evaluaton criteria type master ----- Thilini 10.12.2020-----------------------// 
Route::get('view_eval_crit_type_list', 'EvaluationCriteriaController@view_eval_crit_type_list');
Route::get('view_add_eval_crit_type', 'EvaluationCriteriaController@view_add_eval_crit_type');
Route::post('save_eval_types', 'EvaluationCriteriaController@save_eval_types');
Route::get('search_add_eval_crit_type', 'EvaluationCriteriaController@search_add_eval_crit_type');
//Route::get('delete_crit_type/{id}', 'EvaluationCriteriaController@delete_crit_type');
Route::get('delete_crit_type_status', 'EvaluationCriteriaController@delete_crit_type_status');
Route::get('del_crit_type', 'EvaluationCriteriaController@del_crit_type');
Route::get('del_criteria_type', 'EvaluationCriteriaController@del_criteria_type');
//----------------Evaluaton criteria type master ----- Thilini 10.12.2020-----------------------// 

//----------------Evaluaton criteria master ----- Thilini 10.12.2020-----------------------// 
Route::get('view_eval_crit_mst_list', 'EvaluationCriteriaMasterController@view_eval_crit_mst_list');
Route::get('view_add_eval_crit_mst', 'EvaluationCriteriaMasterController@view_add_eval_crit_mst');
Route::post('save_eval_crit_mst', 'EvaluationCriteriaMasterController@save_eval_crit_mst');
Route::get('edit_crit_mst_status', 'EvaluationCriteriaMasterController@edit_crit_mst_status');
Route::get('delete_crit_mst_record/{id}', 'EvaluationCriteriaMasterController@delete_crit_mst_record');
Route::get('view_editing_crit_mst/{id}', 'EvaluationCriteriaMasterController@view_editing_crit_mst');
Route::post('update_eval_crit_mst', 'EvaluationCriteriaMasterController@update_eval_crit_mst');
//----------------Evaluaton criteria master ----- Thilini 10.12.2020-----------------------// 

//----------------Agent Evaluation master ----- Thilini 20.12.2020-----------------------// 
Route::get('view_agnt_eval_mst_list', 'AgentEvaluationMasterController@view_agnt_eval_mst_list');
Route::get('view_add_agnt_eval_mst', 'AgentEvaluationMasterController@view_add_agnt_eval_mst');
Route::post('save_agnt_eval_mst', 'AgentEvaluationMasterController@save_agnt_eval_mst');
Route::get('search_evaluation_mst_data', 'AgentEvaluationMasterController@search_evaluation_mst_data');
Route::get('view_edit_agnt_eval_mst/{eval_id}', 'AgentEvaluationMasterController@view_edit_agnt_eval_mst');
Route::get('get_agnt_allo/{eval_id}', 'AgentEvaluationMasterController@get_agnt_allo');
Route::post('update_agnt_eval_mst', 'AgentEvaluationMasterController@update_agnt_eval_mst');
Route::get('view_agnt_eval_recordings/{eval_id}', 'AgentEvaluationMasterController@view_agnt_eval_recordings');
//----------------Agent Evaluaton master ----- Thilini 20.12.2020-----------------------// 

//----------------Agent Quality Evaluation master ----- Thilini 20.12.2020-----------------------// 
Route::get('view_agnt_quality_eval_list', 'AgentEvaluationMasterController@view_agnt_quality_eval_list');
Route::get('search_agnt_quality_eval', 'AgentEvaluationMasterController@search_agnt_quality_eval');
Route::get('view_recording/{id}/{contxt_id}/{rec_id}', 'AgentEvaluationMasterController@view_recording');
Route::get('view_recording_files', 'AgentEvaluationMasterController@view_recording_files');
Route::get('play_recording', 'AgentEvaluationMasterController@play_recording');
Route::post('save_eval_agnt_ans_data', 'AgentEvaluationMasterController@save_eval_agnt_ans_data');
Route::post('calculate_agnt_eval_marks', 'AgentEvaluationMasterController@calculate_agnt_eval_marks');
Route::get('view_eval_crit_weight/{id}/{eval_contxt_id}', 'AgentEvaluationMasterController@view_eval_crit_weight');
Route::post('save_eval_crit_weight', 'AgentEvaluationMasterController@save_eval_crit_weight');
Route::get('view_weight_add_details', 'AgentEvaluationMasterController@view_weight_add_details');
Route::get('search_agnt_eval_rate_card', 'AgentEvaluationMasterController@search_agnt_eval_rate_card');
Route::get('view_agnt_eval_rate_list', 'AgentEvaluationMasterController@view_agnt_eval_rate_list');
//----------------Agent Quality Evaluation master ----- Thilini 20.12.2020-----------------------//   

//----------------Queue Allocation to Team Leader ----- Thilini 30.11.2020-----------------------// 
Route::get('view_queue_allo_tmldr', 'QueueAlloTeamLeaderController@view_queue_allo_tmldr');
Route::post('save_allocated_queues_tmldr', 'QueueAlloTeamLeaderController@save_allocated_queues_tmldr');
Route::get('get_queue_allo_tmldr', 'QueueAlloTeamLeaderController@get_queue_allo_tmldr');
Route::get('get_all_company_queueus', 'QueueAlloTeamLeaderController@get_all_company_queueus');
Route::get('get_selected_queue_allo_team_leader', 'QueueAlloTeamLeaderController@get_selected_queue_allo_team_leader');

//----------------Queue Allocation to Company ----- Thilini 30.11.2020-----------------------// 

//----------------agent Allocation to Team Leader ----- Thilini 30.11.2020-----------------------// 
Route::get('view_agnt_allo_tmldr', 'AgentAlloTeamLeaderController@view_agnt_allo_tmldr');
Route::post('save_allocated_agent_tmldr', 'AgentAlloTeamLeaderController@save_allocated_agent_tmldr');
Route::get('get_agnt_allo_tmldr', 'AgentAlloTeamLeaderController@get_agnt_allo_tmldr');
//----------------Queue Allocation to Company ----- Thilini 30.11.2020-----------------------// 


//----------------super user Allocation to company ----- Thilini 30.11.2020-----------------------// 
Route::get('view_sup_user_allo_com', 'SupUserAlloCompanyController@view_sup_user_allo_com');
Route::post('save_allocated_supuser_com', 'SupUserAlloCompanyController@save_allocated_supuser_com');
Route::get('get_sup_user_allo_com', 'SupUserAlloCompanyController@get_sup_user_allo_com');
//----------------Queue Allocation to Company ----- Thilini 30.11.2020-----------------------//

Route::get('search_abn_details','AgentdashboardController@search_abn_details');


// IVR Reports

Route::get('IVRreport','IVRController@index');
Route::get('SearchIVRDetails','IVRController@SearchIVRDetails');
Route::get('IVRDropDetail','IVRDropController@index');
Route::get('SearchIVRDropDetails','IVRDropController@SearchDropIVR');
Route::get('IVRExtensionDial','ExtensionTransferController@index');
Route::get('SearchExtensionTransfer','ExtensionTransferController@searchdata');

//WhatsApp Module
Route::get('WhatsAppsrvDashboard','WhatAppsrvController@index');
Route::get('LoadWhatsappMessages','WhatAppsrvController@LoadWhatsappMessages');
Route::get('ViewWACcusList/{refNo}', 'WhatAppsrvController@ViewWACcusList');
Route::get('ViewWACcustomerList/{refNo}', 'WhatAppsrvController@ViewWACcustomerList');
Route::post('discardWACsrv','WhatAppsrvController@discardWACsrv');
Route::post('inquireWACsrv','WhatAppsrvController@inquireWACsrv');
Route::get('custAllocation/{wac_num}','WhatAppsrvController@custAllocation');
Route::get('ArchiveWhatsAppsrvDashboard','WhatAppsrvController@WhatsappSRVArchiveindex');
Route::get('LoadWhatsappArchiveMessages','WhatAppsrvController@LoadWhatsappArchiveMessages');

Route::get('AddcontactsbyWACcontact/{refNo}', 'ContactController@addnewcontactbyWhatsappContact');
Route::get('addnewcustbyWhatsappContact/{refNo}', 'ContactController@addnewcustbyWhatsappContact');
Route::post('addconbyWACnum', 'ContactController@addconbyWACnum');
Route::post('CustAlloconbyWACnum', 'ContactController@CustAlloconbyWACnum');

Route::get('viewaddticketbyWacNum/{contactid}/{refNo}','TicketController@viewaddticketWhatsappNum');
Route::post('wacsrvinsertTicket', 'TicketController@wacsrvinsertTicket');

Route::get('ReadWhatsappChat/{ref_no}','WhatsAppMsgController@ReadWhatsappChat');
Route::get('Get_updated_chat_by_ref','WhatsAppMsgController@Get_updated_chat_by_ref');
Route::get('SendWhatsAppMessage','WhatsAppMsgController@SendMessage');
Route::get('NewMessages','WhatsAppMsgController@NewMessages');
Route::post('SendMediaMessage','WhatsAppMsgController@MediaMessage');


// WebChat Routes - Starting
Route::get('WebChatsrvDashboard','WebChatsrvController@index');
Route::get('LoadWebChatMessages','WebChatsrvController@LoadWebChatMessages');
Route::get('ArchiveWebChatsrvDashboard','WebChatsrvController@WebChatSRVArchiveindex');
Route::get('LoadWebChatArchiveMessages','WebChatsrvController@LoadWebChatArchiveMessages');
Route::get('ViewWebChatcusList/{refNo}', 'WebChatsrvController@ViewWebChatcusList');
Route::get('ViewWebChatcustomerList/{refNo}', 'WebChatsrvController@ViewWebChatcustomerList');
Route::get('custWCAllocation/{wc_num}','WebChatsrvController@custWCAllocation');
Route::get('discardWCsrv','WebChatsrvController@discardWCsrv');
Route::post('inquireWCsrv','WebChatsrvController@inquireWCsrv');

Route::get('ReadWebChat/{ref_no}','WebChatMsgController@ReadWebChat');
Route::get('GetChatByRefNo','WebChatMsgController@GetChatByRefNo');
Route::get('SendWebChatMessage','WebChatMsgController@SendWCMessage');

Route::get('addnewcontactbyWebChatContact/{refNo}', 'ContactController@addnewcontactbyWebChatContact');
Route::post('addconbyWebChatnum', 'ContactController@addconbyWebChatnum');
Route::get('addnewcustbyWebChatContact/{refNo}', 'ContactController@addnewcustbyWebChatContact');
Route::post('CustAlloconbyWebChatnum', 'ContactController@CustAlloconbyWebChatnum');

Route::get('viewaddticketWebChat/{contactid}/{refNo}','TicketController@viewaddticketWebChat');
Route::post('webchatsrvinsertTicket', 'TicketController@webchatsrvinsertTicket');

// Webchat Routes - ending


// --------------------Abandon callback & Callback Request Report -----------------------------
Route::get('abandoncallbackreport', 'AbndoncallbackController@view_abncallback_report');
Route::get('viewcallbackdata', 'AbndoncallbackController@search_abn_callback_report');
Route::get('abanupdatecallback', 'AbndoncallbackController@updatecallback');
Route::get('callbackrequestreport', 'CallbackRequestController@view_callback_req_report');
Route::get('viewcallbackrequestdata', 'CallbackRequestController@search_callback_req_report');
Route::get('view_callback_req_report_for_mail', 'CallbackRequestController@search_callback_req_report_for_mail');
Route::get('updatecallback', 'CallbackRequestController@updatecallback');
Route::get('viewcallbackdata_for_mail', 'AbndoncallbackController@viewcallbackdata_for_mail');
// --------------------Abandon callback & Callback Request Report -----------------------------


// ----------------------------- Agent Profile ------------------------------

Route::get('AgentProfileDetails','AgentProfileController@index');

//---------------Agent Action Summary Report------Madhushika --------------------//
Route::get('view_agentaction_summ_report','AgentactionController@view_agentaction_summ_report');
Route::get('search_agentaction_summ_report','AgentactionController@search_agentaction_summ_report');

//----------------Abandon Hourly Report ----- Thilini 09.03.2020-----------------------// 
Route::get('view_abn_hourly_report', 'AbncallsController@view_abn_hourly_report');
Route::get('search_abn_hourly_report', 'AbncallsController@search_abn_hourly_report');
//----------------Abandon Hourly Report ----- Thilini 09.03.2020-----------------------// 


//----------------Abandon Queue Hourly Report ----- Thilini 15.03.2020--------------------// 
Route::get('view_queue_abn_hourly_report', 'AbncallsController@view_queue_abn_hourly_report');
Route::get('search_queue_abn_hourly_report', 'AbncallsController@search_queue_abn_hourly_report');
//----------------Abandon Queue Hourly Report ----- Thilini 15.03.2020--------------------//

//-----------------Login Reset--- thilini 07.09.2020 --------------------------//
Route::post('login_reset', 'UsersController@login_reset');
Route::post('reset_logout_datetime', 'UsersController@reset_logout_datetime');
//------------------Login Reset ------thilini 07.09.2020 --------------------------//

//----------------Ring no answer Report ----- Madhushika 07.04.2021--------------------// 
Route::get('view_agnt_ring_detail_report', 'AgentringreportController@view_agnt_ring_detail_report');
Route::get('search_ring_no_ans_detail', 'AgentringreportController@search_ring_no_ans_detail');
//----------------Ring no answer Report ----- Madhushika 07.04.2021--------------------//

//----------------Extension Skill det report ----- Madhushika 08.04.2021--------------------// 
Route::get('view_extension_list', 'ExtenSkillDetController@view_extension_list');
Route::get('getextension', 'ExtenSkillDetController@getextension');
Route::get('getdatadownload', 'CampaginDownloadController@getdatadownload');
//----------------Extension Skill det report ----- Madhushika 08.04.2021--------------------//

Route::get('occupancyReport', 'OCReportController@index');
Route::get('searchOCData', 'OCReportController@searchOCData');

//----------------Break exceeding detail report ----- Madhushika 03.05.2021--------------------//
Route::get('view_br_exceed_repo', 'breakReportController@view_br_exceed_repo');
Route::get('seacrh_break_status', 'breakReportController@seacrh_break_status');
Route::get('check_break_exceeded', 'AgentdashboardController@check_break_exceeded');
Route::post('save_break_exc_reason', 'AgentdashboardController@save_break_exc_reason');
//----------------Break exceeding detail report ----- Madhushika 03.05.2021--------------------//

Route::get('queue_waiting_time_report', 'QWaitingTimeReportController@index');
Route::get('searchqueue_waiting_time', 'QWaitingTimeReportController@searchqueue_waiting_time');
Route::get('by_softphone_Crmform', 'AgentdashboardController@by_softphone_Crmform');

//----------------Agent Schedule Detail Report ----- Badra 06.05.2021--------------------//
Route::get('view_agent_reschedule_list', 'CampaginContoller@view_agent_reschedule_list');
Route::get('seacrh_reschedule_call_list', 'CampaginContoller@seacrh_reschedule_call_list');
//----------------Agent Schedule Detail Report ----- Badra 06.05.2021--------------------//

//----------------Data Rating IVR report ----- Madhushika 06.05.2021--------------------//
Route::get('view_call_rate_det', 'CallRatingDetailController@view_call_rate_det');
Route::get('search_call_rating_report', 'CallRatingDetailController@search_call_det_report');
Route::get('download_rating_callrecord','CallRatingDetailController@download_rating_callrecord');
Route::get('play_rating_callrecord','CallRatingDetailController@play_rating_callrecord');
Route::get('view_call_rate_summ_agnt','CallRatingAgentController@view_call_rate_summ_agnt');
Route::get('search_call_rating_summ_agnt','CallRatingAgentController@search_call_rating_summ_agnt');
Route::get('view_call_rate_summ_queue','CallRatingQueueController@view_call_rate_summ_queue');
Route::get('search_call_rating_summ_que','CallRatingQueueController@search_call_rating_summ_que');
//----------------Data Rating IVR report ----- Madhushika 06.05.2021--------------------//

Route::get('open_agntdb_addcusview', 'ContactController@open_agntdb_addcusview');
Route::get('open_agntdb_inboundCall','InboundCallController@inboundCallUpdateRecordInfo');

//----------------Escalate tickets process ----- Madhushika 22.04.2021--------------------// 
Route::get('esc_ticketdetail_sendbyemail', 'TicketController@esc_ticketdetail_sendbyemail');

//----------------Break Approval Module ----- Madhushika 25.05.2021--------------------//
Route::get('view_br_approval_master', 'BreakApprovalController@view_br_approval_master');
Route::get('view_add_br_approval', 'BreakApprovalController@view_add_br_approval');
Route::post('save_br_approval', 'BreakApprovalController@save_br_approval');
Route::get('search_br_approval_list', 'BreakApprovalController@search_br_approval_list');
Route::get('status_update', 'BreakApprovalController@status_update');
Route::get('br_appr_delete', 'BreakApprovalController@br_appr_delete');
Route::get('check_br_def_time', 'AgentdashboardController@check_br_def_time');
Route::get('view_br_req_approval', 'BreakApprovalController@view_br_req_approval');
Route::get('br_approval_req', 'BreakApprovalController@br_approval_req');
// Route::get('updatebreakreq', 'BreakApprovalController@updatebreakreq');
Route::get('approvebreakreq', 'BreakApprovalController@approvebreakreq');
Route::get('br_reject_reason', 'BreakApprovalController@br_reject_reason');
Route::get('br_reject_with_pen_time', 'BreakApprovalController@br_reject_with_pen_time');
Route::get('view_edit_br_appr_shedule/{shed_id}', 'BreakApprovalController@view_edit_br_appr_shedule');
Route::post('save_edit_br_approval', 'BreakApprovalController@save_edit_br_approval');
//----------------Break Approval Module ----- Madhushika 25.05.2021--------------------//

//----------------Break exceeding detail report ----- Madhushika 03.05.2021--------------------//
Route::get('view_br_exceed_repo', 'breakReportController@view_br_exceed_repo');
Route::get('seacrh_break_status', 'breakReportController@seacrh_break_status');
Route::get('check_break_exceeded', 'AgentdashboardController@check_break_exceeded');
Route::post('save_break_exc_reason', 'AgentdashboardController@save_break_exc_reason');
//----------------Break exceeding detail report ----- Madhushika 03.05.2021--------------------//

Route::get('sms_test', 'SmsTestController@index');
Route::get('sms_send', 'SmsTestController@sms_send');

//------------------------get SRV Details Madhushika 23.03.2020-----------------------//
Route::get('get_whatapp_det', 'InboundCallController@get_whatapp_det');
Route::get('get_sms_det', 'InboundCallController@get_sms_det');
Route::get('get_fax_det', 'InboundCallController@get_fax_det');
Route::get('get_email_det', 'InboundCallController@get_email_det');
//------------------------get SRV Details Madhushika 23.03.2020-----------------------//

//----------------Ticket Summary Dashboard ----- Sachith 01.06.2021--------------------//
Route::get('view_ticket_summary', 'TicketSummaryDbContoller@index');
Route::get('search_ticket_summary', 'TicketSummaryDbContoller@search_ticket_summary');
Route::get('search_agent_ticket_summary', 'TicketSummaryDbContoller@search_agent_ticket_summary');
//----------------Ticket Summary Dashboard ----- Sachith 01.06.2021--------------------//

//----------------Queue Allocation ----- MAdhushika,Sachith 03.06.2021--------------------//
Route::get('view_que_allo', 'QueueAllocationController@view_que_allo');
Route::get('save_que_allo', 'QueueAllocationController@save_que_allo');
Route::get('view_que_allo/{sip_id}', 'QueueAllocationController@update');
//----------------Queue Allocation ----- Madhushika,Sachith 03.06.2021--------------------//

//----------------SMS Campaign ----- Madhushika 23.06.2021--------------------//
Route::get('view_sms_csv_upload','upload_sms_csvController@view_sms_csv_upload');
Route::post('upload_file','upload_sms_csvController@upload_file');
Route::post('getresults','upload_sms_csvController@getresults');
Route::get('view_sms_campaign','SmsCampaginController@view_sms_campaign');
Route::get('view_insertsmscamp', 'SmsCampaginController@view_insertsmscamp');
Route::post('savesmscamp', 'SmsCampaginController@savesmscamp');
Route::get('search_sms_camp_det', 'SmsCampaginController@search_sms_camp_det');
Route::get('view_contact_allo/{campaign_id}', 'SmsCampaginController@view_contact_allo');
Route::get('saveassigncamp', 'SmsCampaginController@saveassigncamp');
Route::get('get_contact_det', 'SmsCampaginController@get_contact_det');
Route::get('get_sms_camp_count','SmsCampaginController@get_sms_camp_count');
Route::get('view_edit_campgn/{campaign_id}','SmsCampaginController@view_edit_campgn');
Route::post('save_edit_sms_camp','SmsCampaginController@save_edit_sms_camp');
Route::get('view_smscamp_det_repo','SmsCampaginController@view_smscamp_det_repo');
Route::get('get_sms_camp_det','SmsCampaginController@get_sms_camp_det');

Route::get('view_sentsms_det_repo','SmsCampaginController@view_sentsms_det_repo');
Route::get('get_sent_sms_det','SmsCampaginController@get_sent_sms_det');
//Route::get('view_contact_allo/{campaign_id}/{batch_id}','SmsCampaginController@view_update_contact_allo');
//Route::get('update', 'SmsCampaginController@update');
//Route::get('get_inbatch_id', 'SmsCampaginController@get_inbatch_id');
//----------------SMS Campaign ----- Madhushika 23.06.2021--------------------//

Route::get('emailtest', 'emailtest@emailtest');

Route::get('check_callback_before_add', 'AgentdashboardController@check_callback_before_add');

Route::get('view_callhistory_detail_report','CallHistoryDetReportController@view_callhistory_detail_report');
Route::get('search_callhistory_detail_report','CallHistoryDetReportController@search_callhistory_detail_report');

Route::get('view_calls_det_report2','CallsdetailsController2@view_calls_det_report2');
Route::get('search_call_det_report2','CallsdetailsController2@search_call_det_report');

Route::get('search_queue_det_report2','QueueController@search_queue_det_report_sql');

Route::get('check_call_befor_log', 'NewCallLogController@check_call_befor_log');


Route::get('check_out_api', 'OutApiController@check_out_api');
Route::get('getdetailsofcaller_Out_Api', 'OutApiController@getdetailsofcaller_Out_Api');
Route::get('open_agntdb_addcusview_Out_Api', 'OutApiController@open_agntdb_addcusview_Out_Api');
Route::get('open_agntdb_inboundCall_Out_Api','OutApiController@inboundCallUpdateRecordInfo_Out_Api');
Route::get('SaveCallLog_Out_Api', 'OutApiController@SaveCallLog_Out_Api');
Route::get('SaveCallLog_Auto_OB_Api', 'OutApiController@SaveCallLog_Auto_OB_Api');

Route::get('view_ob_api_report','ObApiCallReportController@view_ob_api_report');
Route::get('search_ob_api_report','ObApiCallReportController@search_ob_api_report');

Route::get('SaveCallbackLog_report', 'AbndoncallbackController@SaveCallbackLog_report');

Route::get('view_call_sum_day','CallSummaryDayController@view_call_sum_day');
Route::get('search_call_sum_rep_by_company2','CallSummaryDayController@search_call_sum_rep_by_company');
Route::get('search_call_sum_rep_by_queue2','CallSummaryDayController@search_call_sum_rep_by_queue');
Route::get('search_call_sum_rep_by_agent2','CallSummaryDayController@search_call_sum_rep_by_agent');
Route::get('get_queue_data2', 'CallSummaryDayController@get_queue_data');
Route::get('get_agent_data2', 'CallSummaryDayController@get_agent_data');

Route::get('view_detail_call_log','DetailCallLogController@view_detail_call_log');
Route::get('search_detail_call_log','DetailCallLogController@search_call_det_report');

Route::get('view_mobile_calls_det_report','MobileCallDetailsController@view_mobile_calls_det_report');
Route::get('search_mobile_call_det_report','MobileCallDetailsController@search_mobile_call_det_report');

Route::get('download_rep', 'CallsdetailsController2@download');

