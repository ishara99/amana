<!DOCTYPE html>
<html>

@include('header_new')
<script>
$(document).ready(function() {
searchdata();
});
</script>

<!-- Content Wrapper. Contains page content -->
<!-- Start body -->
<div class="content-wrapper">
<div class="col-lg-12 ">
    <h1 class="form_caption">Abandon Callback Report</h1>
</div>
<div class="container-fluid" style="margin-left: 33px; margin-top: 7px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:0px">
            <form class="form-horizontal" action="{{url('searchContact')}}" method="post" id="form">
                <div class="form-group-inner">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds" id="com_id" name="com_id" onchange="loadQeues();"
                                            required>
                                            <option value="All">Company</option>
                                            <?php 
                                            foreach($get_com_data as $com_value){ ?>
                                            <option value="<?php echo $com_value->id ?>">
                                                <?php echo $com_value->com_name ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="user" name="user" value="{{ Session::get('username')}}" >
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12" style="padding-left:10px">
                                        <select class="form-control   textfeilds"  id="queue_id" name="queue_id"
                                            required>
                                            <option value="All">Queue</option>
                                            <?php 
                                            foreach($getdndstatus as $value){ ?>
                                            <option value="<?php echo $value->extension ?>">
                                                <?php echo $value->descr ?></option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 30px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">From</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 00:00:00'); ?>" name="frm_date"
                                            id="frm_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group-inner" style="width:100%; ">

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
                                        style="padding-left: 0px; padding-right: 5px;">
                                        <label class="login2 pull-right pull-right-pro" style="font-size: 15px;">To</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
                                        style="padding-left: 0px; padding-right: 0px;">
                                        <input type="text" class="some_class" 
                                            value="<?php echo date('Y-m-d 23:00:00'); ?>" name="to_date"
                                            id="to_date">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"
                                style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-group-inner" style="width:100%;">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <div class="button-style-four btn-mg-b-10">
                                            <button type="button" class="btn btn-custon-four btn-success attr_btn"
                                                style="width:78px; " onclick="searchdata()">Search &nbsp</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    <div class="sparkline13-list">
        <div class="sparkline13-graph">
            <div class="datatable-dashv1-list custom-datatable-overright">
                <table id="queue_report" class="table table-bordered table-striped tablerowsize" cellspacing="0"
                    width="100%">
                    <thead class="table_head">
                        <tr>
                            <th><p class="text-center">Date Time</p></th>
                            <th><p class="text-center">Queue</p></th>
                            <th><p class="text-center">Queue Name</p></th>
                            <th><p class="text-center">CLI</p></th>
                            <th><p class="text-center">Customer Name</p></th>
                            <th><p class="text-center">DID</p></th>
                            <th><p class="text-center">Agent ID</p></th>
                            <th><p class="text-center">Agent Name</p></th>
                            <th><p class="text-center">Duration</p></th>
                            <th><p class="text-center">Preset One</p></th>
                            <th><p class="text-center">Preset Two</p></th>
                            <th><p class="text-center">Comment</p></th>          
                            <th><p class="text-center">Status</p></th>
                            <th><p class="text-center">Callback time</p></th>
                            <th><p class="text-center">Callback Call Status</p></th>
                            <th><p class="text-center">In Date/Time</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<input type="hidden" id="token" value="{{ csrf_token() }}">
</div>
<!-- ./col -->
</div>
<!-- /.row -->
</div>
<br><br>
<script>

    $(document).ready(function () {

        searchdata();
        
    });

    function searchdata() {
        var queue_id = document.getElementById("queue_id").value;
        var queue_id_ = $('#queue_id option:selected').text();
        queue_id_1 = queue_id_.trim();
        var to_date = document.getElementById("to_date").value;
        var frm_date = document.getElementById("frm_date").value;
        var com_id = document.getElementById("com_id").value;
        var com_id_ = $('#com_id option:selected').text();
        com_id_1 = com_id_.trim();
        var user = document.getElementById("user").value;
        var report_name = "Abandon Callback Report";
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds(); 

        $("#queue_report").dataTable().fnDestroy();

        $('#queue_report').DataTable({
            "processing": true,
            "scrollX": true,
            "ajax": {
                "url": "viewcallbackdata",

                "type": "GET",
                data: {
                    queue_id: queue_id,
                    to_date: to_date,
                    frm_date: frm_date,
                    com_id:com_id,

                },
            },
            "columns": [
                { "data": "incoming_date" },
                { "data": "extension" },
                { "data": "descr" },
                { "data": "incoming_num" },
                { "data": "fullName" },
                { "data": "did_num" },
                { "data": "agnt_sipid" },
                { "data": "agnt_name" },
                { "data": "waiting_time" },
                { "data": "cat_one_prerem_id" },
                { "data": "cat_two_prerem_id" },
                { "data": "call_log" },             
                {
                 sortable: false,
                 "render": function ( data, type, full, meta ) {
                     var ID = full.id;
                     var status = full.cbstatus;
                     if(status=='1'){
                      return '<input onclick="update(1,'+ID+')" type="checkbox" checked  id='+ID+' value='+status+'>';
                     }else{
                      return '<input onclick="update(0,'+ID+')" type="checkbox"  id='+ID+' value='+status+'>';
                     }
                     
                 }
                },
                { "data": "callback_datetime" },
                { "data": "callbackStatus" },
                { "data": "in_datetime" }

            ],
            "columnDefs": [{
                className: "text-center",
                "targets": [0,1,2,3,4,5,6,7,8]
            },
            {  }
            ],
            "order": [
                [0, "desc"]
            ],
            "pageLength": 25,

            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'pdfHtml5',
            orientation: 'landscape', 
            pageSize: 'A3',
            title: '',
            filename: report_name.toString(),
           
            customize: function (doc) 
            {
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',              
                            },
                            {
                                alignment: 'left',
                                text: ['To Date: ',{ text:to_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: '',                                              
                            },
                            {
                                alignment: 'left',                                          
                                text: ['From Date: ',{ text: frm_date.toString() }]                       
                            }
                        ],
                            margin: 5
                     });  
                     doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['Date Time: ',{ text: datetime.toString() }],              
                            },
                            {
                                alignment: 'left',
                                text: ['Queue: ',{ text: queue_id_1.toString() }]                       
                            }
                        ],
                            margin: 5
                     });
                    doc.content.splice( 0,0,{
                    columns:
                        [ 
                            {
                                alignment: 'left',
                                text: ['User: ',{ text: user.toString() }],                                              
                            },
                            {
                                alignment: 'left',                                          
                                text: ['Company: ',{ text: com_id_1.toString() }],                    
                            }
                        ],
                            margin: 5
                    });  
                    doc['header']=(function() {
                        return {
                            columns: [
                                {
                                    alignment: 'center',  
                                    fontSize: 12,                        
                                    text: ['Report Name: ',{ text: report_name.toString() }]                                                                                 
                                }

                            ],
                            margin: 20
                        }
                    });
        }  
        },
        {extend:'excelHtml5',text: 'EXCEL',className: 'btn-primary',  title:report_name.toString(),
                
            customize: function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var numrows = 3;
                var clR = $('row', sheet);

                //update Row
                clR.each(function () {
                    var attr = $(this).attr('r');
                    var ind = parseInt(attr);
                    ind = ind + numrows;
                    $(this).attr("r",ind);
                });

                // Create row before data
                $('row c ', sheet).each(function () {
                    var attr = $(this).attr('r');
                    var pre = attr.substring(0, 1);
                    var ind = parseInt(attr.substring(1, attr.length));
                    ind = ind + numrows;
                    $(this).attr("r", pre + ind);
                });

                function Addrow(index,data) {
                    msg='<row r="'+index+'">'
                    for(i=0;i<data.length;i++){
                        var key=data[i].key;
                        var value=data[i].value;
                        msg += '<c t="inlineStr" r="' + key + index + '">';
                        msg += '<is>';
                        msg +=  '<t>'+value+'</t>';
                        msg+=  '</is>';
                        msg+='</c>';
                    }
                    msg += '</row>';
                    return msg;
                }

                //insert
                var r1 = Addrow(1, [{ key: 'A', value: 'Report Name :'+report_name }, 
                                    { key: 'B', value: 'Date Time :'+datetime },
                                    { key: 'C', value: 'Report Generated User :'+user }
                                ]);

                var r2 = Addrow(2, [{ key: 'A', value: 'Company :'+com_id_1 }, 
                                    { key: 'B', value: 'Queue:'+queue_id_1},
                                    { key: 'C', value: 'From Date :'+frm_date},
                                    { key: 'D', value: 'To Date :'+to_date}
                                    ]);
                sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ sheet.childNodes[0].childNodes[1].innerHTML;
            }
        } 
        ]

        });
            $('input[type="search"]').css({
		        'width': '350px',
		        'display': 'inline-block'
		    });
    }
function loadQeues(){
	var com_id = document.getElementById("com_id").value;
	$.ajax({
            url: 'getQeueData',
            type: 'GET',
            data: {com_id: com_id},
            success: function (response)
            {
                var model = $('#queue_id');
                model.empty();

                var datalist;
                datalist += "<option value='All'>Queue</option>";
                $.each(response, function (index, element) {
                    $.each(element, function (colIndex, c) {
                        datalist += "<option value='" + c.extension + "'>" + c.descr+ "</option>";
                    });
                });
                $('#queue_id').html(datalist);
            }
        });
    }
</script>


<script>


    function update(status,id){

        var status = status;
        var id = id;
        $.ajax({
            url: 'abanupdatecallback',
            type: 'GET',
            data: {id: id,status:status},
            success: function (response) {
                window.alert("Updated");
            }
        });
    

    }

function change_rp_type(type) {

    var rp_type = document.getElementById('rp_type');
    rp_type.innerHTML = '';
    if(type=="day")
    {
        $('#rp_type').append('<p class="text-center">Date</p>');
    }else if(type=="hr")
    {
        $('#rp_type').append('<p class="text-center">Hour</p>');
    }else if(type=="summary")
    {
        $('#rp_type').append('<p class="text-center">#</p>');
    }
}



function init() {
    var element = getsubdeps(document.getElementById('deps').value);
}
</script>
<script>
//$.datetimepicker.setLocale('en');
$('.some_class').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
});

</script>
<script>
$('#queue_report').DataTable({
    "processing": true,
    "pageLength": 25,
    "scrollX": true,
});
</script>
@include('footer')

</body>

</html>