<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;
use DateTime;
use DatePeriod;
use DateInterval;


require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class AbndoncallbackController extends Controller{
  
    
    public function view_abncallback_report(){	
		
		if(Util::isAuthorized("view_abncall_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_abncall_report")=='DENIED'){
            return view('permissiondenide');
        }
		
        Util::log('Abandon Callback Details Report','View');
        
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getdndstatus  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();
      
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Abandon Callback Report Dashboard",$username,"View Abandon Callback Report");


        return view('view_abn_callback_report',compact('getdndstatus','get_com_data'));

	}

    public function search_abn_callback_report(Request $request){  
   
        $userid=session('userid');
        $getcomid = DB::SELECT("SELECT com_id FROM `user_master` WHERE `id` = $userid");
        $comid = $getcomid[0]->com_id;

        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();  
        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');

        $data = array();

        $data=DB::SELECT("SELECT DISTINCT 
                                    `tbl_calls_evnt`.`in_datetime`,
                                    `tbl_calls_evnt`.`cbstatus`,
                                    `tbl_calls_evnt`.`id`,
                                    `tbl_calls_evnt`.`linkedid` AS `rec_linkedid`,
                                     `tbl_calls_evnt`.`cre_datetime` AS incoming_date,
                                      `tbl_calls_evnt`.`frm_caller_num` AS incoming_num,
                                       `tbl_calls_evnt`.`did_num`,
                                        `csp_callhistory_detail`.`sipid` AS agnt_sipid,
                                        CONCAT(`user_master`.`fname`,' ',`user_master`.`lname`) AS `agnt_name`,
                                         `csp_callhistory_detail`.`callback_Status`, `csp_callhistory_detail`.`call_log`,
                                           `csp_callhistory_detail`.`callback_datetime`,
                                            `queues_config`.`extension`,
                                             `queues_config`.`descr`,
            (SELECT SEC_TO_TIME( ROUND( tbl_calls_evnt.ring_sec_count ) ) ) AS waiting_time,
            (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_two_prerem_id`) AS `cat_two_prerem_id`,
            (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_one_prerem_id`) AS `cat_one_prerem_id`,
            (SELECT status FROM `tbl_calls_evnt` WHERE `uniqueid` = `csp_callhistory_detail`.`callback_unq_id` ) AS `callbackStatus`,
            (SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName` FROM `csp_contact_master` WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`incoming_num`, -9, 9) LIMIT 1) AS `fullName`
            FROM `tbl_calls_evnt` 
            JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid`
            LEFT JOIN `csp_callhistory_detail` ON `tbl_calls_evnt`.`linkedid` = `csp_callhistory_detail`.unq_id  and `csp_callhistory_detail`.log_type = 'AbandonCallback'
            LEFT JOIN `user_master` ON `user_master`.`id` = `csp_callhistory_detail`.created_userid
            WHERE `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' AND '$to_date' AND `tbl_calls_evnt`.`desc` = 'ABANDON'  AND `queues_config`.`com_id`='$get_com_id->com_id' AND IF ('$queue_id' != 'All' , `tbl_calls_evnt`.`agnt_queueid` = '$queue_id', `tbl_calls_evnt`.`agnt_queueid`) GROUP BY `tbl_calls_evnt`.`cre_datetime`");



        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Abandon Callback Report",$username,"Search Abandon Callback Report");
        
        return compact('data',$data);
        
    }

public function updatecallback(){ 
        $id = $_GET['id'];
        // $orgstatus = $_GET['status'];
    
    $tablest  = DB::table('tbl_calls_evnt') 
        ->where('id', $id)
        ->first();
        $status=0;

        
        if($tablest->cbstatus == 0){
            $status=1;
        }else{
            $status=0;
        }

        DB::table('tbl_calls_evnt')
            ->where('id', $id)
            ->update(['cbstatus' => $status]);

         
    }



public function viewcallbackdata_for_mail(Request $request){  
   
        $userid=224;
        $getcomid = DB::SELECT("SELECT com_id FROM `user_master` WHERE `id` = $userid");
        $comid = $getcomid[0]->com_id;

        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();  
        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');

        $data = array();

        $data=DB::SELECT("SELECT DISTINCT 
                                    `tbl_calls_evnt`.`in_datetime`,
                                    `tbl_calls_evnt`.`cbstatus`,
                                    `tbl_calls_evnt`.`id`,
                                    `tbl_calls_evnt`.`linkedid` AS `rec_linkedid`,
                                     `tbl_calls_evnt`.`cre_datetime` AS incoming_date,
                                      `tbl_calls_evnt`.`frm_caller_num` AS incoming_num,
                                       `tbl_calls_evnt`.`did_num`,
                                        `csp_callhistory_detail`.`sipid` AS agnt_sipid,
                                        CONCAT(`user_master`.`fname`,' ',`user_master`.`lname`) AS `agnt_name`,
                                         `csp_callhistory_detail`.`callback_Status`, `csp_callhistory_detail`.`call_log`,
                                           `csp_callhistory_detail`.`callback_datetime`,
                                            `queues_config`.`extension`,
                                             `queues_config`.`descr`,
            (SELECT SEC_TO_TIME( ROUND( tbl_calls_evnt.ring_sec_count ) ) ) AS waiting_time,
            (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_two_prerem_id`) AS `cat_two_prerem_id`,
            (SELECT remark FROM `csp_preset_remark` WHERE id = `csp_callhistory_detail`.`cat_one_prerem_id`) AS `cat_one_prerem_id`,
            (SELECT status FROM `tbl_calls_evnt` WHERE `uniqueid` = `csp_callhistory_detail`.`callback_unq_id` ) AS `callbackStatus`,
            (SELECT CONCAT(`title`,' ',`firstname`,' ',`lastname`) AS `fullName` FROM `csp_contact_master` WHERE SUBSTRING(`primary_contact`, -9, 9) = SUBSTRING(`incoming_num`, -9, 9) LIMIT 1) AS `fullName`
            FROM `tbl_calls_evnt` 
            JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid`
            LEFT JOIN `csp_callhistory_detail` ON `tbl_calls_evnt`.`linkedid` = `csp_callhistory_detail`.unq_id  and `csp_callhistory_detail`.log_type = 'AbandonCallback'
            LEFT JOIN `user_master` ON `user_master`.`id` = `csp_callhistory_detail`.created_userid
            WHERE `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' AND '$to_date' AND `tbl_calls_evnt`.`desc` = 'ABANDON'  AND `queues_config`.`com_id`='$get_com_id->com_id' AND IF ('$queue_id' != 'All' , `tbl_calls_evnt`.`agnt_queueid` = '$queue_id', `tbl_calls_evnt`.`agnt_queueid`) GROUP BY `tbl_calls_evnt`.`cre_datetime`");



        return compact('data',$data);
        
    }


}