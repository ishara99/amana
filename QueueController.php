<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Util;
use Mockery\Expectation;

require app_path().'/Http/Helpers/helpers.php';
require app_path().'/../vendor/autoload.php';
class QueueController extends Controller
{
    public function view_queue_report(){	
		
		if(Util::isAuthorized("view_queue_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_queue_report")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log('Queue Report','View');
        $userid=session('userid');

        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getdndstatus  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Queue Report Dashboard",$username,"View Queue Report");

        return view('view_queuereport',compact('getdndstatus','get_com_data'));

    }
    
    public function view_queue_det_report(){	
		
		if(Util::isAuthorized("view_queue_det_report")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_queue_det_report")=='DENIED'){
            return view('permissiondenide');
        }
        Util::log('Queue Details Report','View');

        $userid=session('userid');
        
        $get_com_id  = DB::table('user_master')
                            ->where('id',$userid)
                            ->first();

        $getdndstatus  = DB::table('asterisk.queues_config')
                        ->select('extension','descr') 
                        ->Where('queues_config.com_id',$get_com_id->com_id)
                        ->get();
      
        $get_com_data  = DB::table('tbl_com_mst')->Where('id',$get_com_id->com_id)->get();

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Queue Details Report Dashboard",$username,"View Queue Details Report");


        return view('view_queue_det_report',compact('getdndstatus','get_com_data'));

	}

    public function search_queue_det_report(request $request){
		$userid=session('userid');
		$get_com_id  = DB::table('user_master')
				->where('id',$userid)
				->first();
		$queue_id= $request->input('queue_id');
		if($queue_id!="All"){
			$queue=$request->input('queue_id');
		}else{
			$queue='';
		}
		$to_date= $request->input('to_date');
		$frm_date= $request->input('frm_date');
        	$com_id_select= $request->input('com_id');
		if($com_id_select!="All"){
			$com_id=$com_id_select;
		}else{
			$com_id=$get_com_id->com_id;
                }
		$sl = 7;
		$abn_sl = 5;
		$report_type= $request->input('report_type');

		if($report_type=="summary")
		{
		    $deleteold_rec  =  DB::table('tbl_queue_summary_report')
                            ->where('userid', $userid)
                            ->delete();


		               DB::statement("call queue_repo_sum('$userid','$com_id','$to_date','$frm_date')");
			       
			$data=DB::table('tbl_queue_summary_report')
                            ->where('userid', $userid)
                            ->get();
			return compact('data',$data);
		   
		}else if($report_type=="day")
		{
			 $deleteold_rec  =  DB::table('tbl_queue_day_report')
                            ->where('userid', $userid)
                            ->delete();

		               DB::statement("call queue_repo_day('$userid','$com_id','$to_date','$frm_date')");
			       
			$data=DB::table('tbl_queue_day_report')
                            ->where('userid', $userid)
                            ->get();
			return compact('data',$data);

			       
		}else if($report_type=="hr")
		{
//exit();
		    $deleteold_rec  =  DB::table('tbl_queue_hrly_report')
                            ->where('userid', $userid)
                            ->delete();

		               DB::statement("call queue_repo_hourly('$userid','$com_id','$to_date','$frm_date')");
			       
			$data=DB::table('tbl_queue_hrly_report')
                            ->where('userid', $userid)
                            ->get();
            
            $ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,"User Search Queue Details Report",$username," Search Queue Details Report");
                
			return compact('data',$data);

		}
		
	}

        public function search_queue_det_report_sql(request $request){
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                ->where('id',$userid)
                ->first();
        $queue_id= $request->input('queue_id');
        if($queue_id!="All"){
            $queue=$request->input('queue_id');
        }else{
            $queue='';
        }
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
            $com_id_select= $request->input('com_id');
        if($com_id_select!="All"){
            $com_id=$com_id_select;
        }else{
            $com_id=$get_com_id->com_id;
                }
        $sl = 7;
        $abn_sl = 5;
        $report_type= $request->input('report_type');

        if($report_type=="summary")
        {
            $deleteold_rec  =  DB::table('tbl_queue_summary_report')
                            ->where('userid', $userid)
                            ->delete();


                       // DB::statement("call queue_repo_sum('$userid','$com_id','$to_date','$frm_date')");
                   
            
                            $data=DB::SELECT("SELECT 
                                                    `tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
                                                    `tbl_calls_evnt`.`date` AS cur_date,
                                                    `tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
                                                    HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
                                                    `queues_config`.`extension`,
                                                    `queues_config`.`descr`,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS offerd_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'  
                                                    ) AS offerd_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS answer_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                                                        AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' OR tbl_calls_evnt.DESC = 'CONNECT' ) 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'  
                                                    ) AS answer_calls,

                                                                                                        (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS q_breakout_calls
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.desc = 'EXITWITHKEY' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'
                                                    ) AS q_breakout_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS ans_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'
                                                    ) AS ans_calls,
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'  
                                                    ) AS answer_times,
                                                    
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                    ) AS answer_acwtimes,
                                                    
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                                SUM(
                                                                tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                    ) AS tot_ht,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND tbl_calls_evnt.ring_sec_count < (SELECT data FROM `phonikip_db`.`func_data` where type='x') 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'
                                                    ) AS answer_call_sl,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'ABANDON' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'  
                                                    ) AS tot_abn_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'ABANDON' 
                                                        AND tbl_calls_evnt.ring_sec_count < (SELECT data FROM `phonikip_db`.`func_data` where type='y')
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                    ) AS tot_abn_calls_sl,
                                                    
                                                    
                                                    
                                                    
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'
                                                    ) AS hold_calls,
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'  
                                                    ) AS hold_time,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS transout_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'BlindTransfer' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                    ) AS transout_calls,
                                                    `tbl_calls_evnt`.`date` AS `date_or_hour`,
                                                    (SELECT
                                                        SEC_TO_TIME(
                                                        ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                    ) AS ring_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'
                                                    ) AS answer_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'
                                                    ) AS acw_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                    ) AS hold_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'  
                                                    ) AS ring_sec_count
                                                    FROM
                                                    `tbl_calls_evnt`
                                                    INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
                                                WHERE
                                                    `queues_config`.`com_id` = '$com_id' 
                                                    AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date' 
                                                GROUP BY
                                                    `tbl_calls_evnt`.`agnt_queueid` ;");


            return compact('data',$data);
           
        }else if($report_type=="day")
        {
             $deleteold_rec  =  DB::table('tbl_queue_day_report')
                            ->where('userid', $userid)
                            ->delete();

                       // DB::statement("call queue_repo_day('$userid','$com_id','$to_date','$frm_date')");
                   
            $data=DB::SELECT("SELECT 
                                                    `tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
                                                    `tbl_calls_evnt`.`date` AS cur_date,
                                                    `tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
                                                    HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
                                                    `queues_config`.`extension`,
                                                    `queues_config`.`descr`,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS offerd_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS offerd_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS answer_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                                                        AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' OR tbl_calls_evnt.DESC = 'CONNECT') 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS answer_calls,

                                                                                                        (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS q_brakout_calls
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.desc = 'EXITWITHKEY' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS q_breakout_calls,

                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS ans_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS ans_calls,
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS answer_times,
                                                    
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS answer_acwtimes,
                                                    
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                                SUM(
                                                                tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS tot_ht,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND tbl_calls_evnt.ring_sec_count < (SELECT data FROM `phonikip_db`.`func_data` where type='x')
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS answer_call_sl,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'ABANDON' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS tot_abn_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'ABANDON' 
                                                        AND tbl_calls_evnt.ring_sec_count < (SELECT data FROM `phonikip_db`.`func_data` where type='y') 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS tot_abn_calls_sl,
                                                    
                                                    
                                                    
                                                    
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS hold_calls,
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS hold_time,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS transout_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'BlindTransfer' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS transout_calls,
                                                    `tbl_calls_evnt`.`date` AS `date_or_hour`,
                                                    (SELECT
                                                        SEC_TO_TIME(
                                                        ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS ring_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS answer_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS acw_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS hold_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND ( tbl_calls_evnt.date = cur_date ) 
                                                    GROUP BY
                                                        tbl_calls_evnt.date 
                                                    ) AS ring_sec_count
                                                    FROM
                                                    `tbl_calls_evnt`
                                                    INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
                                                WHERE
                                                    `queues_config`.`com_id` = '$com_id' 
                                                    AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'   
                                                GROUP BY
                                                    `tbl_calls_evnt`.`date`,
                                                    `tbl_calls_evnt`.`agnt_queueid`;");

            return compact('data',$data);

                   
        }else if($report_type=="hr")
        {
//exit();
           $deleteold_rec  =  DB::table('tbl_queue_hrly_report')
                            ->where('userid', $userid)
                            ->delete();

                       for ($hour=0; $hour < 24; $hour++) { 

                       $data1=DB::SELECT("SELECT 
                                                    `tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
                                                    `tbl_calls_evnt`.`date` AS cur_date,
                                                    `tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
                                                    HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
                                                    `queues_config`.`extension`,
                                                    `queues_config`.`descr`,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS offerd_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date'
                                                    AND '$to_date'))) AS offerd_calls,

                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.ring_sec_count ))) AS tot_ring_times
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                     
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date'
                                                    AND '$to_date'))) AS tot_ring_times,

                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.ring_sec_count ))) AS tot_ring_times
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                     
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date'
                                                    AND '$to_date'))) AS tot_ring_times,

                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS q_breakout_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.desc = 'EXITWITHKEY' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date'
                                                    AND '$to_date'))) AS q_breakout_calls,
                                              
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS answer_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
                                                        AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
                                                        AND (
                                                    HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                                  AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS answer_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS ans_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS ans_calls,
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date'
                                                    AND '$to_date'))) AS answer_times,
                                                    
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS answer_acwtimes,
                                                    
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                                SUM(
                                                                tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS tot_ht,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND tbl_calls_evnt.ring_sec_count <= 7 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS answer_call_sl,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'ABANDON' 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS tot_abn_calls,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'ABANDON' 
                                                        AND tbl_calls_evnt.ring_sec_count <= 5 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS tot_abn_calls_sl,
                                                    
                                                    
                                                    
                                                    
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS hold_calls,
                                                    (
                                                    SELECT
                                                        SEC_TO_TIME(
                                                            ROUND(
                                                            SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour'
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS hold_time,
                                                    (
                                                    SELECT
                                                        COUNT( tbl_calls_evnt.id ) AS transout_calls 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.DESC = 'BlindTransfer' 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date')) AS transout_calls,
                                                    '$hour' as hours,
                                                    (SELECT
                                                        SEC_TO_TIME(
                                                        ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS ring_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS answer_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                    AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS acw_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
                                                    FROM
                                                        tbl_calls_evnt
                                                        INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND tbl_calls_evnt.STATUS = 'ANSWER' 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date'))) AS hold_sec_count,
                                                    (
                                                    SELECT
                                                        ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
                                                    FROM
                                                        tbl_calls_evnt 
                                                    WHERE
                                                        tbl_calls_evnt.agnt_queueid = extension 
                                                        AND (
                                                HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' 
                                            AND `tbl_calls_evnt`.`cre_datetime` BETWEEN '$frm_date' 
                                                    AND '$to_date')) AS ring_sec_count_new
                                                    FROM
                                                    `tbl_calls_evnt`
                                                    INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
                                                WHERE
                                                `queues_config`.`com_id` = '$com_id' 
                                                    AND
                                        (
                                            `tbl_calls_evnt`.`date` between '$frm_date' and '$to_date' AND 
                                            HOUR ( tbl_calls_evnt.cre_datetime ) = '$hour' AND tbl_calls_evnt.agnt_queueid = extension 
                                        ) GROUP BY tbl_calls_evnt.agnt_queueid;");

            foreach ($data1 as $val) {
                $data[] = [
                        'date_or_hour' => $val->hours,
                        'extension' => $val->extension,
                        'descr' => $val->descr,
                        'offerd_calls' => $val->offerd_calls,
                        'answer_calls' => $val->answer_calls,
                        'q_breakout_calls' => $val->q_breakout_calls,
                        'tot_ring_times' => $val->tot_ring_times,
                        'ring_sec_count' => $val->ring_sec_count,
                        'answer_calls' => $val->answer_calls,
                        'answer_times' => $val->answer_times,
                        'answer_sec_count' => $val->answer_sec_count,
                        'answer_calls' => $val->answer_calls,
                        'answer_acwtimes' => $val->answer_acwtimes,
                        'acw_sec_count' => $val->acw_sec_count,
                        'tot_ht' => $val->tot_ht,
                        'answer_call_sl' => $val->answer_call_sl,
                        'tot_abn_calls' => $val->tot_abn_calls,
                        'tot_abn_calls_sl' => $val->tot_abn_calls_sl,
                        'hold_calls' => $val->hold_calls,
                        'hold_time' =>$val->hold_time,
                        'hold_sec_count' => $val->hold_sec_count,
                        'transout_calls' => $val->transout_calls,
                        'ring_sec_count_new' => $val->ring_sec_count_new,
                    ];
            }
        }


            
            $ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,"User Search Queue Details Report",$username," Search Queue Details Report");
                
            return compact('data');

        }
        
    }

    public function search_queue_det_report_original(Request $request){   
   
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();
        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $sl = 7;
        $abn_sl = 5;
        $report_type= $request->input('report_type');

        if($report_type=="summary")
        {
            $rep_type_condition = "and (tbl_calls_evnt.cre_datetime between '$frm_date' and '$to_date')";
            $date_or_hour = DB::raw('ROW_NUMBER() OVER(ORDER BY ID DESC) AS date_or_hour');
        }else if($report_type=="day")
        {
            $rep_type_condition = 'AND (tbl_calls_evnt.date = cur_date) group by tbl_calls_evnt.date';
            $date_or_hour = "tbl_calls_evnt.date as date_or_hour";
        }else if($report_type=="hr")
        {
            $rep_type_condition = 'AND (HOUR(tbl_calls_evnt.cre_datetime) = hour AND (tbl_calls_evnt.date = cur_date))';
            $date_or_hour =  DB::raw('HOUR(tbl_calls_evnt.cre_datetime) as date_or_hour');
        }
        // $frm_date_ = date('Y-m-d',strtotime($frm_date));
        // $to_date_ = date('Y-m-d',strtotime($to_date));

        // $get_date_range = DB::select("select * from 
        //             (select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) selected_date from
        //             (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
        //             (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
        //             (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
        //             (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
        //             (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
        //             where selected_date between '$frm_date_' and '$to_date_'");


        if($report_type=="hr")
        {

           
            $deleteold_rec  =  DB::table('tbl_queue_hrly_report')
                            ->where('userid', $userid)
                            ->delete();

            $getextensions  = DB::table('asterisk.queues_config')
                            ->select('extension','descr') 
                            ->get();

            foreach($getextensions as $value) {
                DB::statement('call queue_hourly(?, ?, ?, ?, ?)',[$frm_date,$to_date,$userid,$value->extension,$get_com_id->com_id]);
            }
           

            $data = $dataq = DB::table('tbl_queue_hrly_report')
                            ->select('tbl_queue_hrly_report.*') 
                            ->where('tbl_queue_hrly_report.userid',$userid);
            if($queue_id!="All")
            {
                $data = $dataq->Where('tbl_queue_hrly_report.extension',$queue_id); 
            }
            $data = $dataq->get();



        }else
        {

            $data = $dataq = DB::table('tbl_calls_evnt')
                    ->select(
                    
    
                        'tbl_calls_evnt.agnt_queueid as queueid',
                        'tbl_calls_evnt.date',
                        'tbl_calls_evnt.cre_datetime as cur_datetime',
                        DB::raw('HOUR(tbl_calls_evnt.cre_datetime) as hour'),
                        'tbl_calls_evnt.date as cur_date',
                        'queues_config.extension',
                        'queues_config.descr',
                        DB::raw("(select COUNT(tbl_calls_evnt.id) AS offerd_calls from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status = 'ENTERQUEUE'
                                    $rep_type_condition) as offerd_calls"),

                        DB::raw("(select COUNT(tbl_calls_evnt.id) AS answer_calls from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.STATUS = 'ENTERQUEUE'AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' )
                                    $rep_type_condition) as answer_calls"),

                        DB::raw("(select COUNT(tbl_calls_evnt.id) AS ans_calls from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as ans_calls"),

                        DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.answer_sec_count))) AS answer_times from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as answer_times"),
                        
                        DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.answer_sec_count)/answer_calls)) AS avg_answer_times from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as avg_answer_times"),

                        DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.acw_sec_count))) AS answer_acwtimes from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as answer_acwtimes"),
                        
                        DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.acw_sec_count)/answer_calls)) AS avg_answer_acwtimes from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as avg_answer_acwtimes"),

                        DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.answer_sec_count + IFNULL(tbl_calls_evnt.acw_sec_count,0)))) AS tot_ht from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as tot_ht"),
                
                        DB::raw("(select COUNT(tbl_calls_evnt.id) AS answer_call_sl from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status = 'ANSWER' and tbl_calls_evnt.ring_sec_count <= $sl
                                    $rep_type_condition) as answer_call_sl"),

                        DB::raw("(select COUNT(tbl_calls_evnt.id) AS tot_abn_calls from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.desc = 'ABANDON'  
                                    $rep_type_condition) as tot_abn_calls"),

                        DB::raw("(select COUNT(tbl_calls_evnt.id) AS tot_abn_calls_sl from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.desc = 'ABANDON' and tbl_calls_evnt.ring_sec_count <= $abn_sl
                                        $rep_type_condition) as tot_abn_calls_sl"),

                        DB::raw("(select SUM(answer_call_sl/(offerd_calls - tot_abn_calls_sl)*100) )as service_level"),
                        DB::raw("(select SUM((tot_abn_calls-tot_abn_calls_sl)/(offerd_calls-tot_abn_calls_sl)*100) )as tot_abn_rate_sl"),
                        DB::raw("(select SUM((tot_abn_calls)/(offerd_calls)*100) )as tot_abn_rate"),
                        DB::raw("(select SUM((tot_abn_calls-tot_abn_calls_sl)/(offerd_calls)*100) )as tot_abn_rate_outofsl"),

                        DB::raw("(SELECT SEC_TO_TIME(ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count))) as hold_time FROM tbl_calls_evnt 
                                INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                $rep_type_condition) as hold_time"),

                        DB::raw("(SELECT COUNT(tbl_calls_hold_evnts.id) as hold_calls FROM tbl_calls_evnt 
                                    INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as hold_calls"),

                        DB::raw("(SELECT SEC_TO_TIME(ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count)/hold_calls)) as avg_hold_time FROM tbl_calls_evnt 
                                    INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.status  = 'ANSWER'
                                    $rep_type_condition) as avg_hold_time"),

                        DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.ring_sec_count)/answer_calls)) AS ASA from tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid 
                                    $rep_type_condition) as ASA"),

                        
                        DB::raw("(SELECT COUNT(tbl_calls_evnt.id) AS transout_calls FROM tbl_calls_evnt 
                                    where tbl_calls_evnt.agnt_queueid  = queueid and tbl_calls_evnt.desc  = 'BlindTransfer'
                                    $rep_type_condition) as transout_calls"),

                        $date_or_hour
                                    )
                    ->join('asterisk.queues_config','queues_config.extension','=','tbl_calls_evnt.agnt_queueid')
                    ->Where('queues_config.com_id',$get_com_id->com_id)
                    //->leftjoin('phonikip_db.tbl_calls_hold_evnts','tbl_calls_hold_evnts.linkedid','=','tbl_calls_evnt.linkedid')
                    ->whereBetween('tbl_calls_evnt.cre_datetime', array($frm_date, $to_date));

                    if($queue_id!="All")
                    {
                        $data = $dataq->Where('tbl_calls_evnt.agnt_queueid',$queue_id); 
                    }
                
                    if($report_type=="day")
                    {
                        $data = $dataq->groupby('tbl_calls_evnt.date');                    
                    }
                    $data = $dataq->groupby('tbl_calls_evnt.agnt_queueid');   
                    $data = $dataq->get(); 
                    
        }    
                //dd($data);
                
        return compact('data',$data);
        
    }

    public function search_queuereport(Request $request)
    {  
   
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();

        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');

if($queue_id!='All'){
    $get_queues  = DB::select("SELECT * 
        from asterisk.queues_config 
        where queues_config.extension  = '$queue_id'");
}else{
    $get_queues  = DB::select("SELECT * 
        from asterisk.queues_config 
        where queues_config.com_id  = '$get_com_id->com_id'");
}


$current = strtotime($frm_date);
$last = strtotime($to_date);

while( $current <= $last ) {

     $dates[] = date("Y-m-d", $current);
     $current = strtotime("+1 day", $current);
}

foreach ($dates as $key0 => $value0) {
            
$cur_date=$value0;

        foreach ($get_queues as $key => $val) {

        $queue=$val->extension;    
        $queueInfo=$val->extension."-".$val->descr;

        $get_acd_calls  = DB::select("select COUNT(tbl_calls_evnt.id) AS acd_calls 
            from tbl_calls_evnt 
            where tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
            AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' OR tbl_calls_evnt.DESC = 'CONNECT' ) 
            AND (tbl_calls_evnt.date = '$cur_date') 
            AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_acd_calls){
            $acd_calls=$get_acd_calls[0]->acd_calls;
          }else{
            $acd_calls=0;
          }

        $get_q_breakout_calls  = DB::select("select COUNT(tbl_calls_evnt.id) AS q_breakout_calls 
            from tbl_calls_evnt 
            where tbl_calls_evnt.desc  = 'EXITWITHKEY'
            AND (tbl_calls_evnt.date = '$cur_date') 
            AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_q_breakout_calls){
            $q_breakout_calls=$get_q_breakout_calls[0]->q_breakout_calls;
          }else{
            $q_breakout_calls=0;
          }  

    if($q_breakout_calls!=0 || $acd_calls!=0){

        $get_acd_times  = DB::select("select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.answer_sec_count))) AS acd_times,
            SUM(tbl_calls_evnt.answer_sec_count) AS answer_sec_count 
            from tbl_calls_evnt 
            where tbl_calls_evnt.status  = 'ANSWER'
            AND (tbl_calls_evnt.date = '$cur_date') 
            AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_acd_times){
            $acd_times=$get_acd_times[0]->acd_times;
            $answer_sec_count=$get_acd_times[0]->answer_sec_count;
          }else{
            $acd_times=gmdate("H:i:s",0);
            $answer_sec_count=0;
          }

          if($acd_calls!=0){
            $avg_acd_times=gmdate("H:i:s",($answer_sec_count/$acd_calls));
          }else{
            $avg_acd_times=gmdate("H:i:s",0);
          }

         $get_acw_times  = DB::select("select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.acw_sec_count))) AS acw_times,
            SUM(tbl_calls_evnt.acw_sec_count) AS acw_sec_count 
            from tbl_calls_evnt 
            where tbl_calls_evnt.status  = 'ANSWER'
            AND (tbl_calls_evnt.date = '$cur_date') 
            AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_acw_times){
            $acw_times=$get_acw_times[0]->acw_times;
            $acw_sec_count=$get_acw_times[0]->acw_sec_count;
          }else{
            $acw_times=gmdate("H:i:s",0);
            $acw_sec_count=0;
          }

          if($acd_calls!=0){
            $avg_acw_times=gmdate("H:i:s",($acw_sec_count/$acd_calls));
          }else{
            $avg_acw_times=gmdate("H:i:s",0);
          } 


         $get_transout_calls  = DB::select("select COUNT(tbl_calls_evnt.id) AS transout_calls 
            from tbl_calls_evnt 
            where tbl_calls_evnt.desc  = 'BlindTransfer'
            AND (tbl_calls_evnt.date = '$cur_date') 
            AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_transout_calls){
            $transout_calls=$get_transout_calls[0]->transout_calls;
          }else{
            $transout_calls=0;
          }

         $get_hold_calls  = DB::select("select 
            COUNT(tbl_calls_hold_evnts.id) as hold_calls 
            FROM tbl_calls_evnt 
            INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
            where tbl_calls_evnt.status  = 'ANSWER'
            AND (tbl_calls_evnt.date = '$cur_date') AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_hold_calls){
            $hold_calls=$get_hold_calls[0]->hold_calls;
          }else{
            $hold_calls=0;
          }

          $get_avg_hold_time  = DB::select("select 
            SEC_TO_TIME(ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count)/$hold_calls)) as avg_hold_time 
            FROM tbl_calls_evnt 
            INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
            where tbl_calls_evnt.status  = 'ANSWER'
            AND (tbl_calls_evnt.date = '$cur_date') 
            AND ( tbl_calls_evnt.agnt_queueid = '$queue' )");
          
          if($get_avg_hold_time){
            $avg_hold_time=$get_avg_hold_time[0]->avg_hold_time;
          }else{
            $avg_hold_time=gmdate("H:i:s",0);
          }

          
            $data[] = [
                  'cur_date' => $cur_date,
                  'queueInfo' => $queueInfo,
                  'acd_calls' => $acd_calls,
                  'q_breakout_calls' => $q_breakout_calls,
                  'acd_times' => $acd_times,
                  'avg_acd_times' => $avg_acd_times,
                  'acw_times' => $acw_times,
                  'avg_acw_times' => $avg_acw_times,
                  'transout_calls' => $transout_calls,
                  'hold_calls' => $hold_calls,
                  'avg_hold_time' => $avg_hold_time,
                 
              ]; 
          }

    }

}

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Queue Report",$username,"Search Queue Report");
        
return compact('data');

    }

public function search_queuereport_old(Request $request)
    {  
   
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();

        $queue_id= $request->input('queue_id');
        $to_date= $request->input('to_date');
        $frm_date= $request->input('frm_date');
        $com_id= $request->input('com_id');


        if($queue_id!="All")
        {
            $queue_condition = " tbl_calls_evnt.agnt_queueid  = $queue_id and";
        
        }else
        {
            $queue_condition ="";
        }

        $data = $dataq = DB::table('tbl_calls_evnt')
                ->select(
            'tbl_com_mst.com_name', 
                    'tbl_calls_evnt.date as cur_date',
            DB::raw('CONCAT_WS("-",tbl_calls_evnt.agnt_queueid,asterisk.queues_config.descr) AS queueInfo'),
                    'tbl_calls_evnt.agnt_queueid AS queue',
                    DB::raw("(select COUNT(tbl_calls_evnt.id) AS acd_calls from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as acd_calls"),

                    DB::raw("(select COUNT(tbl_calls_evnt.id) AS q_breakout_calls from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.desc  = 'EXITWITHKEY'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as q_breakout_calls"),
                    
                    DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.answer_sec_count))) AS acd_times from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as acd_times"),
                    
                    DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.answer_sec_count)/acd_calls)) AS avg_acd_times from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as avg_acd_times"),

                    DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.acw_sec_count))) AS acw_times from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as acw_times"),

                    DB::raw("(select SEC_TO_TIME(ROUND(SUM(tbl_calls_evnt.acw_sec_count)/acd_calls)) AS avg_acw_times from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as avg_acw_times"),

                    DB::raw("(SELECT COUNT(tbl_calls_hold_evnts.id) as hold_calls FROM tbl_calls_evnt 
                                INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue )
                                GROUP BY tbl_calls_evnt.date) as hold_calls"),

                    DB::raw("(SELECT SEC_TO_TIME(ROUND(SUM(tbl_calls_hold_evnts.hold_sec_count)/hold_calls)) as avg_hold_time FROM tbl_calls_evnt 
                                INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
                                where $queue_condition tbl_calls_evnt.status  = 'ANSWER'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue )
                                GROUP BY tbl_calls_evnt.date) as avg_hold_time"),

                    DB::raw("(select COUNT(tbl_calls_evnt.id) AS transout_calls from tbl_calls_evnt 
                                where $queue_condition tbl_calls_evnt.desc  = 'BlindTransfer'
                                AND (tbl_calls_evnt.date = cur_date) AND ( tbl_calls_evnt.agnt_queueid = queue ) group by tbl_calls_evnt.date) as transout_calls")
                                )
                //->join('user_master','user_master.id','=','tbl_agnt_evnt.agnt_userid')
                ->join('asterisk.queues_config','queues_config.extension','=','tbl_calls_evnt.agnt_queueid')
        ->join('tbl_com_mst','tbl_com_mst.id','=','queues_config.com_id');
                //->Where('queues_config.com_id',$get_com_id->com_id)

        $userid=session('userid');
        $getcomid = DB::SELECT("SELECT com_id FROM `user_master` WHERE `id` = $userid");
        $comid = $getcomid[0]->com_id;
        
        $get_com_id  = DB::table('user_master')
                        ->where('id',$userid)
                        ->first();

        if($com_id!="All")
                {

                    $data = $dataq->Where('queues_config.com_id',$com_id);
        }else{
            $data = $dataq->Where('queues_config.com_id',$get_com_id->com_id);
        }
                $data = $dataq->whereBetween('tbl_calls_evnt.date', array($frm_date, $to_date));
                if($queue_id!="All")
                {
                    $data = $dataq->Where('tbl_calls_evnt.agnt_queueid',$queue_id); 
                }
                
                $data = $dataq->groupby('tbl_calls_evnt.date','tbl_calls_evnt.agnt_queueid')        
                ->get();      
        
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Search Queue Report",$username,"Search Queue Report");
    
                
        return compact('data',$data);
        
    }
    
 	public function getQeueData(){
		$com_id=$_GET['com_id'];
		$data =DB::SELECT('SELECT * FROM asterisk.queues_config WHERE com_id='. $com_id);
		return compact('data');
	
    	}

}