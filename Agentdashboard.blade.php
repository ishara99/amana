@include('header_new')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!--<section class="content-header">
        <h1 class="form_caption" style="font-size:25px; text-align:center; margin-top: 14px; margin-bottom: 14px;">Softphone Management Console</h1>
    </section>-->
    <!-- Main content -->
    <!-- ./col -->
    <input type="hidden" id="token" value="{{ csrf_token() }}">
</div>
<!-- /.content-wrapper -->
<div class="wrapper">	
<section class="content_header_ex">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin-bottom:-25px;padding-botton:-10px;">
		<h1 class="form_caption_ex">Softphone Management Console</h1>
</div>
	
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-bottom:-25px;padding-botton:-10px;">
	<!-- <iframe  src="http://dpmccs.dpg.lk:5000/AgentStatus/{{session()->get('endpoint')}}" style="width:600px; height:65px;border:none; margin-top:20px; float:right;" scrolling="no"></iframe> --></div>
		<!-- <iframe  src="http://dpmccs.dpg.lk:3000/AgentStatus/2606" style="width:700px; height:65px;border:none; margin-top:20px; float:right;" scrolling="no"></iframe></div> -->
</div>
        <!-- //<h1 class="form_caption_ex">Softphone Management Console</h1> -->
		@if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (\Session::has('flash_message'))

            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<input type="hidden" id="sessionMessage" data-initial-value="" name="sessionMessage" value="{!! session('flash_message') !!}">
			{{ Session::forget('flash_message') }}
            @endif		

            @if ($errors->any())
                <div class="alert alert-danger">

                    <ul>

                        @foreach ($errors->all() as $error)

                        <li>{{ $error }}</li>

                        @endforeach

                    </ul>

                </div>
            @endif
			
</section>
<section>
<div class="container-fluid">
<!--Left menu -->
<div class="col-lg-2 col-md-2  col-sm-2  col-xs-12 menu_left_bar">
<div class="panelc-group" id="accordionMenu" role="tablist" aria-multiselectable="true">
 
    <div class="panelc panelc-default">
      <div class="panelc-heading panelcone_bg" role="tab" id="headingone">
        <h4 class="panelc-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
         	Conferrence
        </a>
      </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse in " role="tabpanel" aria-labelledby="headingone">
        <div class="panelc-body panelcone_bg">
          <ul class="nav">
            <li><button onclick="Conf_call()" id="s_conf" class="left_button"><span class="glyphicon glyphicon-ok"></span></button></li>
            <li><button onclick="Add_conf()" id="a_conf" class="left_button"><span class="glyphicon glyphicon-plus"></span></button></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panelc panelc-default">
      <div class="panelc-heading panelctwo_bg" role="tab" id="headingtwo">
        <h4 class="panelc-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			Transfer
        </a>
      </h4>
      </div>
      <div id="collapseThree" class="panelc-collapse collapse in" role="tabpanel" aria-labelledby="headingtwo">
        <div class="panelc-body panelctwo_bg">
          <ul class="nav">
            <li><button onclick="Blind_transfer()" id="blind_tra" class="left_button">Blind</button></li>
            <li><button onclick="Attended_transfer()" id="attend_tra" class="left_button">Attend</button></li>

          </ul>
        </div>
      </div>
    </div>
    <div class="panelc panelc-default">
      <div class="panelc-heading panelcthree_bg" role="tab" id="headingthree">
        <h4 class="panelc-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
    		Supervisor
        </a>
      </h4>
      </div>
      <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingthree">
        <div class="panelc-body panelcthree_bg">
          <ul class="nav">
            <li><button onclick="chanspy_channel('from-spy', 'Call Spy')" id="spy" class="left_button">Spy</button></li>
			<li><button onclick="chanspy_channel('from-whisper', 'Call Whisper')" id="whisper" class="left_button">Whisper</button></li>
			<li><button onclick="chanspy_channel('from-barge', 'Call Barge')" id="barge" class="left_button">Barge</button></li>
          </ul>
        </div>
      </div>
	</div>
 <div><button onclick="abn_det_popup();" id="" type="button" class="btn  btn-lg btn-block detail_sub__btn detail_sub__btnone_over calllist_btn">Call Back List</button></div>
 <!-- <div><button onclick="statusModel()" id="" type="button" style="margin-top: 1px; font-size: inherit;" class="btn  btn-lg btn-block detail_sub__btn detail_sub__btnone_over calllist_btn">Agent Profile</button></div> -->

    
  </div>
</div>
<!--Left menu -->
<div class="col-lg-10 col-md-10  col-sm-10  col-xs-12">
<input type="hidden" name="agtstatus" id="agtstatus" value="<?php if (!empty($getdndstatus)) {
    echo $getdndstatus->status;
} ?>" />
<input type="hidden" name="incoming_num" id="incoming_num" value="{{session()->get('CallerIDNum')}}" />
<input type="hidden" name="linkid" id="linkid" value="{{session()->get('linkedid')}}" />
<input type="hidden" name="dial_dtmf_num" id="dial_dtmf_num" value="" />
<input type="hidden" name="usertypeid" id="usertypeid" value="{{session()->get('usertypeid')}}" />
<input type="hidden" name="incoming_linkedid" id="incoming_linkedid" value="{{$linkedid}}" />

<div class="form-inline">

		    <div class="form-group">
			<label class="paddinglf lablesty" style="margin-top: 19px; margin-right: 5px;">CLI</label>
		    </div>
		    <div class="form-group">
			<input type="text" class="form-control" name="dial_num" id="dial_num" placeholder="" size="20" style="margin-top:24px; border: 1px solid #565555 !important; height:32px; font-size:14px;">
			<div id="div_numberpad"></div>	
			</div>
			

				<div id="" class="form-group">
				    <button title="Dial" onclick="Dial_call()" id="dial" class="dial_button" style="margin-top:15px;" ></button>
				</div>

				
				<div id="" class="form-group">
				    <button title="Dial Pad" id="dial_pad" class="dialpad_button" style="margin-top:15px;" ></button>
				</div>
				

				<div id="" class="form-group">
					<button  title="Phone Book" id=""  onclick="ph_bk_popup();" class="phonebook_button" style="margin-top:15px;" ></button>
				</div>

				<?php if (!empty($getdndstatus)) {
				if ($getdndstatus->status != 'Outbound') {
					?>
					<div id="" class="form-group">
						<button title="Outbound Mode" onclick="obmode_status();" id="ob_mode_on" type="button" class="obmode_button" style="margin-top:15px;" ></button>
					</div>
				<?php } else { ?>
					<div id="" class="form-group">
						<button title="inbound Mode" onclick="obmode_status();" id="ob_mode_off" type="button" class="obmode_button_op" style="margin-top:15px;" ></button>
					</div>
					<?php }
				}
				?>

				<?php if (!empty($getdndstatus)) {
				if ($getdndstatus->status == "Break") {
					?>
					<div id="" class="form-group">
						<button title="Break" onclick="changebreakstatus()" id="start_break" type="button" class="timeout_button_op"  style="margin-top:12px;"></button>	
					</div>
				<?php } else { ?>
					<div id="" class="form-group">
						<button  title="Break" onclick="changebreakstatus()" id="start_break" type="button" class="timeout_button"  style="margin-top:12px;" >></button>		
					</div>
				<?php }
				}
				?>
		

				<?php if (!empty($getdndstatus)) {
					if ($getdndstatus->status == 'ACW') {
						?>
						<div id="" class="form-group">
							<button  title="ACW" onclick="end_acw()"  id="end_acw" type="button" class="acw_button_op"  style="margin-top:15px;" ></button>
						</div>
					<?php } else { ?>
						<div id="" class="form-group">
							<button title="ACW"   onclick="start_acw()"  id="start_acw" type="button" class="acw_button"  style="margin-top:15px;" ></button>
						</div>
							<?php }
						}
					?>
			

				
				<?php if (!empty($getdndstatus)) {
				if ($getdndstatus->status == 'Online' || $getdndstatus->status == 'Break' || $getdndstatus->status == 'ACW' || $getdndstatus->status == 'In Call' || $getdndstatus->status == 'Out Call' || $getdndstatus->status == 'Ringing' || $getdndstatus->status == 'Outbound' || $getdndstatus->status == 'Dialing') {
					?>
					<div id="" class="form-group" >
						<button  title="On" type="button detailbtn" class="offline_button" onclick="disply_offline_reason();" id="offline"  style="margin-top:15px;" ></button>
					</div>
				<?php } else if ($getdndstatus->status == 'Offline') { ?>
					<div id="" class="form-group">
						<button  title="Off" onclick="dnd_off();" id="online" type="button detailbtn" class="online_button"  style="margin-top:15px;" ></button>
					</div>
				<?php }
				}
				?>
				
				<div id="" class="form-group queue_info">
				
					<input type="text" class="form-control form_control_ex" name="queInfo" id="queInfo" placeholder="Queue Info " size="40" style=" margin-top: 24px; background-color: #f7f3f3; 
					border: 1px solid #fff !important; font-size:14px;">

				</div>

				<div id="" class="form-group">
					<button  id="agtstatus_div" type="button" class="status_button">
					<?php
					if (!empty($getdndstatus)) 
					{
						if ($getdndstatus->status == 'Offline') 
						{
					    	echo 'Offline';
						}
						else if($outbound_st=="Outbound On")
						{
							echo 'Outbound';
						}
						else if ($getdndstatus->status == 'In Call' || $getdndstatus->status == 'Out Call' || $getdndstatus->status == 'Online' || $getdndstatus->status == 'Ringing' || $getdndstatus->status == 'Busy' || $getdndstatus->status == 'Dialing') 
						{
					    	echo 'Online';
						}
						else
						{
					    	echo $getdndstatus->status. "-".$getdndstatus->status_des;
						}
	             	}
				    ?></button>
				</div>

				<div id="" class="form-group">
						<button id="" type="button" class="fom_button">{{session()->get('endpoint')}}</button>
				</div>

			</div>
			
				<div id="csp_div" >
				</div>
			
			<div id="caller_div">

			</div>
</div>
</div>
</section>

<!--End New Softphone Management Console -->

	<!-- Break reason Pop Up menu -->
	<div class="modal fade bd-example-modal-sm" id="breakmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	     aria-hidden="true">
	    <div class="modal-dialog modal-sm" role="document">
		<form class="form-horizontal" action="changebreakstatus" method="GET" id="breakmodal"
		      name="breakmodal" enctype="multipart/form-data">
		    <div class="modal-content">
			<div class="modal-header">
			    <h5 class="modal-title" id="exampleModalLabel">Select a Break Reason</h5>
			    <button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			    </button>
			</div>
			<div class="modal-body" id="ticket_model">
			    <input type="hidden" name="endpoint" id="endpoint"
				   value="{{session()->get('endpoint')}}" />
			    <select class="form-control  custom-select-value"  id="brkreason" name="brkreason">
				<?php foreach ($getbreak_reasons as $value) { ?>
									<option value="<?php echo $value->status ?>">
									<?php echo $value->status ?></option>

				<?php } ?>
			    </select>
			</div>
			<div class="modal-footer" id="ticket_model_footer">
			    <button type="submit" style="width:30%;" class="btn btn btn-primary btn-flat" >Save</button>
			</div>
		    </div>
		</form>
	    </div>
	</div>
	<!-- Break reason Pop Up menu -->
	
	<!-- Offline reason Pop Up menu -->
	<div class="modal fade bd-example-modal-sm" id="offlinemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	     aria-hidden="true">
	    <div class="modal-dialog modal-sm" role="document">
		<form class="form-horizontal" action="" method="GET" id="offlinemodal"
		      name="offlinemodal" enctype="multipart/form-data">
		    <div class="modal-content">
			<div class="modal-header">
			    <h5 class="modal-title" id="exampleModalLabel">Select a Offline Reason</h5>
			    <button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			    </button>
			</div>
			<div class="modal-body" id="ticket_model">
			    <input type="hidden" name="endpoint" id="endpoint"
				   value="{{session()->get('endpoint')}}" />
			    <select class="form-control  custom-select-value"  id="offlinereason" name="offlinereason">
				<?php foreach ($getoffline_reasons as $value) { ?>
									<option value="<?php echo $value->status ?>">
					<?php echo $value->status ?></option>

				<?php } ?>
			    </select>
			</div>
			<div class="modal-footer" id="ticket_model_footer">
			    <button type="button" onclick="dnd_on();" style="width:30%;"
				    class="btn btn btn-primary btn-flat" >
				Save</button>
			</div>
		    </div>
		</form>
	    </div>
	</div>
	<!-- Offline reason Pop Up menu -->


	<!-- Show Phone Book Pop Up menu -->
	<div class="modal fade bd-example-modal-sm" id="ph_bk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	     aria-hidden="true">
	    <div class="modal-dialog modal-sm" role="document" style="width:1000px;">
		<form class="form-horizontal" action="changebreakstatus" method="GET" id="ph_bk"
		      name="ph_bk" enctype="multipart/form-data">
		    <div class="modal-content">
			<div class="modal-header">
			    <h5 class="modal-title" id="exampleModalLabel">Phone Book</h5>
			    <button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			    </button>
			</div>
			<div class="modal-body" id="ticket_model">
			    <section class="content">
				<div class="sparkline13-list">
				    <div class="sparkline13-graph">
					<div class="datatable-dashv1-list custom-datatable-overright">
					    <table id="ph_bk_tbl" class="table table-bordered table-striped tablerowsize " cellspacing="0" width="100%">
						<thead class="table_head">
						    <tr>
							<th><p class="text-center">Company</p></th>
							<th ><p class="text-center">Contact Type</p></th>
							<th ><p class="text-center">Full Name </p></th>
							<th><p class="text-center">Contact Number 1</p></th>
							<th><p class="text-center">Contact Number 2</p></th>
							<th><p class="text-center">Contact Number 3</p></th> 
							<th><p class="text-center">Description</p></th>             
						    </tr>
						</thead>
						<tbody>

						</tbody>
					    </table>
					</div>
				    </div>
			    </section>
			</div>

		    </div>
		</form>
	    </div>
	</div>

<!-- Show Abandon Pop Up menu -->
	<div class="modal fade bd-example-modal-sm" id="abn_det" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	     aria-hidden="true">
	    <div class="modal-dialog modal-sm" role="document" style="width:1000px;">
			<form class="form-horizontal"  method="GET" id="abn_det"
		      name="abn_det" enctype="multipart/form-data">
		    	<div class="modal-content">
					<div class="modal-header">
					</div>
						<div class="modal-body" id="ticket_model">
			   			
			   				<div class="nav-tabs-custom"style="background-color:#c2d4f2;" >
                                <ul class="nav nav-tabs">
                                     <li class="active" id="tab_div1" style="padding-right: 0px;">
                                        <a id="tab1_li" href="#tab_1_aban" onclick="searchAbnDet()" style="background-color:#d7eceb" data-toggle="tab">Abandon</a>
                                    </li>
                                    <li>
                                        <a id="tab2_li" href="#tab_2_callb" onclick="search_cl_bck()" data-toggle="tab">Call back</a>
                                    </li>          
                                </ul>
                                <div class="tab-content" >
                                    <div id="tab_1_aban" class="tab-pane active">
                                        <table id="abn_det_tbl" class="table table-bordered table-striped tablerowsize " >
											<thead class="table_head">
												<tr>
													<th>Date Time</th>
													<th>Queue ID</th>
													<th>Queue Name</th>
													<th>CLI</th>
													<th>Name</th>
													<th>Waiting time</th>  
													<th>Call</th>  
													<th>Abandon Callback Status</th> 
												</tr>
											</thead>
											<tbody>
                                            
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
										</table>
									</div>
                                    
                                    <div id="tab_2_callb" class="tab-pane">
                                        <table id="tbl_clbck" class="table table-bordered table-striped tablerowsize">
                                            <thead class="table_head">
                                                <tr>
													<th>Date Time</th>
                                                    <th>Entered Number</th>
                                                    <th>Queue</th>
													<th>Queue Name</th>                                               
													<th>Called CLI</th>
													<th>Name</th>
													<th>Call</th>  
													<th>Callback Request Status</th>           
                                                </tr>
                                            </thead>
                                            <tbody>
                                            
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>

                                </div>
                            </div>
						</div>
				</div>
			</form>
	    </div>
	</div>

	<div class="modal fade bd-example-modal-sm" style="width:100%;" id="addcbstatusdashboard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content" style="width:700px; margin-left:-300px;">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Call Log</h5>
					<button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="ticket_model">
					<div class="content-wrapper">
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<form action="#">
										<input type="hidden" name="agent" id="agent" value="" />
										<input type="hidden" name="linkid" id="linkid" value="" />
										<div class="form-group-inner">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
												<div class="row">
													<div class="form-group-inner">
														<div class="row">
															<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
																<label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important;">Contact Number</label>
															</div>
															<div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
																<input class="form-control textfeilds" name="phonumber" id="phonumber" style="width: 120%; height: 26px;" value="" readonly>
																<!-- <input class="form-control textfeilds" name="statusCalltext" id="statusCalltext" style="width: 120%; height: 26px;" value="" readonly> -->
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
																<div class="row">
																	<input type="hidden" name="linkunid" id="linkunid" value="">
																	<div class="form-group-inner">
																		<div class="row">
																			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
																				<label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important; margin-left:-2px;">Preset Remark 1</label>
																			</div>
																			<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
																				<select class="form-control custom-select-value" id="categories" style="margin-left:7px; width:95%;" name="categories" onchange = "addPreset_forlist(document.getElementById('categories'))">
																					<option value="">Category</option>
																					@foreach($categories as $cat)
                                                                                    	<option  value='{{$cat->id}}'>{{$cat->remark}}</option>
                                                                                	@endforeach
																					
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
																<div class="row">
																	<div class="form-group-inner">
																		<div class="row">
																			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
																				<label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important; margin-left:-2px;">Preset Remark 2</label>
																			</div>
																			<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
																				<select class="form-control custom-select-value" id="inquiries" style="margin-left:7px; width:95%;" name="inquiries" onchange = "addPreset_forlist(document.getElementById('inquiries'))">
																					<option value="">Inquiry</option>
																					@foreach($inquiries as $inq)
                                                                                    	<option  value='{{$inq->id}}'>{{$inq->remark}}</option>
                                                                                	@endforeach
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
																<div class="row">
																	<div class="form-group-inner">
																		<div class="row">
																			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
																				<label class="login2 pull-right pull-right-pro" style="margin-top: 0px !important; margin-left:-2px;">Remark</label>
																			</div>
																			<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12" style="margin-left:7px;">
																				<textarea name="setremark" cols="54" rows="5"  id="setremark" ></textarea>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
															    <div class="button-style-four btn-mg-b-10" style="float:right">
																	<button type="button" class="btn btn-custon-four btn-success attr_btn" style="width:78px; " onclick="SaveCallback()">Save &nbsp</button>
																	<button type="button" class="btn btn-custon-four btn-danger attr_btn" class="close" data-dismiss="modal" style="width:78px;" onclick="refresh()"> Close </button>
															    </div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Status List -->
	<div class="modal fade bd-example-modal-lg" id="StatusModel" tabindex="-1" role="dialog" aria-labelledby="StatusModelLabel" aria-hidden="true" >
		<div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="StatusModelLabel">Agent Profile</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body" id="status_model">
	                <table style='font-size:14px' align="center" width='90%'>
	                    <tr class='row'>
	                        <td class='col-md-3' style="padding: 5px;" align="left"> <label class='control-label labels'><strong>Login Time: </strong></label></td>
	                        <td class='col-md-4' style="padding: 5px;"><span id="loginid"></span></td>
	                        <td class='col-md-3' style="padding: 5px;"align='left'><strong>Total Online: </strong></td>
	                        <td class='col-md-3' style="padding: 5px;"><span id="totalonlineid"></span></td>
	                    </tr>
	                    <tr class='row'>
	                        <td class='col-md-3' style="padding: 5px;" align='left'><label class='control-label labels' ><strong>Total Busy: </strong></label> </td>
	                        <td class='col-md-3' style="padding: 5px;"><span id="busyid"></span></td>
	                        <td class='col-md-3' style="padding: 5px;" align='left'> <label class='control-label labels' ><strong>Total Idle: </strong></label> </td>
	                        <td class='col-md-3' style="padding: 5px;"><span id="idleid"></span></td>
	                    </tr>
	                    <tr class='row'>
	                        <td class='col-md-3' style="padding: 5px;" align='left'> <label class='control-label labels' ><strong>Total Break Time: </strong></label></td>
	                        <td class='col-md-3' style="padding: 5px;"><span id="totalbreakid"></span></td>
	                    </tr>
	                    <tr class="row">
	                        <td class='col-md-3' style="padding: 5px;" align='left'><strong>Ring No Answer: </strong></td>
	                        <td class='col-md-3' style="padding: 5px;"><span id="ringnoanid"></span></td>
	                    </tr>
	                    <tr class='row'>	                        
	                        <td class='col-md-3' style="padding: 5px;" align='left'> <label class='control-label labels' ><strong>Number of Calls: </strong></label></td>
	                        <td class='col-md-3' style="padding: 5px;"><span id="nocallsid"></span></td>
	                    </tr>
	                   
	                </table><br>
	                <table id="AgentBreaklist" align="center" width='70%'>

	                	<thead class="table_head">
	                        <tr>
	                            <th style="padding: 7px;" class="text-center">Break</th>
	                            <th style="padding: 7px;" class="text-center">Number of Breaks</th>
	                            <th style="padding: 7px;" class="text-center">Time</th>
	                        </tr>
	                    </thead>
	                    <tbody id="breakData">
	                    </tbody>
	                   
	                </table>
	            </div>
	            <div class="modal-footer" id="StatusModelfooter">

	                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>

	            </div>
	        </div>
	    </div>
	</div>

	</div>
		<!-- Break reason Pop Up menu -->
		<div class="modal fade bd-example-modal-sm" id="br_exceed_time_reason" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	     aria-hidden="true">
	    <div class="modal-dialog modal-sm" role="document">
		<form class="form-horizontal" action="{{url('save_break_exc_reason')}}" method="POST" id="br_exceed_time_reason_"
		      name="br_exceed_time_reason_" enctype="multipart/form-data">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <div class="modal-content">
			<div class="modal-header">
			    <h5 class="modal-title" id="exampleModalLabel">Enter the Break Exceed Reason</h5>
			    <button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			    </button>
			</div>
			<div class="modal-body" id="br_exceed_time_reason__">
			    <input type="hidden" name="endpoint" id="endpoint"
				   value="{{session()->get('endpoint')}}" />
				
				<textarea  cols="35" rows="5"  id="br_ex_reason" name="br_ex_reason" ></textarea>
			</div>
			<div class="modal-footer" id="ticket_model_footer">
			    <button type="submit" style="width:30%;" class="btn btn btn-primary btn-flat" >Save</button>
			</div>
		    </div>
		</form>
	    </div>
	</div>
	<!-- Break reason Pop Up menu -->
			
	<script type="text/javascript">


		function statusModel() {
			// body...

			$.ajax({
				type: "GET",
				url: '{{url('AgentProfileDetails')}}',
				data:  {},
				success: function (response) {

					var bodyData = '';

					$("#breakData").empty();                   	

					console.log(response);
					$.each(response, function (index, c) {

						$('#loginid').html(c.cre_datetime);
						$('#totalonlineid').html(c.ReadyTime);
						$('#busyid').html(response.TalkTime);
						$('#idleid').html(response.idleTime);
						$('#nocallsid').html(response.CallCount);
						$('#ringnoanid').html(response.ringNoanswer);
						$('#totalbreakid').html(response.BreakTime);
	
					});

					$.each(response.data,function(index,row){

						bodyData+="<tr>"
						bodyData+="<td style='padding: 6px'>"+row.description+"</td><td style='padding: 6px'>"+row.breakCount+"</td><td style='padding: 6px'>"+row.time+"</td>";
						bodyData+="</tr>";
						
					});

					$("#breakData").append(bodyData).css({"text-align": "center"});

				}

			});

			$('#StatusModel').modal('show');
		}
		
		function SaveCallback() {
			var pho_number = document.getElementById("phonumber").value;
			var category = document.getElementById("categories").value;
			// var logtypetext = document.getElementById("statusCalltext").value;
			var inquiry = document.getElementById("inquiries").value;
			var remark = document.getElementById("setremark").value;
			var linkid = document.getElementById("linkunid").value;

			$.ajax({
			    url: 'SaveCallbackLog',
			    type: 'GET',
			    data: {pho_number: pho_number, category: category, inquiry: inquiry, remark: remark, linkid:linkid},
			    success: function (response)
			    {
				//alert(response);
				if (response == "Call Log Added Successfully!") {
				    alert(response);
				    document.getElementById("phonumber").value = "";
				    document.getElementById("categories").value = "";
				    document.getElementById("inquiries").value = "";
				    document.getElementById("setremark").value = "";
				    $('#addcbstatusdashboard').modal('hide');
				    //return false;
				} else if (response == "Data Saving Error!") {
				    alert(response);


				}
			    }
			});
   		}

   		function search_cl_bck(){

			$("#tbl_clbck").dataTable().fnDestroy();

			$('#tbl_clbck').DataTable({
				"processing": true,
				"ajax": {
					url: 'get_clbck_det',
					type: 'GET',
					data: {},
				},
				"columns":
				[
					{ "data": "credate" },
					{ "data": "callnumber" },
					{ "data": "queue" },
					{ "data": "descr" },
					{ "data": "frm_caller_num" },				   
					{ "data": "fullName" },				   
					{
						sortable: false,
						"render": function (data, type, full, meta) {
							var CLI = full.callnumber;
							var LnkID = full.uniqueid;
							return '<a style="color: #000000" onclick="pick_phone_num_calbk_requ_aban(\''+CLI+'\', \''+LnkID+'\')" role="button" class="text-center"><button type="button" class="btn btn-info">Call</button></a>';
						}
					},
					{
						sortable: false,
						"render": function (data, type, full, meta) {
							var CLI = full.callnumber;
							var LnkID = full.uniqueid;

							

								return '<a style="color: #000000;" onclick="addCallbackStatus(\''+CLI+'\', \''+LnkID+'\')" role="button" id="'+LnkID+'" class="text-center"><button type="button" class="btn btn-info" >Add Status</button></a>';
							
						}
					},

			   	],
	   
			 	"pageLength": 10,

			 	"columnDefs": [
					 { className: "text-center", "targets": [0,1,2,3,4,5] }
				 ],

			 	"order": [[ 0, "desc" ]],
		 	});
		}

		function Dial_aban_call(linkunid) {

			var endpoint = document.getElementById("endpoint").value;
			var dial_num = document.getElementById("dial_num").value;

			searchAbnDet();
			
			$.ajax({
			    url: 'callback_Dial_call',
			    type: 'GET',
			    data: { endpoint: endpoint, dial_num: dial_num, linkunid: linkunid },

			    success: function (response) {
					if (response != "") {
						//$('#dialhangupbtn').html('');
					    //alert(response);
					    //window.open("{{url ('inboundcall')}}" + "?number=" + response, "_blank");
					    // getdetailsofcaller(endpoint,response);
					    document.getElementById("linkid").value = response;
						document.getElementById("dial_dtmf_num").value = dial_num;
					    //alert("GO Offline Successfully!!");
					    /*var datalist = document.getElementById('agtstatus_div');
					     datalist.innerHTML = '';
					     $('#agtstatus_div').append(
					     'Outcall');
					     $('#dialhangupbtn').append(
					     '<button onclick="hangup_channel()" id="hangup" class="btn detail_dial__btn detail_sub__btntwo">Hangup</button>');*/


					}
				//opensocket(endpoint);
			    }
			});
	    }

	    function callback_request_call(linkunid) {

			var endpoint = document.getElementById("endpoint").value;
			var dial_num = document.getElementById("dial_num").value;
			// alert(linkunid);
			// var linkunid_str=""+linkunid;
	    	// document.getElementById(linkunid).disabled = false;
	    	// search_cl_bck();
			// alert(linkunid);
			/*if(!checknicvalidate(dial_num))
			 {
			 return false;
			 }*/
			$.ajax({
			    url: 'callback_request_Dial_call',
			    type: 'GET',
			    data: { endpoint: endpoint, dial_num: dial_num, linkunid: linkunid },

			    success: function (response) {
					if (response != "") {
						//$('#dialhangupbtn').html('');
					    //alert(response);
					    //window.open("{{url ('inboundcall')}}" + "?number=" + response, "_blank");
					    // getdetailsofcaller(endpoint,response);
					    document.getElementById("linkid").value = response;
						document.getElementById("dial_dtmf_num").value = dial_num;
	    				


					    //alert("GO Offline Successfully!!");
					    /*var datalist = document.getElementById('agtstatus_div');
					     datalist.innerHTML = '';
					     $('#agtstatus_div').append(
					     'Outcall');
					     $('#dialhangupbtn').append(
					     '<button onclick="hangup_channel()" id="hangup" class="btn detail_dial__btn detail_sub__btntwo">Hangup</button>');*/


					}
				//opensocket(endpoint);
			    }
			});
	    }

		// function addPreset(remark){
	 //        // var totalRemark = document.getElementById('setremark');
	 //        // var secondrem = $("#"+remark).find('option:selected').text();

	 //        // totalRemark.value = totalRemark.value
	 //        //     + ((totalRemark.value=='') ? '' : ' - ')
	 //        //     + secondrem;
	 //        //     // document.getElementById('preset1').value="";
	 //        //     // document.getElementById('preset2').value="";

	 //        var totalRemark = document.getElementById('setremark')
  //       	totalRemark.value = totalRemark.value
  //           + ((totalRemark.value=='') ? '' : ' - ')
  //           + remark;
	 //    }

		function pick_phone_num_aban(number, linkunid){

			document.getElementById("dial_num").value = number;
			Dial_aban_call(linkunid);
		//$('#abn_det').modal('hide');
	    }

	    function pick_phone_num_calbk_requ_aban(number, linkunid){

			document.getElementById("dial_num").value = number;
			callback_request_call(linkunid);

		//$('#abn_det').modal('hide');
	    }


   		function addCallbackStatus(number, linkunid){


			var linkunid=linkunid;
			var number=number;

		    $.ajax({
		        url: 'check_callback_before_add',
		        type: 'GET',
		        data: {
		            linkunid: linkunid
		        },
		        success: function(response) {
		            if (response == "OK") 
		            {
		            	document.getElementById("phonumber").value = number;
						document.getElementById("linkunid").value = linkunid;
						$('#addcbstatusdashboard').modal('show');

		            }else{
						alert('Please Dial before add the status');		            	
		            }
		        }
		    });

		}



	</script>

<script >
	(function ($) {
		$.fn.numKey = function (options) {
		    var defaults = {
			limit: 100,
			disorder: false
		    }
		    var options = $.extend({}, defaults, options);
		    var input = $(this);
		    var nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
		    if (options.disorder) {
			nums.sort(function () {
			    return 0.5 - Math.random();
			});
		    }
		    var html = '\
			<div class="fuzy-numKey" style="display: block;height: 228px;left: 270px;">\
			<div class="fuzy-numKey-active">﹀</div>\
			<table>\
			<tr>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[7] + '</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[8] + '</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[9] + '</td>\
			</tr>\
			<tr>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[4] + '</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[5] + '</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[6] + '</td>\
			</tr>\
			<tr>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[1] + '</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[2] + '</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[3] + '</td>\
			</tr>\
			<tr>\
			<td class="fuzy-numKey-darkgray fuzy-numKey-active fuzy-numKey-modify">Clear</td>\
			<td class="fuzy-numKey-lightgray fuzy-numKey-active">' + nums[0] + '</td>\
			<td class="fuzy-numKey-darkgray fuzy-numKey-active">&larr;</td>\
			</tr>\
			</table>\
			<style type="text/css">\
			* {\
				padding: 0 0;\
				margin: 0 0;\
			}\
			.fuzy-numKey {\
				width: 65%;\
				position: absolute;\
				display: none;\
				background-color: #333;\
				text-align: center;\
				padding: 3%;\
				height: 30%;\
				z-index: 999;\
		    	background-color: #fffbfb;\
		    	border: 1px solid #d8d8d8;\
			}\
			.fuzy-numKey div {\
				height: 15%;\
				width: 100%;\
				font-weight: bold;\
				font-family: "微软雅黑";\
				background-color: #f3f3f3;\
				color: #00377a;\
				margin-bottom: 6%;\
				border-radius: 0px;\
				line-height: 200%;\
		    	border: 1px solid #00377a;\
			}\
			.fuzy-numKey table {\
				width: 100%;\
				height: 76%;\
			}\
			.fuzy-numKey td {\
				font-weight: bold;\
				font-family: "微软雅黑";\
				width: 33%;\
		        font-size: 15px;\
				color: #00377a;\
				border-radius: 0px;\
		        border: 1px solid #fff;\
			}\
			.fuzy-numKey-darkgray{\
				background: #c8cbcc\
			}\
			.fuzy-numKey-lightgray{\
				background: #e5e7e8;\
		    	border: 1px solid #91ceec;\
			}\
			.fuzy-numKey-active:active {\
				background: #00c1d5;\
			}\
		.fuzy-numKey-modify{\
		    color: #fff important;\
		}\
			</style>\
			</div>';
		    input.on("click", function () {
			//$(this).attr('readonly', 'readonly');
			$(".fuzy-numKey").remove();
			$('#div_numberpad').append(html);
			$(".fuzy-numKey").show(100);
			$(".fuzy-numKey table tr td").on("click", function () {
			    if (isNaN($(this).text())) {
				if ($(this).text() == 'Clear') {
				    input.val('');
				} else {
				    //alert($(this).text());
				    input.val(input.val().substring(0, input.val().length - 1));
				}
			    } else {
				//alert($(this).text());
				play_dtmf($(this).text());
				input.val(input.val() + $(this).text());
				if (input.val().length >= options.limit) {
				    input.val(input.val().substring(0, options.limit));
				    remove();
				}
			    }
			});
			$(".fuzy-numKey div").on("click", function () {
			    remove();
			});
		    });

		    function remove() {
			$(".fuzy-numKey").hide(100, function () {
			    $(".fuzy-numKey").remove();
			});
			input.removeAttr('readonly');
		    }
		}
	    })(jQuery)

	    $("#dial_num").numKey({
		limit: 10,
		disorder: false
	    });

	    $("#dial_pad").on("click", function () {
			$("#dial_num").click();
			});
</script>

<script >
function play_dtmf(dtmf_num) {
    
    var endpoint = document.getElementById("endpoint").value;
    var dial_dtmf_num = document.getElementById("dial_dtmf_num").value;
   // alert(dtmf_num)
    $.ajax({
        url: 'play_dtmf',
        type: 'GET',
        data: {
            endpoint: endpoint,
            dial_dtmf_num: dial_dtmf_num,
            dtmf_num:dtmf_num
        },
        success: function(response) {
            if (response != "") 
            {

            }
        }
    });

}
function checknicvalidate(contact_number) {

		const regex = /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\d)\d{6}$/;
		let matches;
		matches = regex.exec(contact_number);
		if (matches == null) {
		    // alert(' Enter Valid Mobile Number');
			bootbox.alert({
    				message: "Enter Valid Mobile Number",
					size: 'small',
    			callback: function () {		
					return false;
    				}
					});
		   
		} else {

		    return true;
		}

	    }
function searchdata(incoming_number) {
		var endpoint = document.getElementById("endpoint").value;
		var divname = incoming_number + "search_text";
		alert(divname);
		var search_text = document.getElementById(divname).value;


		$("#" + incoming_number + "tickettable").dataTable().fnDestroy();
		//alert(search_text);
		$("#" + incoming_number + "tickettable").DataTable({
		    "processing": true,
		    "ajax": {
			"url": "getcustomersbysearch",
			"type": "GET",
			data: {
			    search_text: search_text
			},
		    },
		    "columns": [{
			    "data": "contact_name"
			},
			{
			    "data": "primary_contact"
			},
			{
			    "data": "secondary_contact"
			},
			{
			    "data": "secondary_contact2"
			},
			{
			    "data": "email"
			},
			{
			    "data": "address"
			},
			{
			    sortable: false,
			    "render": function (data, type, full, meta) {
				// var id = ""+full.id;
				// return '<a style="color: #000000" href="ticket/'+id+'" role="button" class="text-center"><span class="glyphicon glyphicon-eye-open"></span></a>';
				return ' <td style=" width:2%;"><a style="color: #00a7d0" href="../../inboundCallUpdateRecordInfo/' +
					full.primary_contact + '/' + full.id + '/' + incoming_number + '/' + endpoint +
					'">View</a></td>';
			    }
			},
		    ],
		    "columnDefs": [{
			    className: "text-center",
			    "targets": [5, 6]
			}],
		    "order": [
			[1, "desc"]
		    ],
		    "pageLength": 25
		});
	    }

function act_hold() {
		var endpoint = document.getElementById("endpoint").value;
		//var socket = document.getElementById("socket").value;


		$.ajax({
		    url: 'act_hold',
		    type: 'GET',
		    data: {
			endpoint: endpoint
		    },
		    success: function (response) {
			if (response == "Notfound") {
			    // alert("Please open the softphone application");
				bootbox.alert({
    				message: "Please open the softphone application",
					size: 'small',
    			callback: function () {		
    				}
					});
			} else if (response == "false") {
			    // alert("Allrady Online !!");
				bootbox.alert({
    				message: "Allrady Online !!",
					size: 'small',
    			callback: function () {			    	
    				}
					});
			} else {
			    // alert("GO Online Successfully!!");
				bootbox.alert({
    				message: "GO Online Successfully!!",
					size: 'small',
    			callback: function () {			    	
    				}
					});

			    //location.href = "{{url ('Agentdashboard')}} ";
			}
			//location.href = "{{url ('Agentdashboard')}} ";
		    }
		});
	    }

function dnd_off() {
		var endpoint = document.getElementById("endpoint").value;

		$.ajax({
		    url: 'dnd_off',
		    type: 'GET',
		    data: {
			endpoint: endpoint
		    },
		    success: function (response) {
			if (response == "Notfound") {
			    // alert("Please open the softphone application");
				bootbox.alert({
    				message: "Please open the softphone application",
					size: 'small',
    			callback: function () {			    	
    				}
					});
			} else if (response == "false") {
			    // alert("Allrady Online !!");
				bootbox.alert({
    				message: "Allrady Online !!",
					size: 'small',
    			callback: function () {			    	
    				}
					});
			} else {
			    //alert("GO Online Successfully!!");
			    var datalist = document.getElementById('agtstatus_div');
			    datalist.innerHTML = '';
			    $('#agtstatus_div').append(
				    'Online');
			    // location.href = "{{url ('Agentdashboard')}} ";
			    location.reload();
			    opensocket(endpoint);

			}
			//location.href = "{{url ('Agentdashboard')}} ";
			$('#onoff').html('<a href="#" class="myButtonD" onclick="dnd_on();" style="width:185px;">Go Offline</a>');
		    }
		});
	    }

function obmode_status() {
		var endpoint = document.getElementById("endpoint").value;

		$.ajax({
		    url: 'obmode_status',
		    type: 'GET',
		    data: {
			endpoint: endpoint
		    },
		    success: function (response) {
			if (response == "Notfound") {
			    // alert("Please open the softphone application");
				bootbox.alert({
    				message: "Please open the softphone application",
					size: 'small',
    			callback: function () {		
			    	// location.href = "{{url ('Agentdashboard')}} ";
			    	location.reload();
			    	
    				}
					});

			} else if (response == "Outbound On") {
			    // alert("Outbound Mode On Successfully");
				bootbox.alert({
    				message: "Outbound Mode On Successfully",
					size: 'small',
    			callback: function () {
        			var datalist = document.getElementById('agtstatus_div');
			    	datalist.innerHTML = '';
			    	$('#agtstatus_div').append(
				    	'Online');
			    	// location.href = "{{url ('Agentdashboard')}} ";
			    	location.reload();
			    	opensocket(endpoint);
    				}
					});
			    
			} else if (response == "Outbound Off") {
			//  alert("Outbound Mode Off Successfully");
				bootbox.alert({
    				message: "Outbound Mode Off Successfully!",
	 				size: 'small',
    			callback: function () {
        		var datalist = document.getElementById('agtstatus_div');
			    datalist.innerHTML = '';
			    $('#agtstatus_div').append(
				    'Online');
			    // location.href = "{{url ('Agentdashboard')}} ";
			    location.reload();
			    opensocket(endpoint);
			}
		});
			   
			}
			//location.href = "{{url ('Agentdashboard')}} ";
			$('#onoff').html('<a href="#" class="myButtonD" onclick="dnd_on();" style="width:185px;">Go Offline</a>');
		    }
		});
	    }

function opensocket(endpoint) {

		$.ajax({
		    url: 'opensocket',
		    type: 'GET',
		    data: {
			endpoint: endpoint
		    },
		    success: function (response) {
			//alert(response['Linkedid']);
			if (response != "") {
			    document.getElementById("linkid").value = response['Linkedid'];
			    document.getElementById("incoming_num").value = response['CallerIDNum'];
			    getdetailsofcaller(endpoint, response['CallerIDNum'],response['Linkedid']);
			    getQueInfo(document.getElementById("linkid").value);
			    getIncommingNo(document.getElementById("linkid").value);
			}
			opensocket(endpoint);
		    }
		});
	    }

function start_acw() {
		var endpoint = document.getElementById("endpoint").value;
		var incoming_num = document.getElementById("incoming_num").value;
		var dial_num = document.getElementById("dial_num").value;
		var linkid = document.getElementById("linkid").value;

		

		if (dial_num != "")
		{
		    acw_num = dial_num;
		} else
		{
		    acw_num = incoming_num;
		}

		$.ajax({
		    url: 'start_acw',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			acw_num: acw_num,
			linkid: linkid
		    },
		    success: function (response) {
			if (response == "Online") {
				bootbox.alert({
    				message: "Can Not Start ACW !!",
					size: 'small'
					});
			}else if (response != "")
			{
				bootbox.alert({
    				message: "ACW Start Successfull!!",
					size: 'small',
    			callback: function () {		
					document.getElementById("linkid").value = response;
			    	var datalist = document.getElementById('agtstatus_div');
			    	datalist.innerHTML = '';
			    	$('#agtstatus_div').append(
				    'ACW');
			    	}
					});
			   //location.href = "{{url ('Agentdashboard')}} ";
				location.reload();
			}

		    }
		});
	    }
function end_acw() {
		var endpoint = document.getElementById("endpoint").value;
		var incoming_num = document.getElementById("incoming_num").value
		var dial_num = document.getElementById("dial_num").value
		var linkid = document.getElementById("linkid").value

		if (dial_num != "")
		{
		    acw_num = dial_num;
		} else
		{
		    acw_num = incoming_num;
		}
		$.ajax({
		    url: 'end_acw',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			acw_num: acw_num,
			linkid: linkid
		    },
		    success: function (response) {
		    	//alert(response);
			if (response == "acw off") {
			     
				bootbox.alert({
    				message: "ACW End Successfull!!",
					size: 'small',
    			callback: function () {		
					// location.href = "{{url ('Agentdashboard')}} ";
					location.reload();
			    	}
					});
			    //location.href = "{{url ('Agentdashboard')}} ";
			}

		    }
		});
	    }

function Conf_call() {
		var endpoint = document.getElementById("endpoint").value;
		var dial_num = document.getElementById("dial_num").value;
		var incoming_num = document.getElementById("incoming_num").value
		var conf_num = "";
		if ($('#dial_num').val().length === 0) {
			alert('This field is required!');
			$("#dial_num").focus();
			return false;
		}
		if (incoming_num != "")
		{
		    conf_num = incoming_num;
		} else
		{
		    conf_num = dial_num;
		}

		// alert(incoming_num);
		// alert(dial_num);
		$.ajax({
		    url: 'Conf_call',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			conf_num: dial_num
		    },
		    success: function (response) {
			if (response != "") {
			    // alert("Conference Started Successfully!");
				bootbox.alert({
    				message: "Conference Started Successfully!",
					size: 'small',
    			callback: function () {		
					// location.href = "{{url ('Agentdashboard')}} ";
			    	}
					});
				//location.reload();
			    //window.open("{{url ('inboundcall')}}" + "?number=" + response, "_blank");
			    // getdetailsofcaller(endpoint,response);
			}
			//opensocket(endpoint);
		    }
		});
	    }

function Attended_transfer() {
		var endpoint = document.getElementById("endpoint").value;
		var dial_num = document.getElementById("dial_num").value;
		var incoming_num = document.getElementById("incoming_num").value;
		var incoming_linkedid = document.getElementById("incoming_linkedid").value;
		if ($('#dial_num').val().length === 0) {
			alert('This field is required!');
			$("#dial_num").focus();
			return false;
		}
		$.ajax({
		    url: 'Attended_transfer',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			dial_num: dial_num,
			incoming_num: incoming_num,
			incoming_linkedid: incoming_linkedid		    },
		    success: function (response) {
//alert(response);
			if (response == "true") {
			    alert("Successfully Attend Transfered!");
			    //window.open("{{url ('inboundcall')}}" + "?number=" + response, "_blank");
			    // getdetailsofcaller(endpoint,response);
			    location.reload();
			}else
			{
				alert("Failed Attended Transfer!");

			}
			//opensocket(endpoint);
		    }
		});
	    }

function Blind_transfer() {
		var endpoint = document.getElementById("endpoint").value;
		var dial_num = document.getElementById("dial_num").value;
		if ($('#dial_num').val().length === 0) {
			alert('This field is required!');
			$("#dial_num").focus();
			return false;
		}
		$.ajax({
		    url: 'Blind_transfer',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			dial_num: dial_num
		    },
		    success: function (response) {
			if (response != "") {
			    // alert("Successfully Blind Transfered!");
				bootbox.alert({
    				message: "Successfully Blind Transfered!",
					size: 'small',
    			callback: function () {		
					// location.href = "{{url ('Agentdashboard')}} ";
			    	}
					});
				
			    //window.open("{{url ('inboundcall')}}" + "?number=" + response, "_blank");
			    // getdetailsofcaller(endpoint,response);
			}
			//opensocket(endpoint);
		    }
		});
	    }

function chanspy_channel(context, msg) {
		var endpoint = document.getElementById("endpoint").value;
		var dial_num = document.getElementById("dial_num").value;
		if ($('#dial_num').val().length === 0) {
			alert('This field is required!');
			$("#dial_num").focus();
			return false;
		}
		$.ajax({
		    url: 'chanspy_channel',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			dial_num: dial_num,
			context: context
		    },
		    success: function (response) {
			if (response != "") {
			    // alert(msg + " Successfully!");
				bootbox.alert({
    				message: (msg + " Successfully!"),
					size: 'small',
    			callback: function () {		
			    	}
					});

			}

		    }
		});
	    }

function Add_conf() {
		var endpoint = document.getElementById("endpoint").value;
		var dial_num = document.getElementById("dial_num").value;
		//alert(incoming_num);
		if ($('#dial_num').val().length === 0) {
			alert('This field is required!');
			$("#dial_num").focus();
			return false;
		}
		$.ajax({
		    url: 'Add_conf',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			dial_num: dial_num
		    },
		    success: function (response) {
			if (response != "") {
			    // alert("Successfully Added to Conference!");
				bootbox.alert({
    				message: "Successfully Added to Conference!",
					size: 'small',
    			callback: function () {		

			    	}
					});
				location.reload();
			}
			//opensocket(endpoint);
		    }
		});
	    }

function Dial_call() {

		var endpoint = document.getElementById("endpoint").value;
		var dial_num = document.getElementById("dial_num").value;
		if ($('#dial_num').val().length === 0) {
			alert('This field is required!');
			$("#dial_num").focus();
			return false;
		}
		$.ajax({
		    url: 'Dial_call',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			dial_num: dial_num
		    },
		    success: function (response) {
			if (response != "") {
			    document.getElementById("linkid").value = response;
				document.getElementById("dial_dtmf_num").value = dial_num;

			}
	
		    }
		});
	    }

function getdetailsofcaller(endpoint, cl_number,linkedid) {

		$.ajax({
		    url: 'getdetailsofcaller',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			cl_number: cl_number,
			linkedid: linkedid
		    },
		    success: function (response) {
			var datalist = document.getElementById('caller_div');
			datalist.innerHTML = '';
			$('#caller_div').append(response);
			//alert(response);
		    }
		});
	    }

		function open_agntdb_addcusview(endpoint, cl_number,linkedid) {
			//alert(cl_number);
		$.ajax({
			url: 'open_agntdb_addcusview',
			type: 'GET',
			data: {
			endpoint: endpoint,
			cl_number: cl_number,
			linkedid: linkedid
			},
			success: function (response) {

			var datalist_csp = document.getElementById('csp_div');
			datalist_csp.innerHTML = '';
			$('#csp_div').append(response);
			//alert(response);
			}
		});
		}

		function close_csp_div() {

				localStorage.removeItem("cus_id");
				var datalist_csp = document.getElementById('csp_div');
				datalist_csp.innerHTML = '';
		}
function changebreakstatus() {
		var endpoint = document.getElementById("endpoint").value;
		var agtstatus = document.getElementById("agtstatus").value;
		
		var brkreason = "";

		if (agtstatus == "Online") 
		{
			
		    $('#breakmodal').modal('show');
		} 
		else if(agtstatus == "Break")
		{

		    $.ajax({
			url: 'changebreakstatus',
			type: 'GET',
			data: {
			    endpoint: endpoint,
			    brkreason: brkreason
			},
			success: function (response) {

				console.log(response);
			    if (response == "Notfound") 
				{
					// alert("Please open the softphone application");
					bootbox.alert({
    					message: "Please open the softphone application",
						size: 'small',
    					callback: function () {		
						// location.href = "{{url ('Agentdashboard')}} ";
			    		}
						});
			    } 
				else if (response == "on") 
				{
					 // location.href = "{{url ('Agentdashboard')}} ";
					 location.reload();
			    } else if (response == "Offline") {
					
					bootbox.alert({
    					message: "User Not Online!!",
						size: 'small',
    				callback: function () {		
						//  location.href = "{{url ('Agentdashboard')}} ";
			    		}
					});
			    }
				else if (response == "exceed") {
					
					$('#br_exceed_time_reason').modal('show');

			    }
			    else {
				
					// location.href = "{{url ('Agentdashboard')}} ";
					location.reload();
			    }
			    // location.href = "{{url ('Agentdashboard')}} ";
			}
		    });
		}else
		{
			bootbox.alert({
    				message: "User Not Online!!",
					size: 'small',
    			callback: function () {		
					// location.href = "{{url ('Agentdashboard')}} ";
			    	}
					});
		}
		
	    }



function disply_offline_reason() {
		    $('#offlinemodal').modal('show');
}
function dnd_on() {
		var endpoint = document.getElementById("endpoint").value;
		var offlinereason = document.getElementById("offlinereason").value;

		$.ajax({
		    url: 'dnd_on',
		    type: 'GET',
		    data: {
			endpoint: endpoint,
			offlinereason:offlinereason
		    },
		    success: function (response) {
			if (response == "Notfound") {
			    // alert("Please open the softphone application");
				bootbox.alert({
    				message: "Please open the softphone application",
					size: 'small',
    			callback: function () {		
					
			    	}
					});
			} else if (response == "false")
			{
			    // alert("Allrady Offline !!");
				bootbox.alert({
    				message: "Allrady Offline !!",
					size: 'small',
    			callback: function () {						
			    	}
					});
			} else if (response == "break")
			{
			    // alert("Agent On Break Can not Make Device Offline !!");
				bootbox.alert({
    				message: "Agent On Break Can not Make Device Offline !!",
					size: 'small',
    			callback: function () {					
			    	}
					});
			} else
			{
			    //alert("GO Offline Successfully!!");
			    var datalist = document.getElementById('agtstatus_div');
			    datalist.innerHTML = '';
			    // $('#agtstatus_div').append(
			    //     'Offline');
			    // location.href = "{{url ('Agentdashboard')}} ";
			    location.reload();
			}
			//location.href = "{{url ('Agentdashboard')}} ";
			//$('#onoff').html('<a href="#" class="myButtonD" onclick="dnd_off();" style="width:185px;">Go Offline</a>');
		    }
		});
	    }
function getQueInfo(linkeid) {

		var linkeid = linkeid;

		$.ajax({
		    url: 'getQueInformation',
		    type: 'GET',
		    data: {
			linkeid: linkeid
		    },
		    success: function (response) {

			/*var datalist = document.getElementById('queInfo');
			 datalist.innerHTML = '';
			 $('#queInfo').append(
			 '<label class="paddinglf lablesty">Queue Info - '+response+'</label>');*/
				if(response!="Notfound"){
					getQueDestination(response);
				}

		    }
		});
	    }
function getQueDestination(endpoint) {

		var endpoint = endpoint;

		$.ajax({
		    url: 'getQueDestination',
		    type: 'GET',
		    data: {
			endpoint: endpoint
		    },
		    success: function (response) {

			if(response!="Notfound"){
				var datalist = document.getElementById('queInfo').value = 'Queue Info  ' + endpoint + '-' + response;
				datalist.innerHTML = '';
				$('#queInfo').append(
					'<label class="paddinglf lablesty" style="font-size: 16px;">Queue Info - ' + endpoint + '-' + response + '</label>');

				}else{
					var datalist = document.getElementById('queInfo').value = 'Queue Info  ';

					}

		    }
		});
	    }

function getIncommingNo(linkeid) {

		var linkeid = linkeid;

		$.ajax({
		    url: 'getIncommingNo',
		    type: 'GET',
		    data: {
			linkeid: linkeid
		    },
		    success: function (response) {

				if(response!="Notfound"){
					document.getElementById('dial_num').value = response;
				}else{
					document.getElementById('dial_num').value = "";
				}
		    }
		});
	    }

</script>

<script>
$(document).ready(function () {
	
		var cus_no = localStorage.getItem("cus_id");
  		// document.getElementById("cus_profile_id").innerHTML = cus_no;
  		 // alert(x);
  		 //alert(localStorage.getItem("linkedid"));
		var agtstatus = document.getElementById('agtstatus').value;
		var endpoint = document.getElementById("endpoint").value;
		var incoming_num = document.getElementById("incoming_num").value;
		var linkid = document.getElementById("linkid").value;
		var usertypeid = document.getElementById("usertypeid").value;

		setTimeout(function(){                                                                                     
	if(cus_no != null && localStorage.getItem("linkedid") == linkid)
  		{
  			document.getElementById('click_con').click();
  		}
  		else
  		{

  		}},2000);
		
		
		//alert(usertypeid);
		if(usertypeid!=17)
		{
			$("#whisper").addClass("btn_disabled");
			$("#spy").addClass("btn_disabled");
			$("#barge").addClass("btn_disabled");
			document.getElementById("whisper").disabled = true;
		    document.getElementById("spy").disabled = true;
			document.getElementById("barge").disabled = true;
		}
		AgentCallButtonStatus(agtstatus, incoming_num);
		if (agtstatus == "Online") {
		    //opensocket(endpoint);

		}

		if (agtstatus != 'Break' || agtstatus != 'Offline') {
			if (incoming_num != "") {
			 getdetailsofcaller(endpoint, incoming_num,linkid);
			}

			if (linkid != "" && incoming_num != "") {
				getQueInfo(linkid);
				getIncommingNo(linkid);
			}
		}

			// $.ajax({
	  //           url: 'getInquaryList',
	  //           type: 'GET',
	  //           success: function (response)
	  //           {
	  //               var model = $('#inquiries');
	  //               // model.empty();
	  //               var i = 0

	  //               var datalist;
	  //               datalist += "<option value=''>Select Inquiry</option>";
	  //               $.each(response, function (index, element) {
	  //                   $.each(element, function (colIndex, c) {
	  //                       datalist += "<option value='" + c.id + "'>" + c.name+ "</option>";
	  //                   });
	  //               });
	  //               // $('#inquiries').html(datalist);
	  //           }
	  //       });

		 //  	$.ajax({
	  //           url: 'getCategoryList',
	  //           type: 'GET',
	  //           success: function (response)
	  //           {
	  //               var model = $('#categories');
	  //               // model.empty();
	  //               var i = 0

	  //               var datalist;
	  //               datalist += "<option value=''>Select Category</option>";
	  //               $.each(response, function (index, element) {
	  //                   $.each(element, function (colIndex, c) {
	  //                       datalist += "<option value='" + c.id + "'>" + c.name+ "</option>";
	  //                   });
	  //               });
	  //               // $('#categories').html(datalist);
	  //           }
	  //       });

	         $(".awesomplete").attr("style","width:100%");

	         @if(Session::has('jsAlert'))	
					
						alert({{ session()->get('jsAlert') }});
					
				setTimeout(function(){                                                                                     
					$(".alert").alert('close')
					},3000);
			@endif


	    });
	
function AgentCallButtonStatus(agtstatus,incoming_num) {

		if (agtstatus == 'Offline') {
			document.getElementById("start_break").disabled = true;
			document.getElementById("ob_mode_on").disabled = true;
			document.getElementById("start_acw").disabled = true;
			document.getElementById("dial_pad").disabled = true;
			
		    document.getElementById("blind_tra").disabled = true;
		    document.getElementById("attend_tra").disabled = true;
		    document.getElementById("dial").disabled = true;
		    document.getElementById("s_conf").disabled = true;
		    document.getElementById("a_conf").disabled = true;
		    document.getElementById("whisper").disabled = true;
		    document.getElementById("spy").disabled = true;
			document.getElementById("barge").disabled = true;
			
			$("#ob_mode_on").addClass("btn_disabled");
			$("#start_break").addClass("btn_disabled");
			$("#dial").addClass("btn_disabled");
			$("#start_acw").addClass("btn_disabled");
			$("#dial_pad").addClass("btn_disabled");
			$("#s_conf").addClass("btn_disabled");
			$("#a_conf").addClass("btn_disabled");
			$("#whisper").addClass("btn_disabled");
			$("#spy").addClass("btn_disabled");
			$("#barge").addClass("btn_disabled");
			$("#blind_tra").addClass("btn_disabled");
			$("#attend_tra").addClass("btn_disabled");
		}

		if (agtstatus == 'Break') {
			document.getElementById("ob_mode_on").disabled = true;
			document.getElementById("start_acw").disabled = true;
			document.getElementById("dial_pad").disabled = true;
			
		    document.getElementById("blind_tra").disabled = true;
		    document.getElementById("attend_tra").disabled = true;
		    document.getElementById("dial").disabled = true;
		    document.getElementById("s_conf").disabled = true;
		    document.getElementById("a_conf").disabled = true;
		    document.getElementById("whisper").disabled = true;
		    document.getElementById("spy").disabled = true;
			document.getElementById("barge").disabled = true;
			
			$("#ob_mode_on").addClass("btn_disabled");
			$("#dial").addClass("btn_disabled");
			$("#start_acw").addClass("btn_disabled");
			$("#dial_pad").addClass("btn_disabled");
			$("#s_conf").addClass("btn_disabled");
			$("#a_conf").addClass("btn_disabled");
			$("#whisper").addClass("btn_disabled");
			$("#spy").addClass("btn_disabled");
			$("#barge").addClass("btn_disabled");
			$("#blind_tra").addClass("btn_disabled");
			$("#attend_tra").addClass("btn_disabled");
		}

		
		

	    }

function addCallLog() {
			$('#addcalllog').modal('show');
}
function agentCallHistory() {
			$('#agentCallHis').modal('show');
}
</script>
<script>

function ph_bk_popup()
{
	searchPhoneBook();
	$('#ph_bk').modal('show');
}

function pick_ph_num(num)
{
	document.getElementById("dial_num").value = num;
	$('#ph_bk').modal('hide');
}

function abn_det_popup()
{
	search_cl_bck();
	searchAbnDet();
	$('#abn_det').modal('show');
}


</script>
<script>
function searchAbnDet() {

$("#abn_det_tbl").dataTable().fnDestroy();

$('#abn_det_tbl').DataTable({
		"processing": true,
		//"scrollX": true,
		"ajax": {
			"url": "search_abn_details",
			"type": "GET",
			data: {},
		},
		"columns": [

			{ "data": "incoming_date" },
			{ "data": "extension" },
			{ "data": "descr" },
			{ "data": "incoming_num" },
			{ "data": "fullName" },
			{ "data": "waiting_time" },

			{
				sortable: false,
				"render": function (data, type, full, meta) {
					var CLI = full.incoming_num;
					var LnkID = full.rec_linkedid;
					return '<a style="color: #000000" onclick="pick_phone_num_aban(\''+CLI+'\', \''+LnkID+'\')" role="button" class="text-center"><button type="button" class="btn btn-info">Call</button></a>';
				}
			},
			{
				sortable: false,
				"render": function (data, type, full, meta) {
					var CLI = full.incoming_num;
					var LnkID = full.rec_linkedid;


						return '<a style="color: #000000;" onclick="addCallbackStatus(\''+CLI+'\', \''+LnkID+'\')" role="button" class="text-center"><button type="button" class="btn btn-info">Add Status</button></a>' ;

					
				}
			},

		],
		"columnDefs": [{
			className: "text-center",
			"targets": [0,1,2,3,4,5,6]
			
		},
		//{ "width": "15%", "targets": [0] },
		],
		"order": [
			[0, "desc"]
		],
		"pageLength": 25,


		buttons: [
			'excel'
		]

	});
}
function searchPhoneBook()
{

	$("#ph_bk_tbl").dataTable().fnDestroy();
	$('#ph_bk_tbl').DataTable({
		    "processing": true,
		    "ajax": {
			"url": "searchdata",

			"type": "GET",
			data: {},
		    },
		    "columns": [
			{
			    "data": "company"
			},
			{
			    "data": "contact_type"
			},
			{
			    "data": "contact_name"
			},
			{
			    sortable: false,
			    "render": function (data, type, full, meta) {
				var contact_one = full.contact_one;
				return '<a style="color: #000000" onclick="pick_ph_num(' + contact_one +
					')" role="button" class="text-center">' + contact_one + '</a>';
			    }
			},
			{
			    sortable: false,
			    "render": function (data, type, full, meta) {
				var contact_two = full.contact_two;
				return '<a style="color: #000000" onclick="pick_ph_num(' + contact_two +
					')" role="button" class="text-center">' + contact_two + '</a>';
			    }
			},
			{
			    sortable: false,
			    "render": function (data, type, full, meta) {
				var contact_three = full.contact_three;
				return '<a style="color: #000000" onclick="pick_ph_num(' + contact_three +
					')" role="button" class="text-center">' + contact_three + '</a>';
			    }
			},
			{
			    "data": "description"
			},
		    ],
		    "pageLength": 25,

		    buttons: [
			'excel'
		    ]

		});
	    }

$("#abn_det_tbl").DataTable({
        "pageLength": 25,


   "columnDefs": [
       {"text-align": "center", "targets": 0},
	   

       { "width": "114%", "targets": 0 },
       { "width": "15%", "targets": 3 },
       { "width": "15%", "targets": 4 }
   ]

    });
</script>

<!-- inbound form scripts -->



<script>
    // Get the modal
    // var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    // var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    // var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    // btn.onclick = function() {
       
    //         modal.style.display = "block";
   
    // }

    // When the user clicks on <span> (x), close the modal
    // span.onclick = function() {
    //     modal.style.display = "none";
    // }

    // When the user clicks anywhere outside of the modal, close it
    // window.onclick = function(event) 
    // {
    //     if (event.target == modal) 
    //     {
    //         modal.style.display = "none";
    //     }
    // }

    // function closemodal() 
    // {
    //     modal.style.display = "none";
    // }

    // function updatestatus()
    // {
  
    //     document.forms["add_clips"].submit();
    // }
</script>
<script>
function addContact(number){
	   //alert(number);
       location.href='{{url("addnewcontact")}}' + "/" + number;
   }  
function addPreset_forlist(remark){
    var totalRemark = document.getElementById('setremark');
    // var secondrem = $("#"+remark).find('option:selected').text();
    var secondrem = remark.options[remark.selectedIndex].text;

    totalRemark.value = totalRemark.value
        + ((totalRemark.value=='') ? '' : ' - ')
        + secondrem;

        console.log("asdsa");
        
}
function addCallLog() 
{
    //var z =document.getElementById("orders");
    var x = document.getElementById("myDIV");
    var y = document.getElementById("myDIVT");
    if (x.style.display === "none") 
    {
        x.style.display = "block";
	    y.style.display = "none";
       // z.style.display = "none";

    }
    else
    {
        x.style.display = "none";
	    y.style.display = "none";
        //z.style.display = "none";

    }
    }
    
    function addTicket(){
var z =document.getElementById("orders");
	var x = document.getElementById("myDIVT");
	var y = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "none";

    } else {
            x.style.display = "none";
        y.style.display = "none";
    z.style.display = "none";

    }
    }

  

</script>

<script>

//edit 4-1-2019
$('#orderList').DataTable({
        "processing": true,
        "pageLength": 25,
        "order": [[0,'desc']]
    });

//end edit 4-1-2091
    $('#historytable').DataTable({
        "processing": true,
        "pageLength": 25,
        "order": [[0,'desc']]
    });

    $('#opentable').DataTable({
        "processing": true,
        "pageLength": 25,
        "order": [[0,'desc']]
    });

$('#workingtable').DataTable({
        "processing": true,
        "pageLength": 25,
        "order": [[0,'desc']]
    });

    $('#closedtable').DataTable({
        "processing": true,
        "pageLength": 25,
        "order": [[0,'desc']]
    });

</script>

<script>

function getValue(obj1,obj2,obj3){

    var inputId =  obj1;
    var listId = obj2;
    var hfieldId = obj3;

 //   $('#'+inputId).change(function(){
        var value = $('#'+inputId).val();
        var attributeList = $('#'+listId);
        var option = attributeList.find("[value='" + value + "']");

        if (option.length > 0) {
            var datavalue = option.data("value");
            $('#'+hfieldId).val(datavalue);
            // do stuff with the id
        }else{
            var datavalue = "";
            $('#'+hfieldId).val(datavalue);
            $('#'+inputId).val(datavalue);
            // alert("Invalid Input");
            bootbox.alert({
    				message: "Invalid Input",
					size: 'small',
    			callback: function () {			
    				}
					});
        }
        // var abc = $("#browsers option[value='" + $('#input').val() + "']").attr('data-id');
        // alert(abc);
 //   });
}



    function saveRecord(){

    	
    	document.getElementById("save_calllog_btn").disabled = true;
      setTimeout(function(){document.getElementById("save_calllog_btn").disabled = false;},5000);

	//document.getElementById('form_new').submit();
	var cusid=document.getElementById("cusid_calllog").value;
		var sipid=document.getElementById("sipid").value;
		var pho_number=document.getElementById("incomingcall_callLog").value;
		var remark=document.getElementById("remark").value;
		var category=document.getElementById("category").value;
		var inquiry=document.getElementById("inquiry").value;
		var firstname=document.getElementById("firstname").value;
		var lastname=document.getElementById("lastname").value;
        var agent=document.getElementById("agent").value;

        <?php if(isset($_GET['linkedid'])){ ?>
        	var linkid="{{$_GET['linkedid']}}";
        <?php }else{ ?>
	        var linkid=document.getElementById("linkid").value;
        <?php } ?>

		var cus_name=firstname+' '+lastname;
			$.ajax({
				url: 'SaveCallLog',
				type: 'GET',
				data: {
					cusid: cusid,
					sipid: sipid,
					pho_number: pho_number,
					remark: remark,
					category: category,
					inquiry: inquiry,
					cus_name: cus_name,
                    agent: agent,
                    linkid:linkid
					
				},
				success: function(response) {
					if(response=="Call Log Added Successfully!"){
					// alert('Call Log Added Successfully!');
                    bootbox.alert({
    				    message: "Call Log Added Successfully!",
					    size: 'small',
    			    callback: function () {		
                        location.reload();
    				}
					});
					}else if(response=="Data Saving Error!"){
						// alert('Data Saving Error!');
                        bootbox.alert({
    				    message: "Data Saving Error!",
					    size: 'small',
    			    callback: function () {		
    				}
					});
					}
					//document.getElementById('scheduleDate').value='<?php echo date('Y-m-d 00:00:00'); ?>';
				   //$('#breakmodal').modal('hide');
				}
			});
    }
    function colorizetab1() {
        document.getElementById("tab1_li").style="background-color:#d7eceb";
        document.getElementById("tab2_li").style="";
        document.getElementById("tab3_li").style="";
	document.getElementById("tab4_li").style="";
    }

    function colorizetab2() {
        document.getElementById("tab2_li").style="background-color:#d7eceb";
        document.getElementById("tab1_li").style="";
        document.getElementById("tab3_li").style="";
	document.getElementById("tab4_li").style="";
    }

    function colorizetab3() {
        document.getElementById("tab3_li").style="background-color:#d7eceb";
        document.getElementById("tab1_li").style="";
        document.getElementById("tab2_li").style="";
	document.getElementById("tab4_li").style="";
    }
    function colorizetab() {
        document.getElementById("tab3_li").style="";
        document.getElementById("tab1_li").style="";
        document.getElementById("tab2_li").style="";
	document.getElementById("tab4_li").style="background-color:#d7eceb";
    }
    
    
    ///$.datetimepicker.setLocale('en');

    $('.some_class').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
	timepicker: false
    });

    $('#datetimepicker_dark').datetimepicker({theme: 'dark'});

    $(".awesomplete").on('keyup', function (event) {
        if(event.keyCode==13) {
            addPreset(event.target.value);
        }
    });

    function showaddcomment(ticket_id){

        $.ajax({
        url: '{{url('getticketforcomment')}}',
        type: 'GET',
        data: {ticket_id: ticket_id},
        success: function (response)
        {
            var op="" ;
            $.each(response, function (index, c) {
        
                if(c.referenceperson==null)
                            {
                                c.referenceperson=""; 
                            }
                            if(c.orderreference==null)
                            {
                                c.orderreference=""; 
                            }
                op+=' <table style="font-size:14px" width="100%">';
                op+='<tr class="row">';
                op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong> Ticket Number</strong></label></td>';
                op+='<td class="col-md-3"><label for="department" class="control-label labels" ></label><input type="hidden" name="ticket_id" value="'+c.id+'">'+c.ticket_num+'</td>';
                op+='<td class="col-md-3"align="right"><strong>Ticket Type</strong></td>';
                op+='<td class="col-md-3" >'+c.tickettype+'</td>';
                op+='</tr>';
                op+='<tr class="row">';
                op+='<td class="col-md-3" align="right"><label for="department" class="control-label labels" ><strong>Service Unit</strong></label> </td>';
                op+='<td class="col-md-3"><label for="department" class="control-label labels" ><input type="hidden" name="lvlone_id" value="'+c.lvlone_id+'">'+c.lvlone_name+'</label></td>';
                op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Locations</strong></label> </td>';
                op+='<td class="col-md-3"> <label for="department" class="control-label labels" ><input type="hidden" name="lvltwo_id" value="'+c.lvltwo_id+'">'+c.lvltwo_name+' </label></td>';
                op+='</tr>';
                op+='<tr class="row">';
                op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Category</strong></label></td>';
                op+='<td class="col-md-3"><label for="department" class="control-label labels" ><input type="hidden" name="lvlthr_id" value="'+c.lvlthr_id+'">'+c.lvlthr_name+'</label> </td>';
                op+='<td class="col-md-3" align="right"><strong>KPI Level</strong></td>';
                op+='<td class="col-md-3"><input type="hidden" name="status" value="'+c.status+'">'+c.kpilevel_name+'</td>';
                op+='</tr>';
                op+='<tr class="row">';
                op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Ticket Cap. Mode</strong></label></td>';
                op+='<td class="col-md-3"><label for="department" class="control-label labels" >'+c.ticketcapturemode+'</label> </td>';
                op+='<td class="col-md-3" align="right"><strong>Reference No</strong></td>';
                op+='<td class="col-md-3">'+c.orderreference+'</td>';
                op+='</tr>';
    
                op+='</table>';
        

               
                
                //});
             });
             $("#tkt_details_header").html(op);
            //$("#ticket_model_footer").html(op2);
            $('#myModal').modal('show');
             //tkt_details_header
           
        }
        });



        $('#myModal').modal('show');

    }
    function submitTicket(){
        var allRequired = true;
        $('.input-required').each(function(){
            if( $(this).val() == "" ){
                allRequired = false;
            }
        });
//alert(allRequired);
        if(!allRequired){
            // alert("Please fill out all the necessary fields");
            bootbox.alert({
    				message: "Please fill out all the necessary fields",
					size: 'small',
    			callback: function () {		
    				}
					});
        }else{
			bootbox.confirm({
    				message: "Are You Sure You Want to Continue",
					size: 'small',
    			callback: function (result) {
						//if()
    				
            // var myform = document.getElementById("form_new_ticket");
            // var fd = new FormData(myform);
            // console.log(fd);
            //var answer = window.confirm("Are You Sure You Want to Continue ")
            if (result==true) {
                $.ajax({
                    type: "POST",
                    url: '{{url('insertticket')}}',
                    data:  $('#form_new_ticket').serialize(),
                    success: function (response) {
                        var op="" ;
                        var op2="" ;
                        
                        $.each(response, function (index, c) {
                //alert(c.ticket_num);
                            if(c.referenceperson==null)
                            {
                                c.referenceperson=""; 
                            }
                            if(c.orderreference==null)
                            {
                                c.orderreference=""; 
                            }
                            op+=' <table style="font-size:14px" width="100%">';
                            op+='<tr class="row">';
                            op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong> Ticket Number</strong></label></td>';
                            op+='<td class="col-md-3"><label for="department" class="control-label labels" ></label>'+c.ticket_num+'</td>';
                            op+='<td class="col-md-3"align="right"><strong>Ticket Type</strong></td>';
                            op+='<td class="col-md-3" >'+c.tickettype+'</td>';
                            op+='</tr>';
                            op+='<tr class="row">';
                            op+='<td class="col-md-3" align="right"><label for="department" class="control-label labels" ><strong>Service Unit</strong></label> </td>';
                            op+='<td class="col-md-3"><label for="department" class="control-label labels" >'+c.lvlone_name+'</label></td>';
                            op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Locations</strong></label> </td>';
                            op+='<td class="col-md-3"> <label for="department" class="control-label labels" >'+c.lvltwo_name+' </label></td>';
                            op+='</tr>';
                            op+='<tr class="row">';
                            op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Category</strong></label></td>';
                            op+='<td class="col-md-3"><label for="department" class="control-label labels" >'+c.lvlthr_name+'</label> </td>';
                            op+='<td class="col-md-3" align="right"><strong>KPI Level</strong></td>';
                            op+='<td class="col-md-3">'+c.kpilevel_name+'</td>';
                            op+='</tr>';
                            op+='<tr class="row">';
                            op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Ticket Cap. Mode</strong></label></td>';
                            op+='<td class="col-md-3"><label for="department" class="control-label labels" >'+c.ticketcapturemode+'</label> </td>';
                            op+='<td class="col-md-3" align="right"><strong>Reference No</strong></td>';
                            op+='<td class="col-md-3">'+c.orderreference+'</td>';
                            op+='</tr>';
                            op+='<tr class="row">';
                            op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Reference Person</strong></label></td>';
                            op+='<td class="col-md-3"><label for="department" class="control-label labels" >'+c.referenceperson+'</label> </td>';
                            op+='</tr>';
                            op+='</table>';
                            op+=' <table style="font-size:14px" width="100%">';
                            op+='<tr class="row">';
                            op+='<td class="col-md-3" align="right"> <label for="department" class="control-label labels" ><strong>Comments</strong></label></td>';
                            op+='<td class="col-md-9"><textarea class="form-control input-required" rows="4"  >'+c.remark+'</textarea></td>';
                            op+='</tr>';
                            op+='</table>';

                            op2+='<button type="button" onclick="send_email('+c.id+')" class="btn btn-success">Send Email</button>';
                            op2+='<button type="button" onclick="send_sms('+c.id+')" class="btn btn-primary">Send SMS</button>';
                            op2+='<button type="button" onclick="refresh()" class="btn btn-secondary" data-dismiss="modal">Close</button>';
                            
                            //});
							//alert(op);
							 $("#ticket_model_new").html(op);
                        $("#ticket_model_footer_new").html(op2);
                        $('#exampleModal_new').modal('show');
                        });
                  
                    
                    
                        
															
															 
                    
							}
                    
						});
                   
					}else{
						//nothing
					}
				}
			});
            
       
  

	//document.getElementById('form_new_ticket').submit();
   }
    }
    
function getkpilvloflvlthr(obj) {

var hlevel3 = document.getElementById('hlevel3').value;
//alert(hlevel2);
 $.ajax({
     url: '{{url('getkpilvloflvlthr')}}',
     type: 'GET',
     data: {id: hlevel3},
     success: function (response)
     {
 
         var model = $('#preset1data1');
         model.empty();
         var i = 0;

         var datalist;
 //datalist += "<option disabled selected value> -- select a Department -- </option>";
         $.each(response, function (index, element) {
     //alert(element.department_id);
             $.each(element, function (colIndex, c) {
                 datalist += "<option data-value='"+c.id+"' value='"+ c.kpilevel_name+"'/>";
                });
             });
         $('#preset1data1').html(datalist);
     }
         });
}
    function getlevelthrdata(obj) {

       var hlevel2 = document.getElementById('hlevel2').value;
       //alert(hlevel2);
        $.ajax({
            url: '{{url('getlvlthrdata')}}',
            type: 'GET',
            data: {id: hlevel2},
            success: function (response)
            {
		
                var model = $('#preset1data3');
                model.empty();
                var i = 0;

                var datalist;
		//datalist += "<option disabled selected value> -- select a Department -- </option>";
                $.each(response, function (index, element) {
		    //alert(element.department_id);
                    $.each(element, function (colIndex, c) {
                        datalist += "<option data-value='"+c.id+"' value='"+ c.name+"'/>";
                    });
                    });
                $('#preset1data3').html(datalist);
            }
                });
    }
    function getleveltwodata(obj) {

var hlevel1 = document.getElementById('hlevel1').value;
 $.ajax({
     url: '{{url('getlvltwodata')}}',
     type: 'GET',
     data: {id: hlevel1},
     success: function (response)
     {
        
         var model = $('#preset1data2');
         model.empty();
         var i = 0;
         var datalist;
 //datalist += "<option disabled selected value> -- select a Department -- </option>";
         $.each(response, function (index, element) {
     //alert(element.department_id);
             $.each(element, function (colIndex, c) {
                 datalist += "<option data-value='"+c.id+"' value='"+c.name+"'/>";
             });
             });
         $('#preset1data2').html(datalist);
     }
         });
}
    function getlevel2s(obj) {
        $.ajax({
            url: '{{url('getlevel2')}}',
            type: 'GET',
            data: {id: obj},
            success: function (response)
            {
		
                var model = $('#preset1data2');
                model.empty();
                var i = 0;

                var datalist;
		//datalist += "<option disabled selected value> -- select a Department -- </option>";
                $.each(response, function (index, element) {
		    //alert(element.department_id);
                    $.each(element, function (colIndex, c) {
                        datalist += "<option data-value='"+element.dept_id+"' value='"+ element.level2_name+"'/>";
                    });
                    });
                $('#preset1data2').html(datalist);
            }
                });
    }
    function send_email(id) {
    //alert(id);
    $.ajax({
        url: '../../../../ticketdetail_sendbyemail',
        type: 'GET',
        data: {ticket_id: id},
        success: function (response) {
            //alert(response);
            if(response =="false")
            {
                // alert("Ticket customer doesn't have a E-mail Address");
                bootbox.alert({
    				message: "Ticket customer doesn't have a E-mail Address",
					size: 'small',
    			callback: function () {		
    				}
					});
            }else
            {
                // alert("E-Mail Sent Successfully !!!");
                bootbox.alert({
    				message: "E-Mail Sent Successfully !!!",
					size: 'small',
    			callback: function () {		
    				}
					});

            }
        
        }
    });
    }
    function refresh() {
        location.reload(); 
    }
function send_sms(id) {
    //alert(id);
    $.ajax({
        url: '../ticketdetail_sendviasms',
        type: 'GET',
        data: {ticket_id: id},
        success: function (response) {
            //alert(response);
            if(response =="false")
            {
                // alert("Ticket customer doesn't have a E-mail Address");
                bootbox.alert({
    				message: "Ticket customer doesn't have a E-mail Address",
					size: 'small',
    			callback: function () {		
    				}
					});
            }else
            {
                // alert("E-Mail Sent Successfully !!!");
                bootbox.alert({
    				message: "E-Mail Sent Successfully !!!",
					size: 'small',
    			callback: function () {		
    				}
					});
            }
        
        }
    });
    }
    function getSubDept(obj){
	
        $.ajax({
            url: '{{url('getlevel3')}}',
            type: 'GET',
            data: {id: obj},
            success: function (response)
            {
		
                var model = $('#preset1data3');
                model.empty();
                var i = 0;

                var datalist;
		// datalist += "<option disabled selected value> -- select a Sub Department -- </option>";
                $.each(response, function (index, element) {
		    //alert(element.department_id);
                    //$.each(element, function (colIndex, c) {
                        datalist += "<option data-value='"+element.level3_id+"' value='" +element.level3_name+ "'/>";
                    //});
                });
                $('#preset1data3').html(datalist);
		
             
            }
        });
    }
    
    function getAgent(obj){
	var level2=document.getElementById('level2').value;
        $.ajax({
            url: '{{url('getAgentlevel3sWise')}}',
            type: 'GET',
            data: {id: obj, level2:level2},
            success: function (response)
            {
		
                var model = $('#users');
                model.empty();
                var i = 0;

                var datalist;
		        datalist += "<option disabled selected value> -- select an Agent -- </option>";
                $.each(response, function (index, element) {
		        //alert(element.department_id);
                    //$.each(element, function (colIndex, c) {
                        datalist += "<option value='" + element.user_id + "'>" + element.username+ "</option>";
                    //});
                });
                $('#users').html(datalist);
		
              
    }
        });
    }
    

function getkpi_levels(obj){
    $.ajax({
            url: '{{url('getkpi_levelsLocationWise')}}',
            type: 'GET',
            data: {id: obj},
            success: function (response)
            {

                var model = $('#preset1data1');
                model.empty();
                var i = 0;
   
                var datalist;
		 //datalist += "<option disabled selected value> -- select a Sub Department -- </option>";
                $.each(response, function (index, element) {
		    //alert(element.department_id);
                    //$.each(element, function (colIndex, c) {
                        datalist += "<option data-value='"+element.id+"' value='" + element.kpi_level+"'/>";
                    //});
                });
                $('#preset1data1').html(datalist);
    
              
            }
        });
}


 function getdepartments() {
     var obj =document.getElementById('hlevel2').value;
    // alert(obj);
        $.ajax({
            url: '{{url('getlevel2')}}',
            type: 'GET',
            data: {id: obj},
            success: function (response)
            {
		
                var model = $('#preset1data3');
                model.empty();
                var i = 0;

                var datalist;
		//datalist += "<option disabled selected value> -- select a Department -- </option>";
                $.each(response, function (index, element) {
		    //alert(element.department_id);
                    //$.each(element, function (colIndex, c) {
                        datalist += "<option data-value='"+element.dept_id+"' value='"+ element.department_name+"'/>";
                    //});
                    });
                $('#preset1data3').html(datalist);
            }
                });
    }
function getOrderHeader(){
	var z =document.getElementById("orders");
	var x = document.getElementById("myDIVT");
	var y = document.getElementById("myDIV");
    if (z.style.display == "none" || z.style.display == "") {
        z.style.display = "block";
	y.style.display = "none";
x.style.display = "none";

    } else {
        z.style.display = "none";
	//x.style.display = "block";
	//y.style.display = "block";
x.style.display = "none";
y.style.display = "none";

    }

     var obj=document.getElementById('cus').value;

	 $.ajax({
            url: '{{url('getOrderHeaderDetail')}}',
            type: 'GET',
            data: {id: obj},
            success: function (response)
            { 
		 var model = $('#orders');
                model.empty();
		var datalist = "<table style='border-collapse: collapse;"+
  "width: 100%;'><thead><tr><th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>INVOICE DATE</th><th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+
"INVOICE NO</th>"+
"<th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>SALES POINT</th><th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+
"ITEM CODE</th><th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>UNIT PRICE</th><th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>QTY</th><th style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>DISCPCNT</th></tr></thead><tbody>";
		  //$.each(response, function (index, element) {
		   
                    $.each(response, function (colIndex, c) {
//alert(c.invdate);
                        //console.log(c.SHOWROOM);
                        datalist += "<tr style='border: 1px solid #dddddd;text-align: left;padding: 8px;'><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.invdate+"</td><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.invoiceno+"</td><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.salespoint+"</td><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.itemcode+"</td><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.unitprice+"</td><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.qty+"</td><td style='border: 1px solid #dddddd;text-align: left;padding: 8px;'>"+c.discpcnt+"</td></tr>";
                    }); 		     $('#orders').html(datalist);
            }
                });
	/* $.ajax({
            url: '{{url('getOrderHeaderDetail')}}',
            type: 'GET',
            data: {id: obj},
            success: function (response)
            {
		 var model = $('#orders');
                model.empty();
		var datalist;
		 $.each(response, function (index, element) {
		   
                    $.each(element, function (colIndex, c) {
			 datalist += "<table><tr>";
                        console.log(c['SHOWROOM']);

                    });
                    });
		     $('#orders').html(datalist);
                
            }


                });*/




    }
function downloadexcel(){
//alert(obj);
var obj=document.getElementById('cus').value;
        location.href = "{{url ('downloadexcel')."/"}}" +obj;
    

}

function sendPromo(){
	var status =true;
	var contact_number=document.getElementById('contact_number').value;
	var mobilenumber=document.getElementById('mobile_number').value;
	if(mobilenumber==""){
		// alert('Please Enter Client Mobile No. to send Promotion SMS!');
        bootbox.alert({
    				message: "Please Enter Client Mobile No. to send Promotion SMS!",
					size: 'small',
    			callback: function () {	
                    return false;		
    				}
					});
		
	}else{
		location.href = "{{url ('sendPromo')."/"}}" +contact_number+'/'+mobilenumber;
		//status=true;
	}
        
    
}
function updatecontact(){
    $.ajax({
        url: '{{url('inbound_updatecontact')}}',
        type: 'POST',
        data: $('#form_new_ticket').serialize(),
        success: function (response)
        {
            location.reload(); 
           
        }
        });

   } 
    
</script>



@include('footer')

</body>

</html>