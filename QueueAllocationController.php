<?php
/**
 * User: Thilini
 * Date: 04/09/2020
 * Time: 5.20 pm
 */

namespace App\Http\Controllers;
use DB;
use App\Http\Util;
use Illuminate\Support\Facades\Input;
use PHPMailer\PHPMailer;
use Carbon\Carbon;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class QueueAllocationController
{
    public function index($queue_id) 
    {

        if(Util::isAuthorized("viewqueueallocation")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("viewqueueallocation")=='DENIED'){
            return view('permissiondenide');
        }

        Util::log("View queue allocation", "View");


        $getqueue =DB::SELECT('SELECT * FROM asterisk.queues_config WHERE extension='.$queue_id);
        // $getstrategy =DB::select('SELECT asterisk.queues_details.extension 
        //                             FROM asterisk.queues_details');
        // (mewa wenama functn ekaka liynn)
        // return compact('getextension');
        $get_sip_ids= DB::select('SELECT CONCAT("row1",ps_endpoints.id)  AS DT_RowId,ps_endpoints.id AS exten_id,
                            (SELECT CONCAT("Local/", exten_id, "@from-queue/n,0") )AS sip_num_str,
                            (SELECT queues_details.id as exten
                            FROM
                                asterisk.queues_details
                                WHERE
                                queues_details.id = '.$queue_id.' AND
                                queues_details.`data`= sip_num_str) as exten,
                                (SELECT queues_details.flags as flags
                            FROM
                                asterisk.queues_details
                                WHERE
                                queues_details.id = '.$queue_id.' AND
                                queues_details.`data`= sip_num_str ) as flags
                            FROM
                                asterisk.ps_endpoints
                            WHERE
                                ps_endpoints.auth !=""  ORDER BY -flags DESC');  
                                
			$getStrategy =DB::SELECT('SELECT asterisk.queues_details.`data` AS statergy
                            FROM asterisk.queues_details
                            WHERE queues_details.id = "'.$queue_id.'" AND queues_details.keyword="strategy"');
            
            $ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,"User Open the Queue Allocation Form",$username,"View Queue Allocation Form");
            
        return view("queueAllocation",compact('getqueue','get_sip_ids','getStrategy'));
   
    }


    public function getsrvboxbytype()
    {
        $id = $_GET['id'];

        $data= DB::table('ps_endpoints')
                    ->select('ps_endpoints.*')
                    ->where('ps_endpoints.extension',$id)
                    ->get();
            
        return compact('data');
    
    }

   public function queueassignagent(Request $req)
    { 
        $strategy  =Request::input('ring_str');
        $extension =Request::input('extension');
        $selected_agents=Request::input('selected_agents');    
        $i = 0 ;
		$newMemberArr=[];

        DB::table('asterisk.queues_details')
                ->where('asterisk.queues_details.id',$extension)
                ->where('asterisk.queues_details.keyword',"member")
                ->delete();
        if(isset($_POST['submit'])){
            // exit();
            
            if(isset($_POST['check'])){

            $var = $_POST['check']; 
            
            foreach($var as $value){ 
            
                //echo $value.'<br/>'; 
                $sip_num_str= 'Local/'.$value.'@from-queue/n,0';
				$sip_num_strForFile= 'member=Local/'.$value.'@from-queue-custom/n,0,'.$value.',hint:2603@ext-local';
				array_push($newMemberArr,$sip_num_strForFile);//0719273965
    
                         $insert_ps_auths = DB::insert("INSERT INTO `asterisk`.`queues_details` (`id`, `keyword`, `data`, `flags`)
                                                         VALUES('$extension', 'member','$sip_num_str' , $i)");
                        $i++;
            }

            }

         }
        

        DB::table('asterisk.queues_details')
        ->where('asterisk.queues_details.id',$extension)
        ->where('asterisk.queues_details.keyword',"strategy")
        ->delete();

      
    
        $insert_ps_auths = DB::insert("INSERT INTO `asterisk`.`queues_details` (`id`, `keyword`, `data`, `flags`)
                                                VALUES('$extension', 'strategy','$strategy' ,'0')");
          
        $file = '/etc/asterisk/queues_custom.conf';
		header('Content-Type: text/plain');
        $contents = file_get_contents($file);
        // print_r($contents);
        // exit;	
		$tagsReplace = preg_split('/(\[[^]]+\])/', $contents, -1, PREG_SPLIT_DELIM_CAPTURE);	
	    $searchfor = '['.$extension.']';
        $arrVall = array_search($searchfor,$tagsReplace,true);
        
		$arrSipNumbers=$newMemberArr;
		//print_r($arrSipNumbers);
		$cesta=[];
		$cestaOther=[];
		$newAllArr=[];
			$newArr=$arrVall+1;
            //print_r($tagsReplace);
           // exit;
			foreach ($tagsReplace as $key => $valueTR) {
				//echo $key .'|'.$value;
				   if($key == $newArr){
					$your_array = array_diff(explode("\n", rtrim($valueTR)),array(""));
						 foreach ($your_array as $key => $value) { // for normal array
						
							$valueNew = preg_split("/\r\n|\n|\r/", rtrim($value));
							
								foreach ($valueNew as $key => $valueMember) {
									//echo $valueMember;
									if(strpos($valueMember, 'member=') !== false) {
						
										array_push($cesta,$value);
						//array_push($newArrayForSelectedEll,$valueMember);						
							
									}else{
										array_push($cestaOther,$value);
									}   
								}						
							}
                       
						$result = array_diff($valueNew, $cesta);
						$result_new=array_merge($result,$arrSipNumbers);
						$result_new1=array_merge($cestaOther,$result_new);
                        //print_r($result_new1);exit;
                        
						array_push($newAllArr,array_unique($result_new1));
						
				   }else{
					   $cesta = array_diff(explode("\n", rtrim($valueTR)),array(""));	
					   array_push($newAllArr,$cesta);
				   }
				   //$all_result=array_merge($cesta,$result_new);
				   
			}
			$content = "";
//print_r($newAllArr);
//exit;
//print_r($newAllArr);
		foreach($newAllArr as $section1=>$values1){
			if(count($values1)<=1){
			if ($section1 % 2 == 0) {
				foreach($values1 as $key=>$section){
							//append the section 
							$content .= $section; 
							//append the values
							
						}
						$content .="\n";
				}else{
					if ($section1!=1) {
					$content .="\n";
					}
					foreach($values1 as $key=>$section){
							//append the section 
							$content .= $section; 
							//append the values
							
						}
						
				}
			}else{
			//print_r(array_filter($values1));
			$content .="\n";
			$content .= implode("\n", array_filter($values1));
			$content .="\n";
				
		}
		}
		//echo $content;
		//exit();
		//echo $content;
		if (!$handle = fopen($file, 'w')) { 
			return false; 
		}

		$success = fwrite($handle, $content);
		fclose($handle); 
				
            $company_name=config('app.company_name');
            $cookie='/var/www/html/'.$company_name.'/ck.txt';

			$ch = curl_init('http://'.$_SERVER['SERVER_ADDR'].':8088/rawman?action=login&username=ami_user&secret=passw0rd');
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
			curl_setopt($ch, CURLOPT_POSTFIELDS,"");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close ($ch);
			echo $result;
			echo "<br>";
	
			
	 		$cookie_get='/var/www/html/'.$company_name.'/ck.txt';



			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://".$_SERVER['SERVER_ADDR'].":8088/rawman?action=Command&ActionID=command&Command=core%20reload");
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
			curl_setopt($ch, CURLOPT_POSTFIELDS,"");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close ($ch);
			echo $response;
            echo "<br>";

            

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Allocate Queue",$username,"Allocate Queue");
        
        return redirect()->back();
        
    
    }


    public function getqueueallocationagent()
    {
        //$id = $_GET['id'];
        $queue_id = $_GET['queue_id'];
        $data = DB::select('SELECT CONCAT("row1",ps_endpoints.id)  AS DT_RowId,ps_endpoints.id AS exten_id,
                            (SELECT CONCAT("Local/", exten_id, "@from-queue/n,0") )AS sip_num_str,
                            (SELECT queues_details.id as exten
                            FROM
                                asterisk.queues_details
                                WHERE
                                queues_details.id = '.$queue_id.' AND
                                queues_details.`data`= sip_num_str) as exten,
                                (SELECT queues_details.flags as flags
                            FROM
                                asterisk.queues_details
                                WHERE
                                queues_details.id = '.$queue_id.' AND
                                queues_details.`data`= sip_num_str ) as flags
                            FROM
                                asterisk.ps_endpoints
                            WHERE
                                ps_endpoints.auth !=""  ORDER BY -flags DESC');
        
       /* echo '<table id="qn_tbl" class="table table-bordered table-dragable" cellspacing="0">';
        echo'<thead>';
        echo'<tr style="background-color:#9cc1ea ">';
        echo'<th width="30%" class="text-center" ><p >Extension</p></th>';
        echo'<th width="20%" ><span class="glyphicon glyphicon-ok" ></span></th>';
        echo'</tr>';
        echo'</thead>';
        echo'<tbody>';

        foreach ($data as $value) {
            echo '<tr class="grabable"  id="'.$value->DT_RowId.'" role="row">';
            echo '<td class="text-center" > <option value="'.$value->exten_id.'">'.$value->exten_id.'</option> </td>';
            echo '<td style="width: 3% " class="text-center" >';
            if($value->exten!=null){
            echo '<input type="checkbox" checked="checked" id="value_check" name ="check[]" value="'.$value->exten_id.'" >'; 
            
            }else{
            echo '<input type="checkbox" id="value_check" name ="check[]" value="'.$value->exten_id.'" >';
        }
        echo '</td></tr>';
    }
    echo'</tbody>';
    echo '</table>';*/

 
    return $data;

       // return compact('data');
    
    }

    public function view_queue_allo_company() 
    {
       
        $get_companies =DB::SELECT('SELECT * FROM tbl_com_mst');
        
        $getqueue =DB::SELECT('SELECT * FROM asterisk.queues_config');

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Queue Allocation to Company Form",$username,"View Queue Allocation to Company Form");

        return view("queueAllocationCompany",compact('get_companies','getqueue'));
    }

    public function save_allocate_queues(Request $req)
	{
        $com_id =Request::input('company_id');
		$selected_queues=Request::input('selected_queues');  
        
        // print_r($selected_queues);exit();
        
		if(!empty($selected_queues))
		{
			foreach ($selected_queues as $selected_queues) 
			{    
			
				$data=array('com_id'=>$com_id);
               
                DB::table('asterisk.queues_config')
                        ->where('extension', $selected_queues)
                        ->update($data); 
			}
		}
        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Allocate Queue To Company",$username,"Allocate Queue To Company");
        return redirect()->back();              
	}

    public function get_queue_allo_company()
    {
        $getqueue =DB::SELECT('SELECT * FROM asterisk.queues_config');

        return compact('getqueue');
    }
    public function select_ring_str()
    {
        //$extension =Request::input('extension');
        $extension = $_GET['extension'];

        $getqueue =DB::SELECT('SELECT asterisk.queues_details.`data` AS statergy
                            FROM asterisk.queues_details
                            WHERE queues_details.id = "'.$extension.'" AND queues_details.keyword="strategy"');                             
       return $getqueue[0]->statergy;
    
    }
	public function viewQueList()
    {
        if(Util::isAuthorized("viewqueueallocation")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("viewqueueallocation")=='DENIED'){
            return view('permissiondenide');
        }

        Util::log("View queue allocation", "View");


        $getqueue =DB::SELECT('SELECT * FROM asterisk.queues_config');
        
            
            $ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,"User Open the Queue Allocation Form",$username,"View Queue Allocation Form");
            
        return view("queueListDb",compact('getqueue'));
    }
    public function view_que_allo()
    {
        if(Util::isAuthorized("view_queue_allocation")=='LOGGEDOUT'){
            return redirect('/');
        }
        if(Util::isAuthorized("view_queue_allocation")=='DENIED'){
            return view('permissiondenide');
        }

        Util::log("View queue allocation to user", "View");

        $userid=session('userid');
		$get_com_id  = DB::table('user_master')
                                ->where('id',$userid)
                                ->first();
        $getqueue  = DB::table('asterisk.queues_config')
                            ->select('extension','descr')
                            ->where('com_id',$get_com_id->com_id)
                            ->get(); 
                                                   
        $get_sip_ids= DB::table('asterisk.ps_endpoints')
                            ->select('id')
                            ->where('auth',"!=","")
                            ->get(); 

        $ipaddress = (new UsersController())->get_client_ip();
        $username=session()->get('username');
        Util::user_auth_log($ipaddress,"User Open the Queue Allocation to user Form",$username,"View Queue Allocation to User Form");

        $selected_sip='0';

        $inqus=null;
            
        return view("view_que_allo",compact('getqueue','get_sip_ids','selected_sip','inqus'));
    }

    public function update($sip_id)
    {
        $userid=session('userid');
        $get_com_id  = DB::table('user_master')
                                ->where('id',$userid)
                                ->first();

        $get_sip_ids= DB::table('asterisk.ps_endpoints')
                            ->select('id')
                            ->where('auth',"!=","")
                            ->get(); 

        $getqueue  = DB::table('asterisk.queues_config')
                            ->select('extension','descr')
                            ->where('com_id',$get_com_id->com_id)
                            ->get(); 
        $data="Local/".$sip_id."@from-queue/n,0";
        $inqus= DB::select("SELECT * FROM `asterisk`.`queues_details` where data='$data'");

        $selected_sip=$sip_id;
            
        return view("view_que_allo",compact('getqueue','get_sip_ids','inqus','selected_sip'));
    }

    public function save_que_allo(Request $req)
    { 
        $userid=session('userid');
		$get_com_id  = DB::table('user_master')
                                ->where('id',$userid)
                                ->first();
        $sip_id = $_GET['sip_id'];
        
        $datentime = Carbon::now();  
        $userid=session()->get('userid');
        $i=0;

        if(isset($_GET['checkedq'])){

        $checkedq = $_GET['checkedq'];    
                
                foreach ($checkedq as $checkedq) 
                {    
                    $file = '/etc/asterisk/queues_custom.conf';
                    header('Content-Type: text/plain');

                    $c = file_get_contents($file);
                    $tagsReplace = preg_split('/(\[[^]]+\])/', $c, -1, PREG_SPLIT_DELIM_CAPTURE);   


                    $que='['.$checkedq.']';

                    $searchfor =$que;
                    $arrVall = array_search($searchfor,$tagsReplace,true);
                    
                    if($arrVall==false){
                        return "Error_queue_is_missing";
                    }

                }
            }
        
        // member=Local/5919@from-queue-custom/n,0,"5919",hint:5919@ext-local
        $sip_num_strForFile= 'member=Local/'.$sip_id.'@from-queue-custom/n,0,'.$sip_id.',hint:'.$sip_id.'@ext-local';

        $data_q_det="Local/".$sip_id."@from-queue/n,0";

        DB::table('asterisk.queues_details')
                ->where('asterisk.queues_details.data',$data_q_det)
                ->where('asterisk.queues_details.keyword',"member")
                ->delete();
        $newQueueArr=[];
        $checkedqIN=[];
        if(isset($_GET['checkedq'])){

        $checkedq = $_GET['checkedq'];    
                
                foreach ($checkedq as $checkedq) 
                {    
                    
                    $checkedq_new='['.$checkedq.']';

                    array_push($newQueueArr,$checkedq_new);

                    array_push($checkedqIN,$checkedq);                   
                    
                    DB::insert("INSERT INTO `asterisk`.`queues_details` (`id`,`keyword`,`data`,`flags`) VALUES ('$checkedq','member','$data_q_det','$i');"); 
                    $i++;                  
                }
            }

        $getOtherQueues  = DB::table('asterisk.queues_config')
                            ->select('extension','descr')
                            ->where('com_id',$get_com_id->com_id)
                            ->whereNotIn('extension', $checkedqIN)
                            ->get();   

        $getOtherComQueues  = DB::table('asterisk.queues_config')
                            ->select('extension','descr')
                            ->where('com_id','<>',$get_com_id->com_id)
                            ->get();                            
        
        


        // print_r($getOtherComQueues);

        $file = '/etc/asterisk/queues_custom.conf';
        header('Content-Type: text/plain');


        $content="\n";
        $handle = fopen($file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if(strpos($line, ';') !== false) {          // Geting Comments
                    $content.=$line;                         
                }
            }
        }

        $contents0 = file_get_contents($file);
        $tagsReplace = preg_split('/(\[[^]]+\])/', $contents0, -1, PREG_SPLIT_DELIM_CAPTURE);   

        // print_r($newQueueArr);

        $arrMemberOcom = array();
        $arrOtherOcom = array();
        $merge_resultOcom = array();

foreach($getOtherComQueues as $keyOcom => $queuedOcom){ //Get Other Company Queues
        $queueO='['.$queuedOcom->extension.']';

        $searchfor =$queueO;
        $arrVall = array_search($searchfor,$tagsReplace,true);
        if($arrVall!=false){
        
        $newArr=$arrVall+1;
                
                $arrMemberOcom[$queueO]=array();

                foreach (explode("\n", $tagsReplace[$newArr]) as $k => $val) {
                    if(strpos($val, 'member=') !== false) {
                          if(!strlen($val)==0)  
                            $arrMemberOcom[$queueO][]=$val;
                    }else{
                          if(!strlen($val)==0)
                            $arrOtherOcom[$queueO][]=$val;
                    }
                }

                $merge_resultOcom[$queueO]=array_merge($arrOtherOcom[$queueO],$arrMemberOcom[$queueO]);
            }
        }        


        $arrMember = array();
        $arrOther = array();
        $merge_result = array();

foreach($getOtherQueues as $key => $queued){ //Change Other Queues(Remove)
        $queue='['.$queued->extension.']';

        $searchfor =$queue;
        $arrVall = array_search($searchfor,$tagsReplace,true);
        if($arrVall!=false){
        
        $newArr=$arrVall+1;
        
                foreach (explode("\n", $tagsReplace[$newArr]) as $k => $val) {
                    if(strpos($val, 'member=') !== false) {
                          if(!strlen($val)==0)  
                            $arrMember[$queue][]=$val;
                    }else{
                          if(!strlen($val)==0)
                            $arrOther[$queue][]=$val;
                    }
                }

        if(isset($arrMember[$queue]) && in_array($sip_num_strForFile, $arrMember[$queue]) ){ //Check Availability
                    $arrMemberNew[$queue]=array_diff($arrMember[$queue],[$sip_num_strForFile]); //Removing
        }else{
            $arrMemberNew[$queue]=array();
        }

                $merge_result[$queue]=array_merge($arrOther[$queue],$arrMemberNew[$queue]);
            }
        }


        $arrMember2 = array();
        $arrOther2 = array();
        $merge_result2=array();

foreach($newQueueArr as $keyN => $queueN){ //Change Available Queues(Add)

        $searchfor = $queueN;
        $arrVall = array_search($searchfor,$tagsReplace,true);
        
        if($arrVall!=false){

        $newArr=$arrVall+1;
                
                $arrMember2[$queueN]=array();

                foreach (explode("\n", $tagsReplace[$newArr]) as $k => $val) {
                    if(strpos($val, 'member=') !== false) {
                          if(!strlen($val)==0)  
                            $arrMember2[$queueN][]=$val;
                    }else{
                          if(!strlen($val)==0)
                            $arrOther2[$queueN][]=$val;
                    }
                }

                if(!in_array($sip_num_strForFile, $arrMember2[$queueN])){ //Check Availability
                    array_push($arrMember2[$queueN],$sip_num_strForFile); //Adding
                }

                $merge_result2[$queueN]=array_merge($arrOther2[$queueN],$arrMember2[$queueN]);
            }
        }

                $allMerge=array_merge($merge_resultOcom,$merge_result,$merge_result2);

                // print_r($allMerge);

                foreach ($allMerge as $keyOne => $dataSub) {
                    $content.=$keyOne."\n";

                    foreach ($dataSub as $keyTwo => $dataString) {
                        $content.=$dataString."\n";
                    }
                    $content.="\n";
                }


        if (!$handle = fopen($file, 'w')) { 
            return false; 
        }

        $success = fwrite($handle, $content);
        fclose($handle);        

                
            $company_name=config('app.company_name');
            $cookie='/var/www/html/'.$company_name.'/ck.txt';

            $ch = curl_init('http://'.$_SERVER['SERVER_ADDR'].':8088/rawman?action=login&username=ami_user&secret=passw0rd');
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
            curl_setopt($ch, CURLOPT_POSTFIELDS,"");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close ($ch);
    
            
            $cookie_get='/var/www/html/'.$company_name.'/ck.txt';



            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"http://".$_SERVER['SERVER_ADDR'].":8088/rawman?action=Command&ActionID=command&Command=core%20reload");
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_get);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET' );
            curl_setopt($ch, CURLOPT_POSTFIELDS,"");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close ($ch);

            $ipaddress = (new UsersController())->get_client_ip();
            $username=session()->get('username');
            Util::user_auth_log($ipaddress,"Queues Allocated to user",$username,"Queues Allocated to user"); 
        
        return "OK";
    }

}